//////////////////////////////////////////////////////////////////
// NAME:					CriticalSectionGT.cpp				//
// LAST MODIFICATION DATE:	30-Mar-2016							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Application							//
//////////////////////////////////////////////////////////////////
#include "stdafx.h"

#include "x64CriticalSectionGT.h"

CCriticalSectionGT::CCriticalSectionGT(void)
{
	::InitializeCriticalSection(&m_cs_CriticalSection);
}

CCriticalSectionGT::~CCriticalSectionGT(void)
{
	::DeleteCriticalSection(&m_cs_CriticalSection);
}

void CCriticalSectionGT::EnterCriticalSection(void)
{
	::EnterCriticalSection(&m_cs_CriticalSection);
}

void CCriticalSectionGT::LeaveCriticalSection(void)
{
	::LeaveCriticalSection(&m_cs_CriticalSection);
}

BOOL CCriticalSectionGT::TryEnterCriticalSection(void)
{
	return ::TryEnterCriticalSection(&m_cs_CriticalSection);
}

