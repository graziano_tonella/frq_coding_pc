//////////////////////////////////////////////////////////////////
//																//
// NAME:					CStringTokenizerGT.cpp				//
// LAST MODIFICATION DATE:	02-Sep-2014							//
// AUTHOR:					Graziano Tonella					//
//////////////////////////////////////////////////////////////////
#include "pch.h"
#include "StringTokenizerGT.h"


CStringTokenizerGT::CStringTokenizerGT(void)
{
	sTokens.RemoveAll();
	m_nCount	= 0;
	m_nIndwex	= 0;
}

CStringTokenizerGT::~CStringTokenizerGT(void)
{
	sTokens.RemoveAll();
	m_nCount	= 0;
	m_nIndwex	= 0;
}

BOOL CStringTokenizerGT::Tokenizer(CString sString,CString sDelimiter)
{
	sTokens.RemoveAll();
	m_nCount	= 0;
	m_nIndwex	= 0;

	
	if(sDelimiter.GetLength() != 1)
		return FALSE;
	CString sTmp;
	for(int nLoop=0;nLoop<sString.GetLength();nLoop++)
	{
		//TCHAR pippo = str.GetAt(i);
		if(sString.GetAt(nLoop) == sDelimiter.GetAt(0))
		{
			sTokens.Add(sTmp);
			sTmp.Empty();
		}
		else
			sTmp += sString.GetAt(nLoop);
	}
	if(sTmp.GetLength() != 0) // Something else
		sTokens.Add(sTmp);
	//
	m_nCount = sTokens.GetCount();

	return TRUE;
}

CString CStringTokenizerGT::GetTokenAt(int nElement)
{
	if(nElement < 0 || nElement >= m_nCount)
		throw CString(_T("nElement Out Of Range"));
	else
		return sTokens.GetAt(nElement);
}

int CStringTokenizerGT::GetCountTokens(void)
{
	return m_nCount;
}

/*
/////////////////////////////////////////////////
// Method is used to fetch the tokens.
/////////////////////////////////////////////////
CString CStringTokenizer::GetNextElement()
{
	if(index>=count)
	{
		throw CString("Index out of Bounds");
		return "Index out of Bounds";
	}
	else
	{
		index++;
		return elements.GetAt(index-1);	
	}
}

/////////////////////////////////////////////////
//fetch the elements at given position 
/////////////////////////////////////////////////
CString CStringTokenizer::GetElementAt(int index)
{
	if(index>=count ||index<0)
	{
		throw CString("Index out of Bounds");
	}
	else
		return elements.GetAt(index);	
}

/////////////////////////////////////////////////
//AM Search Item in string
// ret val = >=0 position
//			 -1 element not found
/////////////////////////////////////////////////
int CStringTokenizer::FindElement(CString item)
{
	int findItem = -1;
	
	for(int i=0; i < elements.GetSize(); i++)
	{
		if(elements.GetAt(i)==item)
		{
			findItem = i;
			break;
		}
	}

	return findItem;
}
*/
/*
#pragma once
class CStringTokenizerGT
{
public:
	int	 GetCountTokens(void);
	CString	GetNextToken(void);

private:
	CStringArray	sTokens;
	int				m_nCount;
	int				m_nIndwex;
};
*/