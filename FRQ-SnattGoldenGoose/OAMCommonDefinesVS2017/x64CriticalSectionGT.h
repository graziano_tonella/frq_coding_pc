//////////////////////////////////////////////////////////////////
// NAME:					CCriticalSectionGT.cpp				//
// LAST MODIFICATION DATE:	30-Mar-2016							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Include								//
//////////////////////////////////////////////////////////////////
#pragma once

//#define __CRITICALSECTIONGT_NODLL__
#ifndef __CRITICALSECTIONGT_NODLL__
//	#define	__GTXDLL_BUILDDLL__
	#ifdef	__GTXDLL_BUILDDLL__
		#define	CRITICALSECTIONGT_EXPORT	__declspec(dllexport)
	#else
		#define	CRITICALSECTIONGT_EXPORT	__declspec(dllimport)
	#endif
#else
	#define	CRITICALSECTIONGT_EXPORT
#endif

class CRITICALSECTIONGT_EXPORT CCriticalSectionGT
{
public:
	CCriticalSectionGT(void);
	~CCriticalSectionGT(void);
	void	EnterCriticalSection(void);
	void	LeaveCriticalSection(void);
	BOOL	TryEnterCriticalSection(void);

protected:
	CRITICAL_SECTION	m_cs_CriticalSection;
};
