//////////////////////////////////////////////////////////////////
//																//
// NAME:					CSharedFileGT.cpp					//
// LAST MODIFICATION DATE:	29/06/2010							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Shared File Handling				//
//////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SharedFileGT.H"


CSharedFileGT::CSharedFileGT()
{
	m_nFileHandle	= _SHAREDFILEGT_INVALID_HANDLE;
	m_bSuccess		= TRUE;
}

CSharedFileGT::~CSharedFileGT()
{
	if(m_nFileHandle != _SHAREDFILEGT_INVALID_HANDLE)
		Close();
}

BOOL CSharedFileGT::OpenNew(LPCTSTR lpszFileName)
{
	m_bSuccess	= TRUE;

	errno_t ErrNum = _tsopen_s(&m_nFileHandle,lpszFileName,_O_RDWR|_O_CREAT|_O_TRUNC|_O_BINARY,_SH_DENYRW,_S_IWRITE|_S_IREAD);
	if(m_nFileHandle == _SHAREDFILEGT_INVALID_HANDLE)
		m_bSuccess = FALSE;
	StoreOpenERRNO(ErrNum);
	return m_bSuccess;
}

BOOL CSharedFileGT::OpenToAppend(LPCTSTR lpszFileName)
{
	m_bSuccess	= TRUE;
	errno_t ErrNum = _tsopen_s(&m_nFileHandle,lpszFileName,_O_RDWR|_O_BINARY|_O_APPEND|_O_CREAT,_SH_DENYRW,_S_IWRITE|_S_IREAD);
	if(m_nFileHandle == _SHAREDFILEGT_INVALID_HANDLE)
		m_bSuccess = FALSE;
	StoreOpenERRNO(ErrNum);
	return m_bSuccess;
}

BOOL CSharedFileGT::OpenToReadAndWrite(LPCTSTR lpszFileName)
{
	m_bSuccess	= TRUE;
	errno_t ErrNum = _tsopen_s(&m_nFileHandle,lpszFileName,_O_RDWR|_O_BINARY,_SH_DENYRW,_S_IWRITE|_S_IREAD);
	if(m_nFileHandle == _SHAREDFILEGT_INVALID_HANDLE)
		m_bSuccess = FALSE;
	StoreOpenERRNO(ErrNum);
	return m_bSuccess;
}

BOOL CSharedFileGT::OpenToReadOnly(LPCTSTR lpszFileName)
{
	m_bSuccess	= TRUE;
	errno_t ErrNum = _tsopen_s(&m_nFileHandle,lpszFileName,_O_RDONLY|_O_BINARY,_SH_DENYRW,_S_IREAD);
	if(m_nFileHandle == _SHAREDFILEGT_INVALID_HANDLE)
		m_bSuccess = FALSE;
	StoreOpenERRNO(ErrNum);
	return m_bSuccess;
}

BOOL CSharedFileGT::Write(LPVOID lpBuffer,UINT uBufLen)
{
	if(_write(m_nFileHandle,lpBuffer,uBufLen) == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	return TRUE;
}

BOOL CSharedFileGT::Read(LPVOID lpBuffer,UINT uBufLen)
{
	int rc;
	rc = _read(m_nFileHandle,lpBuffer,uBufLen);
	if(rc == 0 || rc == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	return TRUE;
}

long CSharedFileGT::GetLength()
{
	long	lLength = 0;
	long	lPosition = CSharedFileGT::GetPosition();
	if(CSharedFileGT::SeekToEnd() == TRUE)
	{
		lLength = CSharedFileGT::GetPosition();
		CSharedFileGT::SeekFromBegin(lPosition);
	}
	return lLength;
}

BOOL CSharedFileGT::SeekToBegin()
{
	if(_lseek(m_nFileHandle,0L,SEEK_SET) == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	return TRUE;
}

BOOL CSharedFileGT::SeekToEnd()
{
	if(_lseek(m_nFileHandle,0L,SEEK_END) == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	return TRUE;
}

BOOL CSharedFileGT::SeekFromBegin(long lOffset)
{
	if(_lseek(m_nFileHandle,lOffset,SEEK_SET) == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	return TRUE;
}

BOOL CSharedFileGT::SeekFromCurrent(long lOffset)
{
	if(_lseek(m_nFileHandle,lOffset,SEEK_CUR) == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	return TRUE;
}

BOOL CSharedFileGT::SeekFromEnd(long lOffset)
{
	if(_lseek(m_nFileHandle,lOffset,SEEK_END) == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	return TRUE;
}

long CSharedFileGT::GetPosition()
{
	return (long) _tell(m_nFileHandle);
}

BOOL CSharedFileGT::IsEndOfFile()
{
	if(_eof(m_nFileHandle) == 0)
		return FALSE;
	return TRUE;
}

BOOL CSharedFileGT::Commit()
{
	if(_commit(m_nFileHandle) == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	return TRUE;
}

BOOL CSharedFileGT::Close()
{
	if(_close(m_nFileHandle) == _SHAREDFILEGT_INVALID_HANDLE)
	{
		m_bSuccess	= FALSE;
		return FALSE;
	}
	m_nFileHandle = _SHAREDFILEGT_INVALID_HANDLE;
	return TRUE;
}

BOOL CSharedFileGT::GetFileResult()
{
	return m_bSuccess;
}

void CSharedFileGT::StoreOpenERRNO(errno_t ErrNum)
{
	
	if(ErrNum == 0)
	{
		_tcscpy_s(m_szErrno,sizeof(m_szErrno)/sizeof(_TCHAR),_T("OK"));
		return;
	}
	switch(ErrNum)
	{
	case EACCES:
		_tcscpy_s(m_szErrno,sizeof(m_szErrno)/sizeof(_TCHAR),_T("EACCES"));
		break;
	case EEXIST:
		_tcscpy_s(m_szErrno,sizeof(m_szErrno)/sizeof(_TCHAR),_T("EEXIST"));
		break;
	case EINVAL:
		_tcscpy_s(m_szErrno,sizeof(m_szErrno)/sizeof(_TCHAR),_T("EINVAL"));
		break;
	case EMFILE:
		_tcscpy_s(m_szErrno,sizeof(m_szErrno)/sizeof(_TCHAR),_T("EMFILE"));
		break;
	case ENOENT:
		_tcscpy_s(m_szErrno,sizeof(m_szErrno)/sizeof(_TCHAR),_T("ENOENT"));
		break;
	default:
		_tcscpy_s(m_szErrno,sizeof(m_szErrno)/sizeof(_TCHAR),_T("UNKNOWN...See Default In StoreOpenERRNO"));
		break;
	}
}
