//////////////////////////////////////////////////////////////////
//																//
// NAME:					CServerClientFDXGT.h				//
// LAST MODIFICATION DATE:	28-Jul-2015							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					TCP/IP connection					//
//							Server/Client Full Duplex			//
//////////////////////////////////////////////////////////////////
#pragma once


#include "NetworkST.h"
#include "ProtocolCML.h"
#include "x64SharedFlatQueueGT.h"
#include "Timers.h"
class CServerClientFDXGT : public CNetworkST
{
public:
	CServerClientFDXGT(void);
	virtual ~CServerClientFDXGT(void);
	//
	DWORD	Destroy(DWORD dwMilliseconds);
	DWORD	Create(USHORT nSocketType,LPCTSTR lpszIPAddress,int nPortNumber,DWORD dwBufferSize,DWORD dwPollingTime,UINT nRcvTimeout,CSharedFlatQueueGT *pFlatQueueIn,DWORD dwMemoryFlatQueueOut,DWORD dwKeepAlive,_TCHAR* pszChannel);
	void	SetProperties(BYTE bySourceNode,BYTE byDestNode);
	void	ExcludeProtocolControls(BOOL bExcludeNODEControl,BOOL bExcludeMSGPRGControl,BOOL bExcludeMSGPacking,BOOL bSendAlwaysKeepAlive);
	void	DoNotSendSTDKeepAlive(WORD wKeepAliveMsgID);
	void	SendTelegramAtFixedLength(DWORD dwTelegramLength);
	void 	LimitTelegramLength(DWORD dwTelegramLength);
	void	AlterMsgIdByAddingInterfaceId(WORD wInterfaceId);
	void	SetPostMessage(HWND hwndID,UINT uMsgID, WORD wNetworkId = 0);
	BOOL	IsRunning(void);
	void	GetRGBConnectionState(COLORREF* pcrColor);

	WORD	GetSentMsgID(void);
	BYTE	GetSentPrgID(void);
	WORD	GetReceMsgID(void);
	BYTE	GetRecePrgID(void);
	void	ClearSentReceMsgIDs(void);

	CProtocolCML	m_ProtocolCML;

	CSharedFlatQueueGT	m_FlatQueueOut;

private:
	void	OnChangeConnectionState(WPARAM wSocketType, LPARAM lNewState);
	DWORD	OnPrepareMsgToSend(LPBYTE pBuffer, DWORD dwBufferLen);
	BOOL	OnSendCompleted(BOOL bSendOk, int nErrorCode);
	void	OnReceive(LPBYTE pData, DWORD dwDataLen);
	BOOL	OnTimeout(void);

	CRITICAL_SECTION	m_cs_Wait;

	CTimer55ms			m_t55msKeepAliveTimer;
	DWORD				m_dwKeepAlive;

	LPBYTE				m_pMsgCML;
	CSharedQueueST		m_QueueCML;
	CSharedFlatQueueGT	*m_pFlatQueueIn;

	HWND				m_hwndID;
	UINT				m_uMsgID;
	WORD				m_wNetworkId;
	BYTE				m_byConnectionState;
	BOOL				m_bConnected;
	BOOL				m_bHasBeenSentOK;
	DWORD				m_MemOfdwTotalMsgSize;
	WORD				m_wSentMsgID;
	WORD				m_wReceMsgID;
	BYTE				m_bySourceNode;
	BYTE				m_byDestNode;
	BYTE				m_bySourceEnvPrgNumber;
	BYTE				m_byDestinationEnvPrgNumber;
	COLORREF			m_crColor;

private:
	BOOL				m_bExcludeNODEControl;
	BOOL				m_bExcludeMSGPRGControl;
	BOOL				m_bExcludeMSGPacking;
	BOOL				m_bSendAlwaysKeepAlive;
	CString				m_sChannel;

	BOOL				m_bDoNotSendSTDKeepAlive;
	WORD				m_wKeepAliveMsgID;

	BOOL				m_bSendTelegramAtFixedLength;
	DWORD				m_dwFixedTelegramLength;

	BOOL				m_bLimitTelegramLength;
	DWORD				m_dwLimitedTelegramLength;

	BOOL				m_bAlterMsgId;
	WORD				m_wAlterMsgId;

	SYSTEMTIME			m_csSystemTime;
	CString				m_sString;
	CFile				m_TraceFile;


protected:
	BOOL				m_bJustConnected;
	DWORD				m_dwTotalMsgSize;
	DWORD				m_dwTotalDataSize;
	LPBYTE				m_pOriginOfBuffer;
	LPBYTE				m_pMsgBuffer;
	LPBYTE				m_pMsgSENT;
	STRUCT_ENVELOPECML	m_csENVELOPE;
	STRUCT_TAILCML		m_csTAIL;
	inline void PrepareENVELOPE(LPBYTE pBuffer);
	inline void PrepareDATA(DWORD dwBufferLen);
	inline void PrepareTAIL(void);
	inline BOOL CheckAllMessages(void);
	inline void NotifyKeepAliveMsg(void);

};


inline void CServerClientFDXGT::PrepareENVELOPE(LPBYTE pBuffer)
{
	m_dwTotalMsgSize	= 0;
	m_dwTotalDataSize	= 0;
	m_pOriginOfBuffer   = pBuffer;
	m_pMsgBuffer		= pBuffer;

	::ZeroMemory(&m_csENVELOPE,sizeof(m_csENVELOPE));
	m_csENVELOPE.bySOM			= CML_SOM;
	m_csENVELOPE.byClass		= 0;
	m_csENVELOPE.bySourceNode	= m_bySourceNode;
	m_csENVELOPE.byDestNode		= m_byDestNode;
	m_csENVELOPE.byProg			= m_bySourceEnvPrgNumber;

	m_dwTotalMsgSize += SIZE_STRUCT_ENVELOPECML;
	m_pMsgBuffer	 += SIZE_STRUCT_ENVELOPECML;

	m_csENVELOPE.wLen = (WORD) m_dwTotalDataSize;
}

inline void CServerClientFDXGT::PrepareDATA(DWORD dwBufferLen)
{
	DWORD	dwFirstItemSize;
	//
	if(m_bDoNotSendSTDKeepAlive == FALSE && m_bySourceEnvPrgNumber == 0)
		return;

	while(m_FlatQueueOut.GetCount())
	{
		// Get size of first item available in queue
		dwFirstItemSize = m_FlatQueueOut.GetFirstItemSize();
		if(dwFirstItemSize <= SIZE_STRUCT_SINGLEMSGCML)
		{
			if((m_dwTotalDataSize + dwFirstItemSize) < (dwBufferLen - (DWORD) (SIZE_STRUCT_TAILCML + SIZE_STRUCT_ENVELOPECML)) )
			{
				// Remove It From Queue
				m_FlatQueueOut.GetItem((LPBYTE)m_pMsgCML,TRUE,NULL);
				::CopyMemory(m_pMsgBuffer,m_pMsgCML,dwFirstItemSize);
				m_pMsgBuffer += dwFirstItemSize;
				m_dwTotalDataSize += dwFirstItemSize;
				// Update Number Of Messages In Envelope
				m_csENVELOPE.wNumMsg++;
				//
				//
				m_wSentMsgID = ((STRUCT_SINGLEMSGCML*) m_pMsgCML)->csHeader.wMsgId;
			}
			else
				break;
		}
		else
		{
			// Remove All Msg From Queue
			m_FlatQueueOut.Empty();
			TRACE(_T("CServerClientFDXGT (%s) - Wrong Size %d While Sending Message\n"),m_sChannel,dwFirstItemSize);
			break;
		}
		//
		if(m_bExcludeMSGPacking == TRUE)
			break;
		if(m_bSendTelegramAtFixedLength == TRUE)
			break;
		if(m_bLimitTelegramLength == TRUE)
		{
			if(m_dwTotalDataSize >= m_dwLimitedTelegramLength)
				break;
		}
		
	}
}

inline void CServerClientFDXGT::PrepareTAIL(void)
{
	//
	// Update Number Of Messages In Envelope
	//
	m_dwTotalMsgSize += m_dwTotalDataSize;

	m_csENVELOPE.wLen = (WORD) m_dwTotalDataSize;
	::CopyMemory(m_pOriginOfBuffer,&m_csENVELOPE,SIZE_STRUCT_ENVELOPECML);
	//
	// Copy TAIL
	//
	::ZeroMemory(&m_csTAIL,sizeof(m_csTAIL));
	m_csTAIL.byEOM = CML_EOM;
	::CopyMemory(m_pMsgBuffer,&m_csTAIL,sizeof(m_csTAIL));
	m_dwTotalMsgSize += SIZE_STRUCT_TAILCML;

	if(++m_bySourceEnvPrgNumber == 0)
		m_bySourceEnvPrgNumber = 1;
}

inline BOOL CServerClientFDXGT::CheckAllMessages(void)
{
	STRUCT_ENVELOPECML*		pEnvelope;
	STRUCT_SINGLEMSGCML*	pcsSINGLEMSGCML;
	STRUCT_TAILCML*			pcsTAIL;
	LPBYTE					pMsgCML;
	WORD					wTotLen;

	pMsgCML		= m_pMsgCML;
	pEnvelope	= (STRUCT_ENVELOPECML*)pMsgCML;
	pMsgCML		+= SIZE_STRUCT_ENVELOPECML;
	wTotLen		= 0;
	for(WORD wNumMsg=0;wNumMsg<pEnvelope->wNumMsg;wNumMsg++)
	{
		pcsSINGLEMSGCML = (STRUCT_SINGLEMSGCML*) pMsgCML;
		//
		// Verify Message Size
		//
		WORD wMsgSize = (WORD) (SIZE_STRUCT_HEADERMSGCML+pcsSINGLEMSGCML->csHeader.wDataLen);
		if(wMsgSize > SIZE_STRUCT_SINGLEMSGCML)
		{
			TRACE(_T("CServerClientFDXGT (%s) - Wrong Size While Receiving Message ID: %d Size: %d \n"),m_sChannel,pcsSINGLEMSGCML->csHeader.wMsgId,wMsgSize);
			return FALSE;
		}
		pMsgCML	+= wMsgSize;
		wTotLen	+= wMsgSize;
	}

	// Check Total Size
	if(pEnvelope->wLen != wTotLen)
		return FALSE;

	// Check EOM
	pcsTAIL = (STRUCT_TAILCML*)pMsgCML;

	if(pcsTAIL->bySpare != 0 || pcsTAIL->wSpare != 0 || pcsTAIL->byEOM != CML_EOM)
		return FALSE;
	
	return TRUE;
}

inline void CServerClientFDXGT::NotifyKeepAliveMsg(void)
{
	STRUCT_SINGLEMSGCML	csSINGLEMSGCML;


	::ZeroMemory(&csSINGLEMSGCML,sizeof(csSINGLEMSGCML));

	csSINGLEMSGCML.csHeader.wMsgId	 = m_wKeepAliveMsgID;
	csSINGLEMSGCML.csHeader.wDataLen = 0;

	if(m_pFlatQueueIn != NULL)
		m_pFlatQueueIn->PutItem((LPBYTE) &csSINGLEMSGCML,(DWORD) (SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
}
