//////////////////////////////////////////////////////////
//                                                      //
//                                                      //
// NAME:					MappedFileST.h				//
//                                                      //
// VERSION:					1.4							//
//                                                      //
// CREATION DATE:			27/10/1998                  //
//                                                      //
// LAST MODIFICATION DATE:	30/03/2016                  //
//                                                      //
// AUTHOR:					Davide Calabro'             //
//							Basato su codice Logosystem //
//                                                      //
// FILE TYPE:				Include C++                 //
//                                                      //
// PURPOSE:         La funzione di questa classe e'     //
//                  quella di offrire un metodo semplice//
//                  e standard per gestire files mappati//
//                  sia in memoria che su disco         //
//                                                      //
//////////////////////////////////////////////////////////

#pragma once

//#define __MAPPEDFILEST_NODLL__
#ifndef __MAPPEDFILEST_NODLL__
	//#define	__GTXDLL_BUILDDLL__
	#ifdef	__GTXDLL_BUILDDLL__
		#define	MAPPEDFILEST_EXPORT	__declspec(dllexport)
	#else
		#define	MAPPEDFILEST_EXPORT	__declspec(dllimport)
	#endif
#else
	#define	MAPPEDFILEST_EXPORT
#endif

class MAPPEDFILEST_EXPORT CMappedFileST
{
public:
	CMappedFileST(void);
	virtual ~CMappedFileST(void);
	void*	Open(LPCTSTR lpszFileName,LPCTSTR lpszMappingObjectName,DWORD dwSize,BOOL bCreateAnyway = TRUE);
	void*	OpenInMemoryOnly(LPCTSTR lpszMappingObjectName,DWORD dwMaximumSizeHigh,DWORD dwMaximumSizeLow);
	BOOL	Flush(void);
	BOOL	Close(void);
	BOOL	IsMapped(void);
	BOOL	FileExists(void);
	void*	Connect(LPCTSTR lpszMappingObjectName);
	DWORD	GetLastError(void);
	static const short GetVersionI(void);
	static const char* GetVersionC(void);

private:
	void*	MapView(HANDLE hFile);
	BOOL	InitializeFile(HANDLE hHandle, LONG lDistanceToMove);
	void	Shutdown(void);

	BOOL	m_bFileExists;
	BOOL	m_bFileOpen;
	BOOL	m_bConnected;	// TRUE if memory mapped file has been opened by means of calling of "Connect"
	BOOL	m_bInMemory;	// TRUE if memory mapped file is in memory only

	HANDLE	m_hFile;		// HANDLE restituito da CreateFile
	HANDLE	m_hMap;			// HANLDE restituito da CreateFileMapping
	void*	m_lpMapFile;
	DWORD	m_dwLastError;
};
