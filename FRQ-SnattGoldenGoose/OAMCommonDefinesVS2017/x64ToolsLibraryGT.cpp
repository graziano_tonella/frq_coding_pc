//////////////////////////////////////////////////////////////////
// NAME:					CToolsLibraryGT.cpp					//
// LAST MODIFICATION DATE:	18-Jul-2018							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Include								//
//////////////////////////////////////////////////////////////////

#include "pch.h"
#include "x64ToolsLibraryGT.h"


CToolsLibraryGT::CToolsLibraryGT(void)
{
}

CToolsLibraryGT::~CToolsLibraryGT(void)
{
}

BOOL CToolsLibraryGT::CheckDirectory(LPCTSTR lpPathName)
{
	if(::CreateDirectory(lpPathName,NULL) == FALSE)
	{
		DWORD dwError = ::GetLastError();
		if(dwError != ERROR_ALREADY_EXISTS)
			return FALSE;
	}
	return TRUE;
}


int CToolsLibraryGT::DeleteFiles(LPCTSTR lpszFilePath,LPCTSTR lpszFileName,int nDays,int nHours,int nMinutes)
{
	WIN32_FIND_DATA	csFindFileData;
	FILETIME		csFILETIME;
	SYSTEMTIME		csSYSTEMTIME;
	HANDLE			hFind;
	CString			sString;

	

	::GetLocalTime(&csSYSTEMTIME);
	if(::SystemTimeToFileTime(&csSYSTEMTIME,&csFILETIME) == FALSE)
		return -1;

	__int64* pi64FileTime;
	pi64FileTime = (__int64*) &csFILETIME;

	//#define	HundredNanoSecondsInSecond	(__int64) 10000000;
	#define	HundredNanoSecondsInMinute	(__int64) 10000000 * 60;
	#define	HundredNanoSecondsInHour	(__int64) 10000000 * 60 * 60;
	#define	HundredNanoSecondsInDay		(__int64) 10000000 * 60 * 60 * 24;
		__int64 i64ElapsedTime = 0;
		i64ElapsedTime += (__int64)nDays * HundredNanoSecondsInDay;
		i64ElapsedTime += (__int64)nHours * HundredNanoSecondsInHour;
		i64ElapsedTime += (__int64)nMinutes * HundredNanoSecondsInMinute;
	//#undef	HundredNanoSecondsInSecond
	#undef		HundredNanoSecondsInMinute
	#undef		HundredNanoSecondsInHour
	#undef		HundredNanoSecondsInDay

	(*pi64FileTime) -= (__int64) i64ElapsedTime;
	if(::FileTimeToSystemTime(&csFILETIME,&csSYSTEMTIME) == FALSE)
		return -1;

	sString.Format(_T("%s%s"),lpszFilePath,lpszFileName);
	hFind = ::FindFirstFile(sString,&csFindFileData);
	BOOL	bFound = (BOOL) (hFind == INVALID_HANDLE_VALUE ? FALSE : TRUE);
	int		nFound	 = 0;
	int		nDeleted = 0;
	while(bFound)
	{
		bool bSkip=false;
		if(csFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			bSkip=true;

		if(bSkip == false)
		{
			nFound++;

			//VERIFY(::FileTimeToSystemTime(&csFindFileData.ftLastWriteTime,&csSYSTEMTIME) == TRUE);
			if(::CompareFileTime(&csFindFileData.ftLastWriteTime,&csFILETIME) <= 0)
			{
				sString.Format(_T("%s%s"),lpszFilePath,csFindFileData.cFileName);
				::DeleteFile(sString);
				nDeleted++;
			}
		}

		if(::FindNextFile(hFind,&csFindFileData) == FALSE)
		{
			bFound = FALSE;
			::FindClose(hFind);
		}
	}
	return nDeleted;
}


int CToolsLibraryGT::DeleteAllFiles(LPCTSTR lpszFilePath, LPCTSTR lpszFileName)
{
	WIN32_FIND_DATA	csFindFileData;
	HANDLE			hFind;
	CString			sString;



	sString.Format(_T("%s%s"), lpszFilePath, lpszFileName);
	hFind = ::FindFirstFile(sString, &csFindFileData);
	BOOL	bFound = (BOOL)(hFind == INVALID_HANDLE_VALUE ? FALSE : TRUE);
	int		nFound = 0;
	int		nDeleted = 0;
	while (bFound)
	{
		bool bSkip = false;
		if (csFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			bSkip = true;

		if (bSkip == false)
		{
			nFound++;
			sString.Format(_T("%s%s"), lpszFilePath, csFindFileData.cFileName);
			::DeleteFile(sString);
			nDeleted++;
		}

		if (::FindNextFile(hFind, &csFindFileData) == FALSE)
		{
			bFound = FALSE;
			::FindClose(hFind);
		}
	}
	return nDeleted;
}


BOOL CToolsLibraryGT::RenameFile(LPCTSTR pszOldName,LPCTSTR pszNewName)
{
/*
	try
	{
		CFile::Rename(pszOldName,pszNewName);
	}
	catch(...)
	{
		return FALSE;
	}
	return TRUE;
*/
//	extern int errno;
	BOOL bRename;
	#ifdef _MBCS
		bRename = (BOOL) (rename(pszOldName,pszNewName)==0?TRUE:FALSE);
	#else
		bRename = (BOOL) (_wrename(pszOldName,pszNewName)==0?TRUE:FALSE);
	#endif		
	return bRename;
}

DWORD CToolsLibraryGT::GetNumberOfProcessors(void)
{
	SYSTEM_INFO	csSYSTEM_INFO;
	//GetSystemInfo(&csSYSTEM_INFO);
	GetNativeSystemInfo(&csSYSTEM_INFO);
	return csSYSTEM_INFO.dwNumberOfProcessors;



/*
#include <windows.h>
  #include <stdio.h>
  #pragma comment(lib, "user32.lib")
  
  void main()
  {
     SYSTEM_INFO siSysInfo;
   
     // Copy the hardware information to the SYSTEM_INFO structure. 
   
     GetSystemInfo(&siSysInfo); 
   
     // Display the contents of the SYSTEM_INFO structure. 
  
     printf("Hardware information: \n");  
     printf("  OEM ID: %u\n", siSysInfo.dwOemId);
     printf("  Number of processors: %u\n", 
        siSysInfo.dwNumberOfProcessors); 
     printf("  Page size: %u\n", siSysInfo.dwPageSize); 
     printf("  Processor type: %u\n", siSysInfo.dwProcessorType); 
     printf("  Minimum application address: %lx\n", 
        siSysInfo.lpMinimumApplicationAddress); 
     printf("  Maximum application address: %lx\n", 
        siSysInfo.lpMaximumApplicationAddress); 
     printf("  Active processor mask: %u\n", 
        siSysInfo.dwActiveProcessorMask); 
  }
  
*/
}

BOOL CToolsLibraryGT::IsWow64(void)
{
	BOOL	bWow64 = FALSE;

	typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
  
	LPFN_ISWOW64PROCESS fnIsWow64Process;

      //IsWow64Process is not available on all supported versions of Windows.
      //Use GetModuleHandle to get a handle to the DLL that contains the function
      //and GetProcAddress to get a pointer to the function if available.
  
	fnIsWow64Process = (LPFN_ISWOW64PROCESS) GetProcAddress(
          GetModuleHandle(TEXT("kernel32")),"IsWow64Process");
  
	if(NULL != fnIsWow64Process)
	{
		if (!fnIsWow64Process(GetCurrentProcess(),&bWow64))
		{
			//handle error
		}
	}
	return bWow64;
}

time_t CToolsLibraryGT::TimeFromSystemTime(const SYSTEMTIME* pTime)
{
	struct	tm	csTm;
	::ZeroMemory(&csTm,sizeof(csTm));
	csTm.tm_year	= pTime->wYear -1900;
	csTm.tm_mon		= pTime->wMonth - 1;
	csTm.tm_mday	= pTime->wDay;
	csTm.tm_hour	= pTime->wHour;
	csTm.tm_min		= pTime->wMinute;
	csTm.tm_sec		= pTime->wSecond;
	csTm.tm_isdst	= (int) -1;

	return mktime(&csTm);
}


BOOL CToolsLibraryGT::GetFileFromSMPCViaFTPConnection(CString& rsFilePath,CString& rsFileName,int* pnFileLength)
{
	// create a session object to initialize WININET library
	// Default parameters mean the access method in the registry
	// (that is, set by the "Internet" icon in the Control Panel)
	// will be used.
	//CInternetSession( LPCTSTR pstrAgent = NULL, DWORD dwContext = 1, DWORD dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG, LPCTSTR pstrProxyName = NULL, LPCTSTR pstrProxyBypass = NULL, DWORD dwFlags = 0 );
	CInternetSession InternetSession(_T("FromSMPC"));


	CFtpConnection*	pConnect = NULL;


	BOOL bRet = FALSE;
	try
	{
		bRet = InternetSession.SetOption(INTERNET_OPTION_CONNECT_RETRIES,1,0);
		bRet = InternetSession.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT,1500,0);

		#define INTERNET_FTP_PORT21 21
		//pConnect = InternetSession.GetFtpConnection(_T("172.31.255.167")/*_T("smpc")*/,_T("AfterSalesPOLAND"),_T("POLANDAfterSales"),INTERNET_FTP_PORT21,FALSE);
		pConnect = InternetSession.GetFtpConnection(_T("10.162.59.21"),_T("Administrator"),_T("Strong01"),INTERNET_FTP_PORT21,FALSE);
		#undef	INTERNET_FTP_PORT21

		CString	sString;
		bRet = pConnect->GetCurrentDirectory(sString);
/*
		CString	sString;
		bRet = pConnect->GetCurrentDirectory(sString);
		sString = _T("\\AfterSalesGTX\\Snatt");
		bRet = pConnect->SetCurrentDirectory(sString);

		sString.Empty();
		bRet = pConnect->GetCurrentDirectory(sString);
		sString.Format(_T("%s%s"),_T("D:\\"),rsFileName);
		sString.Format(_T("%s%s"),rsFilePath,rsFileName);
		
		bRet = pConnect->GetFile(rsFileName,sString,FALSE,FILE_ATTRIBUTE_NORMAL,FTP_TRANSFER_TYPE_BINARY | INTERNET_FLAG_RELOAD,1);
		//bRet = pConnect->PutFile(sString,rsFileName,FTP_TRANSFER_TYPE_BINARY,1);

*/
		int nFileSize = 0;
		if(pnFileLength != NULL)
			*pnFileLength=0;
        // use a file find object to enumerate files
        CFtpFileFind finder(pConnect);
        // start looping
        BOOL bWorking = finder.FindFile(_T("*"));
        //BOOL bWorking = finder.FindFile(_T("*.*"));
		FILETIME	fT;

        while (bWorking)
        {
            bWorking = finder.FindNextFile();
            //printf_s("%s\n", (LPCTSTR) finder.GetFileURL());
			TRACE(_T("%s\n"), (LPCTSTR) finder.GetFileName());
			TRACE(_T("%d\n"), (LPCTSTR) finder.GetLength());
			TRACE(_T("%d\n"), (LPCTSTR) finder.GetCreationTime(&fT));
			TRACE(_T("%s\n"), (LPCTSTR) finder.GetFilePath());
			TRACE(_T("%s\n"), (LPCTSTR) finder.GetFileTitle());
			TRACE(_T("%d\n"), (LPCTSTR) finder.GetLastAccessTime(&fT));
			TRACE(_T("%d\n"), (LPCTSTR) finder.GetLastWriteTime(&fT));
			TRACE(_T("%s\n"), (LPCTSTR) finder.GetRoot());
			TRACE(_T("%d\n"), (LPCTSTR) finder.IsArchived());
			TRACE(_T("%d\n"), (LPCTSTR) finder.IsCompressed());
			TRACE(_T("%d\n"), (LPCTSTR) finder.IsDirectory());
			TRACE(_T("%d\n"), (LPCTSTR) finder.IsReadOnly());
			//
			//char szFileName[50];
			//::WideCharToMultiByte(CP_ACP,NULL,rsFileName,-1,szFileName,sizeof(szFileName)-1,NULL,NULL);
			sString = finder.GetFileName();
			if(sString.Compare(rsFileName) == 0)
			{
				nFileSize = (int) finder.GetLength();
				if(pnFileLength != NULL)
					*pnFileLength=nFileSize;
			}
        }

/*
BOOL PutFile(
   LPCTSTR pstrLocalFile,
   LPCTSTR pstrRemoteFile,
   DWORD dwFlags = FTP_TRANSFER_TYPE_BINARY,
   DWORD_PTR dwContext = 1 
);

BOOL GetFile(
   LPCTSTR pstrRemoteFile,
   LPCTSTR pstrLocalFile,
   BOOL bFailIfExists = TRUE,
   DWORD dwAttributes = FILE_ATTRIBUTE_NORMAL,
   DWORD dwFlags = FTP_TRANSFER_TYPE_BINARY,
   DWORD_PTR dwContext = 1 
);
*/

/*
		bRet=FALSE;
		DWORD dwGLE = ERROR_SUCCESS;
		::SetLastError(ERROR_SUCCESS);
		bRet = pConnect->Remove(rsFileName);
		if(bRet == FALSE)
			dwGLE = ::GetLastError();
		LPVOID lpMsgBuf;
		FormatMessage( 
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM,
        //FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dwGLE,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
        (LPTSTR) &lpMsgBuf,
        0,
        NULL);

		CString strLastError((LPTSTR)lpMsgBuf);

		LocalFree( lpMsgBuf );
*/

/*
CString GetLastErrorDesc() {
    LPVOID lpMsgBuf;
    DWORD l_err = GetLastError();
    FormatMessage( 
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM | 
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        l_err,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
        (LPTSTR) &lpMsgBuf,
        0,
        NULL 
    );

    // Process any inserts in lpMsgBuf.
    // ...
    // Display the string.
    #ifdef _DEBUG_MSG
    MessageBox( NULL, (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
    #endif

    CString strLastError((LPTSTR)lpMsgBuf);
    //Remove cr lf chars
    //int n = strBang.Replace(_T("field hockey"), _T("soccer"));
    int n1 = strLastError.Replace(_T("\n"),_T(""));
    int n2 = strLastError.Replace(_T("\r"),_T(""));
    //_ASSERT(n == 1);

    // Free the buffer.
    LocalFree( lpMsgBuf );
    return strLastError;
}
*/

	}
	catch (CInternetException* pEx)
	{
		TCHAR sz[1024];
		pEx->GetErrorMessage(sz,1024);
		TRACE(_T("ERROR! %s\n"),sz);
		pEx->Delete();
		return FALSE;
	}
	// if the connection is open, close it
	if(pConnect != NULL)
		pConnect->Close();
	delete pConnect;

	return bRet;
}

/*

// cftpfilefind_class.cpp
// Compile with: /MT /EHsc
#include <afx.h>
#include <afxwin.h>
#include <afxinet.h>
#include <stdio.h>

CWinApp theApp;

int main()
{
    if (!AfxWinInit(::GetModuleHandle(NULL), NULL, 
                    ::GetCommandLine(), 0))
    {
        // catastropic error! MFC can't initialize
        return -1;
    }

    // create a session object to initialize WININET library
    // Default parameters mean the access method in the registry
    // (that is, set by the "Internet" icon in the Control Panel)
    // will be used.

    CInternetSession sess(_T("MyProgram/1.0"));

    CFtpConnection* pConnect = NULL;

    try
    {
        // Request a connection to ftp.microsoft.com. Default
        // parameters mean that we'll try with username = ANONYMOUS
        // and password set to the machine name @ domain name
        pConnect = sess.GetFtpConnection(_T("ftp.microsoft.com"));

        // use a file find object to enumerate files
        CFtpFileFind finder(pConnect);

        // start looping
        BOOL bWorking = finder.FindFile(_T("*"));

        while (bWorking)
        {
            bWorking = finder.FindNextFile();
            printf_s("%s\n", (LPCTSTR) finder.GetFileURL());
        }
    }
    catch (CInternetException* pEx)
    {
        TCHAR sz[1024];
        pEx->GetErrorMessage(sz, 1024);
        printf_s("ERROR!  %s\n", sz);
        pEx->Delete();
     }

    // if the connection is open, close it
    if (pConnect != NULL) 
    {
        pConnect->Close();
        delete pConnect;
    }

   return 0;

   */
/*
BOOL CCoreAppHnd::GetFileFromSMPCViaFTPConnection(CString& rsFileName)
{
	// create a session object to initialize WININET library
	// Default parameters mean the access method in the registry
	// (that is, set by the "Internet" icon in the Control Panel)
	// will be used.
	//CInternetSession( LPCTSTR pstrAgent = NULL, DWORD dwContext = 1, DWORD dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG, LPCTSTR pstrProxyName = NULL, LPCTSTR pstrProxyBypass = NULL, DWORD dwFlags = 0 );
	CInternetSession InternetSession(_T("FromSMPC"));

	CFtpConnection*	pConnect = NULL;


	BOOL bRet = FALSE;
	try
	{
		#define INTERNET_FTP_PORT22 22
		pConnect = InternetSession.GetFtpConnection(_T("172.24.79.99"),_T("gtonel"),_T("gtonel"),INTERNET_FTP_PORT22,FALSE);
		#undef	INTERNET_FTP_PORT22

		CString	sString;
		sString.Format(_T("%s%s"),_T(PATH_XMLFILES),rsFileName);
		bRet = pConnect->GetFile(rsFileName,sString,FALSE,FILE_ATTRIBUTE_NORMAL,FTP_TRANSFER_TYPE_BINARY,1);
	}
	catch (CInternetException* pEx)
	{
		TCHAR sz[1024];
		pEx->GetErrorMessage(sz,1024);
		TRACE(_T("ERROR! %s\n"),sz);
		pEx->Delete();
		return bRet;
	}
	// if the connection is open, close it
	if(pConnect != NULL)
		pConnect->Close();
	delete pConnect;

	return bRet;
}

*/