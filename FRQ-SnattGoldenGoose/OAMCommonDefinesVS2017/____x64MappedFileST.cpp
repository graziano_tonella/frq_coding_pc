//////////////////////////////////////////////////////////
//														//
//                                                      //
// NAME:					MappedFileST.cpp            //
//														//
// LAST MODIFICATION DATE:	30/03/2016					//
//                                                      //
// See MappedFileST.h for details                       //
//                                                      //
//////////////////////////////////////////////////////////
#include "stdafx.h"
//
#include "x64MappedFileST.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMappedFileST::CMappedFileST(void)
{
	// All' inizio il file non e' mappato (quindi aperto)
	m_bFileOpen		= FALSE;
	m_bConnected	= FALSE;
	m_bInMemory		= FALSE;
	m_hFile			= NULL;
	m_hMap			= NULL;
	m_lpMapFile		= NULL;
	m_dwLastError	= 0;
}


CMappedFileST::~CMappedFileST(void)
{
	// Se mappatura ancora attiva al momento della distruzione
	// dell' istanza allora ne forzo la chiusura
	if(IsMapped() == TRUE)
		Close();
}

// Attiva la mappatura di un file in memoria
//
// Input:	lpszFileName			Path e nome del file su disco
//			lpszMappingObjectName	Nome Logico del file su disco
//			dwSize					Dimensione in byte del buffer
//			bCreateAnyway				TRUE	Il file viene aperto oppure creato
//										ed inizializzato
//									FALSE	Il file viene aperto SOLO se gia'
//											esistente (viene comunque fatto
//											il controllo sulla sua lunghezza)
// 
// Output:	void*					Valido se tutto ok
//			NULL					Errore (per il codice usare GetLastError)
//									Il file non esiste (bCreateAnyway = FALSE)
void* CMappedFileST::Open(LPCTSTR lpszFileName,LPCTSTR lpszMappingObjectName,DWORD dwSize,BOOL bCreateAnyway)
{
	DWORD	dwFileSize;
	DWORD	dwError;

	// Se mappatura non attiva allora esco
	if(IsMapped() == TRUE)
		return NULL;

	// Ci aspettiamo che il file esista gia'
	m_bFileExists = TRUE;


	// Apriamo il file
	#define _CREATEFILE_STD
	#ifdef  _CREATEFILE_STD
		m_hFile = ::CreateFile(	
			lpszFileName,						// File name
			GENERIC_READ|GENERIC_WRITE,			// Read-Write Access
			0,									// No sharing
			NULL,								// No security attributes
			(bCreateAnyway == TRUE) ? OPEN_ALWAYS : OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL | 
			FILE_FLAG_SEQUENTIAL_SCAN |
			FILE_FLAG_RANDOM_ACCESS, 
			NULL);								// Ignore hTemplateFile
	#else
		#pragma message("GTO: Abilitato In Data 27 Ottobre 2009 "__FILE__)
		m_hFile = ::CreateFile(	
			lpszFileName,						// File name
			GENERIC_READ|GENERIC_WRITE,			// Read-Write Access
			0,									// No sharing
			NULL,								// No security attributes
			(bCreateAnyway == TRUE) ? OPEN_ALWAYS : OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL	| 
			FILE_FLAG_RANDOM_ACCESS	|
			FILE_FLAG_WRITE_THROUGH, 
			NULL);								// Ignore hTemplateFile
	#endif


	// Se la CreateFile fallisce allora esco
	if (m_hFile == INVALID_HANDLE_VALUE)
	{
		m_dwLastError = ::GetLastError();
		return NULL;
	}

	// Leggo la dimensione del file	
	dwFileSize = ::GetFileSize(m_hFile, NULL);
	// Se impossibile determinare la lunghezza del file
	if (dwFileSize == 0xffffffff)
	{
		m_dwLastError = ::GetLastError();
		Shutdown();
		return NULL;
	}
	// Se dimensione diversa da quella del buffer
	// allora inizializzo il file con il buffer di default
	if (dwFileSize != dwSize)
	{
		// Se la InitializeFile fallisce allora esco
		if (InitializeFile(m_hFile, dwSize) == FALSE)
		{
			m_dwLastError = ::GetLastError();
			Shutdown();
			return NULL;
		}
		m_bFileExists = FALSE;
	}

	::SetLastError(0);

	// Create a mapping object from the file
	m_hMap = ::CreateFileMapping(
		m_hFile,				// Handle we got from CreateFile
		NULL,					// No security attributes
		PAGE_READWRITE,			// read-write
		0, 0,					// Max size = current size of file
		lpszMappingObjectName);	// Mapping Object Name

	// Se la CreateFileMapping fallisce allora esco
	dwError = ::GetLastError();
	if (m_hMap == NULL || dwError == ERROR_ALREADY_EXISTS)
	{
		m_dwLastError = dwError;
		Shutdown();
		return NULL;
	}

	m_lpMapFile = MapView(m_hMap);

	return m_lpMapFile;
} // End of Open

// Attiva la mappatura di un file in memoria
//
// Input:	lpszMappingObjectName	Nome Logico del file
//									Terminal Services:   The name can have a "Global\" or "Local\" prefix to explicitly
//									create the object in the global or session namespace.
//			dwMaximumSizeHigh		Dimensione in byte del buffer
//			dwMaximumSizeLow		Dimensione in byte del buffer
// 
// Output:	void*					Valido se tutto ok
//			NULL					Errore (per il codice usare GetLastError)
void* CMappedFileST::OpenInMemoryOnly(LPCTSTR lpszMappingObjectName,DWORD dwMaximumSizeHigh,DWORD dwMaximumSizeLow)
{
	DWORD	dwError;

	m_hFile			= NULL;
	m_bFileExists	= FALSE;

	// Se mappatura attiva allora esco
	if(IsMapped() == TRUE)
		return NULL;

	::SetLastError(0);

	// Create a mapping object in system paging
	m_hMap = ::CreateFileMapping(
		INVALID_HANDLE_VALUE,	// file mapping object is backed by the system paging file instead of by a file in the file system
		NULL,					// No security attributes
		PAGE_READWRITE,			// read-write
		dwMaximumSizeHigh,		// Max size = current size of file
		dwMaximumSizeLow,		// Max size = current size of file
		lpszMappingObjectName);	// Mapping Object Name

	// Se la CreateFileMapping fallisce allora esco
	dwError = ::GetLastError();
	if (m_hMap == NULL || dwError == ERROR_ALREADY_EXISTS)
	{
		m_dwLastError = dwError;
		Shutdown();
		return NULL;
	}

	m_lpMapFile = MapView(m_hMap);

	if(m_bFileOpen == TRUE)
		m_bInMemory = TRUE;

	return m_lpMapFile;
} // End of OpenInMemoryOnly


// Inizializza il file con il buffer di default
BOOL CMappedFileST::InitializeFile(HANDLE hHandle, LONG lDistanceToMove)
{
	if (::SetFilePointer(hHandle, lDistanceToMove, 0, FILE_BEGIN) != 0xFFFFFFFF)
		return ::SetEndOfFile(hHandle);

	return FALSE;
} // End of InitializeFile


// Forza la scrittura dei dati eventualmente ancora presenti
// in cache
//
// Input:
//
// Output:	TRUE		Tutto ok
//			FALSE		Errore
//						oppure mappatura non attiva
BOOL CMappedFileST::Flush(void)
{
	BOOL	bRet;
	// Se mappatura non attiva allora esco
	if (IsMapped() == FALSE)
		return FALSE;

	// Forzo scrittura
	bRet = ::FlushViewOfFile(m_lpMapFile, 0);

	return bRet;
} // End of Flush


// Esegue la "de-mappatura" del file in memoria
// Prima della chiusura esegue una Flush() di sicurezza
//
// Input:
//
// Output:	TRUE		Tutto ok, file chiuso
//			FALSE		Errore durante la Flush()
//						oppure il file non era aperto
BOOL CMappedFileST::Close(void)
{
	// Se mappatura non attiva allora esco
	if (IsMapped() == FALSE)
		return FALSE;

	// Se fallisce la Flush allora esco
	if (Flush() == FALSE)
		return FALSE;

	// Termino mappatura
	if(::UnmapViewOfFile(m_lpMapFile) == FALSE)
		return FALSE;

	Shutdown();

	return TRUE;
} // End of Close


// In caso di anomalia durante la fase di inizializzazione
// del file mappato in memoria (tramite Open) viene chiamata 
// per chiudere gli eventuali HANDLE aperti e per impostarli
// a valori di default (NULL)
//
// Input:
//
// Output:
void CMappedFileST::Shutdown(void)
{
	// Le seguenti due CloseHandle DEVONO essere eseguite
	// in quest' ordine: 
	// 1- m_hMap
	// 2- m_hFile
	// Questo perche' Shutdown viene chiamata anche da Close()
	// che esegue la "de-mappatura" del file e gli handle
	// devono essere chiusi in ordine inverso a come sono
	// stati aperti
	if (m_hMap != NULL)
		::CloseHandle(m_hMap);

	if (m_hFile != NULL)
		::CloseHandle(m_hFile);

	// Impostazione valori di default
	m_bFileOpen	= FALSE;
	m_bConnected= FALSE;
	m_bInMemory	= FALSE;
	m_hFile		= NULL;
	m_hMap		= NULL;
	m_lpMapFile	= NULL;
} // End of Shutdown


// Restituisce lo stato della mappatura
//
// Input:
//
// Output:	TRUE		Mappatura attiva
//			FALSE		Mappatura non attiva
BOOL CMappedFileST::IsMapped(void)
{
	BOOL bState;

	bState = (	m_bFileOpen	== TRUE &&
				(m_bInMemory == TRUE || m_bConnected == TRUE || (m_bConnected == FALSE && m_hFile != NULL)) &&
				m_hMap		!= NULL &&
				m_lpMapFile	!= NULL);

	return bState;
} // End of IsMapped


// Indica se al momento della Open(...) il file richiesto
// era gia' presente su disco oppure e' stato creato
//
// Input:
// 
// Output:	TRUE		Il file esisteva gia'
//			FALSE		Il file non esisteva oppure
//						era di dimensioni minori a quelle
//						della struttura da mappare
//						ATTENZIONE: la funzione restituisce 
//						FALSE anche quando viene chiamata prima
//						di attivare la mappatura (con Open)
BOOL CMappedFileST::FileExists(void)
{
	// Se mappatura non attiva allora esco
	if (IsMapped() == FALSE)
		return FALSE;

	return m_bFileExists;
} // End of FileExists

void* CMappedFileST::Connect(LPCTSTR lpszMappingObjectName)
{
	m_hMap = ::OpenFileMapping(
				FILE_MAP_ALL_ACCESS,
				FALSE,
				lpszMappingObjectName);

	if (m_hMap == NULL)
	{
		m_dwLastError = ::GetLastError();
		Shutdown();
		return NULL;
	}

	m_lpMapFile = MapView(m_hMap);

	if(m_bFileOpen == TRUE)
		m_bConnected = TRUE;

	return m_lpMapFile;
} // End of Connect

// Restituisce l' ultimo errore identificato e
// memorizzato dalla classe
//
// Input:
//
// Output:	Ultimo errore
DWORD CMappedFileST::GetLastError(void)
{
	return m_dwLastError;
} // End of GetLastError


// Restituisce la versione della classe.
// Sapere la versione puo' essere utile quando si usa
// la classe in progetti differenti
//
// Dividere per 10 per ottenere il numero reale di versione
//
// Input:
//
// Output:	Versione in formato numerico
const short CMappedFileST::GetVersionI(void)
{
	return 14;
} // End of GetVersionI


const char* CMappedFileST::GetVersionC(void)
{
  return "1.4";
} // End of GetVersionC


void* CMappedFileST::MapView(HANDLE hFile)
{
	void* pMapView;

	// Map the file to memory
	pMapView = ::MapViewOfFile(
				hFile,				// Handle we got from CreateFileMapping
				FILE_MAP_WRITE,		// read-write access
				0, 0,				// Offset into file = beginning of file
				0);					// Map entire file

	// Se la MapViewOfFile fallisce allora esco
	if (pMapView == NULL)
	{
		m_dwLastError = ::GetLastError();
		Shutdown();
	}
	else
	// A questo punto la mappatura e' riuscita
	m_bFileOpen = TRUE;

	return pMapView;
} // End of MapView
