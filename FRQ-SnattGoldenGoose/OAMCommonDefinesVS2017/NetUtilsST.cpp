#include "stdafx.h"
#include "NetUtilsST.h"

CNetUtilsST::CNetUtilsST()
{
}

CNetUtilsST::~CNetUtilsST()
{
}

//WSAEFAULT The name parameter is not a valid part of the user address space, or the buffer size specified by namelen parameter is too small to hold the complete host name. 
//WSANOTINITIALISED A successful WSAStartup call must occur before using this function. 
//WSAENETDOWN The network subsystem has failed. 
//WSAEINPROGRESS A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function. 
DWORD CNetUtilsST::GetLocalHostName(LPTSTR lpszLocalHostName)
{
	DWORD	dwRetValue = NETUTILSST_OK;
	DWORD	dwRetValueFunc;
	char	szHostName[256];
	CString	sHostName;

	dwRetValueFunc = GetHostName_(szHostName, sizeof(szHostName));
	if (dwRetValueFunc != NETUTILSST_OK)
	{
		dwRetValue = dwRetValueFunc;
	}
	else
	{
		sHostName = szHostName;
		::wsprintf(lpszLocalHostName, (LPCTSTR)sHostName);
	}

	return dwRetValue;
} // End of GetLocalHostName

DWORD CNetUtilsST::GetLocalIPAddress(in_addr* pcsAddr, UINT nAdapter)
{
	HOSTENT*	pHostEnt;
	DWORD		dwRetValue = NETUTILSST_OK;
	DWORD		dwRetValueFunc;
	int			nError;
	char		szHostName[256];

	// Get this machines host name
	dwRetValueFunc = GetHostName_(szHostName, sizeof(szHostName));
	if (dwRetValueFunc != NETUTILSST_OK)
	{
		dwRetValue = dwRetValueFunc;
	}
	else
	{
		// Get host information from the host name
		pHostEnt = ::gethostbyname(szHostName);
		if (pHostEnt == NULL)
		{
			nError = ::WSAGetLastError();
			switch (nError)
			{
				case WSAEFAULT:
					dwRetValue = NETUTILSST_WSAEFAULT;
					break;
				case WSANOTINITIALISED:
					dwRetValue = NETUTILSST_WSANOTINITIALISED;
					break;
				case WSAENETDOWN:
					dwRetValue = NETUTILSST_WSAENETDOWN;
					break;
				case WSAEINPROGRESS:
					dwRetValue = NETUTILSST_WSAEINPROGRESS;
					break;
				case WSAHOST_NOT_FOUND:
					dwRetValue = NETUTILSST_WSAHOST_NOT_FOUND;
					break;
				case WSATRY_AGAIN:
					dwRetValue = NETUTILSST_WSATRY_AGAIN;
					break;
				case WSANO_RECOVERY:
					dwRetValue = NETUTILSST_WSANO_RECOVERY;
					break;
				case WSANO_DATA:
					dwRetValue = NETUTILSST_WSANO_DATA;
					break;
				default:
					dwRetValue = NETUTILSST_NOK;
					break;
			} // switch
		}
		else
		{
			// Check the length of the IP adress
			if (pHostEnt->h_length != 4)
			{
				dwRetValue = NETUTILSST_NO32BITSADDR;
			} // if
			else
			{
				try
				{
					// If adapter exists
					if (pHostEnt->h_addr_list[nAdapter] != NULL)
					{
						::CopyMemory(&pcsAddr->S_un.S_addr, pHostEnt->h_addr_list[nAdapter], pHostEnt->h_length);
					}
					else
					{
						dwRetValue = NETUTILSST_INVALIDADAPTERNUMBER;
					} // else
				}
				catch(...)
				{
					dwRetValue = NETUTILSST_BADPARAM;
				}
			} // else
		} // else
	} // else

	return dwRetValue;
} // End of GetLocalIPAddress

DWORD CNetUtilsST::GetLocalIPAddress(LPTSTR lpszAddr, UINT nAdapter)
{
	DWORD		dwRetValue = NETUTILSST_OK;
	DWORD		dwRetValueFunc;
	in_addr		csAddr;

	dwRetValueFunc = GetLocalIPAddress(&csAddr, nAdapter);
	if (dwRetValueFunc != NETUTILSST_OK)
	{
		dwRetValue = dwRetValueFunc;
	}
	else
	{
		try
		{
			::wsprintf(lpszAddr, _T("%d.%d.%d.%d"), csAddr.S_un.S_un_b.s_b1,
													csAddr.S_un.S_un_b.s_b2, 
													csAddr.S_un.S_un_b.s_b3, 
													csAddr.S_un.S_un_b.s_b4);

		}
		catch(...)
		{
			dwRetValue = NETUTILSST_BADPARAM;
		}
	} // else

	return dwRetValue;
} // End of GetLocalIPAddress

DWORD CNetUtilsST::GetHostName_(char* lpszHostName, int nNameLen)
{
	DWORD	dwRetValue = NETUTILSST_OK;
	int		nRetValue;
	int		nError;

	nRetValue = ::gethostname(lpszHostName, nNameLen);
	if (nRetValue == SOCKET_ERROR)
	{
		nError = ::WSAGetLastError();
		switch (nError)
		{
			case WSAEFAULT:
				dwRetValue = NETUTILSST_WSAEFAULT;
				break;
			case WSANOTINITIALISED:
				dwRetValue = NETUTILSST_WSANOTINITIALISED;
				break;
			case WSAENETDOWN:
				dwRetValue = NETUTILSST_WSAENETDOWN;
				break;
			case WSAEINPROGRESS:
				dwRetValue = NETUTILSST_WSAEINPROGRESS;
				break;
			default:
				dwRetValue = NETUTILSST_NOK;
				break;
		} // switch
	} // if

	return dwRetValue;
} // End of GetHostName_
