// NAME:					CObjectTollsGT.cpp					//
// LAST MODIFICATION DATE:	24-Jul-2018							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Object Tools (NO GLOBAL ISTANCE)	//
//////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ObjectToolsGT.h"


CObjectToolsGT::CObjectToolsGT(void)
{
}

CObjectToolsGT::~CObjectToolsGT(void)
{

}

char* CObjectToolsGT::FromUINT16ToAsciiZ(UINT16 u16Value)
{
	// 0 to 65,535
	sprintf_s(m_szArrayBuffer[m_nIxArrayBuffer], sizeof(m_szArrayBuffer[m_nIxArrayBuffer]), "%0*u", DIM_ASCII_UINT16, u16Value);
	return m_szArrayBuffer[m_nIxArrayBuffer];
}

char* CObjectToolsGT::FromUINT32ToAsciiZ(UINT32 u32Value)
{
	_SetArrayIndex();

	// 0 to 4,294,967,295
	sprintf_s(m_szArrayBuffer[m_nIxArrayBuffer], sizeof(m_szArrayBuffer[m_nIxArrayBuffer]), "%0*u", DIM_ASCII_UINT32, u32Value);
	return m_szArrayBuffer[m_nIxArrayBuffer];
}

char* CObjectToolsGT::FromUINT64ToAsciiZ(UINT64 u64Value)
{
	// 0 to 18,446,744,073,709,551,615
	sprintf_s(m_szArrayBuffer[m_nIxArrayBuffer], sizeof(m_szArrayBuffer[m_nIxArrayBuffer]), "%0*I64u", DIM_ASCII_UINT64, u64Value);
	return m_szArrayBuffer[m_nIxArrayBuffer];
}


void CObjectToolsGT::_SetArrayIndex(void)
{
	m_nIxArrayBuffer++;
	if (m_nIxArrayBuffer < 0 || m_nIxArrayBuffer >= (int) ARRAY_ELEMENTS)
		m_nIxArrayBuffer = 0;
}

#ifdef ____aaaaa___________

include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#define SUCCESS_STAT 0

/**
* checks if a specific directory exists
* @param dir_path the path to check
* @return if the path exists
*/
bool dirExists(std::string dir_path)
{
	struct stat sb;

	if (stat(dir_path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode))
		return true;
	else
		return false;
}

/**
* deletes all the files in a folder (but not the folder itself). optionally
* this can traverse subfolders and delete all contents when recursive is true
* @param dirpath the directory to delete the contents of (can be full or
* relative path)
* @param recursive true = delete all files/folders in all subfolders
*                  false = delete only files in toplevel dir
* @return SUCCESS_STAT on success
*         errno on failure, values can be from unlink or rmdir
* @note this does NOT delete the named directory, only its contents
*/
int DeleteFilesInDirectory(std::string dirpath, bool recursive)
{
	if (dirpath.empty())
		return SUCCESS_STAT;

	DIR *theFolder = opendir(dirpath.c_str());
	struct dirent *next_file;
	char filepath[1024];
	int ret_val;

	if (theFolder == NULL)
		return errno;

	while ((next_file = readdir(theFolder)) != NULL)
	{
		// build the path for each file in the folder
		sprintf(filepath, "%s/%s", dirpath.c_str(), next_file->d_name);

		//we don't want to process the pointer to "this" or "parent" directory
		if ((strcmp(next_file->d_name, "..") == 0) ||
			(strcmp(next_file->d_name, ".") == 0))
		{
			continue;
		}

		//dirExists will check if the "filepath" is a directory
		if (dirExists(filepath))
		{
			if (!recursive)
				//if we aren't recursively deleting in subfolders, skip this dir
				continue;

			ret_val = DeleteFilesInDirectory(filepath, recursive);

			if (ret_val != SUCCESS_STAT)
			{
				closedir(theFolder);
				return ret_val;
			}
		}

		ret_val = remove(filepath);
		//ENOENT occurs when i folder is empty, or is a dangling link, in
		//which case we will say it was a success because the file is gone
		if (ret_val != SUCCESS_STAT && ret_val != ENOENT)
		{
			closedir(theFolder);
			return ret_val;
		}

	}

	closedir(theFolder);

	return SUCCESS_STAT;
}

#endif
