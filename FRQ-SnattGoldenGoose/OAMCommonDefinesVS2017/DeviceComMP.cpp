#include "stdafx.h"
#include "DeviceComMP.h"
#include <process.h>
//
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
//
//	Created:	19/November/1999
//	Updated:	18/05/2004
//
//
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//
CDeviceComMP::CDeviceComMP()
{
	//
	::ZeroMemory(&m_dtpThreadParam,SIZE_STRUCT_DCOM_THR_PARAM);
	//
	m_bInit=FALSE;
	m_dwErrorCode=0;
	//
	m_dtpThreadParam.hDeviceCom=INVALID_HANDLE_VALUE;
	m_dtpThreadParam.hThread=INVALID_HANDLE_VALUE;
	//
}
//
CDeviceComMP::~CDeviceComMP()
{
	//
	Destroy(500);
	//
}
//
BOOL CDeviceComMP::Init()
{
	// We must check if Init was already runned ...
	if (m_bInit == TRUE)
	{
		SetLastError(DEVICECOM_ALREADYINIT);
		return FALSE;
	}
	//

	//
	m_bInit=TRUE; // We have make a correct Init ...
	SetLastError(DEVICECOM_OK);
	return TRUE;
    //
}
//
BOOL CDeviceComMP::Create(BYTE byComPort,DWORD dwBaudRate,char* szComFlags,DWORD dwBufferSize,DWORD dwPollingTime,DWORD dwRtsControl)
{
	// We must check if the class was initialized ...
	if (m_bInit == FALSE)
	{
		SetLastError(DEVICECOM_NOINIT);
		return FALSE;
	}
	//
	if (dwBufferSize<0 || dwBufferSize>4096)
	{
		SetLastError(DEVICECOM_BUFFERTOOBIG);
		return FALSE;
	}
	//
	if ( (szComFlags[0]-'0')<4 || (szComFlags[0]-'0')>8)
	{
		SetLastError(DEVICECOM_WRONGCOMFLAGS);
		return FALSE;
	}
	//
	if (strchr("NOESM",szComFlags[1]) == NULL)
	{
		SetLastError(DEVICECOM_WRONGCOMFLAGS);
		return FALSE;
	}
	//
	if ( szComFlags[2]!='1' && szComFlags[2]!='2')
	{
		SetLastError(DEVICECOM_WRONGCOMFLAGS);
		return FALSE;
	}
	// We have to transform from number to string ( 1 = COM1 )
	CString	sComDeviceName;
	sComDeviceName.Format(_T("\\\\.\\COM%u"),byComPort);
	//
    m_dtpThreadParam.hDeviceCom=CreateFile((LPCTSTR) sComDeviceName, GENERIC_READ | GENERIC_WRITE,
						  0,                    // exclusive access
						  NULL,                 // no security attrs
						  OPEN_EXISTING,
						  FILE_FLAG_OVERLAPPED, // overlapped I/O
						  NULL );
	//
    if (m_dtpThreadParam.hDeviceCom == INVALID_HANDLE_VALUE)
	{
		//
		SetLastError(DEVICECOM_WRONGCOMPORT);
		return FALSE;
		//
	}
	//
	SetCommMask(m_dtpThreadParam.hDeviceCom,0);
	// dimensiona i buffers del driver
	SetupComm(m_dtpThreadParam.hDeviceCom,dwBufferSize,dwBufferSize);
	// clear driver e buffers status 
	PurgeComm(m_dtpThreadParam.hDeviceCom, PURGE_TXABORT | PURGE_RXABORT |
					        PURGE_TXCLEAR | PURGE_RXCLEAR );
	//
	COMMTIMEOUTS CommTimeOuts;
	::ZeroMemory(&CommTimeOuts,sizeof(CommTimeOuts));
	CommTimeOuts.ReadIntervalTimeout = MAXDWORD;
	//
    if(SetCommTimeouts(m_dtpThreadParam.hDeviceCom,&CommTimeOuts) == FALSE)
		return FALSE;
	//
    DCB dcb;
	dcb.DCBlength = sizeof( DCB ) ;
	// get comm state
	GetCommState(m_dtpThreadParam.hDeviceCom,&dcb);
	// setup hardware flow control
	dcb.fOutxCtsFlow = FALSE ;                 // CTS output flow control
	dcb.fOutxDsrFlow = FALSE ;                 // DSR output flow control 
	dcb.fDtrControl = DTR_CONTROL_DISABLE ;    // DTR flow control type
	dcb.fDsrSensitivity = FALSE;               // DSR sensitivity 
	dcb.fRtsControl = dwRtsControl;				// By User
	// setup software flow control
	dcb.fTXContinueOnXoff = TRUE;             // XOFF continues Tx  
	dcb.fOutX = FALSE ;                        // XON/XOFF out flow control 
	dcb.fInX = FALSE ;                         // XON/XOFF in flow control  
	dcb.XonLim = 0 ;                         // transmit XON threshold  
	dcb.XoffLim = 0 ;                        // transmit XOFF threshold 
	dcb.XonChar = 0;                        // Tx and Rx XON character  
	dcb.XoffChar = 0;                       // Tx and Rx XOFF character 
	// other various settings
	dcb.fParity = TRUE;                        // enable parity checking 
	dcb.fBinary = TRUE;				 	       // binary mode, no EOF check 
	dcb.fErrorChar = FALSE ;                   // enable error replacement 
	dcb.fNull = FALSE ;                        // enable null stripping 
	dcb.fAbortOnError = FALSE ;                // abort reads/writes on error 
	dcb.ErrorChar = '?';                       // error replacement character 
	dcb.EofChar = 0;                           // end of input character 
	dcb.EvtChar = 0;                           // received event character 
	// setup data control 
	dcb.BaudRate = dwBaudRate;                 // current baud rate 
	dcb.ByteSize = szComFlags[0]-'0';          // number of bits/byte, 4-8
	//
	switch(szComFlags[1]) // Parity 0-4=no,odd,even,mark,space 
	{
	  case 'N': // None
		  dcb.fParity = FALSE;                        // enable parity checking 
		  dcb.Parity=0;
		  break;
	  case 'O': // Odd
		  dcb.Parity=1;
		  break;
	  case 'E': // Even
		  dcb.Parity=2;
		  break;
	  case 'M': // None
		  dcb.Parity=3;
		  break;
	  case 'S': // None
		  dcb.Parity=4;
		  break;
	}
	//
	switch(szComFlags[2]) // StopBits 0,1,2 = 1, 1.5, 2 
	{
	  case '1': // None
		  dcb.StopBits=0;
		  break;
	  case '2': // Odd
		  dcb.StopBits=2;
		  break;
	}
	//
	if(SetCommState(m_dtpThreadParam.hDeviceCom,&dcb) == FALSE)
		return FALSE;
	//
	// We must initialize each thread's parameters
	m_dtpThreadParam.pParent=this;
	m_dtpThreadParam.dwThrExitCode=0;
	m_dtpThreadParam.dwBufferSize=dwBufferSize;
	m_dtpThreadParam.dwPollingTime=dwPollingTime;
	//
	m_dtpThreadParam.hExitEv=::CreateEvent(NULL,FALSE,FALSE,NULL);
	if (m_dtpThreadParam.hExitEv == NULL)
	{
        //
		CloseHandle(m_dtpThreadParam.hDeviceCom);
		m_dtpThreadParam.hDeviceCom=INVALID_HANDLE_VALUE;
		//
		SetLastError(DEVICECOM_EVENTCREATIONERROR);
		return FALSE;
		//
	}
	//
	m_dtpThreadParam.hServiceEv=::CreateEvent(NULL,FALSE,FALSE,NULL);
	if (m_dtpThreadParam.hServiceEv == NULL)
	{
        //
		CloseHandle(m_dtpThreadParam.hDeviceCom);
		m_dtpThreadParam.hDeviceCom=INVALID_HANDLE_VALUE;
		//
		CloseHandle(m_dtpThreadParam.hExitEv);
		m_dtpThreadParam.hExitEv=NULL;
		//
		SetLastError(DEVICECOM_EVENTCREATIONERROR);
		return FALSE;
		//
	}
	//
	::ResetEvent(m_dtpThreadParam.hExitEv); // Just to be sure !
	m_dtpThreadParam.hThread=(HANDLE)_beginthreadex(NULL,0,
	                                 ThreadSerial,
	                                 &m_dtpThreadParam,
									 0,
									 (unsigned int*)&(m_dtpThreadParam.dwThreadID));
	//
	if (m_dtpThreadParam.hThread == NULL)
	{
		//
		CloseHandle(m_dtpThreadParam.hDeviceCom);
		m_dtpThreadParam.hDeviceCom=INVALID_HANDLE_VALUE;
		//
		CloseHandle(m_dtpThreadParam.hExitEv);
		m_dtpThreadParam.hExitEv=NULL;
		//
		CloseHandle(m_dtpThreadParam.hServiceEv);
		m_dtpThreadParam.hServiceEv=NULL;
		//
		SetLastError(DEVICECOM_THRCREATIONERROR);
		return FALSE;
		//
	}
	//
	DWORD dwResult;
	dwResult=WaitForSingleObject(m_dtpThreadParam.hServiceEv,5000);
	switch(dwResult)
	{
	  case WAIT_OBJECT_0: //
		  //
		  if (m_dtpThreadParam.dwThrExitCode != 0)
		  {
				//
				CloseHandle(m_dtpThreadParam.hDeviceCom);
				m_dtpThreadParam.hDeviceCom=INVALID_HANDLE_VALUE;
				//
				CloseHandle(m_dtpThreadParam.hExitEv);
				m_dtpThreadParam.hExitEv=NULL;
				//
				CloseHandle(m_dtpThreadParam.hServiceEv);
				m_dtpThreadParam.hServiceEv=NULL;
				//
				SetLastError(DEVICECOM_THRPARAMERROR);
				return FALSE;
				//
		  }
		  //
		  break;

	  case WAIT_TIMEOUT: //
		  //
		  CloseHandle(m_dtpThreadParam.hDeviceCom);
		  m_dtpThreadParam.hDeviceCom=INVALID_HANDLE_VALUE;
		  //
		  CloseHandle(m_dtpThreadParam.hExitEv);
		  m_dtpThreadParam.hExitEv=NULL;
		  //
		  CloseHandle(m_dtpThreadParam.hServiceEv);
		  m_dtpThreadParam.hServiceEv=NULL;
		  //
		  SetLastError(DEVICECOM_THRCREATIONERROR);
		  return FALSE;
		  //
		  break;
	}
	//
	SetLastError(DEVICECOM_OK);
	return TRUE;
	//
}
//
DWORD CDeviceComMP::GetLastError()
{
	//
	return m_dwErrorCode;
	//
}
//
LPSTR CDeviceComMP::GetLastErrorStr()
{
	//
	switch(m_dwErrorCode)
	{
	  case DEVICECOM_OK: 
		  return "No error";
		  break;
	  case DEVICECOM_NOINIT: 
		  return "Init() not called !";
		  break;
	  case DEVICECOM_ALREADYINIT: 
		  return "Init() already called !";
		  break;
	  case DEVICECOM_WRONGCOMFLAGS:
		  return "Wrong COM flags !";
		  break;
	  case DEVICECOM_EVENTCREATIONERROR: 
		  return "Event created with error !";
		  break;
	  case DEVICECOM_WRONGCOMPORT: 
		  return "Wrong COM port number or COM busy !";
		  break;
	  case DEVICECOM_THRCREATIONERROR: 
		  return "Thread creation error !";
		  break;
	  case DEVICECOM_ALLOCERROR: 
		  return "Memory allocation error !";
		  break;
      case DEVICECOM_BUFFERTOOBIG:
		  return "Buffer size too big (MAX 4Kb) !";
		  break;
      case DEVICECOM_THRPARAMERROR:
		  return "Wrong thread parameter/s !";
		  break;
	}
	//
	return "Illegal error code !";
	//
}
//
DWORD CDeviceComMP::SetLastError(DWORD dwError)
{
	//
	m_dwErrorCode=dwError;
	return m_dwErrorCode;
    //
}
//
BOOL CDeviceComMP::Destroy(DWORD dwMilliseconds)
{
	//
	if (m_bInit == FALSE)
	    return FALSE;
	//
	if (m_dtpThreadParam.hThread != NULL)
	{
		SetEvent(m_dtpThreadParam.hExitEv);
		::WaitForSingleObject(m_dtpThreadParam.hThread,dwMilliseconds);
		//
		CloseHandle(m_dtpThreadParam.hThread);
		m_dtpThreadParam.hThread=NULL;
	}
	//
	if (m_dtpThreadParam.hExitEv != NULL)
	{
		CloseHandle(m_dtpThreadParam.hExitEv);
		m_dtpThreadParam.hExitEv=NULL;
	}
	//
	if (m_dtpThreadParam.hDeviceCom != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_dtpThreadParam.hDeviceCom);
		m_dtpThreadParam.hDeviceCom=INVALID_HANDLE_VALUE;
	}
	//
	return TRUE;
	//
}
//
#define JMP_STEP(s)\
{                  \
	nThrStep=s; \
};
//
unsigned int __stdcall CDeviceComMP::ThreadSerial(LPVOID pParam)
{
	//
	STRUCT_DCOM_THR_PARAM* p;
	CDeviceComMP* pParent;
	//
	BOOL  bResult;
	DWORD dwResult;
	DWORD dwEventMsk;
	DWORD	dwModemStat;
	DWORD dwBytesToRead=0;
	DWORD dwBytesRead;
	DWORD dwBytesToWrite=0;
	DWORD dwBytesWrite;
	OVERLAPPED oOsCom;
	OVERLAPPED oOverlappedRead;
	OVERLAPPED oOverlappedWrite;
	HANDLE hHandles[2];
	COMSTAT ComStat ;
	DWORD dwErrorFlags;
	BOOL bThrExit;
	int nThrStep;
	BYTE* pbBuffer;
	//
	p=(STRUCT_DCOM_THR_PARAM*)pParam;
	//
	if (p->pParent == NULL)
	{
		//
		p->dwThrExitCode=1;
		::SetEvent(p->hServiceEv);
		return(p->dwThrExitCode);
		//
	}
	//
	pParent=(CDeviceComMP*)p->pParent;
	//
	::ZeroMemory(&oOsCom,sizeof(OVERLAPPED));
	::ZeroMemory(&oOverlappedRead,sizeof(OVERLAPPED));
	::ZeroMemory(&oOverlappedWrite,sizeof(OVERLAPPED));
	//
    oOsCom.hEvent=::CreateEvent(NULL,TRUE,FALSE,NULL);
	if (oOsCom.hEvent == NULL)
	{
		//
		p->dwThrExitCode=2;
		::SetEvent(p->hServiceEv);
		return(p->dwThrExitCode);
		//
	}
	//
	oOverlappedRead. hEvent=::CreateEvent(NULL,FALSE,FALSE,NULL);
	if (oOverlappedRead.hEvent == NULL)
	{
		//
		p->dwThrExitCode=3;
		CloseHandle(oOsCom.hEvent);
		::SetEvent(p->hServiceEv);
		return(p->dwThrExitCode);
		//
	}
	//
	oOverlappedWrite.hEvent=::CreateEvent(NULL,FALSE,FALSE,NULL);
	if (oOverlappedWrite.hEvent == NULL)
	{
		//
		p->dwThrExitCode=4;
		CloseHandle(oOsCom.hEvent);
		CloseHandle(oOverlappedRead. hEvent);
		::SetEvent(p->hServiceEv);
		return(p->dwThrExitCode);
		//
	}
	//
	pbBuffer  = new BYTE[p->dwBufferSize];
	if (pbBuffer == NULL)
	{
		//
		p->dwThrExitCode=5;
		CloseHandle(oOsCom.hEvent);
		CloseHandle(oOverlappedRead.hEvent);
		CloseHandle(oOverlappedWrite.hEvent);
		::SetEvent(p->hServiceEv);
		return(p->dwThrExitCode);
		//
	}
	//
	::ZeroMemory(pbBuffer,p->dwBufferSize);
	//
	bThrExit=FALSE;
	nThrStep=DEVICECOM_THRSTEP_WAIT_RX;
	p->dwThrExitCode=0;
	::SetEvent(p->hServiceEv);
	//
	while(bThrExit == FALSE) // Main loop
	{
		switch(nThrStep) // Step of the Thread
		{
			  //
		  case DEVICECOM_THRSTEP_WAIT_RX: // Thread running main loop
			  //
			  dwEventMsk = (DWORD) (EV_RXCHAR|EV_ERR|EV_CTS);
			  SetCommMask(p->hDeviceCom,dwEventMsk);
			  if (WaitCommEvent(p->hDeviceCom,&dwEventMsk,&oOsCom) == FALSE)
  			    if (dwResult=::GetLastError() != ERROR_IO_PENDING)
				{
					dwResult=0;
				}
			  //
              ::ZeroMemory(&hHandles,sizeof(hHandles));
			  hHandles[0]=p->hExitEv;
			  hHandles[1]=oOsCom.hEvent;
			  dwResult=::WaitForMultipleObjects(2,hHandles,FALSE,p->dwPollingTime);
			  //
			  switch(dwResult)
			  {
				case WAIT_OBJECT_0: // ExitEvent
					//
					bThrExit=TRUE;
					break;
                    //
				case WAIT_OBJECT_0+1: // Com Event
					//
					if (dwEventMsk == 0)
						break;
					//
					if (dwEventMsk & EV_RXCHAR)
					{
                        JMP_STEP(DEVICECOM_THRSTEP_RX)
					}
					if (dwEventMsk & EV_ERR)
					{
						ClearCommError(p->hDeviceCom,&dwErrorFlags,&ComStat);
						pParent->OnError(dwErrorFlags);
					}
					//
					/*
					if(dwEventMsk & EV_CTS)
					{
						pParent->OnEventCTS();
					}
					*/
					//
					break;
					//
				case WAIT_TIMEOUT:
					//
					pParent->OnTimeout();
					JMP_STEP(DEVICECOM_THRSTEP_TX)
					//
					if(GetCommModemStatus(p->hDeviceCom,&dwModemStat) == TRUE)
					{
						if(dwModemStat & MS_CTS_ON)
							pParent->OnEventCTS();
					}
					break;
					//
                default: 
					//
					break;
			  }
			  //
			  break;
              //
		  case DEVICECOM_THRSTEP_RX: // The COM have one or more byte ready ...
			  //
              // recupera il numero di byte in coda e flag di errore
              if (ClearCommError(p->hDeviceCom,&dwErrorFlags,&ComStat) == FALSE)
			  {
				  dwResult=::GetLastError();
			  }
              //
			  dwBytesToRead=min(p->dwBufferSize,ComStat.cbInQue);
			  if (dwBytesToRead == 0)
			  {
			      JMP_STEP(DEVICECOM_THRSTEP_TX)
				  break;
			  }
			  //
			  dwBytesRead=0;
			  //
			  ::ZeroMemory(pbBuffer,p->dwBufferSize);
			  bResult=::ReadFile(p->hDeviceCom,
				                 pbBuffer,
					       	     dwBytesToRead,&dwBytesRead,
							     &oOverlappedRead);
			  //
			  if (bResult == FALSE)
			  {
				  if ((dwResult=::GetLastError()) == ERROR_IO_PENDING)
				  {
					  // We have to wait the pending event ....
					  ::ZeroMemory(&hHandles,sizeof(hHandles));
					  hHandles[0]=p->hExitEv;
					  hHandles[1]=oOverlappedRead.hEvent;
					  dwResult=::WaitForMultipleObjects(2,hHandles,FALSE,INFINITE);
					  switch(dwResult)
					  {
						case WAIT_OBJECT_0: // Exit Event
							//
							bThrExit=TRUE;
							break;
							//
						case WAIT_OBJECT_0+1: // End of pending events ...
							//
                            bResult=GetOverlappedResult(p->hDeviceCom,&oOverlappedRead,&dwBytesRead,FALSE);
							if (bResult == TRUE)
							{
								pParent->OnReceive(pbBuffer,dwBytesRead);
							    JMP_STEP(DEVICECOM_THRSTEP_TX)
							}
							else
							{
								dwResult=::GetLastError();
							}
							break;
							//
						default:
							//
							break;
					  }
					  //
				  }
				  //
			  }
			  //
			  pParent->OnReceive(pbBuffer,dwBytesRead);
			  JMP_STEP(DEVICECOM_THRSTEP_TX)
			  //
			  break;
			  //
		  case DEVICECOM_THRSTEP_TX: // Check if we have to send something ...
			  //
			  if ((dwBytesToWrite=pParent->OnPrepareMsgToSend(pbBuffer,p->dwBufferSize)) == 0)
			  {
				  JMP_STEP(DEVICECOM_THRSTEP_WAIT_RX)
				  break;
			  }
			  //
			  bResult=::WriteFile(p->hDeviceCom,
				                  pbBuffer,
					       	      dwBytesToWrite,&dwBytesWrite,
							      &oOverlappedWrite);
			  //
			  if (bResult == FALSE)
			  {
				  if ((dwResult=::GetLastError()) == ERROR_IO_PENDING)
				  {
					  // We have to wait the pending event ....
					  ::ZeroMemory(&hHandles,sizeof(hHandles));
					  hHandles[0]=p->hExitEv;
					  hHandles[1]=oOverlappedWrite.hEvent;
					  dwResult=::WaitForMultipleObjects(2,hHandles,FALSE,INFINITE);
					  switch(dwResult)
					  {
						case WAIT_OBJECT_0: // Exit Event
							//
							bThrExit=TRUE;
							break;
							//
						case WAIT_OBJECT_0+1: // End of pending events ...
							//
                            bResult=GetOverlappedResult(p->hDeviceCom,&oOverlappedWrite,&dwBytesWrite,FALSE);
							if (bResult == TRUE)
							{
								break;
							}
							else
							{
								dwResult=::GetLastError();
							}
							break;
							//
						default:
							//
							break;
					  }
					  //
				  }
				  //
			  }
			  //
			  JMP_STEP(DEVICECOM_THRSTEP_WAIT_RX)
			  //
			  break;
			  //
		  default: // 
			  //
              break;
		}
		//
		::Sleep(1);
		//
	}
	//
	delete[] pbBuffer;
	//
	CloseHandle(oOsCom.hEvent);
	CloseHandle(oOverlappedRead.hEvent);
	CloseHandle(oOverlappedWrite.hEvent);
	//
	return(p->dwThrExitCode);
	//
	#undef BUFFER_SIZE
}
