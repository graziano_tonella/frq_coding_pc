//////////////////////////////////////////////////////////////////
// NAME:					CSharedFlatQueue.h					//
// LAST MODIFICATION DATE:	30-Mar-2016							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Flat Queue Handling					//
//////////////////////////////////////////////////////////////////
#pragma once

//#define __SHAREDFLATQUEUEGT_NODLL__
#ifndef __SHAREDFLATQUEUEGT_NODLL__
	//#define	__GTXDLL_BUILDDLL__
	#ifdef	__GTXDLL_BUILDDLL__
		#define	SHAREDFLATQUEUEGT_EXPORT	__declspec(dllexport)
	#else
		#define	SHAREDFLATQUEUEGT_EXPORT	__declspec(dllimport)
	#endif
#else
	#define	SHAREDFLATQUEUEGT_EXPORT
#endif

enum
{
	QUEUEGT_OK,
	QUEUEGT_NOINIT,
	QUEUEGT_ALREADYINIT,
	QUEUEGT_EMPTY,
	QUEUEGT_BADPARAM,
	QUEUEGT_NOMEMORY,
	QUEUEGT_FULL,
	QUEUEGT_BUFFERTOOBIG,
	QUEUEGT_SOFWARE_ERROR
};

class SHAREDFLATQUEUEGT_EXPORT CSharedFlatQueueGT
{
public:
	CSharedFlatQueueGT();
	virtual	~CSharedFlatQueueGT();

	DWORD	Create(DWORD dwBufferSize, HWND hWndParent, UINT nMsgParent, HANDLE hEventParent, HANDLE hThreadHandle);
	BOOL	IsCreated();

	void	Destroy();

	DWORD	GetBufferSize();
	DWORD	GetSpaceAvailable();
	DWORD	GetFirstItemSize();
	
	DWORD	PutItem(LPBYTE pbyBuffer, DWORD dwBufferSize);
	DWORD	GetItem(LPBYTE pbyBuffer, BOOL bRemove, DWORD* pdwNumBytesCopied);

	DWORD	GetCount();

	DWORD	Empty();

	DWORD	SaveQueue(LPCTSTR lpszFileName);
	DWORD	RestoreQueue(LPCTSTR lpszFileName);

	static short	GetVersionI()	{return 10;}
	static LPCTSTR	GetVersionC()	{return (LPCTSTR)_T("10");}

private:
	LPBYTE	m_pbyData;
	LPBYTE	m_pbyTop;
	LPBYTE	m_pbyBottom;
	LPBYTE	m_pbyPut;
	LPBYTE	m_pbyGet;

	DWORD	m_dwSpaceAvailable;	// Actual number of bytes free in queue
	DWORD	m_dwBufferSize;		// Max number of bytes can fit in queue
	DWORD	m_dwNumItems;		// Number of items currently in queue

	DWORD	m_dwMaxItems;		// Max Number Of Items Stored
	DWORD	m_dwMinFreeSpace;	// Min Number Of Bytes Free

	CRITICAL_SECTION	m_cs_Wait;

	// Handle of the destination window that will receive the notification message
	HWND	m_hWndParent;		// NULL if not used
	// User message sent when a new item is inserted in queue
	UINT	m_nMsgParent;		// 0 (zero) if not used
	// Event that will be raised when a new item is inserted in queue
	HANDLE	m_hEventParent;		// NULL if not used
	// Thread that will be resumed when a new item is inserted in queue
	HANDLE	m_hThreadHandle;	// NULL if not used
protected:
	//CFile	m_FileRW;
	BOOL	CheckError();

public:
	inline DWORD GetMaxItems();
	inline DWORD GetMinSpaceFree();
};

inline DWORD CSharedFlatQueueGT::GetMaxItems()
{
	DWORD	dwMaxItems;

	::EnterCriticalSection(&m_cs_Wait);
	dwMaxItems = m_dwMaxItems;
	::LeaveCriticalSection(&m_cs_Wait);

	return dwMaxItems;
}

inline DWORD CSharedFlatQueueGT::GetMinSpaceFree()
{
	DWORD	dwMinFreeSpace;

	::EnterCriticalSection(&m_cs_Wait);
	dwMinFreeSpace = m_dwMinFreeSpace;
	::LeaveCriticalSection(&m_cs_Wait);

	return dwMinFreeSpace;
}
