//////////////////////////////////////////////////////////////////
//																//
// NAME:					CTCPIPServerClientFDXGT.cpp			//
// LAST MODIFICATION DATE:	24-Feb-2016							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					TCP/IP connection					//
//							Server/Client Full Duplex			//
//////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "TCPIPServerClientFDXGT.h"


CTCPIPServerClientFDXGT::CTCPIPServerClientFDXGT(void)
{
	::InitializeCriticalSection(&m_cs_Wait);

	m_bJustConnected			= FALSE;
	m_byConnectionState			= 0;
	m_bySourceEnvPrgNumber		= 0;
	m_byDestinationEnvPrgNumber	= 0;

	m_hwndID					= NULL;
	m_uMsgID					= 0;
	m_crColor					= RGB(255,255,255); // White

	m_pMsgTCPIP					= NULL;
	m_pMsgENQUEUED				= NULL;

	m_bExcludeNODEControl		= FALSE;
	m_bExcludeMSGPRGControl		= FALSE;
	m_bExcludeMSGPacking		= FALSE;
	m_bSendAlwaysKeepAlive		= FALSE;
	m_sChannel.Format(_T("No Channel"));

	m_bDoNotSendSTDKeepAlive	= FALSE;

	m_bSendTelegramAtFixedLength= FALSE;
	m_dwFixedTelegramLength		= 0;

	m_bLimitTelegramLength		= FALSE;
	m_dwLimitedTelegramLength	= 0;

	m_bAlterMsgId				= FALSE;
	m_wAlterMsgId				= 0;

	ResetIncomingMessage();
}

CTCPIPServerClientFDXGT::~CTCPIPServerClientFDXGT(void)
{
	CNetworkST::Destroy(2000);

	if (m_pMsgTCPIP != NULL)
		::GlobalFree((HGLOBAL)m_pMsgTCPIP);
	m_pMsgTCPIP = NULL;

	::DeleteCriticalSection(&m_cs_Wait);
}

DWORD CTCPIPServerClientFDXGT::Destroy(DWORD dwMilliseconds)
{
	DWORD	dwRetValue;
	//
	//
	//
	dwRetValue = CNetworkST::Destroy(dwMilliseconds);
	VERIFY(dwRetValue == NETWORKST_OK);

	m_FlatQueueRECEIVEDSTREAM.Destroy();
	m_FlatQueueOut.Destroy();

	return dwRetValue;
}

DWORD CTCPIPServerClientFDXGT::Create(USHORT nSocketType,LPCTSTR lpszIPAddress,int nPortNumber,DWORD dwBufferSize,DWORD dwPollingTime,UINT nRcvTimeout,CSharedFlatQueueGT *pFlatQueueIn,DWORD dwMemoryFlatQueueOut,DWORD dwKeepAlive,_TCHAR* pszChannel)
{
	DWORD	dwRetValue;
	BOOL	bRunAsMaster = FALSE; // Read And Than Write



	//dwRetValue = CNetworkST::CreateSocket(nSocketType,lpszIPAddress,nPortNumber,dwBufferSize,dwPollingTime,nRcvTimeout,bRunAsMaster,TRUE,0,(UINT)-1);
	dwRetValue = CNetworkST::CreateSocket(nSocketType,lpszIPAddress,nPortNumber,dwBufferSize,dwPollingTime,nRcvTimeout,bRunAsMaster,TRUE,-1,-1);
	if(dwRetValue != NETWORKST_OK)
		return dwRetValue;
	//
	m_pFlatQueueIn	= pFlatQueueIn;
	m_dwStreamBufferSize = dwBufferSize;
	//
	dwRetValue = m_FlatQueueRECEIVEDSTREAM.Create((DWORD) (dwBufferSize * 30),NULL,0,NULL,NULL);
	if(dwRetValue != QUEUEGT_OK)
		return dwRetValue;
	//
	dwRetValue = m_FlatQueueOut.Create(dwMemoryFlatQueueOut,NULL,0,NULL,NULL);
	if(dwRetValue != QUEUEGT_OK)
		return dwRetValue;

	//
	// Allocate memory
	//
	m_pMsgTCPIP = NULL;
	m_pMsgTCPIP = (LPBYTE)::GlobalAlloc(GPTR, (SIZE_T) dwBufferSize);
	//
	if(m_pMsgTCPIP == NULL)
		return FALSE;
	//
	m_pMsgENQUEUED = NULL;
	m_pMsgENQUEUED = (LPBYTE)::GlobalAlloc(GPTR, (SIZE_T) dwBufferSize);
	if(m_pMsgENQUEUED == NULL)
		return FALSE;

	m_dwKeepAlive	= dwKeepAlive;

	m_sChannel.Format(_T("%s"),pszChannel);
	
	return (DWORD) NETWORKST_OK;
}

DWORD CTCPIPServerClientFDXGT::Create(USHORT nSocketType, LPVOID lpIPAddress, DWORD dwIPAddressCount, DWORD dwBufferSize, DWORD dwPollingTime, UINT nRcvTimeout, CSharedFlatQueueGT *pFlatQueueIn, DWORD dwMemoryFlatQueueOut, DWORD dwKeepAlive, _TCHAR* pszChannel)
{
	DWORD	dwRetValue;
	BOOL	bRunAsMaster = FALSE; // Read And Than Write


	//dwRetValue = CNetworkST::CreateSocket(nSocketType,lpszIPAddress,nPortNumber,dwBufferSize,dwPollingTime,nRcvTimeout,bRunAsMaster,TRUE,0,(UINT)-1);
	dwRetValue = CNetworkST::CreateSocket(nSocketType, lpIPAddress, dwIPAddressCount, dwBufferSize, dwPollingTime, nRcvTimeout, bRunAsMaster, TRUE, -1, -1);
	if (dwRetValue != NETWORKST_OK)
		return dwRetValue;
	//
	m_pFlatQueueIn = pFlatQueueIn;
	m_dwStreamBufferSize = dwBufferSize;
	//
	dwRetValue = m_FlatQueueRECEIVEDSTREAM.Create((DWORD)(dwBufferSize * 30), NULL, 0, NULL, NULL);
	if (dwRetValue != QUEUEGT_OK)
		return dwRetValue;
	//
	dwRetValue = m_FlatQueueOut.Create(dwMemoryFlatQueueOut, NULL, 0, NULL, NULL);
	if (dwRetValue != QUEUEGT_OK)
		return dwRetValue;

	//
	// Allocate memory
	//
	m_pMsgTCPIP = NULL;
	m_pMsgTCPIP = (LPBYTE)::GlobalAlloc(GPTR, (SIZE_T)dwBufferSize);
	//
	if (m_pMsgTCPIP == NULL)
		return FALSE;
	//
	m_pMsgENQUEUED = NULL;
	m_pMsgENQUEUED = (LPBYTE)::GlobalAlloc(GPTR, (SIZE_T)dwBufferSize);
	if (m_pMsgENQUEUED == NULL)
		return FALSE;

	m_dwKeepAlive = dwKeepAlive;

	m_sChannel.Format(_T("%s"), pszChannel);

	return (DWORD)NETWORKST_OK;
}

void CTCPIPServerClientFDXGT::SetProperties(BYTE bySourceNode,BYTE byDestNode)
{
	m_bySourceNode	= bySourceNode;
	m_byDestNode	= byDestNode;
}

void CTCPIPServerClientFDXGT::ExcludeProtocolControls(BOOL bExcludeNODEControl,BOOL bExcludeMSGPRGControl,BOOL bExcludeMSGPacking,BOOL bSendAlwaysKeepAlive)
{
	m_bExcludeNODEControl	= bExcludeNODEControl;
	m_bExcludeMSGPRGControl	= bExcludeMSGPRGControl;
	m_bExcludeMSGPacking	= bExcludeMSGPacking;
	m_bSendAlwaysKeepAlive  = bSendAlwaysKeepAlive;
}

void CTCPIPServerClientFDXGT::DoNotSendSTDKeepAlive(WORD wKeepAliveMsgID)
{
	m_bDoNotSendSTDKeepAlive	= TRUE;
	m_wKeepAliveMsgID			= wKeepAliveMsgID;
}

void CTCPIPServerClientFDXGT::SendTelegramAtFixedLength(DWORD dwTelegramLength)
{
	m_bSendTelegramAtFixedLength = TRUE;
	m_dwFixedTelegramLength		 = dwTelegramLength;
}

void CTCPIPServerClientFDXGT::LimitTelegramLength(DWORD dwTelegramLength)
{
	m_bLimitTelegramLength	  = TRUE;
	m_dwLimitedTelegramLength = dwTelegramLength;
}

void CTCPIPServerClientFDXGT::AlterMsgIdByAddingInterfaceId(WORD wAlterMsgId)
{
	m_bAlterMsgId = TRUE;
	m_wAlterMsgId = wAlterMsgId;
}

void CTCPIPServerClientFDXGT::SetPostMessage(HWND hwndID,UINT uMsgID, WORD wNetworkId)
{
	m_hwndID = hwndID;
	m_uMsgID = uMsgID;
	m_wNetworkId = wNetworkId;
}

BOOL CTCPIPServerClientFDXGT::IsRunning(void)
{
	return (BOOL) (m_byConnectionState == NETWORKST_CONNSTATE_RUN ? TRUE : FALSE);
}

void CTCPIPServerClientFDXGT::GetRGBConnectionState(COLORREF* pcrColor)
{
	*pcrColor = m_crColor;
}

void CTCPIPServerClientFDXGT::OnChangeConnectionState(WPARAM wSocketType, LPARAM lNewState)
{
	m_bJustConnected = FALSE;

	m_t55msKeepAliveTimer.Stop();

	m_byConnectionState = (BYTE) lNewState;

	switch (lNewState)
	{
		case NETWORKST_CONNSTATE_STOP:
			if(wSocketType == NETWORKST_SOCKET_SERVER)
				m_crColor = RGB(255,0,0); // Red
			else
				m_crColor = RGB(0,0,255); // Blue
			break;
		case NETWORKST_CONNSTATE_CONNECTING:
			m_crColor = RGB(255,255,0); // Yellow
			break;
		case NETWORKST_CONNSTATE_ACCEPTING:
			m_crColor = RGB(255,255,0); // Yellow
			break;
		case NETWORKST_CONNSTATE_RUN:
			m_crColor = RGB(0,255,0); // Green

			ResetIncomingMessage();

			m_FlatQueueRECEIVEDSTREAM.Empty();

			m_wSentMsgID				= 0;
			m_wReceMsgID				= 0;
			m_bySourceEnvPrgNumber		= 0;
			m_byDestinationEnvPrgNumber	= 0;

			m_t55msKeepAliveTimer.Start();

			m_bJustConnected = TRUE;

			break;
		default:
			m_crColor = RGB(255,255,255); // White
			break;
	}
	//
	// Notify Message If Required
	//
	if(m_hwndID != NULL)
		::PostMessage(m_hwndID,m_uMsgID,(WPARAM)lNewState,(LPARAM)m_wNetworkId);
}

DWORD CTCPIPServerClientFDXGT::OnPrepareMsgToSend(LPBYTE pBuffer,DWORD dwBufferLen)
{
//	::GetLocalTime(&m_csSystemTime);
//	m_sString.Format(_T("%02d:%02d:%02d-%03d -->"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
//	TRACE(_T("%s OnPrepareMsgToSend \n"),m_sString);

	
	if(m_bSendAlwaysKeepAlive == FALSE)
	{
		if(m_FlatQueueOut.GetCount() == 0 && m_t55msKeepAliveTimer.IsElapsed(m_dwKeepAlive) == FALSE && m_bJustConnected == FALSE)
			return 0;
		m_bJustConnected = FALSE;
		if(m_bDoNotSendSTDKeepAlive == TRUE && m_FlatQueueOut.GetCount() == 0)
		{
			NotifyKeepAliveMsg();
			m_t55msKeepAliveTimer.Start();
			return 0;
		}
	}
	else
	{
		if(m_t55msKeepAliveTimer.IsElapsed(m_dwKeepAlive) == TRUE || m_bJustConnected == TRUE)
		{
			m_bJustConnected = FALSE;
			
			m_t55msKeepAliveTimer.Start();

			if(m_bDoNotSendSTDKeepAlive == TRUE)
			{
				NotifyKeepAliveMsg();
				return 0;
			}
			//
			//Make A Keep ALive Message
			//
			PrepareENVELOPE(pBuffer);
			PrepareTAIL();
			//
			if(m_bSendTelegramAtFixedLength == TRUE)
				return m_dwFixedTelegramLength;
			return m_dwTotalMsgSize;
		}
		//
		if(m_FlatQueueOut.GetCount() == 0)
			return 0;
	}
	//
	PrepareENVELOPE(pBuffer);
	PrepareDATA(dwBufferLen);
	PrepareTAIL();
	//
	if(m_bSendAlwaysKeepAlive == FALSE)
		m_t55msKeepAliveTimer.Start();
	//
	//TRACE(_T("CTCPIPServerClientFDXGT (%s) - Size Sent:(%d)\n"),m_sChannel,m_dwTotalMsgSize);
	//
	if(m_bSendTelegramAtFixedLength == TRUE)
		return m_dwFixedTelegramLength;
	//
	if(false)
	{
		if(m_sChannel.CompareNoCase(_T("GEDEPHY")) == 0)
		{
			::GetLocalTime(&m_csSystemTime);
			char szDateTime[100];
			sprintf_s(szDateTime,sizeof(szDateTime),"%02d/%02d/%02d %02d:%02d:%02d - ",m_csSystemTime.wDay,m_csSystemTime.wMonth,m_csSystemTime.wYear,m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond);
			//sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
			m_sString.Format(_T("%s_%02d.%02d.%02d_%02d.TXT"),_T("..\\TraceFiles\\OnPrepareMsgToSend"),m_csSystemTime.wDay,m_csSystemTime.wMonth,m_csSystemTime.wYear,m_csSystemTime.wHour);
			//if(m_TraceFile.Open(m_sString,CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
			if(m_TraceFile.Open(m_sString,CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
			{
				m_TraceFile.SeekToEnd();
				m_TraceFile.Write(szDateTime,strlen(szDateTime));
				m_TraceFile.Write(pBuffer,m_dwTotalMsgSize);
				m_TraceFile.Write("\r\n",2);
				m_TraceFile.Close();
			}
		}
	}

	return m_dwTotalMsgSize;
}

BOOL CTCPIPServerClientFDXGT::OnSendCompleted(BOOL bSendOk, int nErrorCode)
{
	if(bSendOk == FALSE)
		return TRUE;
	//
	//TRACE(_T("CTCPIPServerClientFDXGT (%s) - OnSendCompleted returns %d Error %d \n"),m_sChannel,bSendOk,nErrorCode);
	nErrorCode=0; // Just For Warning

	return FALSE;
}

void CTCPIPServerClientFDXGT::OnReceive(LPBYTE pData, DWORD dwDataLen)
{
	STRUCT_TCPIPENVELOPE*	pcsSTRUCT_TCPIPENVELOPE;
	STRUCT_TCPIPMSG*		pcsSTRUCT_TCPIPMSG;
	//DWORD					dwBytesCopied;
	LPBYTE					pMsgStream;
	WORD					wMsgSize;
	//
	if(false)
	{
		if(m_sChannel.CompareNoCase(_T("WBSINCOMING")) == 0)
		{
			SYSTEMTIME	csSystemTime;
			::GetLocalTime(&csSystemTime);
			CString sString;
			char szTmp[100];
			sprintf_s(szTmp,sizeof(szTmp),"%02d/%02d/%02d %02d:%02d:%02d - ",csSystemTime.wDay,csSystemTime.wMonth,csSystemTime.wYear,csSystemTime.wHour,csSystemTime.wMinute,csSystemTime.wSecond);
			//sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
			sString.Format(_T("%s%s_%02d.%02d.%04d_%02d.TXT"),_T("..\\..\\"),_T("OnReceiveWBSINCOMING"),csSystemTime.wDay,csSystemTime.wMonth,csSystemTime.wYear,csSystemTime.wHour);
			CFile	TraceFile;
			if(TraceFile.Open(sString,CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
			{
				TraceFile.SeekToEnd();
				TraceFile.Write(szTmp,strlen(szTmp));
				sString.Empty();
				for(DWORD dwIx=0;dwIx<dwDataLen;dwIx++)
				{
					sprintf_s(szTmp,sizeof(szTmp),"%02X ",pData[dwIx]);
					TraceFile.Write(szTmp,strlen(szTmp));
				}
				TraceFile.Write("\r\n",2);
				TraceFile.Close();
			}
		}
	}
	/*
	TRACE(_T("Rece: "));
	for(DWORD dwAA=0;dwAA<dwDataLen;dwAA++)
		TRACE(_T("%02X"),pData[dwAA]);
	TRACE(_T("\n"));
	*/	
	//
	CheckIncomingMessages(pData,dwDataLen);
	//
	//TRACE(_T("CTCPIPServerClientFDXGT (%s) - GetQueueCount %d \n"),m_sChannel,m_QueueCML.GetCount());
	//
	//
	while(m_FlatQueueRECEIVEDSTREAM.GetCount())
	{
		if(m_FlatQueueRECEIVEDSTREAM.GetItem((LPBYTE)m_pMsgENQUEUED,TRUE,NULL) != QUEUEGT_OK)
			VERIFY(FALSE);

		pMsgStream				= m_pMsgENQUEUED;
		pcsSTRUCT_TCPIPENVELOPE	= (STRUCT_TCPIPENVELOPE*)pMsgStream;
		pMsgStream				+= sizeof(STRUCT_TCPIPENVELOPE);
		//
		// Verify Destination Node
		//
		if(m_bExcludeNODEControl == FALSE)
		{
			if(m_byDestNode != pcsSTRUCT_TCPIPENVELOPE->bySourceNode)
			{
				TRACE(_T("CTCPIPServerClientFDXGT (%s) - Wrong SourceNode: Received %d Expected %d\n"),m_sChannel,pcsSTRUCT_TCPIPENVELOPE->bySourceNode,m_byDestNode);
				continue;
			}
			//
			if(m_bySourceNode != pcsSTRUCT_TCPIPENVELOPE->byDestNode)
			{
				TRACE(_T("CTCPIPServerClientFDXGT (%s) - Wrong DestNode: Received %d Expected %d\n"),m_sChannel,pcsSTRUCT_TCPIPENVELOPE->byDestNode,m_bySourceNode);
				continue;
			}
		}
		//
		// Verify Envelope Progressive Number
		//
		if(m_bExcludeMSGPRGControl == FALSE)
		{
			if(pcsSTRUCT_TCPIPENVELOPE->byProg != 0 && pcsSTRUCT_TCPIPENVELOPE->byProg == m_byDestinationEnvPrgNumber)
			{
				TRACE(_T("CTCPIPServerClientFDXGT (%s) - MyProg == DestProg %d %d"),m_sChannel,pcsSTRUCT_TCPIPENVELOPE->byProg,m_byDestinationEnvPrgNumber);
				continue;
			}
		}
		m_byDestinationEnvPrgNumber = pcsSTRUCT_TCPIPENVELOPE->byProg;
		//
		//
//		if(pEnvelope->wNumMsg == 0)
//			TRACE(_T("CTCPIPServerClientFDXGT (%s) - KEEP-ALIVE Has Been Received"),m_sChannel);
		//
		// Check All Mesasges Before Processing Them
		//
		if(CheckAllMessages() == FALSE)
		{
			TRACE(_T("CTCPIPServerClientFDXGT (%s) - Wrong Message Packet Received \n"),m_sChannel);
			//VERIFY(FALSE);
			// if(m_sChannel.CompareNoCase(_T("DAC")) == 0)
			/*
			CString	sFileName;
			CFile	TraceFile;
			sFileName = _T("D:\\CheckAllMessages.txt");
			SYSTEMTIME	csSystemTime;
			::GetLocalTime(&csSystemTime);
			char szBuffer[100];
			if(TraceFile.Open(sFileName,CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == FALSE)
			{
				//TraceFile.SeekToEnd();
				sprintf_s(szBuffer,sizeof(szBuffer),"%02d/%02d/%02d %02d:%02d:%02d - ",csSystemTime.wDay,csSystemTime.wMonth,csSystemTime.wYear,csSystemTime.wHour,csSystemTime.wMinute,csSystemTime.wSecond);
				TraceFile.Write(szBuffer,strlen(szBuffer));
				::WideCharToMultiByte(CP_ACP,NULL,m_sChannel,-1,szBuffer,sizeof(szBuffer)-1,NULL,NULL);
				TraceFile.Write(szBuffer,strlen(szBuffer));
				TraceFile.Write(" :",2);
				TraceFile.Write(pData,dwDataLen);
				TraceFile.Write("\r\n",2);
				TraceFile.Close();
			}
			*/
			return;
		}
		//
		// UnPack Messages ... If Required
		//
		while(pcsSTRUCT_TCPIPENVELOPE->wNumberOfMESSAGES--)
		{
			pcsSTRUCT_TCPIPMSG = (STRUCT_TCPIPMSG*) pMsgStream;
			//
			// Get Message Size
			//
			wMsgSize = (WORD) sizeof(STRUCT_TCPIPMSGHEADER) + pcsSTRUCT_TCPIPMSG->csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH;
			if(m_bAlterMsgId == TRUE)
				pcsSTRUCT_TCPIPMSG->csSTRUCT_TCPIPMSGHEADER.wMsgID += m_wAlterMsgId;
			if(m_pFlatQueueIn != NULL)
				m_pFlatQueueIn->PutItem((LPBYTE) pcsSTRUCT_TCPIPMSG,(DWORD) (sizeof(STRUCT_TCPIPMSGHEADER) + pcsSTRUCT_TCPIPMSG->csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));

			pMsgStream += wMsgSize;

			m_wReceMsgID = pcsSTRUCT_TCPIPMSG->csSTRUCT_TCPIPMSGHEADER.wMsgID;
		}
	}
}

BOOL CTCPIPServerClientFDXGT::OnTimeout(void)
{
	TRACE(_T("CTCPIPServerClientFDXGT (%s) - OnTimeout return TRUE\n"),m_sChannel);
	return TRUE; // If TRUE Than Close Socket
}
//
WORD CTCPIPServerClientFDXGT::GetSentMsgID(void)
{
	return m_wSentMsgID;
}
//
BYTE CTCPIPServerClientFDXGT::GetSentPrgID(void)
{
	return m_bySourceEnvPrgNumber;
}
//
WORD CTCPIPServerClientFDXGT::GetReceMsgID(void)
{
	return m_wReceMsgID;
}
//
BYTE CTCPIPServerClientFDXGT::GetRecePrgID(void)
{
	return m_byDestinationEnvPrgNumber;
}
//
void CTCPIPServerClientFDXGT::ClearSentReceMsgIDs(void)
{
	m_wSentMsgID=0;
	m_wReceMsgID=0;
}
