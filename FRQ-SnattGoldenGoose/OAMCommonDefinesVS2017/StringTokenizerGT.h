#pragma once
class CStringTokenizerGT
{
public:
	CStringTokenizerGT(void);
	~CStringTokenizerGT(void);
	BOOL Tokenizer(CString sString,CString sDelimiter);
	int	 GetCountTokens(void);
	CString	GetTokenAt(int nElement);
	CString	GetNextToken(void);

private:
	CStringArray	sTokens;
	int				m_nCount;
	int				m_nIndwex;
};
