//////////////////////////////////////////////////////////////////
//																//
// NAME:					CZEBRASocketGT.h					//
// LAST MODIFICATION DATE:	24/08/2011							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Source								//
//////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZEBRASocketGT.h"


CZEBRASocketGT::CZEBRASocketGT(void)
{
	m_byConnectionState	= 0;
	m_crColor			= RGB(255,255,255); // White
	m_sChannel.Format(_T("No Channel"));
}

CZEBRASocketGT::~CZEBRASocketGT(void)
{
	CNetworkST::Destroy(2000);
}

DWORD CZEBRASocketGT::Destroy(DWORD dwMilliseconds)
{
	DWORD	dwRetValue;
	//
	//
	//
	dwRetValue = CNetworkST::Destroy(dwMilliseconds);
	VERIFY(dwRetValue == NETWORKST_OK);

	m_FlatQueueOut.Destroy();

	return dwRetValue;
}

DWORD CZEBRASocketGT::Create(USHORT nSocketType,LPCTSTR lpszIPAddress,int nPortNumber,DWORD dwBufferSize,DWORD dwPollingTime,UINT nRcvTimeout,CSharedFlatQueueGT *pFlatQueueIn,DWORD dwMemoryFlatQueueOut,wchar_t* pwszChannel)
{
	DWORD	dwRetValue;
	BOOL	bRunAsMaster = FALSE; // Read And Than Write
	//
	// dwRetValue = CNetworkST::CreateSocket(nSocketType,lpszIPAddress,nPortNumber,dwBufferSize,dwPollingTime,nRcvTimeout,bRunAsMaster,TRUE,0,(UINT)-1);
	dwRetValue = CNetworkST::CreateSocket(nSocketType,lpszIPAddress,nPortNumber,dwBufferSize,dwPollingTime,nRcvTimeout,bRunAsMaster,TRUE,-1,-1);
	if(dwRetValue != NETWORKST_OK)
		return dwRetValue;
	//
	m_pFlatQueueIn	= pFlatQueueIn;
	//
	dwRetValue = m_FlatQueueOut.Create(dwMemoryFlatQueueOut,NULL,0,NULL,NULL);
	if(dwRetValue != QUEUEGT_OK)
		return dwRetValue;
	//
	m_sChannel.Format(_T("%s"),pwszChannel);

	return (DWORD) NETWORKST_OK;
}

void CZEBRASocketGT::SetParameters(int nPrnID,WORD wDiagnosticMsgID,UINT uPollingTime)
{
	m_nPrnID			= nPrnID;
	m_wDiagnosticMsgID	= wDiagnosticMsgID;
	m_uPollingTime		= uPollingTime;
}

BOOL CZEBRASocketGT::IsRunning()
{
	return (BOOL) (m_byConnectionState == NETWORKST_CONNSTATE_RUN ? TRUE : FALSE);
}

void CZEBRASocketGT::GetRGBConnectionState(COLORREF* pcrColor)
{
	*pcrColor = m_crColor;
}

void CZEBRASocketGT::OnChangeConnectionState(WPARAM wSocketType, LPARAM lNewState)
{
	m_t55msPollingTime.Stop();
	m_t55msTimeOutRX.Stop();
	m_t55msNotifyDiagnostic.Stop();
	//
	m_byConnectionState = (BYTE) lNewState;
	//
	switch (lNewState)
	{
		case NETWORKST_CONNSTATE_STOP:
			if(wSocketType == NETWORKST_SOCKET_SERVER)
				m_crColor = RGB(255,0,0); // Red
			else
				m_crColor = RGB(0,0,255); // Blue
			break;
		case NETWORKST_CONNSTATE_CONNECTING:
			m_crColor = RGB(255,255,0); // Yellow
			break;
		case NETWORKST_CONNSTATE_ACCEPTING:
			m_crColor = RGB(255,255,0); // Yellow
			break;
		case NETWORKST_CONNSTATE_RUN:
			//
			m_crColor = RGB(0,255,0); // Green
			//
			ResetData();
			//
			m_t55msPollingTime.Start();
			m_t55msNotifyDiagnostic.Start();
			//
			break;
		default:
			m_crColor = RGB(255,255,255); // White
			break;
	}
}

DWORD CZEBRASocketGT::OnPrepareMsgToSend(LPBYTE pBuffer,DWORD dwBufferLen)
{
	if(m_t55msPollingTime.IsElapsed(m_uPollingTime) == FALSE)
		return 0;
	m_t55msPollingTime.Start();

	ManageTimeOut();

	NotifyDiagnostic();

	if(m_bSendMsg == FALSE)
		return 0;
	m_bSendMsg = FALSE;

	LPBYTE	pOriginOfBuffer = pBuffer;
	DWORD	dwMsgSize = 0;

	if(m_FlatQueueOut.GetCount() && m_bSendLabel == TRUE && m_csZEBRADIAG.bIsAvailable == TRUE)
	{
		m_bSendLabel = FALSE;

		dwMsgSize = m_FlatQueueOut.GetFirstItemSize();
		if(dwMsgSize <= dwBufferLen)
		{
			m_csZEBRADIAG.wLABELMsgSent++;
			m_FlatQueueOut.GetItem((LPBYTE)&m_csZEBRAMESSAGE,TRUE,NULL);
			::CopyMemory(pOriginOfBuffer,&m_csZEBRAMESSAGE.szMsgData,m_csZEBRAMESSAGE.nMsgLen);
			dwMsgSize = (DWORD) m_csZEBRAMESSAGE.nMsgLen;
		//	NotifyDiagnostic(TRUE);
			TRACE(_T("CZEBRASocketGT %s - Send LABEL Having Size %d \n"),m_sChannel,dwMsgSize);
		}
		else
		{
			dwMsgSize = 0;
			m_FlatQueueOut.Empty();
			TRACE(_T("CZEBRASocketGT %s - Wrong Size %d While Sending Message\n"),m_sChannel,dwMsgSize);
		}
	}
	else
	{
		sprintf_s((char*)pOriginOfBuffer,dwBufferLen,"%c%c%c%c%c%c%c",0x02,'~','H','S',0x03,0x13,0x10);
		dwMsgSize = 7;
		m_csZEBRADIAG.wDIAGMsgSent++;
		PrepareRX();
	}

//	TRACE(_T("CZEBRASocketGT %s - Send: "),m_sChannel);
//	for(DWORD dwAA=0;dwAA<dwMsgSize;dwAA++)
//		TRACE(_T("%02X"),pBuffer[dwAA]);
//	TRACE(_T("\n"));

	return dwMsgSize;
}

BOOL CZEBRASocketGT::OnSendCompleted(BOOL bSendOk, int nErrorCode)
{
	if(m_bSendLabel == FALSE)
	{
		m_bSendMsg	 = TRUE;
		m_bSendLabel = TRUE;
		m_csZEBRADIAG.bIsAvailable = FALSE;
	}

	if(bSendOk == FALSE)
		return TRUE;
	//
//	TRACE(_T("CZEBRASocketGT %s - OnSendCompleted return %d Error %d \n"),m_sChannel,bSendOk,nErrorCode);
	nErrorCode=0; // Just For Warning

	return FALSE;
}

void CZEBRASocketGT::OnReceive(LPBYTE pData, DWORD dwDataLen)
{
	if(m_bWaitAnswer == FALSE)
		return;

	char*	pcsZEBRADIAG = (char*) &m_csZEBRADIAG.csHSR;
	int		nMsgLen = sizeof(STRUCT_ZEBRA_DIAG_HSR);

/*
	if(dwDataLen != nMsgLen)
	{
		m_csZEBRADIAG.bIsAvailable = TRUE;
		return;
	}
*/
	for(DWORD dwIx=0;dwIx<dwDataLen;dwIx++)
	{
		if(m_nNumBytesRX == 0 && pData[dwIx] != 0x02)
			continue;
		pcsZEBRADIAG[m_nNumBytesRX] = (char) pData[dwIx];
		if(++m_nNumBytesRX == nMsgLen)
		{
			m_t55msTimeOutRX.Stop();
			m_bSendMsg	  = TRUE;
			m_bWaitAnswer = FALSE;
			if(m_csZEBRADIAG.csHSR.csDIAGSTR1.bySTX == 0x02 && m_csZEBRADIAG.csHSR.csDIAGSTR2.bySTX == 0x02 && m_csZEBRADIAG.csHSR.csDIAGSTR3.bySTX == 0x02)
			{
				if(m_csZEBRADIAG.csHSR.csDIAGSTR1.cPaperOut == '0' &&  m_csZEBRADIAG.csHSR.csDIAGSTR1.cPause == '0' && m_csZEBRADIAG.csHSR.csDIAGSTR1.cCorruptRAM == '0')
					m_csZEBRADIAG.bIsAvailable = TRUE;
				else
					m_csZEBRADIAG.bIsAvailable = FALSE;
			}
			else
				m_csZEBRADIAG.bIsAvailable = FALSE;
			m_csZEBRADIAG.wDIAGMsgReceived++;
			m_nNumBytesRX = 0;
			break;
		}
	}


//	TRACE(_T("CZEBRASocketGT %s - Rece: "),m_sChannel);
//	for(DWORD dwAA=0;dwAA<dwDataLen;dwAA++)
//		TRACE(_T("%02X"),pData[dwAA]);
//	TRACE(_T("\n"));

}

BOOL CZEBRASocketGT::OnTimeout()
{
//	TRACE(_T("CZEBRASocketGT %s - On TimeOut \n"),m_sChannel);
	return TRUE; // If TRUE Than Close Socket
}

