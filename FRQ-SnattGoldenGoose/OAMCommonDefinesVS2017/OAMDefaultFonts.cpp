#include "pch.h"
#include "OAMDefaultFonts.h"


/*
Hai a disposizione diversi tipi di carattere, raggruppati per famiglie:

    * Times New Roman, Times, Georgia, Serif
    * Courier New, Courier, Mono
    * Verdana, Arial, Helvetica, Geneva, Sans-serif

Ricorda che dagli studi di usabilita fatti, la leggibilita dei caratteri sul monitor e diversa dalla leggibilita sulla carta.
Mentre sulla carta i caratteri piu leggibili sono quelli con le grazie (Serif),
sul monitor la lettura e piu agevolata dai caratteri Sans serif.
In modo particolare i caratteri in assoluto piu leggibili sono il Verdana e il Trebuchet.
*/


COAMDefaultFonts::COAMDefaultFonts(void)
{
	//
	// Create FONT
	//
	VERIFY(m_fntXXXS.CreateFont(
	   12,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,				// nOutPrecision
	   CLIP_TT_ALWAYS,				// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntXXS.CreateFont(
	   14,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,				// nOutPrecision
	   CLIP_TT_ALWAYS,				// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntXS.CreateFont(
	   18,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,				// nOutPrecision
	   CLIP_TT_ALWAYS,				// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntS.CreateFont(
	   22,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,				// nOutPrecision
	   CLIP_TT_ALWAYS,				// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntM.CreateFont(
	   26,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntL.CreateFont(
	   30,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Arial")));				// lpszFacename
	   //_T("Verdana")));				// lpszFacename

	VERIFY(m_fntXL.CreateFont(
	   34,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntXXL.CreateFont(
	   38,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntXXXL.CreateFont(
	   42,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntEditL.CreateFont(
	   46,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntEditXL.CreateFont(
	   50,							// nHeight
	   16,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename


	VERIFY(m_fntContainer.CreateFont(
	   70,							// nHeight
	   11,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename


	VERIFY(m_fntBtnOK.CreateFont(
	   180,							// nHeight
	   50,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_EXTRALIGHT,				// nWeight
	   //FW_HEAVY,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,			// nOutPrecision
	   CLIP_TT_ALWAYS,			// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename


	VERIFY(m_fntBatchList.CreateFont(
	   16,							// nHeight
	   0,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,				// nOutPrecision
	   CLIP_TT_ALWAYS,				// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename

	VERIFY(m_fntAlarmOrWarningText.CreateFont(
	   35,							// nHeight
	   6,							// nWidth
	   0,							// nEscapement
	   0,							// nOrientation
	   FW_NORMAL,					// nWeight
	   FALSE,						// bItalic
	   FALSE,						// bUnderline
	   0,							// cStrikeOut
	   ANSI_CHARSET,				// nCharSet
	   OUT_TT_PRECIS,				// nOutPrecision
	   CLIP_TT_ALWAYS,				// nClipPrecision
	   DEFAULT_QUALITY,				// nQuality
	   DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
	   _T("Verdana")));				// lpszFacename


}

COAMDefaultFonts::~COAMDefaultFonts(void)
{
	m_fntXXXS.DeleteObject();
	m_fntXXS.DeleteObject();
	m_fntXS.DeleteObject();
	m_fntS.DeleteObject();
	m_fntM.DeleteObject();
	m_fntL.DeleteObject();
	m_fntXL.DeleteObject();
	m_fntXXL.DeleteObject();
	m_fntContainer.DeleteObject();
	m_fntBtnOK.DeleteObject();
	m_fntBatchList.DeleteObject();
	m_fntAlarmOrWarningText.DeleteObject();
}

class CFont* COAMDefaultFonts::GetXXXS(void)
{
	return (class CFont*) &m_fntXXXS;
}

class CFont* COAMDefaultFonts::GetXXS(void)
{
	return (class CFont*) &m_fntXXS;
}

class CFont* COAMDefaultFonts::GetXS(void)
{
	return (class CFont*) &m_fntXS;
}

class CFont* COAMDefaultFonts::GetS(void)
{
	return (class CFont*) &m_fntS;
}

class CFont* COAMDefaultFonts::GetM(void)
{
	return (class CFont*) &m_fntM;
}

class CFont* COAMDefaultFonts::GetL(void)
{
	return (class CFont*) &m_fntL;
}

class CFont* COAMDefaultFonts::GetXL(void)
{
	return (class CFont*) &m_fntXL;
}

class CFont* COAMDefaultFonts::GetXXL(void)
{
	return (class CFont*) &m_fntXXL;
}

class CFont* COAMDefaultFonts::GetXXXL(void)
{
	return (class CFont*) &m_fntXXXL;
}

class CFont* COAMDefaultFonts::GetEditL(void)
{
	return (class CFont*) &m_fntEditL;
}

class CFont* COAMDefaultFonts::GetEditXL(void)
{
	return (class CFont*) &m_fntEditXL;
}

class CFont* COAMDefaultFonts::GetContainer(void)
{
	return (class CFont*) &m_fntContainer;
}

class CFont* COAMDefaultFonts::GetBtnOK(void)
{
	return (class CFont*) &m_fntBtnOK;
}

class CFont* COAMDefaultFonts::GetBatchList(void)
{
	return (class CFont*) &m_fntBatchList;
}

class CFont* COAMDefaultFonts::GetAlarmOrWarningText(void)
{
	return (class CFont*) &m_fntAlarmOrWarningText;
}


