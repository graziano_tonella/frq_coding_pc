//////////////////////////////////////////////////////////////////
// NAME:					CToolsLibraryGT.h					//
// LAST MODIFICATION DATE:	18-Jul-2018							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Include								//
//////////////////////////////////////////////////////////////////

#include <io.h>
#include <afxinet.h>

#pragma once
class CToolsLibraryGT
{
public:
	CToolsLibraryGT(void);
	~CToolsLibraryGT(void);

	BOOL	CheckDirectory(LPCTSTR lpPathName);
	int		DeleteFiles(LPCTSTR lpszFilePath, LPCTSTR lpszFileName, int nDays, int nHours, int nMinutes);
	int		DeleteAllFiles(LPCTSTR lpszFilePath, LPCTSTR lpszFileName);
	BOOL	RenameFile(LPCTSTR pszOldName, LPCTSTR pszNewName);

	DWORD	GetNumberOfProcessors(void);

	BOOL	IsWow64(void);

	time_t	TimeFromSystemTime(const SYSTEMTIME* pTime);

	BOOL	GetFileFromSMPCViaFTPConnection(CString& rsFilePath,CString& rsFileName,int* pnFileLength);
};

