//
//	Class:		CNetworkST
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 5.0
//				Visual C++ 6.0
//
//	Version:	See GetVersionC() or GetVersionI()
//
//	Created:	03/August/1999
//	Updated:	14/October/2003
//
//	Author:		Davide Calabro'		davide_calabro@yahoo.com
//
#ifndef _NETWORKST_H_
#define _NETWORKST_H_

#pragma comment(lib, "ws2_32.lib")

// Uncomment following line if you are using this class outside the DLL
#define _NETWORKST_NODLL_

#ifndef _NETWORKST_NODLL_
#ifndef	_CMLHTDLL_NOLIB_
	#ifdef _DEBUG
		#ifdef _UNICODE
			#pragma comment(lib, "CmlHTud.lib")
		#else
			#pragma comment(lib, "CmlHTd.lib")
		#endif
		#else
		#ifdef _UNICODE
			#pragma comment(lib, "CmlHTu.lib")
		#else
			#pragma comment(lib, "CmlHT.lib")
		#endif
	#endif
#endif
#endif

#include <process.h>
#include <winsock2.h>

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// Thread steps
#define NETWORKST_CREATE			0
#define NETWORKST_BIND				1
#define NETWORKST_LISTEN			2
#define NETWORKST_ACCEPT			3
#define NETWORKST_WAITACCEPT		4
#define NETWORKST_CONNECT			5
#define NETWORKST_WAITCONNECT		6
#define NETWORKST_PRERUN			7
#define NETWORKST_RUN				8
#define NETWORKST_READ				9
#define NETWORKST_READOVERLAPPING	10
#define NETWORKST_READFINISHED		11
#define NETWORKST_WRITE				12
#define NETWORKST_WRITEOVERLAPPING	13
#define NETWORKST_WRITEFINISHED		14
#define NETWORKST_CLOSE				15

// Socket types
#define NETWORKST_SOCKET_SERVER		0
#define NETWORKST_SOCKET_CLIENT		1

// Connection states
#define NETWORKST_CONNSTATE_STOP		0
#define NETWORKST_CONNSTATE_CONNECTING	1
#define NETWORKST_CONNSTATE_ACCEPTING	2
#define NETWORKST_CONNSTATE_RUN			3

// Error codes
#define NETWORKST_OK					0
#define NETWORKST_NOINIT				1
#define NETWORKST_ALREADYINIT			2
#define NETWORKST_NOWINSOCK2			3
#define NETWORKST_ALREADYCREATED		4
#define NETWORKST_NOTCREATED			5
#define NETWORKST_THREADKO				6
#define NETWORKST_THREADPARENTKO		7
#define NETWORKST_THREADNETEVENTKO		8
#define NETWORKST_THREADMEMKO			9
#define NETWORKST_INVALIDIPADDR			10
#define NETWORKST_INVALIDSOCKTYPE		11
#define NETWORKST_INVALID_BUFFERSIZE	12
#define NETWORKST_MEMKO					13

typedef struct _STRUCT_NETWORKST_IPADDRESS
{
	TCHAR	szIPAddress[16];	// xxx.yyy.www.zzz + NULL terminator
	WORD	wPortNumber;
} STRUCT_NETWORKST_IPADDRESS;

#define SIZE_STRUCT_NETWORKST_IPADDRESS sizeof(_STRUCT_NETWORKST_IPADDRESS)

#ifndef _NETWORKST_NODLL_
	#ifdef	_CMLHTDLL_BUILDDLL_
		#define	NETWORKST_EXPORT	__declspec(dllexport)
	#else
		#define	NETWORKST_EXPORT	__declspec(dllimport)
	#endif
#else
		#define	NETWORKST_EXPORT
#endif

class NETWORKST_EXPORT	CNetworkST  
{
public:
	CNetworkST();
	virtual ~CNetworkST();

	DWORD CreateSocket(	unsigned short nSocketType, 
						LPVOID lpIPAddress,
						DWORD dwIPAddressCount,
						DWORD dwBufferSize, 
						DWORD dwPollingTime, 
						UINT uRcvTimeout, 
						BOOL bRunAsMaster, 
						BOOL bHardwareKA, 
						UINT uHardwareSendBufferSize, 
						UINT uHardwareRcvBufferSize);

	DWORD CreateSocket(	unsigned short nSocketType, 
						LPCTSTR lpszIPAddress, 
						int nPortNumber, 
						DWORD dwBufferSize, 
						DWORD dwPollingTime, 
						UINT uRcvTimeout, 
						BOOL bRunAsMaster, 
						BOOL bHardwareKA, 
						UINT uHardwareSendBufferSize, 
						UINT uHardwareRcvBufferSize);

	DWORD GetConnectedAddress(LPBYTE lpbyB1, LPBYTE lpbyB2, LPBYTE lpbyB3, LPBYTE lpbyB4, LPWORD lpwPortNumber);

	DWORD Destroy(DWORD dwMilliseconds);

	static short GetVersionI()		{return 20;}
	static LPCTSTR GetVersionC()	{return (LPCTSTR)_T("2.0");}

	BOOL	m_bNoLinger;

protected:
	virtual void OnError(DWORD dwErrorCode);
	virtual BOOL OnTimeout() = 0;
	virtual void OnReceive(LPBYTE pData, DWORD dwDataLen) = 0;
	virtual BOOL OnSendCompleted(BOOL bSendOk, int nErrorCode) = 0;
	virtual DWORD OnPrepareMsgToSend(LPBYTE pBuffer, DWORD dwBufferLen) = 0;
	virtual void OnChangeConnectionState(WPARAM wSocketType, LPARAM lNewState) = 0;

private:
	void SetConnectedAddress(BYTE byB1, BYTE byB2, BYTE byB3, BYTE byB4, WORD wPortNumber);

	BYTE				m_byConnectedIP[4];
	WORD				m_wConnectedPort;

	//CRITICAL_SECTION	m_csWait;

	typedef struct _STRUCT_NETWORKST_THRPARAM
	{
		void* pParent;					// Owner class
		unsigned short nSocketType;		// Socket type	
		LPVOID	lpIPAddress;			// Pointer to STRUCT_NETWORKST_IPADDRESS structures
		DWORD	dwIPAddressCount;		// Number of STRUCT_NETWORKST_IPADDRESS structures pointed by lpIPAddress
		DWORD dwBufferSize;				// Buffer size for incoming/outcoming data	
		UINT uRcvTimeout;				// Receive timeout-value
		DWORD dwPollingTime;			// Milliseconds between two writes
		BOOL bRunAsMaster;				// Indicates if socket is acting as Master or Slave
		BOOL bHardwareKA;				// Hardware Keep Alive
		UINT uHardwareSendBufferSize;	// Hardware send buffer size
		UINT uHardwareRcvBufferSize;	// Hardware receive buffer size
		//LPCRITICAL_SECTION pcsWait;	// Syncronization between class/thread	
		BOOL	bNoLinger;				// Bypass Linger option ?
		HANDLE hExitEvent;				// Exit event
		HANDLE hServiceEvent;			// Service event
		int nExitCode;					// Thread initialization error-code
		BOOL bExit;
		DWORD dwThreadId;				// Thread ID
		HANDLE hThreadHandle;			// Thread handle
	} STRUCT_NETWORKST_THRPARAM;

	STRUCT_NETWORKST_THRPARAM	m_csThread;

	static unsigned __stdcall thrNetwork(LPVOID pParam);
};

#endif
