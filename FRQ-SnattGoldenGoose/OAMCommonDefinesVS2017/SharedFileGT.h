//////////////////////////////////////////////////////////////////
//																//
// NAME:					CSharedFileGT.h						//
// LAST MODIFICATION DATE:	29/06/2010							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Shared File Handling				//
//////////////////////////////////////////////////////////////////
#pragma once


#include <io.h>
#include <fcntl.h>
#include <sys\types.h>
#include <sys\stat.h>
#include <share.h>
#include <errno.h>


class CSharedFileGT
{
public:
	CSharedFileGT();
	virtual ~CSharedFileGT();
	BOOL	OpenNew(LPCTSTR lpszFileName);
	BOOL	OpenToAppend(LPCTSTR lpszFileName);
	BOOL	OpenToReadAndWrite(LPCTSTR lpszFileName);
	BOOL	OpenToReadOnly(LPCTSTR lpszFileName);
	BOOL	Write(LPVOID lpBuffer,UINT uBufLen);
	BOOL	Read(LPVOID lpBuffer,UINT uBufLen);
	long	GetLength();
	BOOL	SeekToBegin();
	BOOL	SeekToEnd();
	BOOL	SeekFromBegin(long lOffset);
	BOOL	SeekFromCurrent(long lOffset);
	BOOL	SeekFromEnd(long lOffset);
	long	GetPosition();
	BOOL	IsEndOfFile();
	BOOL	Commit();
	BOOL	Close();
	BOOL	GetFileResult();
	void	StoreOpenERRNO(errno_t ErrNum);
protected:
	enum{_SHAREDFILEGT_INVALID_HANDLE=(int)-1};

	int		m_nFileHandle;
	BOOL	m_bSuccess;
	int		m_nerrno;
	_TCHAR	m_szErrno[1024];
public:
	inline int	GetErrorNumber();
};

inline int CSharedFileGT::GetErrorNumber()
{
	return m_nerrno;
}
