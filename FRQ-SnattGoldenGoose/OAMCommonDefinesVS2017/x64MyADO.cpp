// MyADO.cpp: implementation of the CMyADO class.
//
// Copyright 2003 Nathan Davies
//
//////////////////////////////////////////////////////////////////////
// Last Modification: 30 March 2016 by Graziano Tonella

#include "pch.h"

#include <stdio.h>
#include <string.h>
#include <wtypes.h>

#include "x64MyADO.h"


// http://www.w3schools.com/ado/ado_datatypes.asp

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//	Class Constructor
CMyADO::CMyADO()
{
	m_pCommandPtr = NULL;
}

//	Class Destructor
CMyADO::~CMyADO()
{
	if( m_pRecordsetPtr )
	{
		if( m_pRecordsetPtr->State == adStateOpen )
			m_pRecordsetPtr->Close();
		m_pRecordsetPtr = NULL;
	}
	m_pCommandPtr = NULL;
}

//	Create a Parameter and Add it to the CommandPtr Object (Which will be used to Execute the Stored Procedure)
HRESULT CMyADO::AddParameter( _bstr_t btParameterName, DataTypeEnum enDataType, ParameterDirectionEnum enParameterDirection, long lSize, _variant_t vtValue )
{
	HRESULT hReturn = S_FALSE;

	if( IsConnected() && IsInitialized())
	{
		try
		{
			_ParameterPtr pParameterPtr = m_pCommandPtr->CreateParameter( btParameterName, enDataType, enParameterDirection, lSize, vtValue );
			m_pCommandPtr->Parameters->Append( pParameterPtr );

			hReturn = S_OK;
		}
		catch(_com_error& e)
		{
			CatchComError("AddParameter",e);
		}
	}

	return hReturn;
}

//	Add Parameter Heler Function for Long Type and Input Direction
HRESULT CMyADO::AddParameterInputLong( _bstr_t btParameterName, long lValue )
{
	return AddParameter( btParameterName, adInteger, adParamInput, sizeof( long ), _variant_t( lValue ));
}

//	Add Parameter Helper Function for Text Type and Input Direction
HRESULT CMyADO::AddParameterInputText( _bstr_t btParameterName, _bstr_t btValue )
{
	return AddParameter( btParameterName, adVarChar, adParamInput, btValue.length(), _variant_t( btValue ));
}

//	Add Parameter Helper Function for Long Type and Input/Output Direction
HRESULT CMyADO::AddParameterInputOutputLong( _bstr_t btParameterName, long lValue )
{
	return AddParameter( btParameterName, adInteger, adParamInputOutput, sizeof( long ), _variant_t( lValue ));
}

//	Add Parameter Helper Function for Text Type and Input/Output Direction
HRESULT CMyADO::AddParameterInputOutputText( _bstr_t btParameterName, _bstr_t btValue, DWORD dwMaxTextSize )
{
	return AddParameter( btParameterName, adVarChar, adParamInputOutput, dwMaxTextSize, _variant_t( btValue ));
}

//	Add Parameter Helper Function for Long Type and Output Direction
HRESULT CMyADO::AddParameterOutputLong( _bstr_t btParameterName )
{
	_variant_t vtNull;

	return AddParameter( btParameterName, adInteger, adParamOutput, 0, vtNull );
}

//	Add Parameter Helper Function for Text Type and Output Direction
HRESULT CMyADO::AddParameterOutputText( _bstr_t btParameterName, DWORD dwMaxTextSize )
{
	_variant_t vtNull;

	return AddParameter( btParameterName, adVarChar, adParamOutput, dwMaxTextSize, vtNull );
}

//	Add Parameter Helper Function for Return Value
HRESULT CMyADO::AddParameterReturnValue()
{
	_variant_t vtNull;

	return AddParameter( "RETURN_VALUE", adInteger, adParamReturnValue, 0, vtNull );
}

//	Close the Current ADO Connection
HRESULT CMyADO::Close()
{
	HRESULT hReturn = S_FALSE;

	if( m_pConnectionPtr )
	{
		if( m_pConnectionPtr->State == adStateOpen )
		{
			try
			{
				hReturn = m_pConnectionPtr->Close();

				m_pConnectionPtr = NULL;
			}
			catch(_com_error& e)
			{
				CatchComError("Close",e);
			}
		}
		else
			hReturn = S_OK;
	}
	else
		hReturn = S_OK;

	return hReturn;
}
//	Execute the Stored Procedure using the CommandPtr Object
// original
// HRESULT CMyADO::Execute()
// modified
HRESULT CMyADO::Execute(long lOptions)
{
	HRESULT hReturn = S_FALSE;

	if( IsConnected() && IsInitialized())
	{
		try
		{
			// original
			// m_pRecordsetPtr = m_pCommandPtr->Execute( NULL, NULL, adCmdStoredProc );
			// modified
			m_pRecordsetPtr = m_pCommandPtr->Execute( NULL, NULL, lOptions);

			hReturn = S_OK;
		}
		catch(_com_error& e)
		{
			CatchComError("Execute",e);
		}
	}

	return hReturn;
}

// new function
HRESULT CMyADO::ExecuteStoredProcedure()
{
	return Execute(adCmdStoredProc);
}
// new function
HRESULT CMyADO::ExecuteQuery(_bstr_t btCommandString)
{
	HRESULT hReturn = S_FALSE;
	if((hReturn=Initialize(btCommandString,adCmdText)) != S_OK)
		return hReturn;
	return Execute(adCmdText);
}

//	Retrieve a Value from the Recordset (which was created during Stored Procedure Execution)
HRESULT CMyADO::GetField( _variant_t vtFieldName, _variant_t& vtValue )
{
	HRESULT hReturn = S_FALSE;

	if( IsConnected() && IsInitialized())
	{
		try
		{
			vtValue = m_pRecordsetPtr->Fields->GetItem( vtFieldName )->Value;

			hReturn = S_OK;
		}
		catch(_com_error& e)
		{
			CatchComError("GetField",e,(char*) (_bstr_t) vtFieldName);
			//char* p = (const char*) (_bstr_t) var;
		}
	}

	return hReturn;
}

//	Retrieve DataType from the Recordset (which was created during Stored Procedure Execution)
HRESULT CMyADO::GetFieldDataType( _variant_t vtFieldName, _variant_t& vtValue )
{
	HRESULT hReturn = S_FALSE;

	if( IsConnected() && IsInitialized())
	{
		try
		{
			m_DataTypeEnum = m_pRecordsetPtr->Fields->GetItem( vtFieldName )->GetType();
			hReturn = S_OK;
		}
		catch(_com_error& e)
		{
			CatchComError("GetFieldDataType",e);
		}
	}

	return hReturn;
}

//	Get Field Helper Function for UNSIGNED 16 BITS Type
HRESULT CMyADO::GetFieldUINT16( _bstr_t btFieldName, UINT16* pu16Value )
{
	_variant_t vtValue;

	HRESULT hReturn = GetFieldDataType( btFieldName, vtValue );

	if( hReturn == S_OK && m_DataTypeEnum == adNumeric)
		hReturn = GetField( btFieldName, vtValue );
	else
		return S_FALSE;

	if( hReturn == S_OK )
		*pu16Value = (UINT16)vtValue;

	return hReturn;
}

//	Get Field Helper Function for UNSIGNED 32 BITS Type
HRESULT CMyADO::GetFieldUINT32( _bstr_t btFieldName, UINT32* pu32Value )
{
	_variant_t vtValue;

	HRESULT hReturn = GetFieldDataType( btFieldName, vtValue );

	if( hReturn == S_OK && m_DataTypeEnum == adNumeric)
		hReturn = GetField( btFieldName, vtValue );
	else
		return S_FALSE;

	if( hReturn == S_OK )
		*pu32Value = ( DWORD )vtValue;

	return hReturn;
}

//	Get Field Helper Function for UNSIGNED 32 BITS Type
HRESULT CMyADO::GetFieldUINT64( _bstr_t btFieldName, UINT64* pu64Value )
{
	_variant_t vtValue;

	HRESULT hReturn = GetFieldDataType( btFieldName, vtValue );

	if( hReturn == S_OK && m_DataTypeEnum == adNumeric)
		hReturn = GetField( btFieldName, vtValue );
	else
		return S_FALSE;

	if( hReturn == S_OK )
		*pu64Value = ( DWORD64 )vtValue;

	return hReturn;
}

//	Get Field Helper Function for BigInt Type
HRESULT CMyADO::GetFieldBigInt( _bstr_t btFieldName, __int64* pn64Value )
{
	_variant_t vtValue;

	HRESULT hReturn = GetFieldDataType( btFieldName, vtValue );

	if( hReturn == S_OK && m_DataTypeEnum == adBigInt)
		hReturn = GetField( btFieldName, vtValue );
	else
		return S_FALSE;

	if( hReturn == S_OK )
		*pn64Value = ( __int64 )vtValue;

	return hReturn;
}

//	Get Field Helper Function for Long Type
HRESULT CMyADO::GetFieldLong( _bstr_t btFieldName, long* plValue )
{
	_variant_t vtValue;

	HRESULT hReturn = GetFieldDataType( btFieldName, vtValue );

	if( hReturn == S_OK && m_DataTypeEnum == adInteger)
		hReturn = GetField( btFieldName, vtValue );
	else
		return S_FALSE;

	if( hReturn == S_OK )
		*plValue = ( long )vtValue;

	return hReturn;
}

//	Get Field Helper Function for Int Type
HRESULT CMyADO::GetFieldInt( _bstr_t btFieldName, int* pnValue )
{
	_variant_t vtValue;

	HRESULT hReturn = GetFieldDataType( btFieldName, vtValue );

	if( hReturn == S_OK && m_DataTypeEnum == adInteger)
		hReturn = GetField( btFieldName, vtValue );
	else
		return S_FALSE;

	if( hReturn == S_OK )
		*pnValue = ( int ) vtValue;

	return hReturn;
}

//	Get Field Helper Function for Text Type
HRESULT CMyADO::GetFieldText( _bstr_t btFieldName, char* szText, DWORD dwMaxTextSize )
{
	_variant_t vtValue;

	HRESULT hReturn = GetField( btFieldName, vtValue );

	if( hReturn == S_OK )
	{
		BOOL bIsNull = (vtValue.vt==VT_NULL);
		if(bIsNull == FALSE)
		{
			_bstr_t btValue = ( _bstr_t )vtValue;

			if( dwMaxTextSize < btValue.length())
				hReturn = S_FALSE;
			else
				strcpy_s( szText, dwMaxTextSize, btValue );
		}
		else
			szText[0] = '\0';
	}

	return hReturn;
}

//	Get Field Helper Function for Text Type
HRESULT CMyADO::GetFieldLongVarBinary( _bstr_t btFieldName, unsigned char* pucBuffer, long lMaxBufferSize, long* plActualSize )
{
	_variant_t vtValue;

	HRESULT hReturn = GetFieldDataType( btFieldName, vtValue );

	if( hReturn == S_OK && m_DataTypeEnum == adLongVarBinary)
		hReturn = GetField( btFieldName, vtValue );
	else
		return S_FALSE;

	if( hReturn == S_OK )
	{
		long	lElements = vtValue.parray->rgsabound[0].cElements;
		BYTE*	pbyData = (BYTE*)vtValue.parray->pvData;

		if( lMaxBufferSize < lElements)
			hReturn = S_FALSE;
		else
		{
			memcpy_s(pucBuffer,lMaxBufferSize,pbyData,lElements);
			if(plActualSize != NULL)
				*plActualSize = lElements;
		}
	}

	return hReturn;
}

//	Retrieve a Parameter (which was previously set up as either an Output or InputOutput Direction and is set during Stored Procedure Execution)
HRESULT CMyADO::GetParameter( _variant_t vtParameterName, _variant_t& vtValue )
{
	HRESULT hReturn = S_FALSE;

	if( IsConnected() && IsInitialized())
	{
		try
		{
			vtValue = m_pCommandPtr->Parameters->GetItem( vtParameterName )->Value;

			hReturn = S_OK;
		}
		catch(_com_error& e)
		{
			CatchComError("GetParameter",e);
		}
	}

	return hReturn;
}

//	Retrieve Parameter Helper Function for Long Type
HRESULT CMyADO::GetParameterLong( _bstr_t btParameterName, long* plValue )
{
	_variant_t vtValue;

	HRESULT hReturn = GetParameter( btParameterName, vtValue );

	if( hReturn == S_OK )
		*plValue = ( long )vtValue;

	return hReturn;
}

//	Retrieve Parameter Helper Function for Return Value
HRESULT CMyADO::GetParameterReturnValue( long* plReturnValue )
{
	return GetParameterLong( "RETURN_VALUE", plReturnValue );
}

//	Retrieve Parameter Helper Function for Text Type
HRESULT CMyADO::GetParameterText( _bstr_t btParameterName, char* szText, DWORD dwMaxTextSize )
{
	_variant_t vtValue;

	HRESULT hReturn = GetParameter( btParameterName, vtValue );

	if( hReturn == S_OK )
	{
		_bstr_t btValue = ( _bstr_t )vtValue;

		if( dwMaxTextSize < btValue.length())
			hReturn = S_FALSE;
		else
			strcpy_s( szText, dwMaxTextSize, btValue );
	}

	return hReturn;
}

//	Retrieve Parameter Helper Function for Text Type
HRESULT CMyADO::GetParameterText( _bstr_t btParameterName, char* szText, DWORD dwMaxTextSize, bool* popBIsNull )
{
	_variant_t vtValue;

	HRESULT hReturn = GetParameter( btParameterName, vtValue );

	if( hReturn == S_OK )
	{
		*popBIsNull = (vtValue.vt==VT_NULL);
		//
		if(!*popBIsNull) {
			_bstr_t btValue = ( _bstr_t )vtValue;

			if( dwMaxTextSize < btValue.length())
				hReturn = S_FALSE;
			else
				strcpy_s( szText, dwMaxTextSize, btValue );
		}
	}

	return hReturn;
}

//	Retrieve the Record Count for the Recordset (which was created during Stored Procedure Execution)
HRESULT CMyADO::GetRecordCount( long* lRecordCount )
{
	HRESULT hReturn = S_FALSE;

	if( m_pRecordsetPtr )
	{
		try
		{
			*lRecordCount = m_pRecordsetPtr->RecordCount;
			hReturn = S_OK;
		}
		catch(_com_error& e)
		{
			CatchComError("GetRecordCount",e);
		}
	}

	return hReturn;
}

//	Close the Recordset and Initialize the CommandPtr Object
// original
// HRESULT CMyADO::Initialize( _bstr_t btStoredProcedureName )
HRESULT CMyADO::Initialize( _bstr_t btCommandStringOrStoredProcedureName, CommandTypeEnum CommandType )
{
	HRESULT hReturn = S_FALSE;

	if( IsConnected())
	{
		m_pCommandPtr = NULL;

		if( m_pRecordsetPtr )
		{
			if( m_pRecordsetPtr->State == adStateOpen )
				m_pRecordsetPtr->Close();

			m_pRecordsetPtr = NULL;
		}
/*		
		if(m_pCommandPtr==NULL)
		{
			OutputDebugString(_T("Error In CMyADO::Initialize() \n") );
		}
		else
*/
		{
			m_pCommandPtr.CreateInstance( __uuidof( Command ));

			m_pCommandPtr->ActiveConnection = m_pConnectionPtr;
			// original
			// m_pCommandPtr->CommandText = btStoredProcedureName;
			// modified
			m_pCommandPtr->CommandText = btCommandStringOrStoredProcedureName;
			// original
			// m_pCommandPtr->CommandType = adCmdStoredProc;
			// modified
			m_pCommandPtr->CommandType = CommandType;
			// modified
			//m_pCommandPtr->CommandTimeout = 60; // The Defaul value Is 30 Seconds 

			hReturn = S_OK;
		}
	}

	return hReturn;
}
// new function
HRESULT CMyADO::InitializeStoredProcedure( _bstr_t btCommandStringOrStoredProcedureName)
{
	return Initialize(btCommandStringOrStoredProcedureName,adCmdStoredProc);
}
//	Check for Connection Status
BOOL CMyADO::IsConnected()
{
	return ( m_pConnectionPtr );
}

//	Check for EOF on the Recordset (which was created during Stored Procedure Execution)
BOOL CMyADO::IsEOF()
{
	BOOL bReturn = TRUE;

	if( m_pRecordsetPtr )
		if( m_pRecordsetPtr->State == adStateOpen && !m_pRecordsetPtr->EndOfFile )
			bReturn = FALSE;

	return bReturn;
}

//	Check for Initialization Status (CommandPtr Object is valid)
BOOL CMyADO::IsInitialized()
{
	return ( m_pCommandPtr );
}

//	Open a new ADO Connection
HRESULT CMyADO::OpenConnection( _bstr_t btConnectionString, _bstr_t btUserID, _bstr_t btPassword )
{

	HRESULT hReturn = S_FALSE;

	if( m_pConnectionPtr == NULL )
	{
		HRESULT hr = m_pConnectionPtr.CreateInstance( __uuidof( Connection ));

		if(hr!=0)
		{
			_com_error e(hr);
			CatchComError("OpenConnection-CreateIstance",e);
		}
		else
		{
			try
			{
				//hReturn = m_pConnectionPtr->Open( btConnectionString, btUserID, btPassword, 0 );
				hReturn = m_pConnectionPtr->Open( btConnectionString, btUserID, btPassword,adConnectUnspecified);

				m_pConnectionPtr->CommandTimeout = 3; // 3 Seconds

				if( hReturn == S_OK )
					m_pConnectionPtr->CursorLocation = adUseClient;
			}
			catch(_com_error& e)
			{
				CatchComError("OpenConnection-Open",e);
				m_pConnectionPtr = NULL;
			}
		}
	}
	else
		hReturn = S_OK;

	return hReturn;
}

//	Move to the Next Record in the Recordset (which was created during Stored Procedure Execution)
HRESULT CMyADO::MoveNext()
{
	HRESULT hResult = S_FALSE;

	if( !IsEOF())
	{
		try
		{
			hResult = m_pRecordsetPtr->MoveNext();
		}
		catch(_com_error& e)
		{
			CatchComError("MoveNext",e);
		}
	}

	return hResult;
}
//	Move to the First Record in the Recordset (which was created during Stored Procedure Execution)
HRESULT CMyADO::MoveFirst()
{
	HRESULT hResult = S_FALSE;

	if( !IsEOF())
	{
		try
		{
			hResult = m_pRecordsetPtr->MoveFirst();
		}
		catch(_com_error& e)
		{
			CatchComError("MoveFirst",e);
		}
	}

	return hResult;
}
//	Move to the Last Record in the Recordset (which was created during Stored Procedure Execution)
HRESULT CMyADO::MoveLast()
{
	HRESULT hResult = S_FALSE;

	if( !IsEOF())
	{
		try
		{
			hResult = m_pRecordsetPtr->MoveLast();
		}
		catch(_com_error& e)
		{
			CatchComError("MoveLast",e);
		}
	}

	return hResult;
}

//	Move to the MovePrevious Record in the Recordset (which was created during Stored Procedure Execution)
HRESULT CMyADO::MovePrevious()
{
	HRESULT hResult = S_FALSE;

	if( !IsEOF())
	{
		try
		{
			hResult = m_pRecordsetPtr->MovePrevious();
		}
		catch(_com_error& e)
		{
			CatchComError("MovePrevious",e);
		}
	}

	return hResult;
}
//	Move to the First Record in the Recordset (which was created during Stored Procedure Execution)
HRESULT CMyADO::MoveToBegin()
{
	HRESULT hResult = S_FALSE;

	try
	{
		hResult = m_pRecordsetPtr->MoveFirst();
	}
	catch(_com_error& e)
	{
		CatchComError("MoveToBegin",e);
	}

	return hResult;
}
