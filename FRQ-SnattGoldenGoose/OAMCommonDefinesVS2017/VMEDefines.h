//
//	File:		VMEDefines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 5.0
//				Visual C++ 6.0
//
//	Created:	14/December/1999
//	Updated:	05/March/2003
//
//	Contributors:	Davide Calabro'
//					Graziano Tonella
// ************************************************
// * BTW: The define __BRIDGE_VME__ Must Be		  *
// *      DEFINED ONLY FOR EUI-BRIDGE APPLICATION *
// ************************************************
//
#ifndef _VMEDefines_H_
#define _VMEDefines_H_

#define	__BRIDGE_VME__

#define	VME_ACK			1
#define	VME_NACK		3
#define VME_TUTTI		0xFFFF
#define VME_FLAG_RTT_NORMAL_ASSIGNED			0x00
#define VME_FLAG_RTT_RTR_REASSIGNEMENT			0x01
#define VME_FLAG_RTT_DATA_RESTORE				0x02
#define VME_FLAG_RTT_RTR_FORCE_ASSIGNED			0x04
#define VME_FLAG_RTT_RTR_SCANNER_ONCEAGAIN		0x08

//////////////////////////////////////////////////////////////////////////
//							STRUCT_ENVELOPECMLVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ENVELOPECMLVME
{
	BYTE		bySOM;
	BYTE		byLen;
	BYTE		byDestNode;
	BYTE		byFunctionCode;
	BYTE		byProg;
	BYTE		bySourceNode;
} STRUCT_ENVELOPECMLVME;
#pragma pack()

#define SIZE_STRUCT_ENVELOPECMLVME sizeof(_STRUCT_ENVELOPECMLVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_TAILCMLVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_TAILCMLVME
{
	BYTE		byCks;
	BYTE		byEOM;
} STRUCT_TAILCMLVME;
#pragma pack()

#define SIZE_STRUCT_TAILCMLVME sizeof(_STRUCT_TAILCMLVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_CMDVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_CMDVME
{
	BYTE		byTipo;
	BYTE		byClasseOgg;
	WORD		wNumOgg;			// Big-Endian	(Motorola)
	WORD		wIdOgg;				// Big-Endian	(Motorola)
} STRUCT_CMDVME;
#pragma pack()

#define SIZE_STRUCT_CMDVME sizeof(_STRUCT_CMDVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_ANSWERVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ANSWERVME
{
	BYTE		byRisposta;
	BYTE		byClasseOgg;
	WORD		wNumOgg;			// Big-Endian	(Motorola)
	WORD		wIdOgg;				// Big-Endian	(Motorola)
} STRUCT_ANSWERVME;
#pragma pack()

#define SIZE_STRUCT_ANSWERVME sizeof(_STRUCT_ANSWERVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_ANSWER2VME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ANSWER2VME
{
	BYTE		byRisposta;
	BYTE		byClasseOgg;
	WORD		wNumOgg;			// Big-Endian	(Motorola)
	WORD		wIdOgg;				// Big-Endian	(Motorola)
	WORD		wNMOgg;				// Big-Endian	(Motorola)
} STRUCT_ANSWER2VME;
#pragma pack()

#define SIZE_STRUCT_ANSWER2VME sizeof(_STRUCT_ANSWER2VME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_HEADERVMERTT
//////////////////////////////////////////////////////////////////////////
enum
{
	VMERTT_COMMAND_NOTHING,
	VMERTT_COMMAND_START,
	VMERTT_COMMAND_STOP,
	VMERTT_COMMAND_EMPTY,
	VMERTT_COMMAND_RESET
};
//
#pragma pack(1)
typedef struct _STRUCT_HEADERVMERTT
{
	WORD	wEndBatch;				// Number Of batch To Close
	BYTE	byNumStructDESTINAVME;	//
	BYTE	byCommand;				// See ENUM TYPEs
} STRUCT_HEADERVMERTT;
#pragma pack()

#define SIZE_STRUCT_HEADERVMERTT sizeof(_STRUCT_HEADERVMERTT)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_DESTINAVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_DESTINAVME
{
	DWORD	dwIdObj;			// Object Identification Number
	WORD	wDest;				// Destination Number
	WORD	wFull1;				// Not Used
	WORD	wFull2;				// Not Used
	WORD	wFull3;				// Not Used
	WORD	wFull4;				// Not Used
	BYTE	byIlScn;			// XY => X=Group  Y=Line
	BYTE	byFlag;				// See Documentation
	WORD	wCell;				// Loaded Cell Number
	BYTE	byRamp;				// Unload Ramp (0=Default)
	char	cOffset;			// Unload Offset (NIF)
	//
	#ifdef	__BRIDGE_VME__
		BYTE	bySpare;
		BYTE	byNumAlternativeDest;
		WORD	wArrayAlternativeDest[10];
	#endif
} STRUCT_DESTINAVME;
#pragma pack()
#define SIZE_STRUCT_DESTINAVME sizeof(_STRUCT_DESTINAVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_HEADERVMERTR(Send)
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_HEADERVMERTRS
{
	BYTE	byNumStructRIDESTINAVME;	//
	BYTE	byNumStructENDBATCHVME;		//
} STRUCT_HEADERVMERTRS;
#pragma pack()

#define SIZE_STRUCT_HEADERVMERTRS sizeof(_STRUCT_HEADERVMERTRS)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_RIDESTINAVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_RIDESTINAVME
{
	WORD	wCell;				// Cell Number
	WORD	wDest;				// Destination Number
	DWORD	dwIdObj;			// Object Identification Number
	BYTE	byRamp;				// Unload Ramp (0=Default)
	char	cOffset;			// Unload Offset (NIF)
	BYTE	byFlag;				// See Documentation
	BYTE	bySpare;			// Available
} STRUCT_RIDESTINAVME;
#pragma pack()
#define SIZE_STRUCT_RIDESTINAVME sizeof(_STRUCT_RIDESTINAVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_ENDBATCHVME
//////////////////////////////////////////////////////////////////////////
#define VME_LAMP_OFF	0x30	// Lamp OFF
#define VME_LAMP_ON		0x31    // Lamp ON
#define VME_LAMP_SLOW	0x32	// Lamp Short Blinking
#define VME_LAMP_FAST	0x34	// Lamp Fast Blinking
#define VME_LAMP_SNAP	0x35	// Lamp SnapShot

#pragma pack(1)
typedef struct _STRUCT_ENDBATCHVME
{
	WORD	wDest;				// Destination Number
	#ifdef	__BRIDGE_VME__
		BYTE	byCommand;			// See VME_LAMP_XXX
		BYTE	byInhibitUnload;	// Used As BOOLEAN
	#else
		BYTE	byLampType;			// See VME_LAMP_XXX
		BYTE	bySpare;
	#endif
} STRUCT_ENDBATCHVME;
#pragma pack()
#define SIZE_STRUCT_ENDBATCHVME sizeof(_STRUCT_ENDBATCHVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_HEADERVMERTR(Receive)
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_HEADERVMERTRR
{
	BYTE	byNumStructESITIVME;		//
	BYTE	byNumStructFREEDESTVME;		//
} STRUCT_HEADERVMERTRR;
#pragma pack()

#define SIZE_STRUCT_HEADERVMERTRR sizeof(_STRUCT_HEADERVMERTRR)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_ESITIVME
//////////////////////////////////////////////////////////////////////////
#define VME_ESITO_OK				0
#define VME_ESITO_REJECT			1
#define VME_ESITO_NORMAL_REASSIGN	2
#define VME_ESITO_NONCARICA			3
#define VME_ESITO_SUCELLA			4
#define VME_ESITO_REJECT_REASSIGN	5
#define VME_ESITO_UNLOAD_EXECUTED	6
#pragma pack(1)
typedef struct _STRUCT_ESITIVME
{
	DWORD	dwIdObj;			// Object Identification Number
	BYTE	byEsito;			// See VME_ESITO_XXX
	BYTE	byFailure;			// Failure Type
	WORD	wCell;				// UnLoad Cell Number
	WORD	wZone;				// Unload Zone Number
	WORD	wDest;				// Destination Number
} STRUCT_ESITIVME;
#pragma pack()
#define SIZE_STRUCT_ESITIVME sizeof(_STRUCT_ESITIVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_FREEDESTVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_FREEDESTVME
{
	WORD	wDest;				// Destination Number
} STRUCT_FREEDESTVME;
#pragma pack()
#define SIZE_STRUCT_FREEDESTVME sizeof(_STRUCT_FREEDESTVME)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_STATOVMERTT
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_STATOVMERTT
{
	char	cModoSBIR;			// See VME_RTT.DOC Document
	char	cMotoSBIR;			// See VME_RTT.DOC Document
	char	cTransSBIR;			// See VME_RTT.DOC Document
	BYTE	byGiriSBIR;			// See VME_RTT.DOC Document
	BYTE	byRiciSBIR;			// See VME_RTT.DOC Document
	char	cEndBatch;			// See VME_RTT.DOC Document
	WORD	wBatchID;			// See VME_RTT.DOC Document
	BYTE	byNumStructSPECIALVMERTT;
} STRUCT_STATOVMERTT;
#pragma pack()
#define SIZE_STRUCT_STATOVMERTT sizeof(_STRUCT_STATOVMERTT)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_SPECIALVMERTT
//////////////////////////////////////////////////////////////////////////
#define VME_SPECIALVMERTT_MSGID_BINHALFFULL		1
#define VME_SPECIALVMERTT_MSGID_BINFULLFILL		2
#define VME_SPECIALVMERTT_MSGID_BINPUSHBUTTON	3
#define VME_SPECIALVMERTT_MSGID_BINFAULT		4
#define VME_SPECIALVMERTT_MSGID_BINDISABLED		5
#define VME_SPECIALVMERTT_MSGID_BINNOTOPERATIVE	6
#pragma pack(1)
typedef struct _STRUCT_SPECIALVMERTT
{
	BYTE	byMsgId;			// See VME_RTT.DOC Document
	BYTE	byDataLen;			// See VME_RTT.DOC Document
	BYTE	byData[256];		// See VME_RTT.DOC Document (Max 256 Bytes)
} STRUCT_SPECIALVMERTT;
#pragma pack()
#define SIZE_STRUCT_SPECIALVMERTT sizeof(_STRUCT_SPECIALVMERTT)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_SCANNERVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_SCANNERVME
{
	WORD	wCell;				// Cell Number
	BYTE	byScannerId;		// Scanner Identification Number
	BYTE	byScannerDataLen;	// Size Of byScannerData
	BYTE	byScannerData[256];	// Data Read By Scanner (Max 256 Bytes)
} STRUCT_SCANNERVME;
#pragma pack()
#define SIZE_STRUCT_SCANNERVME sizeof(_STRUCT_SCANNERVME)

// Comandi RTD
enum
{	
	VMERTD_ABIL			= 1,
	VMERTD_DISAB		= 2,
	VMERTD_RESET		= 3,
	VMERTD_RICSTATUS	= 4,
	VMERTD_RICSTAT		= 5,
	VMERTD_RSTSTAT		= 6,
	VMERTD_FINELOTTO	= 7,
	VMERTD_RICEVENTI	= 8,

	VMERTD_MAX_COMMANDS
};

// Classi oggetto
enum
{	
	VMERTD_ENC			= 1,
	VMERTD_RICEN		= 2,
	VMERTD_UNIBO		= 3,
	VMERTD_MTD			= 4,
	VMERTD_SCAN			= 5,
	VMERTD_TCAR			= 6,
	VMERTD_DEST			= 7,
	VMERTD_CELL			= 8,
	VMERTD_LINT			= 9,
	VMERTD_SBIR			= 10,
	VMERTD_TESTLAMP		= 11,
	VMERTD_TRAINO		= 12,
	VMERTD_ENDWAVE		= 13,
	VMERTD_IN_OUT		= 14,
	VMERTD_LB			= 15,
	VMERTD_PDS			= 16,
	VMERTD_LINPC		= 17,
	VMERTD_CHUTEHALF	= 18,
	VMERTD_CHUTEFULL	= 19,
	VMERTD_CONTROLLO	= 20,
	VMERTD_SCARCON		= 21,
	VMERTD_AUXZERO		= 22,

	VMERTD_MAX_CLASSIOGG
};

// Diagno 1 VMERTD_RICEN
enum
{
	VMERTD_RICEN_FT_CROS1	= 0x01,
	VMERTD_RICEN_FT_CROS2	= 0x02,
	VMERTD_RICEN_FT_FRACE	= 0x04,
	VMERTD_RICEN_FT_RIIN1	= 0x08,
	VMERTD_RICEN_FT_RIIN2	= 0x10,
	VMERTD_RICEN_FT_RIES1	= 0x20,
	VMERTD_RICEN_FT_RIES2	= 0x40
};

// Stato VMERTD_MTD
enum
{
	VMERTD_MTD_NORMAL		= 'N',
	VMERTD_MTD_ANOM			= 'A',
	VMERTD_MTD_AVAR			= 'V'
};

// Diagno 1 VMERTD_TCAR
enum
{
	VMERTD_TCAR_GUASTO_DI		= 0x01,
	VMERTD_TCAR_NMAX_CELLEKO	= 0x02,
	VMERTD_TCAR_GUASTO_SCHEDA	= 0x03
};

// Diagno 1 VMERTD_DEST
enum
{
	VMERTD_DEST_GUASTO_DI_PIENO	= 0x01,
	VMERTD_DEST_TRACK_ON		= 0x02,
	VMERTD_DEST_TRACK_OFF		= 0x03
};

// Diagno 1 VMERTD_CELL
enum
{
	VMERTD_CELL_ALL_TEST_IN		= 0x01,
	VMERTD_CELL_ALL_TEST_ES		= 0x02,
	VMERTD_CELL_ALL_MANC_MOV	= 0x04,
	VMERTD_CELL_ALL_PIANO_KO	= 0x08
};

// Stato VMERTD_AUXZERO
enum
{
	VMERTD_AUXZERO_NORMAL			= 0x00,
	VMERTD_AUXZERO_AVARIA_NOSIG		= 0x01,
	VMERTD_AUXZERO_AVARIA_OUTPOS	= 0x02
};

// Stato VMERTD_CONTROLLO
enum
{
	VMERTD_CONTROLLO_CTL_OK 			= 0x00,
	VMERTD_CONTROLLO_PRIMASCARTO_KO 	= 0x01,
	VMERTD_CONTROLLO_DOPOSCARTO_KO		= 0x02
};

//////////////////////////////////////////////////////////////////////////
//							STRUCT_STATOVME
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_STATOVME
{
	char		cModo;
	char		cMoto;
	WORD		wSvel;				// Big-Endian	(Motorola)
	WORD		wIdVel;				// Big-Endian	(Motorola)
	WORD		wVel;				// Big-Endian	(Motorola)
	BYTE		byCoppia;
	WORD		wNumCariche;		// Big-Endian	(Motorola)
	char		cTrans;
	BYTE		byNumWARQ;
	BYTE		byWARQ[16];
	BYTE		byNumALLQ;
	BYTE		byALLQ[16];
	BYTE		byNumWARS;
	BYTE		byWARS[16];
	BYTE		byNumALLS;
	BYTE		byALLS[16];
	BYTE		byNumGiroSbir;
	BYTE		byNumMaxRicircoli;
	WORD		wNumPiene1;			// Big-Endian	(Motorola)
	WORD		wNumPiene2;			// Big-Endian	(Motorola)
	WORD		wNumPiene3;			// Big-Endian	(Motorola)
	WORD		wNumPiene4;			// Big-Endian	(Motorola)
	WORD		wNumVuote1;			// Big-Endian	(Motorola)
	WORD		wNumVuote2;			// Big-Endian	(Motorola)
	WORD		wNumVuote3;			// Big-Endian	(Motorola)
	WORD		wNumVuote4;			// Big-Endian	(Motorola)
} STRUCT_STATOVME;
#pragma pack()

#define SIZE_STRUCT_STATOVME sizeof(_STRUCT_STATOVME)

#endif