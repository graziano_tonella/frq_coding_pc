#if !defined(AFX_COLORSTATIC_H__66D25331_5E8A_11D2_8971_00A0245BCF44__INCLUDED_)
#define AFX_COLORSTATIC_H__66D25331_5E8A_11D2_8971_00A0245BCF44__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ColorStatic.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorStatic window

class CColorStatic : public CStatic
{
// Construction
public:
	CColorStatic();

// Attributes
public:

protected:
    COLORREF m_crTextColor;
    COLORREF m_crBkColor;
    CBrush   m_brBkgnd;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorStatic)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CColorStatic();
  void SetTextColor(COLORREF);
  void SetBkColor(COLORREF);

	// Generated message map functions
protected:
	//{{AFX_MSG(CColorStatic)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORSTATIC_H__66D25331_5E8A_11D2_8971_00A0245BCF44__INCLUDED_)
