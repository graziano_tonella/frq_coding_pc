// ColorStatic.cpp : implementation file
//

#include "pch.h"
#include "ColorStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorStatic

CColorStatic::CColorStatic()
{
	m_crTextColor = RGB (0, 0, 0);
	m_crBkColor = ::GetSysColor (COLOR_3DFACE);
	m_brBkgnd.CreateSolidBrush (m_crBkColor);
}

CColorStatic::~CColorStatic()
{


}

void CColorStatic::SetTextColor (COLORREF crColor)
{
    m_crTextColor = crColor;
    Invalidate ();
}

void CColorStatic::SetBkColor (COLORREF crColor)
{
    m_crBkColor = crColor;
    m_brBkgnd.DeleteObject ();
    m_brBkgnd.CreateSolidBrush (crColor);
    Invalidate ();
}

BEGIN_MESSAGE_MAP(CColorStatic, CStatic)
	//{{AFX_MSG_MAP(CColorStatic)
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorStatic message handlers

HBRUSH CColorStatic::CtlColor (CDC* pDC, UINT nCtlColor)
{
    pDC->SetTextColor (m_crTextColor);
    pDC->SetBkColor (m_crBkColor);
    return (HBRUSH) m_brBkgnd;
}

