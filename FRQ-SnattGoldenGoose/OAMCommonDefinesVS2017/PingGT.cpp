#include "StdAfx.h"
#include "PingGT.h"

CPingGT::CPingGT(void)
{
	bValid = FALSE;

	// Dynamically load the ICMP.DLL
	hndlIcmp = LoadLibrary(_T("ICMP.DLL"));
	if (hndlIcmp == NULL)
	{
		::AfxMessageBox(_T("CPingGT - Could not load ICMP.DLL"),MB_ICONSTOP);
		return;
	}
	// Retrieve ICMP function pointers
	pIcmpCreateFile  = (HANDLE (WINAPI *)(void))
		GetProcAddress((HMODULE)hndlIcmp,"IcmpCreateFile");
	pIcmpCloseHandle = (BOOL (WINAPI *)(HANDLE))
		GetProcAddress((HMODULE)hndlIcmp,"IcmpCloseHandle");
	pIcmpSendEcho = (DWORD (WINAPI *)
		(HANDLE,DWORD,LPVOID,WORD,PIPINFO,LPVOID,DWORD,DWORD))
		GetProcAddress((HMODULE)hndlIcmp,"IcmpSendEcho");
	// Check all the function pointers
	if (pIcmpCreateFile == NULL		|| 
		pIcmpCloseHandle == NULL	||
		pIcmpSendEcho == NULL)
	{
		::MessageBox(NULL, _T("Error loading ICMP.DLL"), _T("Error:"), MB_OK);
		FreeLibrary((HMODULE)hndlIcmp);
		return;
	}

	bValid = TRUE;
}

CPingGT::~CPingGT(void)
{
	if (hndlIcmp != NULL)
		FreeLibrary((HMODULE)hndlIcmp);
}

int CPingGT::Ping(char* strHost,DWORD dwMilliSecondsToWaitAnswer)
{
	struct in_addr iaDest;		// Internet address structure
	DWORD *dwAddress;			// IP Address
	IPINFO ipInfo;				// IP Options structure
	ICMPECHO icmpEcho;			// ICMP Echo reply buffer
	HANDLE hndlFile;			// Handle for IcmpCreateFile()

    if(!bValid)
	{
		return FALSE;
	}

	//I changed the code to next and the problem of HOSTS disappeared 
	iaDest.s_addr = inet_addr(strHost); 
	// Copy the IP address 
	dwAddress = &iaDest.s_addr ; 
	



	// Copy the IP address
	//dwAddress = (DWORD *)(*pHost->h_addr_list);

	// Get an ICMP echo request handle        
	hndlFile = pIcmpCreateFile();

	// Set some reasonable default values
	ipInfo.Ttl = 255;
	ipInfo.Tos = 0;
	ipInfo.IPFlags = 0;
	ipInfo.OptSize = 0;
	ipInfo.Options = NULL;
	icmpEcho.Status = 0;
	// Reqest an ICMP echo
	DWORD dwRetVal;

	dwRetVal = pIcmpSendEcho(
		hndlFile,		// Handle from IcmpCreateFile()
		*dwAddress,		// Destination IP address
		NULL,			// Pointer to buffer to send
		0,				// Size of buffer in bytes
		&ipInfo,		// Request options
		&icmpEcho,		// Reply buffer
		sizeof(struct tagICMPECHO),
		dwMilliSecondsToWaitAnswer);			// Time to wait in milliseconds
	// Print the results
	iaDest.s_addr = icmpEcho.Source;
	// Close the echo request file handle
	pIcmpCloseHandle(hndlFile);

	if(dwRetVal == 0)
	{
		dwRetVal = GetLastError();
		//TRACE(_T("dwRetVal %u \n"), dwRetVal);
	}

	if (icmpEcho.Status)
	{
		//TRACE(_T("KO - icmpEcho.Status %u \n"),icmpEcho.Status);
		return FALSE;
	}

	//TRACE(_T("OK - icmpEcho.Status %u \n"),icmpEcho.Status);

	return TRUE;
}
