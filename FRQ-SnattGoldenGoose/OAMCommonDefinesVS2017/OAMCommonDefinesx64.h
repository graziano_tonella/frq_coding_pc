//////////////////////////////////////////////////////////////////
//																//
// NAME:					OAMCommonDefinesx64.h				//
// LAST MODIFICATION DATE:	13-OCT-2017							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					It collects definitions, structures,//
//							enumerations and paths				//
//////////////////////////////////////////////////////////////////
#pragma once

//
//
//
//===========================================================================================================================================
//===========================================================================================================================================
//
// T C P / I P   P R O T O C O L   M E S S A G E   S T R U C T U R E
//
//===========================================================================================================================================
//===========================================================================================================================================
//
//
//
#define	TCPIPENVELOPE_SOM	0xFD
#define	TCPIPENVELOPE_EOM	0xFE

// ENVELOPE STRUCTURE
#pragma pack(1)
typedef struct _STRUCT_TCPIPENVELOPE
{
	BYTE	bySOM;
	BYTE	byClass;
	BYTE	bySourceNode;
	BYTE	byDestNode;
	BYTE	byProg;
	BYTE	byFlags;
	WORD	wSpare;
	DWORD	dwMessagesLENGTH;
	WORD	wNumberOfMESSAGES;
} STRUCT_TCPIPENVELOPE;
#pragma pack()

// TAIL STRUCTURE
#pragma pack(1)
typedef struct _STRUCT_TCPIPTAIL
{
	BYTE		bySpare;
	WORD		wSpare;
	BYTE		byEOM;
} STRUCT_TCPIPTAIL;
#pragma pack()

// 
#pragma pack(1)
typedef struct _STRUCT_TCPIPMSGHEADER
{
	WORD	wMsgID;
	WORD	wMsgLENGTH;
} STRUCT_TCPIPMSGHEADER;
#pragma pack()
//
#pragma pack(1)
typedef struct _STRUCT_TCPIPMSG
{
	STRUCT_TCPIPMSGHEADER	csSTRUCT_TCPIPMSGHEADER;
	BYTE	byMsgDATA[USHRT_MAX];
} STRUCT_TCPIPMSG;
#pragma pack()
