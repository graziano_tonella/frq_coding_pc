//////////////////////////////////////////////////////////////////
//																//
// NAME:					CZEBRASocketGT.h					//
// LAST MODIFICATION DATE:	24/08/2011							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Include								//
//////////////////////////////////////////////////////////////////
#pragma once

#include "NetworkST.h"
#include "SharedFlatQueueGT.h"
#include "Timers.h"
#include <CMLDefines.h>

class CZEBRASocketGT : public CNetworkST
{
public:
	CZEBRASocketGT(void);
	virtual ~CZEBRASocketGT(void);

	DWORD	Destroy(DWORD dwMilliseconds);
	DWORD	Create(USHORT nSocketType,LPCTSTR lpszIPAddress,int nPortNumber,DWORD dwBufferSize,DWORD dwPollingTime,UINT nRcvTimeout,CSharedFlatQueueGT *pFlatQueueIn,DWORD dwMemoryFlatQueueOut,wchar_t* pwszChannel);
	void	SetParameters(int nPrnID,WORD wDiagnosticMsgID,UINT uPollingTime);
	BOOL	IsRunning(void);
	void	GetRGBConnectionState(COLORREF* pcrColor);

	CSharedFlatQueueGT	m_FlatQueueOut;

	static WORD		GetVersionI()		{return 1;}
	static LPCTSTR	GetVersionC()		{return (LPCTSTR)_T("1.0");}

private:

	void	OnChangeConnectionState(WPARAM wSocketType, LPARAM lNewState);
	DWORD	OnPrepareMsgToSend(LPBYTE pBuffer, DWORD dwBufferLen);
	BOOL	OnSendCompleted(BOOL bSendOk, int nErrorCode);
	void	OnReceive(LPBYTE pData, DWORD dwDataLen);
	BOOL	OnTimeout();

	CSharedFlatQueueGT	*m_pFlatQueueIn;

	BYTE				m_byConnectionState;
	BOOL				m_bConnected;
	COLORREF			m_crColor;

	CTimer55ms			m_t55msPollingTime;
	CTimer55ms			m_t55msTimeOutRX;
	CTimer55ms			m_t55msNotifyDiagnostic;

	int					m_nPrnID;
	WORD				m_wDiagnosticMsgID;
	UINT				m_uPollingTime;

	CString				m_sChannel;

	SYSTEMTIME			m_csSystemTime;
	CString				m_sString;

protected:
	BOOL	m_bSendMsg;
	BOOL	m_bWaitAnswer;
	BOOL	m_bSendLabel;
	int		m_nNumBytesRX;

	inline void	ResetData();
	inline void	NotifyDiagnostic(BOOL bSendNow=FALSE);
	inline void	PrepareRX();
	inline void	ManageTimeOut();

private:
	//////////////////////////////////////////////
//	STRUCTURE OF ZEBRA DIAGNOSTIC (STRING1)	//
//////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ZEBRA_DIAG_STR1
{
	BYTE	bySTX;
	char	szCommunication[3];
	char	cComma1;
	char	cPaperOut;
	char	cComma2;
	char	cPause;
	char	cComma3;
	char	szLabelLen[4];
	char	cComma4;
	char	szNumOfFormatReceived[3];
	char	cComma5;
	char	cBufferFull;
	char	cComma6;
	char	cDiagnoMode;
	char	cComma7;
	char	cPartialFormat;
	char	cComma8;
	char	szUnused[3];
	char	cComma9;
	char	cCorruptRAM;
	char	cComma10;
	char	cUnderTemperature;
	char	cComma11;
	char	cOverTemperature;
	BYTE	byETX;
	BYTE	byCR;
	BYTE	byLF;
} STRUCT_ZEBRA_DIAG_STR1;
#pragma pack( )
//////////////////////////////////////////////
//	STRUCTURE OF ZEBRA DIAGNOSTIC (STRING2)	//
//////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ZEBRA_DIAG_STR2
{
	BYTE	bySTX;
	char	szFunction[3];
	char	cComma1;
	char	cUnuded;
	char	cComma2;
	char	cHeadUp;
	char	cComma3;
	char	cRibbonOut;
	char	cComma4;
	char	cTemalTransferMode;
	char	cComma5;
	char	cPrintMode;
	char	cComma6;
	char	cPrintWidthMode;
	char	cComma7;
	char	cPartialFormat;
	char	cComma8;
	char	cLabelWaiting;
	char	cComma9;
	char	szLabelsRemaining[6];			// The Manual Is Wrong
	char	cComma10;
	char	cFormatWhilePrinting;
	char	cComma11;
	char	szNumberOfImagesInMemory[3];
	BYTE	byETX;
	BYTE	byCR;
	BYTE	byLF;
} STRUCT_ZEBRA_DIAG_STR2;
#pragma pack( )
//////////////////////////////////////////////
//	STRUCTURE OF ZEBRA DIAGNOSTIC (STRING3)	//
//////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ZEBRA_DIAG_STR3
{
	BYTE	bySTX;
	char	szPassword[4];
	char	cComma1;
	char	cTypeOfRAMInstalled;
	BYTE	byETX;
	BYTE	byCR;
	BYTE	byLF;
} STRUCT_ZEBRA_DIAG_STR3;
#pragma pack( )

//////////////////////////////////////////////////////////
//	STRUCTURE OF ZEBRA DIAGNOSTIC (HOST STATUS RETURN)	//
//////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ZEBRA_DIAG_HSR
{
	STRUCT_ZEBRA_DIAG_STR1	csDIAGSTR1;
	STRUCT_ZEBRA_DIAG_STR2	csDIAGSTR2;
	STRUCT_ZEBRA_DIAG_STR3	csDIAGSTR3;
} STRUCT_ZEBRA_DIAG_HSR;
#pragma pack( )

public:
//////////////////////////////////////
//	STRUCTURE OF ZEBRA DIAGNOSTIC	//
//////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ZEBRA_DIAGNOSTIC
{
	int		nPrnID;
	WORD	wDIAGMsgSent;
	WORD	wDIAGMsgReceived;
	WORD	wLABELMsgSent;
	int		nTimeOut;
	BOOL	bIsAvailable;
	STRUCT_ZEBRA_DIAG_HSR	csHSR;
} STRUCT_ZEBRA_DIAGNOSTIC;
#pragma pack( )


//////////////////////////////////
//	STRUCTURE OF ZEBRA MESSAGE	//
//////////////////////////////////
enum{DIM_ZEBRA_MESSAGE_DATA=5120};
#pragma pack(1)
typedef struct _STRUCT_ZEBRA_MESSAGE
{
	int		nMsgLen;
	char	szMsgData[DIM_ZEBRA_MESSAGE_DATA];
} STRUCT_ZEBRA_MESSAGE;
#pragma pack( )


protected:
	STRUCT_ZEBRA_DIAGNOSTIC	m_csZEBRADIAG;
	STRUCT_ZEBRA_MESSAGE	m_csZEBRAMESSAGE;
};


inline void CZEBRASocketGT::ResetData()
{
	m_bSendMsg		= TRUE;
	m_bWaitAnswer	= FALSE;
	m_bSendLabel	= TRUE;

	::ZeroMemory(&m_csZEBRADIAG,sizeof(m_csZEBRADIAG));
	m_csZEBRADIAG.bIsAvailable = FALSE;
}

inline void CZEBRASocketGT::NotifyDiagnostic(BOOL bSendNow)
{
	if(bSendNow == FALSE)
	{
		if(m_t55msNotifyDiagnostic.IsStart() == FALSE)
			return;
		if(m_t55msNotifyDiagnostic.IsElapsed(3000) == FALSE)
			return;
	}

	m_t55msNotifyDiagnostic.Start();

	m_csZEBRADIAG.nPrnID = m_nPrnID;
	STRUCT_SINGLEMSGCML	csSINGLEMSGCML;
	::ZeroMemory(&csSINGLEMSGCML,sizeof(csSINGLEMSGCML));
	csSINGLEMSGCML.csHeader.wMsgId	 = m_wDiagnosticMsgID;
	csSINGLEMSGCML.csHeader.wDataLen = sizeof(m_csZEBRADIAG);
	::CopyMemory(&csSINGLEMSGCML.byData,&m_csZEBRADIAG,sizeof(m_csZEBRADIAG));
	if(m_pFlatQueueIn != NULL)
		m_pFlatQueueIn->PutItem((LPBYTE) &csSINGLEMSGCML,(DWORD) (SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
}

inline void CZEBRASocketGT::PrepareRX()
{
	m_t55msTimeOutRX.Start();
	m_nNumBytesRX=0;
	m_bWaitAnswer = TRUE;
}

inline void CZEBRASocketGT::ManageTimeOut()
{
	if(m_bWaitAnswer == FALSE)
		return;
//	::GetLocalTime(&m_csSystemTime);
//	m_sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
//	TRACE(_T("%s CZEBRASocketGT::OnTimeOut() - TimeStart %s \n"),m_sString,(m_tTimer55ms.IsStart()==TRUE?_T("YES"):_T("NO")));

	if(m_t55msTimeOutRX.IsStart() == FALSE)
		return;
	if(m_t55msTimeOutRX.IsElapsed(1500) == FALSE)
		return;

	m_t55msTimeOutRX.Stop();
	m_bSendMsg	  = TRUE;
	m_bWaitAnswer = FALSE;

	m_csZEBRADIAG.bIsAvailable = FALSE;
	m_csZEBRADIAG.nTimeOut++;
}

