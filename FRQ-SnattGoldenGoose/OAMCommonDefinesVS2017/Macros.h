//
//	File:		Macros.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 6.0 sp6
//
//	Created:	30/September/2002
//	Updated:	19/March/2004
//
//	Author:		Davide Calabro'		davide_calabro@yahoo.com
//
#ifndef _MACROSST_H_
#define _MACROSST_H_

#define WAITINIT(w)		::InitializeCriticalSection(w)
#define WAITRED(w)		::EnterCriticalSection(w)
#define WAITGREEN(w)	::LeaveCriticalSection(w)
#define WAITDELETE(w)	::DeleteCriticalSection(w)

#define UNISIZEOF(t)	(sizeof(t) * sizeof(TCHAR))

#define NUMCHAR(t)		(sizeof(t) / sizeof(TCHAR))

#define ELEMENTS(t)		(sizeof(t)/sizeof(t[0]))

#define SWAPBYTE(w)		(w = MAKEWORD(HIBYTE(w), LOBYTE(w)))

// Swaps two bytes inside a WORD
// Ex: 0x0102		-->		0x0201
#ifndef SWAPBYTE
#define SWAPBYTE(w)			(w = MAKEWORD(HIBYTE(w), LOBYTE(w)))
#endif

// Swaps two words inside a DWORD
// Ex: 0x01020304	-->		0x03040102
#ifndef SWAPWORD
#define SWAPWORD(dw)		(dw = MAKELONG(MAKEWORD(LOBYTE(HIWORD(dw)), HIBYTE(HIWORD(dw))), MAKEWORD(LOBYTE(LOWORD(dw)), HIBYTE(LOWORD(dw)))))
#endif

// Swaps two words inside a DWORD
// then swaps two bytes inside WORDs
// Ex: 0x01020304	-->		0x04030201
#ifndef SWAPWORDBYTE
#define SWAPWORDBYTE(dw)	(dw = MAKELONG(MAKEWORD(HIBYTE(HIWORD(dw)), LOBYTE(HIWORD(dw))), MAKEWORD(HIBYTE(LOWORD(dw)), LOBYTE(LOWORD(dw)))))
#endif

#ifdef _CONSOLE

#include <assert.h>
#define ASSERT(e)		assert(e)

#endif // _CONSOLE

#endif