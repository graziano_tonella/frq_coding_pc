//////////////////////////////////////////////////////////////////
//																//
//																//
// NAME:					SerialComGT.cpp						//
//																//
// LAST MODIFICATION DATE:	04/07/2008							//
//																//
// AUTHOR:					Graziano Tonella					//
//																//
// FILE TYPE:				Include C++							//
//																//
// PURPOSE:					Serial Port Device Management		//
//															    //
//																//
//////////////////////////////////////////////////////////////////
//
//
//
#include "stdafx.h"
#include "SerialComGT.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSerialComGT::CSerialComGT()
{
	m_uErrorOnPutItem = 0;
	//
	DisableReceive();
}

CSerialComGT::~CSerialComGT()
{
	DisableReceive();
	Destroy(1000);
}

DWORD CSerialComGT::Destroy(DWORD dwMilliseconds)
{
	DWORD		dwRetValue;

	dwRetValue = CDeviceComMP::Destroy(dwMilliseconds);

	// Free memory
	if (m_pMsgCML != NULL)
		::GlobalFree((HGLOBAL)m_pMsgCML);
	//
	m_pMsgCML = NULL;

	return dwRetValue;
}

BOOL CSerialComGT::Create(BYTE byComPort,DWORD dwBaudRate,char* szComFlags,DWORD dwBufferSize,DWORD dwPollingTime,DWORD dwRtsControl,CSharedFlatQueueGT *pFlatQueueIn,DWORD dwSizeFlatQueueOut)
{
	if(CDeviceComMP::Init() == FALSE)
		return FALSE;
	//
	if(CDeviceComMP::Create(byComPort,dwBaudRate,szComFlags,dwBufferSize,dwPollingTime,dwRtsControl) == FALSE)
		return FALSE;
	//
	m_pFlatQueueIn	 = pFlatQueueIn;
	//
	if(m_FlatQueueOut.Create(dwSizeFlatQueueOut,NULL,0,NULL,NULL) != QUEUEGT_OK)
		return FALSE;
	//
	// Allocate memory
	//
	m_pMsgCML = NULL;
	m_pMsgCML = (LPBYTE)::GlobalAlloc(GPTR, SIZE_STRUCT_SINGLEMSGCML);
	//
	if(m_pMsgCML == NULL)
		return FALSE;
	//
	WaitMessage();
	//
	m_tTimer55ms.Stop();
	//
	return TRUE;
}

BOOL CSerialComGT::SetRXProperties(BYTE bySOM,BYTE byEOM,BYTE byMaxLen,WORD wTimeOut,WORD wMsgID)
{
	m_bySOM		= bySOM;
	m_byEOM		= byEOM;
	m_byMaxLen	= byMaxLen;
	m_wTimeOut	= wTimeOut;
	m_wMsgID	= wMsgID;
	//
	//
	if(byMaxLen != 0 && (m_bySOM != 0 || m_byEOM != 0))
		return FALSE;
	if(byMaxLen == 0 && m_byEOM == 0)
		return FALSE;
	if(m_wTimeOut == 0 || m_wMsgID == 0)
		return FALSE;
	//
	return TRUE;
}

DWORD CSerialComGT::OnPrepareMsgToSend(LPBYTE pBuffer, DWORD dwBufferLen)
{
	STRUCT_SINGLEMSGCML	csSINGLEMSGCML;
	DWORD				dwTotalMsgSize = 0;
	DWORD				dwFirstItemSize;
	//
	//
	//
	if(!m_FlatQueueOut.GetCount())
		return 0;
	// Get size of first item available in queue
	dwFirstItemSize = m_FlatQueueOut.GetFirstItemSize();
	if(dwFirstItemSize == 0 || dwFirstItemSize >= dwBufferLen)
		return 0;
	//
	if(m_FlatQueueOut.GetItem((LPBYTE)m_pMsgCML,TRUE,NULL) != QUEUEGT_OK)
		return 0;
	::CopyMemory(&csSINGLEMSGCML,m_pMsgCML,dwFirstItemSize);
	::CopyMemory(pBuffer,csSINGLEMSGCML.byData,csSINGLEMSGCML.csHeader.wDataLen);
	dwTotalMsgSize += csSINGLEMSGCML.csHeader.wDataLen;
	//
	return dwTotalMsgSize;
}

void CSerialComGT::OnReceive(LPBYTE pData,DWORD dwDataLen)
{
	DWORD	dwIx;
	//
	// If a message arrived
	//
	if(dwDataLen == 0)
		return;
	//
	::GetLocalTime(&m_csSystemTime);
	m_sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
	//
	if(m_bEnableRX == FALSE)
	{
		TRACE(_T("%s SerialComGT::OnReceive - Receive Not ENABLED \n"),m_sString);
		return;
	}
	//
	TRACE(_T("%s SerialComGT::OnReceive - Received %d Bytes\n"),m_sString,dwDataLen);
	for(dwIx=0;dwIx<dwDataLen;dwIx++)
	{
		TRACE(_T("%s SerialComGT::OnReceive - Receive BYTE %c \n"),m_sString,(BYTE) pData[dwIx]);
		if(m_wNumBytes >= sizeof(m_csSINGLEMSGCML.byData))
		{
			WaitMessage();
			continue;
		}
		//
		//
		if(m_byMaxLen)
		{
			if(m_tTimer55ms.IsStart() == FALSE)
				m_tTimer55ms.Start();
			//
			m_csSINGLEMSGCML.byData[m_wNumBytes++] = (BYTE) pData[dwIx];
			//
			if(m_wNumBytes < (WORD) m_byMaxLen)
				continue;
			//
			m_csSINGLEMSGCML.csHeader.wMsgId	 = m_wMsgID;
			m_csSINGLEMSGCML.csHeader.wDataLen	 = m_wNumBytes;
			if(m_pFlatQueueIn->PutItem((LPBYTE)&m_csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+m_csSINGLEMSGCML.csHeader.wDataLen)) != QUEUEGT_OK)
				m_uErrorOnPutItem++;
			//
			WaitMessage();
			continue;
		}
		//
		if(m_bWaitSOM == TRUE)
		{
			if(pData[dwIx] != m_bySOM)
				continue;
			//
			m_bWaitSOM	= FALSE;
			m_wNumBytes	= 0;
			//
			if(m_tTimer55ms.IsStart() == FALSE)
				m_tTimer55ms.Start();
		}
		//
		m_csSINGLEMSGCML.byData[m_wNumBytes++] = (BYTE) pData[dwIx];
		//
		if(m_bWaitEOM == TRUE)
		{
			if(pData[dwIx] != m_byEOM)
				continue;
			//
			m_csSINGLEMSGCML.csHeader.wMsgId	 = m_wMsgID;
			m_csSINGLEMSGCML.csHeader.wDataLen	 = m_wNumBytes;
			if(m_pFlatQueueIn->PutItem((LPBYTE)&m_csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+m_csSINGLEMSGCML.csHeader.wDataLen)) != QUEUEGT_OK)
				m_uErrorOnPutItem++;
			//
			WaitMessage();
		}
	}
}

BOOL CSerialComGT::OnTimeout()
{
//	::GetLocalTime(&m_csSystemTime);
//	m_sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
//	TRACE(_T("%s SerialComGT::OnTimeOut() - TimeStart %s \n"),m_sString,(m_tTimer55ms.IsStart()==TRUE?_T("YES"):_T("NO")));

	if(m_tTimer55ms.IsStart() == TRUE)
	{
		if(m_tTimer55ms.IsElapsed(m_wTimeOut) == TRUE)
			WaitMessage();
	}
	//
	return TRUE;
}
//
void CSerialComGT::OnError(DWORD dwError)
{
//	::GetLocalTime(&m_csSystemTime);
//	m_sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
//	TRACE(_T("%s SerialComGT::OnError() - dwError %X \n"),m_sString,dwError);
}

