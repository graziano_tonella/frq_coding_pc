//
//	Class:		CProtocolCML
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 5.0
//				Visual C++ 6.0
//
//	Version:	See GetVersionC() or GetVersionI()
//
//	Created:	11/November/1999
//	Updated:	13/March/2000
//
//	Author:		  Davide Calabro'		davide_calabro@yahoo.com
//  Contributors: Marco Piazza
//
#ifndef _PROTOCOLCML_H_
#define _PROTOCOLCML_H_

// Uncomment following line if you are using this class outside the DLL
#define _PROTOCOLCML_NODLL_

#ifndef _PROTOCOLCML_NODLL_
#ifndef	_CMLHTDLL_NOLIB_
	#ifdef _DEBUG
		#ifdef _UNICODE
			#pragma comment(lib, "CmlHTud.lib")
		#else
			#pragma comment(lib, "CmlHTd.lib")
		#endif
		#else
		#ifdef _UNICODE
			#pragma comment(lib, "CmlHTu.lib")
		#else
			#pragma comment(lib, "CmlHT.lib")
		#endif
	#endif
#endif
#endif

#ifdef _CONSOLE
#include <windows.h>
#include <tchar.h>
#endif

#include "CMLDefines.h"
#include "VMEDefines.h"

#include "Macros.h"
#include "SharedQueueST.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//////////////////////////////////////////////////////////////////////////
//							STRUCT_DEBUG_PROTOCML
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_DEBUG_PROTOCML
{
	DWORD		dwMsgOK;
	DWORD		dwBadSOM;
	DWORD		dwBadEOM;
	DWORD		dwBadCks;
	DWORD		dwOutOfBuffer;
	DWORD		dwQueueError;
} STRUCT_DEBUG_PROTOCML;
#pragma pack()

#define SIZE_STRUCT_DEBUG_PROTOCML sizeof(_STRUCT_DEBUG_PROTOCML)

enum
{
	PROTOCML_WAITSOM = 0,
	PROTOCML_WAITLEN,
	PROTOCML_WAITENVELOPE,
	PROTOCML_WAITDATA,
	PROTOCML_WAITTAIL
};

// Error codes
#define	PROTOCML_OK					0
#define PROTOCML_NOINIT				1
#define PROTOCML_ALREADYINIT		2
#define PROTOCML_BADPARAM			3
#define PROTOCML_NOMEMORY			4
#define PROTOCML_BUFFERTOOSHORT		5
#define PROTOCML_QUEUEERROR			6

// Protocol type
#define PROTOCML_STD                0
#define PROTOCML_VME                1
#define PROTOCML_OLD                2

#ifndef _PROTOCOLCML_NODLL_
#ifdef	_CMLHTDLL_BUILDDLL_
	#define	PROTOCOLCML_EXPORT	__declspec(dllexport)
#else
	#define	PROTOCOLCML_EXPORT	__declspec(dllimport)
#endif
#else
	#define	PROTOCOLCML_EXPORT
#endif

class PROTOCOLCML_EXPORT	CProtocolCML  
{
public:
	CProtocolCML();
	virtual ~CProtocolCML();

	void GetDebug(STRUCT_DEBUG_PROTOCML* pDebug);
	void ResetDebug();
	DWORD Reset();
	DWORD MakeMessage(LPBYTE lpBuffer, DWORD dwBufferSize);
	BYTE MakeCks(LPBYTE pBuffer, WORD wBufferLen);

	DWORD Create(CSharedQueueST* pQueue, int nWichProtocol = PROTOCML_STD);
	DWORD Destroy();

	static short GetVersionI()		{return 12;}
	static LPCTSTR GetVersionC()	{return (LPCTSTR)_T("1.2");}

private:
	DWORD MakeMessageVME   (LPBYTE lpBuffer, DWORD dwBufferSize);
	DWORD MakeMessageCML   (LPBYTE lpBuffer, DWORD dwBufferSize);
	DWORD MakeMessageOLDCML(LPBYTE lpBuffer, DWORD dwBufferSize);
	void ClearAll();

	BOOL				m_bInit;

	LPBYTE				m_pMem;
	DWORD				m_dwMemIndex;
	DWORD				m_dwBufferSize;

	DWORD				m_dwBytesToReceive;
	DWORD				m_dwBytesReceived;

	STRUCT_ENVELOPEOLDCML*	m_pEnvelopeOld;
	STRUCT_TAILOLDCML*		m_pTailOld;

	STRUCT_ENVELOPECML*	    m_pEnvelope;
	STRUCT_TAILCML*		    m_pTail;

	STRUCT_ENVELOPECMLVME*	m_pEnvelopeVME;
	STRUCT_TAILCMLVME*		m_pTailVME;

	int 				m_nWichProtocol;
	int					m_nCrackStep;

	STRUCT_DEBUG_PROTOCML	m_Debug;

	CSharedQueueST*		m_pQueue;
	CRITICAL_SECTION	m_csWait;
};

#endif
