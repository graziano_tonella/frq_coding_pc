//////////////////////////////////////////////////////////////////
//																//
// NAME:					CHighResolutionTimerGT.cpp			//
// LAST MODIFICATION DATE:	30-Mar-2016							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Application							//
//////////////////////////////////////////////////////////////////
#include "stdafx.h"

#include "x64HighResolutionTimerGT.h"

CHighResolutionTimerGT::CHighResolutionTimerGT(void)
{
	m_bSupported = (BOOL) (QueryPerformanceFrequency(&m_liFrequency)==0?FALSE:TRUE);   // current frequency
	m_bStarted = FALSE;
}

CHighResolutionTimerGT::~CHighResolutionTimerGT(void)
{

}

BOOL CHighResolutionTimerGT::IsSupported(void)
{
	return m_bSupported;
}

void CHighResolutionTimerGT::StopCounter(void)
{
	m_bStarted = FALSE;
}

BOOL CHighResolutionTimerGT::StartCounter(void)
{
	if(m_bSupported == FALSE)
		return FALSE;
	if(QueryPerformanceCounter(&m_liStartCounter) == FALSE)
		return FALSE;
	m_bStarted = TRUE;
	return TRUE;
}

BOOL CHighResolutionTimerGT::IsStarted(void)
{
	return m_bStarted;
}

double CHighResolutionTimerGT::GetTimeInSeconds(void)
{
	if(_GetElapsedTime() == FALSE)
		return 0;
	double dbRetVal = (double) ((double)m_liDiffCounter.QuadPart / (double)m_liFrequency.QuadPart);

	return dbRetVal;
}

double CHighResolutionTimerGT::GetTimeInMilliSeconds(void)
{
	if(_GetElapsedTime() == FALSE)
		return 0;
	double dbRetVal = (double) ((1.0e3 * (double)m_liDiffCounter.QuadPart) / (double)m_liFrequency.QuadPart);
	return dbRetVal;
}

double CHighResolutionTimerGT::GetTimeInMicroSeconds(void)
{
	if(_GetElapsedTime() == FALSE)
		return 0;
	double dbRetVal = (double) ((1.0e6 * (double)m_liDiffCounter.QuadPart) / (double)m_liFrequency.QuadPart);
	return dbRetVal;
}

BOOL CHighResolutionTimerGT::_GetElapsedTime(void)
{
	if(m_bSupported == FALSE)
		return FALSE;
	if(m_bStarted == FALSE)
		return FALSE;
	if(QueryPerformanceCounter(&m_liStopCounter) == FALSE)
		return FALSE;
	m_liDiffCounter.QuadPart = m_liStopCounter.QuadPart - m_liStartCounter.QuadPart;
	return TRUE;
}
