// Timers.cpp: implementation of the CTimer55ms class.
//
// Author:		Giuseppe Alessio
//
//////////////////////////////////////////////////////////////////////

#include "pch.h"
#include "Timers.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


void CTimer55ms::Clear()
{
	dwTempo=0;
}

void CTimer55ms::Start()
{
	bSet=TRUE;
	dwTempo = ::GetTickCount();
}

BOOL CTimer55ms::IsStart()
{
	return bSet;
}

BOOL CTimer55ms::IsElapsed(UINT delayMSec)
{
	if(bSet == FALSE) return TRUE;

	DWORD dwLast=GetTickCount();
	if( dwLast-dwTempo >= delayMSec)
	{
		Stop();
		return TRUE;
	}
	else
		return FALSE;
}

DWORD CTimer55ms::Read()
{
	if(bSet == FALSE) return 0;
  return (GetTickCount()-dwTempo);
}

void CTimer55ms::Stop()
{
	dwTempo=0;
	bSet=FALSE;  
}

