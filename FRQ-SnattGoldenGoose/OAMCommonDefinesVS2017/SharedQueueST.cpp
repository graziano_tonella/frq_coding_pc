#include "pch.h"
#include "SharedQueueST.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CSharedQueueST::CSharedQueueST()
{
	m_pData = NULL;

	Destroy();

	::InitializeCriticalSection(&m_cs_Wait);
}

CSharedQueueST::~CSharedQueueST()
{
	Destroy();

	::DeleteCriticalSection(&m_cs_Wait);
}

// This function initializes the queue
//
// Parameters:
//		[in]	dwMaxItems
//				Maximum number of items the queue can contain
//		[in]	dwItemSize
//				Maximum size of each queue's item
//		[in]	hWndParent
//				Handle to a window that will receive a message each time a new item
//				is added to the queue. Can be NULL.
//		[in]	nMsgParent
//				Message to be sent to the window identified by hWndParent. Can be 0 (zero).
//		[in]	hEventParent
//				Handle of an event that will be raised each time a new item
//				is added to the queue. Can be NULL.
//		[in]	hThreadHandle
//				Handle of a thread that will be resumed each time a new item
//				is added to the queue. Can be NULL.
//
// Return value:
//		QUEUEST_OK
//			All ok
//		QUEUEST_ALREADYINIT
//			Queue already initialized
//		QUEUEST_BADPARAM
//			Input parameters not valid
//		QUEUEST_NOMEMORY
//			Cannot allocate memory
//
DWORD CSharedQueueST::Create(DWORD dwMaxItems, DWORD dwItemSize, HWND hWndParent, UINT nMsgParent, HANDLE hEventParent, HANDLE hThreadHandle)
{
	DWORD dwTotalSize;

	if (m_pData != NULL) return QUEUEST_ALREADYINIT;
	//
	if (dwMaxItems == 0 || dwItemSize == 0) return QUEUEST_BADPARAM;

	::EnterCriticalSection(&m_cs_Wait);

	// Total amount of memory to allocate
	dwTotalSize = ((dwItemSize + SIZE_DWORD) * dwMaxItems);
	
	// Allocate memory
	m_pData = (LPBYTE)::GlobalAlloc(GPTR, dwTotalSize);

	// If failed
	if (m_pData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEST_NOMEMORY;
	}

	m_dwMaxItems = dwMaxItems;
	m_dwItemSize = dwItemSize;

	m_pTop = m_pData;
	m_pPut = m_pData;
	m_pGet = m_pData;
	m_pBottom = m_pTop + dwTotalSize;

	m_hWndParent =		hWndParent;
	m_nMsgParent =		nMsgParent;
	m_hEventParent =	hEventParent;
	m_hThreadHandle =	hThreadHandle;

	::LeaveCriticalSection(&m_cs_Wait);

	return QUEUEST_OK;
} // End of Create

// This function returns if queue is initialized
//
// Return value:
//		TRUE
//			Queue initialized
//		FALSE
//			Queue NOT initialized
//
BOOL CSharedQueueST::IsCreated()
{
	BOOL bRetValue;

	::EnterCriticalSection(&m_cs_Wait);
	bRetValue = (m_pData != NULL);
	::LeaveCriticalSection(&m_cs_Wait);

	return bRetValue;
} // End of IsCreated

// This function de-initializes the queue
//
void CSharedQueueST::Destroy()
{
	// Free memory
	if (m_pData != NULL) ::GlobalFree((HGLOBAL)m_pData);

	m_pData =			NULL;
	m_pTop =			NULL;
	m_pBottom =			NULL;
	m_pPut =			NULL;
	m_pGet =			NULL;

	m_dwMaxItems =		0;
	m_dwItemSize =		0;
	m_dwNumItems =		0;
	m_dwMaxHit =		0;
	m_bWasFull =		FALSE;

	m_hWndParent =		NULL;
	m_nMsgParent =		0;
	m_hEventParent =	NULL;
	m_hThreadHandle =	NULL;
} // End of Destroy

// This function returns if the queue is currently full
//
// Return value:
//		TRUE
//			Queue is full
//		FALSE
//			Queue is not full
//
BOOL CSharedQueueST::IsFull()
{
	BOOL bRetValue;

	::EnterCriticalSection(&m_cs_Wait);

	bRetValue = (m_dwNumItems >= m_dwMaxItems);

	::LeaveCriticalSection(&m_cs_Wait);

	return bRetValue;
} // End of IsFull

// This function returns if queue was full
//
// Return value:
//		TRUE
//			Queue was at least one time full
//		FALSE
//			Queue was never full, or queue not initialized
//
BOOL CSharedQueueST::WasFull()
{
	BOOL bWasFull;

	::EnterCriticalSection(&m_cs_Wait);
	bWasFull = m_bWasFull;
	::LeaveCriticalSection(&m_cs_Wait);

	return bWasFull;
} // End of WasFull

// This function returns the size of queue's item
//
// Return value:
//		DWORD
//			Size of each queue's item
//
DWORD CSharedQueueST::GetItemSize()
{
	DWORD dwItemSize;

	::EnterCriticalSection(&m_cs_Wait);
	dwItemSize = m_dwItemSize;
	::LeaveCriticalSection(&m_cs_Wait);

	return dwItemSize;
} // End of GetItemSize

// This function puts a new item in queue
//
// Parameters:
//		[in]	szpBuffer
//				Pointer to buffer to copy from
//		[in]	dwBufferSize
//				Size of byte to copy from szpBuffer
//				If ulBufferSize == 0 the default ItemSize will be assumed
//
// Return value:
//		QUEUEST_OK
//			All ok
//		QUEUEST_NOINIT
//			Queue not initialized
//		QUEUEST_FULL
//			Queue is full
//		QUEUEST_BUFFERTOOBIG
//			ulBufferSize is too big
//
DWORD CSharedQueueST::PutItem(char* szpBuffer, DWORD dwBufferSize)
{
	::EnterCriticalSection(&m_cs_Wait);

	// If queue is not initialized
	if (m_pData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEST_NOINIT;
	}
	// If queue is full
	if (m_dwNumItems >= m_dwMaxItems)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEST_FULL;
	}
	// If dwBufferSize is too big
	if (dwBufferSize > m_dwItemSize)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEST_BUFFERTOOBIG;
	}

	if (szpBuffer != NULL)
	{
		// Following line can be commented out to improve performances
		::ZeroMemory(m_pPut, m_dwItemSize);
		// Add item
		if (dwBufferSize == 0)
		{
			::CopyMemory(m_pPut + SIZE_DWORD, szpBuffer, m_dwItemSize);
			// Store number of bytes copied
			::CopyMemory(m_pPut, &m_dwItemSize, SIZE_DWORD);
		}
		else
		{
			::CopyMemory(m_pPut + SIZE_DWORD, szpBuffer, dwBufferSize);
			// Store number of bytes copied
			::CopyMemory(m_pPut, &dwBufferSize, SIZE_DWORD);
		}
		//
		m_pPut += (m_dwItemSize + SIZE_DWORD);
		// Check bounds
		if (m_pPut >= m_pBottom) m_pPut = m_pTop;

		// Update counters
		m_dwNumItems++;
		if (m_dwNumItems > m_dwMaxHit) m_dwMaxHit = m_dwNumItems;
		if (m_dwNumItems >= m_dwMaxItems) m_bWasFull = TRUE;

		// Send notification
		if (m_hWndParent != NULL)
		{
			if (m_nMsgParent != 0)
				::PostMessage(m_hWndParent, m_nMsgParent, 0, 0);
		}
		if (m_hEventParent != NULL)
		{
			::SetEvent(m_hEventParent);
		}

		if (m_hThreadHandle != NULL)
		{
			::ResumeThread(m_hThreadHandle);
		}
	}

	::LeaveCriticalSection(&m_cs_Wait);

	return QUEUEST_OK;
} // End of PutItem

// This function gets an item from queue
//
// Parameters:
//		[out]	szpBuffer
//				Pointer to a buffer that will receive the item copied from queue.
//				This buffer must be large enough to accept the largest item
//				in queue.
//		[in]	bRemove
//				If bRemove == FALSE the item is copied 
//				into szpBuffer but is NOT removed from queue
//		[out]	dwpNumBytesCopied	
//				Pointer to a DWORD that will receive the
//				number of bytes copied in szpBuffer. Can be NULL.
//
// Return value:
//		QUEUEST_OK
//			All ok
//		QUEUEST_NOINIT
//			Queue not initialized
//		QUEUEST_EMPTY
//			Queue is empty
//		QUEUEST_BADPARAM
//
DWORD CSharedQueueST::GetItem(char* szpBuffer, BOOL bRemove, DWORD* dwpNumBytesCopied)
{
	::EnterCriticalSection(&m_cs_Wait);

	// If queue is not initialized
	if (m_pData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEST_NOINIT;
	}
	// If queue is empty
	if (m_dwNumItems == 0)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEST_EMPTY;
	}

	if (szpBuffer != NULL)
	{
		DWORD dwBytesToBeCopied = 0;
		::CopyMemory(&dwBytesToBeCopied, m_pGet, SIZE_DWORD);

		::CopyMemory(szpBuffer, (m_pGet + SIZE_DWORD), dwBytesToBeCopied);
		// Number of bytes copied
		if (dwpNumBytesCopied != NULL)
		{
//			::CopyMemory(dwpNumBytesCopied, m_pGet, SIZE_DWORD);
			*dwpNumBytesCopied = dwBytesToBeCopied;
		}
	}
	if (bRemove == TRUE)
	{
		m_pGet += (m_dwItemSize + SIZE_DWORD);
		if (m_pGet >= m_pBottom) m_pGet = m_pTop;
		// Update counters
		m_dwNumItems--;
	}

	::LeaveCriticalSection(&m_cs_Wait);

	return QUEUEST_OK;
} // End of GetItem

// This function returns the number of items currently in queue
//
// Return value:
//		DWORD
//			Number of item currently in queue
//
DWORD CSharedQueueST::GetCount()
{
	DWORD dwNumItems;
	
	::EnterCriticalSection(&m_cs_Wait);
	dwNumItems = m_dwNumItems;
	::LeaveCriticalSection(&m_cs_Wait);

	return dwNumItems;
} // End of GetCount

// This function returns the max. number of items stored in queue
//
// Return value:
//		DWORD
//			Greater number of item stored in queue
//
DWORD CSharedQueueST::GetPeak()
{
	DWORD dwMaxHit;
	
	::EnterCriticalSection(&m_cs_Wait);
	dwMaxHit = m_dwMaxHit;
	::LeaveCriticalSection(&m_cs_Wait);

	return dwMaxHit;
} // End of GetPeak

// This function empties the queue
//
// Return value:
//		QUEUEST_OK
//			All ok
//		QUEUEST_NOINIT
//			Queue not initialized
//
DWORD CSharedQueueST::Empty()
{
	DWORD dwTotalSize;

	::EnterCriticalSection(&m_cs_Wait);

	// If queue is not initialized
	if (m_pData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEST_NOINIT;
	}

	// Total amount of memory allocated
	dwTotalSize = ((m_dwItemSize + SIZE_DWORD) * m_dwMaxItems);
	
	// Empty
	m_dwNumItems = 0;
	m_dwMaxHit = 0;
	m_bWasFull = FALSE;
	::ZeroMemory(m_pData, dwTotalSize);
	m_pTop = m_pData;
	m_pPut = m_pData;
	m_pGet = m_pData;
	m_pBottom = m_pTop + dwTotalSize;

	::LeaveCriticalSection(&m_cs_Wait);

	return QUEUEST_OK;
} // End of Empty

// This function returns the size of the first item in queue (the item that will
// be returned on the next GetItem call)
//
// Return value:
//		The size (in bytes) of the first item in queue
//		If queue is empty or it is not initialized the return value is 0 (zero)
//
DWORD CSharedQueueST::GetFirstItemSize()
{
	DWORD	dwSize = 0;

	::EnterCriticalSection(&m_cs_Wait);

	// If queue is not initialized
	if (m_pData == NULL || m_dwNumItems == 0)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return 0L;
	}

	::CopyMemory(&dwSize, m_pGet, SIZE_DWORD);

	::LeaveCriticalSection(&m_cs_Wait);

	return dwSize;
} // End of GetFirstItemSize

#undef SIZE_DWORD
