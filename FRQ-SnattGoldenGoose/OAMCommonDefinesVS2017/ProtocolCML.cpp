#include "pch.h"
//#include "Server_TestApp.h"
#include "ProtocolCML.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CProtocolCML::CProtocolCML()
{
	m_pMem = NULL;

	ClearAll();

	WAITINIT(&m_csWait);
}

CProtocolCML::~CProtocolCML()
{
	Destroy();

	WAITDELETE(&m_csWait);
}

void CProtocolCML::ClearAll()
{
	// Free memory
	if (m_pMem != NULL) ::GlobalFree((HGLOBAL)m_pMem);
	m_pMem = NULL;
	m_dwBufferSize = 0;

	m_pQueue = NULL;

	Reset();
	ResetDebug();

	// Class is now de-initialized
	m_bInit = FALSE;
} // End of ClearAll

DWORD CProtocolCML::Reset()
{
	m_dwMemIndex = 0;

	m_pEnvelope = NULL;
	m_pTail = NULL;

	m_pEnvelopeOld = NULL;
	m_pTailOld = NULL;

	m_pEnvelopeVME = NULL;
	m_pTailVME = NULL;

	m_dwBytesToReceive	= 0;
	m_dwBytesReceived	= 0;

	m_nCrackStep = PROTOCML_WAITSOM;

	return PROTOCML_OK;
} // End of Reset

DWORD CProtocolCML::Create(CSharedQueueST* pQueue, int nWichProtocol)
{
	// If already initialized
	if (m_bInit == TRUE) return PROTOCML_ALREADYINIT;
	// If invalid queue pointer
	if (pQueue == NULL) return PROTOCML_BADPARAM;
	m_pQueue = pQueue;
	m_dwBufferSize = m_pQueue->GetItemSize();
	// If invalid buffer size
	if (m_dwBufferSize == 0) return PROTOCML_BUFFERTOOSHORT;

	// Allocate memory
	m_pMem = (LPBYTE)::GlobalAlloc(GPTR, m_dwBufferSize);
	// If failed
	if (m_pMem == NULL)
	{
		ClearAll();
		return PROTOCML_NOMEMORY;
	}

	m_nWichProtocol = nWichProtocol;

	// Class is now initialized
	m_bInit = TRUE;

	return PROTOCML_OK;
} // End of Create

DWORD CProtocolCML::Destroy()
{
	ClearAll();

	// Class is now de-initialized
	//m_bInit = FALSE;

	return PROTOCML_OK;
} // End of Destroy

DWORD CProtocolCML::MakeMessage(LPBYTE lpBuffer, DWORD dwBufferSize)
{
	DWORD	dwRetValue=(DWORD)-1;

	// If not initialized
	if (m_bInit == FALSE) return PROTOCML_NOINIT;

	WAITRED(&m_csWait);

	switch(m_nWichProtocol)
	{
	  case PROTOCML_STD: //
          dwRetValue = MakeMessageCML(lpBuffer, dwBufferSize);		                  
		  break;

	  case PROTOCML_OLD:
		  dwRetValue = MakeMessageOLDCML(lpBuffer, dwBufferSize);
		  break;

	  case PROTOCML_VME:
		  dwRetValue = MakeMessageVME(lpBuffer, dwBufferSize);
		  break;
	}

	WAITGREEN(&m_csWait);

	return dwRetValue;

} // End of MakeMessage

void CProtocolCML::ResetDebug()
{
	::ZeroMemory(&m_Debug, SIZE_STRUCT_DEBUG_PROTOCML);
} // End of ResetDebug

void CProtocolCML::GetDebug(STRUCT_DEBUG_PROTOCML* pDebug)
{
	WAITRED(&m_csWait);
	::CopyMemory(pDebug, &m_Debug, SIZE_STRUCT_DEBUG_PROTOCML);
	WAITGREEN(&m_csWait);
} // End of GetDebug

DWORD CProtocolCML::MakeMessageCML(LPBYTE lpBuffer, DWORD dwBufferSize)
{
	DWORD		dwLoop = 0;
	DWORD		dwQueueRetValue;
	BOOL		bWhile = TRUE;

	while (bWhile == TRUE)
//	for (dwLoop = 0; dwLoop < dwBufferSize; dwLoop++)
	{
		switch (m_nCrackStep)
		{
			case PROTOCML_WAITSOM:
				if (lpBuffer[dwLoop] == CML_SOM)
				{
					// Store
					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesToReceive = SIZE_STRUCT_ENVELOPECML - 1;
					m_dwBytesReceived = 0;

					m_nCrackStep = PROTOCML_WAITENVELOPE;
				}
				else
				{
					m_Debug.dwBadSOM++;
				}
				break;
			case PROTOCML_WAITENVELOPE:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesReceived++;

					if (m_dwBytesReceived == m_dwBytesToReceive)
					{
						m_pEnvelope = (STRUCT_ENVELOPECML*)m_pMem;

						m_dwBytesReceived = 0;

						if (m_pEnvelope->wLen > 0)
						{
							m_dwBytesToReceive = m_pEnvelope->wLen;
							m_nCrackStep = PROTOCML_WAITDATA;
						}
						else
						{
							m_pTail = (STRUCT_TAILCML*)&m_pMem[m_dwMemIndex];
							m_dwBytesToReceive = SIZE_STRUCT_TAILCML;
							m_nCrackStep = PROTOCML_WAITTAIL;
						}
					}
				break;
			case PROTOCML_WAITDATA:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesReceived++;
					if (m_dwBytesReceived == m_dwBytesToReceive)
					{
						m_dwBytesToReceive = SIZE_STRUCT_TAILCML;
						m_dwBytesReceived = 0;

						m_pTail = (STRUCT_TAILCML*)&m_pMem[m_dwMemIndex];

						m_nCrackStep = PROTOCML_WAITTAIL;
					}
				break;
			case PROTOCML_WAITTAIL:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesReceived++;
					if (m_dwBytesReceived == m_dwBytesToReceive)
					{
						// Check for EOM
						if (m_pTail->byEOM == CML_EOM)
						{
							// Put in queue
							dwQueueRetValue = m_pQueue->PutItem((char*)m_pMem, m_dwMemIndex);
							if (dwQueueRetValue != QUEUEST_OK)
							{
								m_Debug.dwQueueError++;
								Reset();
								return PROTOCML_QUEUEERROR;
							}

							m_Debug.dwMsgOK++;
//							TRACE(_T("Msg OK \n"));
						}
						else
						{
							m_Debug.dwBadEOM++;
						}

						m_dwMemIndex = 0;
						m_dwBytesToReceive	= 0;
						m_dwBytesReceived	= 0;

						m_nCrackStep = PROTOCML_WAITSOM;
					}
				break;
			default:
				ASSERT(FALSE);
				break;
		} // switch

		dwLoop++;
		if (dwLoop >= dwBufferSize)
		{
			bWhile = FALSE;
		}
	} // while

	return PROTOCML_OK;
} // End of MakeMessageCML

DWORD CProtocolCML::MakeMessageOLDCML(LPBYTE lpBuffer, DWORD dwBufferSize)
{
	DWORD		dwLoop = 0;
	DWORD		dwQueueRetValue;
	BYTE		byCks;
	BOOL		bWhile = TRUE;

	while (bWhile == TRUE)
//	for (dwLoop = 0; dwLoop < dwBufferSize; dwLoop++)
	{
		switch (m_nCrackStep)
		{
			case PROTOCML_WAITSOM:
				if (lpBuffer[dwLoop] == CML_SOM)
				{
					// Store
					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesToReceive = SIZE_STRUCT_ENVELOPEOLDCML - 1;
					m_dwBytesReceived = 0;

					m_nCrackStep = PROTOCML_WAITENVELOPE;
				}
				else
				{
					m_Debug.dwBadSOM++;
				}
				break;
			case PROTOCML_WAITENVELOPE:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesReceived++;

					if (m_dwBytesReceived == m_dwBytesToReceive)
					{
						m_pEnvelopeOld = (STRUCT_ENVELOPEOLDCML*)m_pMem;

						m_dwBytesReceived = 0;

						if (m_pEnvelopeOld->byLen > 4)
						{
							m_dwBytesToReceive = m_pEnvelopeOld->byLen-5;
							m_nCrackStep = PROTOCML_WAITDATA;
						}
						else
						{
							m_pTailOld = (STRUCT_TAILOLDCML*)&m_pMem[m_dwMemIndex];
							m_dwBytesToReceive = SIZE_STRUCT_TAILOLDCML;
							m_nCrackStep = PROTOCML_WAITTAIL;
						}
					}
				break;

			case PROTOCML_WAITDATA:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesReceived++;
					if (m_dwBytesReceived == m_dwBytesToReceive)
					{
						m_dwBytesToReceive = SIZE_STRUCT_TAILOLDCML;
						m_dwBytesReceived = 0;

						m_pTailOld = (STRUCT_TAILOLDCML*)&m_pMem[m_dwMemIndex];

						m_nCrackStep = PROTOCML_WAITTAIL;
					}
				break;
			case PROTOCML_WAITTAIL:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesReceived++;
					if (m_dwBytesReceived == m_dwBytesToReceive)
					{
						// Check for EOM
						if (m_pTailOld->byEOM == CML_EOM)
						{
							byCks = MakeCks((LPBYTE)&m_pEnvelopeOld->byDestNode, m_pEnvelopeOld->byLen);
							if (m_pTailOld->byCks == byCks)
							{
								// Put in queue
								dwQueueRetValue = m_pQueue->PutItem((char*)m_pMem, m_dwMemIndex);
								if (dwQueueRetValue != QUEUEST_OK)
								{
									m_Debug.dwQueueError++;
									Reset();
									return PROTOCML_QUEUEERROR;
								}

								m_Debug.dwMsgOK++;
//								TRACE(_T("Msg OK \n"));
							}
							else	m_Debug.dwBadCks++;
						}
						else
						{
							m_Debug.dwBadEOM++;
						}

						m_dwMemIndex = 0;
						m_dwBytesToReceive	= 0;
						m_dwBytesReceived	= 0;

						m_nCrackStep = PROTOCML_WAITSOM;
					}
				break;
			default:
				ASSERT(FALSE);
				break;
		} // switch

		dwLoop++;
		if (dwLoop >= dwBufferSize)
		{
			bWhile = FALSE;
		}
	} // while

	return PROTOCML_OK;
} // End of MakeMessageOLDCML

DWORD CProtocolCML::MakeMessageVME(LPBYTE lpBuffer, DWORD dwBufferSize)
{
	DWORD		dwLoop = 0;
	BOOL		bWhile = TRUE;
	BYTE		byCks;
	DWORD		dwQueueRetValue;

	while (bWhile == TRUE)
//	for (dwLoop = 0; dwLoop < dwBufferSize; dwLoop++)
	{
		switch (m_nCrackStep)
		{
			case PROTOCML_WAITSOM:
				if (lpBuffer[dwLoop] == CML_SOM)
				{
					// Store
					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

//					m_dwBytesToReceive = SIZE_STRUCT_ENVELOPECML - 1;
//					m_dwBytesReceived = 0;

					m_nCrackStep = PROTOCML_WAITLEN;
				}
				else
				{
					m_Debug.dwBadSOM++;
				}
				break;
			case PROTOCML_WAITLEN:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_pEnvelopeVME = (STRUCT_ENVELOPECMLVME*)m_pMem;

					m_dwBytesToReceive = m_pEnvelopeVME->byLen; // lpBuffer[dwLoop];
					m_dwBytesReceived = 0;
					m_nCrackStep = PROTOCML_WAITDATA;
				break;

			case PROTOCML_WAITDATA:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesReceived++;
					if (m_dwBytesReceived == m_dwBytesToReceive)
					{
						m_dwBytesToReceive = SIZE_STRUCT_TAILCMLVME;
						m_dwBytesReceived = 0;

						m_pTailVME = (STRUCT_TAILCMLVME*)&m_pMem[m_dwMemIndex];

						m_nCrackStep = PROTOCML_WAITTAIL;
					}
				break;
			case PROTOCML_WAITTAIL:
					if (m_dwMemIndex >= m_dwBufferSize)
					{
						m_Debug.dwOutOfBuffer++;
						Reset();
						return PROTOCML_BUFFERTOOSHORT;
					}

					m_pMem[m_dwMemIndex] = lpBuffer[dwLoop];
					// 
					m_dwMemIndex++;

					m_dwBytesReceived++;
					if (m_dwBytesReceived == m_dwBytesToReceive)
					{
						// Check for EOM
						if (m_pTailVME->byEOM == CML_EOM)
						{
							// Calculate Checksum
							byCks = MakeCks((LPBYTE)&m_pEnvelopeVME->byDestNode, m_pEnvelopeVME->byLen);
							if (m_pTailVME->byCks == byCks)
							{
								// Put in queue
								dwQueueRetValue = m_pQueue->PutItem((char*)m_pMem, m_dwMemIndex);
								if (dwQueueRetValue != QUEUEST_OK)
								{
									m_Debug.dwQueueError++;
									Reset();
									return PROTOCML_QUEUEERROR;
								}

								m_Debug.dwMsgOK++;
//								TRACE(_T("Msg OK \n"));
							}
							else	m_Debug.dwBadCks++;
						}
						else	m_Debug.dwBadEOM++;

						m_dwMemIndex = 0;
						m_dwBytesToReceive	= 0;
						m_dwBytesReceived	= 0;

						m_nCrackStep = PROTOCML_WAITSOM;
					}
				break;
			default:
				ASSERT(FALSE);
				break;
		} // switch

		dwLoop++;
		if (dwLoop >= dwBufferSize)
		{
			bWhile = FALSE;
		}
	} // while

	return PROTOCML_OK;
} // End of MakeMessageVME

BYTE CProtocolCML::MakeCks(LPBYTE pBuffer, WORD wBufferLen)
{
	BYTE		byCks = 0;
	WORD		wLoop;

	for (wLoop = 0; wLoop < wBufferLen; wLoop++)
	{
		byCks ^= pBuffer[wLoop];
	}

	// Da specifica
	if (byCks == CML_EOM) byCks = 0x0;

	return byCks;
} // End of MakeCks
