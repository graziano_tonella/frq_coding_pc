//////////////////////////////////////////////////////////////////
//																//
// NAME:					CTCPIPServerClientFDXGT.h			//
// LAST MODIFICATION DATE:	24-Feb-2016							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					TCP/IP connection					//
//							Server/Client Full Duplex			//
//////////////////////////////////////////////////////////////////
#pragma once

#include "NetworkST.h"
#include "x64SharedFlatQueueGT.h"
#include "x64HighResolutionTimerGT.h"
#include "Timers.h"
#include "OAMCommonDefinesx64.h"


class CTCPIPServerClientFDXGT : public CNetworkST
{
public:
	CTCPIPServerClientFDXGT(void);
	virtual ~CTCPIPServerClientFDXGT(void);
	//
	DWORD	Destroy(DWORD dwMilliseconds);
	DWORD	Create(USHORT nSocketType,LPCTSTR lpszIPAddress,int nPortNumber,DWORD dwBufferSize,DWORD dwPollingTime,UINT nRcvTimeout,CSharedFlatQueueGT *pFlatQueueIn,DWORD dwMemoryFlatQueueOut,DWORD dwKeepAlive,_TCHAR* pszChannel);
	DWORD	Create(USHORT nSocketType, LPVOID lpIPAddress, DWORD dwIPAddressCount, DWORD dwBufferSize, DWORD dwPollingTime, UINT nRcvTimeout, CSharedFlatQueueGT *pFlatQueueIn, DWORD dwMemoryFlatQueueOut, DWORD dwKeepAlive, _TCHAR* pszChannel);
	void	SetProperties(BYTE bySourceNode,BYTE byDestNode);
	void	ExcludeProtocolControls(BOOL bExcludeNODEControl,BOOL bExcludeMSGPRGControl,BOOL bExcludeMSGPacking,BOOL bSendAlwaysKeepAlive);
	void	DoNotSendSTDKeepAlive(WORD wKeepAliveMsgID);
	void	SendTelegramAtFixedLength(DWORD dwTelegramLength);
	void 	LimitTelegramLength(DWORD dwTelegramLength);
	void	AlterMsgIdByAddingInterfaceId(WORD wInterfaceId);
	void	SetPostMessage(HWND hwndID,UINT uMsgID, WORD wNetworkId = 0);
	BOOL	IsRunning(void);
	void	GetRGBConnectionState(COLORREF* pcrColor);

	WORD	GetSentMsgID(void);
	BYTE	GetSentPrgID(void);
	WORD	GetReceMsgID(void);
	BYTE	GetRecePrgID(void);
	void	ClearSentReceMsgIDs(void);

	CSharedFlatQueueGT	m_FlatQueueOut;

private:
	void	OnChangeConnectionState(WPARAM wSocketType, LPARAM lNewState);
	DWORD	OnPrepareMsgToSend(LPBYTE pBuffer, DWORD dwBufferLen);
	BOOL	OnSendCompleted(BOOL bSendOk, int nErrorCode);
	void	OnReceive(LPBYTE pData, DWORD dwDataLen);
	BOOL	OnTimeout(void);

	CRITICAL_SECTION	m_cs_Wait;

	CTimer55ms			m_t55msKeepAliveTimer;
	DWORD				m_dwKeepAlive;

	CHighResolutionTimerGT		m_HRTimerRX;

	LPBYTE				m_pMsgTCPIP;
	LPBYTE				m_pMsgENQUEUED;

	CSharedFlatQueueGT	m_FlatQueueRECEIVEDSTREAM;
	CSharedFlatQueueGT	*m_pFlatQueueIn;
	DWORD				m_dwStreamBufferSize;

	HWND				m_hwndID;
	UINT				m_uMsgID;
	WORD				m_wNetworkId;
	BYTE				m_byConnectionState;
	BOOL				m_bConnected;
	WORD				m_wSentMsgID;
	WORD				m_wReceMsgID;
	BYTE				m_bySourceNode;
	BYTE				m_byDestNode;
	BYTE				m_bySourceEnvPrgNumber;
	BYTE				m_byDestinationEnvPrgNumber;
	COLORREF			m_crColor;

private:
	BOOL				m_bExcludeNODEControl;
	BOOL				m_bExcludeMSGPRGControl;
	BOOL				m_bExcludeMSGPacking;
	BOOL				m_bSendAlwaysKeepAlive;
	CString				m_sChannel;

	BOOL				m_bDoNotSendSTDKeepAlive;
	WORD				m_wKeepAliveMsgID;

	BOOL				m_bSendTelegramAtFixedLength;
	DWORD				m_dwFixedTelegramLength;

	BOOL				m_bLimitTelegramLength;
	DWORD				m_dwLimitedTelegramLength;

	BOOL				m_bAlterMsgId;
	WORD				m_wAlterMsgId;

	SYSTEMTIME			m_csSystemTime;
	CString				m_sString;
	CFile				m_TraceFile;


protected:
	BOOL				m_bJustConnected;
	DWORD				m_dwTotalMsgSize;
	DWORD				m_dwTotalDataSize;
	LPBYTE				m_pOriginOfBuffer;
	LPBYTE				m_pMsgBuffer;
	STRUCT_TCPIPENVELOPE	m_csSTRUCT_TCPIPENVELOPE;
	STRUCT_TCPIPTAIL		m_csSTRUCT_TCPIPTAIL;

	inline void PrepareENVELOPE(LPBYTE pBuffer);
	inline void PrepareDATA(DWORD dwBufferLen);
	inline void PrepareTAIL(void);
	inline BOOL CheckAllMessages(void);
	inline void NotifyKeepAliveMsg(void);

	#pragma pack(1)
	typedef struct _STRUCT_INCOMINGMSG
	{
		int		nPhase;
		DWORD	dwStreamLength;
		DWORD	dwExpectedMsgSize;
		DWORD	dwExpectedMsgIndex;
		STRUCT_TCPIPENVELOPE	csSTRUCT_TCPIPENVELOPE;
		STRUCT_TCPIPTAIL		csSTRUCT_TCPIPTAIL;
	} STRUCT_INCOMINGMSG;
	STRUCT_INCOMINGMSG	m_csSTRUCT_INCOMINGMSG;

	STRUCT_TCPIPMSG	m_csSTRUCT_TCPIPMSGOUT;

	CTimer55ms			m_t55msRXMessageTimeOutTimer;

	inline void	CheckIncomingMessages(LPBYTE pData,DWORD dwDataLen);
	inline void	ResetIncomingMessage(void);
};

inline void	CTCPIPServerClientFDXGT::ResetIncomingMessage(void)
{
	::ZeroMemory(&m_csSTRUCT_INCOMINGMSG,sizeof(m_csSTRUCT_INCOMINGMSG));
	m_t55msRXMessageTimeOutTimer.Stop();
}

inline void CTCPIPServerClientFDXGT::CheckIncomingMessages(LPBYTE pData,DWORD dwDataLen)
{
//TRACE(_T("Msg Processed: Len %d\n"),dwDataLen);

	for(DWORD dwIx=0;dwIx<dwDataLen;dwIx++)
	{
		switch(m_csSTRUCT_INCOMINGMSG.nPhase)
		{
		case 0:  // Wait SOM
			m_t55msRXMessageTimeOutTimer.Start();
			m_csSTRUCT_INCOMINGMSG.dwStreamLength		= 0;
			m_csSTRUCT_INCOMINGMSG.dwExpectedMsgSize	= 0;
			m_csSTRUCT_INCOMINGMSG.dwExpectedMsgIndex	= 0;
			m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPENVELOPE.bySOM = (BYTE) pData[dwIx];
			if(m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPENVELOPE.bySOM != TCPIPENVELOPE_SOM)
				break;
			m_pMsgTCPIP[m_csSTRUCT_INCOMINGMSG.dwStreamLength++] = (BYTE) pData[dwIx];
			m_csSTRUCT_INCOMINGMSG.nPhase++;
			m_csSTRUCT_INCOMINGMSG.dwExpectedMsgSize	= (DWORD) sizeof(m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPENVELOPE);
			m_csSTRUCT_INCOMINGMSG.dwExpectedMsgIndex	= 1;
			break;
		case 1:	// Wait ENVELOPE
			m_pMsgTCPIP[m_csSTRUCT_INCOMINGMSG.dwStreamLength++] = (BYTE) pData[dwIx];
			if(++m_csSTRUCT_INCOMINGMSG.dwExpectedMsgIndex < m_csSTRUCT_INCOMINGMSG.dwExpectedMsgSize)
				break;
			::CopyMemory(&m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPENVELOPE,m_pMsgTCPIP,sizeof(m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPENVELOPE));
			m_csSTRUCT_INCOMINGMSG.dwExpectedMsgSize = m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPENVELOPE.dwMessagesLENGTH + (DWORD) sizeof(m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPTAIL);
			if(m_csSTRUCT_INCOMINGMSG.dwExpectedMsgSize >= m_dwStreamBufferSize)
			{
				ResetIncomingMessage();
				break;
			}
			m_csSTRUCT_INCOMINGMSG.nPhase++;
			m_csSTRUCT_INCOMINGMSG.dwExpectedMsgIndex = 0;
			break;
		case 2:	// Wait TELEGRAM + TAIL
			m_pMsgTCPIP[m_csSTRUCT_INCOMINGMSG.dwStreamLength++] = (BYTE) pData[dwIx];
			if(++m_csSTRUCT_INCOMINGMSG.dwExpectedMsgIndex < m_csSTRUCT_INCOMINGMSG.dwExpectedMsgSize)
				break;
			::CopyMemory(&m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPTAIL,&m_pMsgTCPIP[m_csSTRUCT_INCOMINGMSG.dwStreamLength-sizeof(m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPTAIL)],sizeof(m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPTAIL));
			if(m_csSTRUCT_INCOMINGMSG.csSTRUCT_TCPIPTAIL.byEOM == TCPIPENVELOPE_EOM)
			{
				VERIFY(m_FlatQueueRECEIVEDSTREAM.PutItem((LPBYTE) m_pMsgTCPIP,m_csSTRUCT_INCOMINGMSG.dwStreamLength) == QUEUEGT_OK);
			}
			ResetIncomingMessage();
			break;
		}
	}
}

inline void CTCPIPServerClientFDXGT::PrepareENVELOPE(LPBYTE pBuffer)
{
	m_dwTotalMsgSize	= 0;
	m_dwTotalDataSize	= 0;
	m_pOriginOfBuffer   = pBuffer;
	m_pMsgBuffer		= pBuffer;

	::ZeroMemory(&m_csSTRUCT_TCPIPENVELOPE,sizeof(m_csSTRUCT_TCPIPENVELOPE));
	m_csSTRUCT_TCPIPENVELOPE.bySOM			= TCPIPENVELOPE_SOM;
	m_csSTRUCT_TCPIPENVELOPE.bySourceNode	= m_bySourceNode;
	m_csSTRUCT_TCPIPENVELOPE.byDestNode		= m_byDestNode;
	m_csSTRUCT_TCPIPENVELOPE.byProg			= m_bySourceEnvPrgNumber;

	m_dwTotalMsgSize += (DWORD) sizeof(m_csSTRUCT_TCPIPENVELOPE);
	m_pMsgBuffer	 += sizeof(m_csSTRUCT_TCPIPENVELOPE);;

	m_csSTRUCT_TCPIPENVELOPE.dwMessagesLENGTH	= m_dwTotalDataSize;
}

inline void CTCPIPServerClientFDXGT::PrepareDATA(DWORD dwBufferLen)
{
	DWORD	dwFirstItemSize;
	//
	if(m_bDoNotSendSTDKeepAlive == FALSE && m_bySourceEnvPrgNumber == 0)
		return;

	while(m_FlatQueueOut.GetCount())
	{
		// Get size of first item available in queue
		dwFirstItemSize = m_FlatQueueOut.GetFirstItemSize();
		if(dwFirstItemSize <= (DWORD) sizeof(STRUCT_TCPIPMSG))
		{
			if((m_dwTotalDataSize + dwFirstItemSize) < (dwBufferLen - (DWORD) (sizeof(STRUCT_TCPIPENVELOPE) + sizeof(STRUCT_TCPIPTAIL))) )
			{
				// Remove It From Queue
				if(m_FlatQueueOut.GetItem((LPBYTE)m_pMsgTCPIP,TRUE,NULL) != QUEUEGT_OK)
					VERIFY(FALSE);
				::CopyMemory(m_pMsgBuffer,m_pMsgTCPIP,dwFirstItemSize);
				m_pMsgBuffer += dwFirstItemSize;
				m_dwTotalDataSize += dwFirstItemSize;
				// Update Number Of Messages In Envelope
				m_csSTRUCT_TCPIPENVELOPE.wNumberOfMESSAGES++;
				//
				//
				m_wSentMsgID = ((STRUCT_TCPIPMSG*) m_pMsgTCPIP)->csSTRUCT_TCPIPMSGHEADER.wMsgID;
			}
			else
				break;
		}
		else
		{
			// Remove All Msg From Queue
			m_FlatQueueOut.Empty();
			TRACE(_T("CTCPIPServerClientFDXGT (%s) - Wrong Size %d While Sending Message\n"),m_sChannel,dwFirstItemSize);
			break;
		}
		//
		if(m_bExcludeMSGPacking == TRUE)
			break;
		if(m_bSendTelegramAtFixedLength == TRUE)
			break;
		if(m_bLimitTelegramLength == TRUE)
		{
			if(m_dwTotalDataSize >= m_dwLimitedTelegramLength)
				break;
		}
		
	}
}

inline void CTCPIPServerClientFDXGT::PrepareTAIL(void)
{
	//
	// Update Number Of Messages In Envelope
	//
	m_dwTotalMsgSize += m_dwTotalDataSize;

	m_csSTRUCT_TCPIPENVELOPE.dwMessagesLENGTH	= m_dwTotalDataSize;

	::CopyMemory(m_pOriginOfBuffer,&m_csSTRUCT_TCPIPENVELOPE,sizeof(m_csSTRUCT_TCPIPENVELOPE));
	//
	// Copy TAIL
	//
	::ZeroMemory(&m_csSTRUCT_TCPIPTAIL,sizeof(m_csSTRUCT_TCPIPTAIL));
	m_csSTRUCT_TCPIPTAIL.byEOM = TCPIPENVELOPE_EOM;
	::CopyMemory(m_pMsgBuffer,&m_csSTRUCT_TCPIPTAIL,sizeof(m_csSTRUCT_TCPIPTAIL));
	m_dwTotalMsgSize += (DWORD) sizeof(m_csSTRUCT_TCPIPTAIL);

	if(++m_bySourceEnvPrgNumber == 0)
		m_bySourceEnvPrgNumber = 1;
}

inline BOOL CTCPIPServerClientFDXGT::CheckAllMessages(void)
{
	STRUCT_TCPIPENVELOPE*	pcsSTRUCT_TCPIPENVELOPE;
	STRUCT_TCPIPMSG*		pcsSTRUCT_TCPIPMSG;
	STRUCT_TCPIPTAIL*		pcsSTRUCT_TCPIPTAIL;

	LPBYTE					pMsgStream;
	DWORD					dwTotLen;

	pMsgStream				= m_pMsgENQUEUED;
	pcsSTRUCT_TCPIPENVELOPE	= (STRUCT_TCPIPENVELOPE*)pMsgStream;
	pMsgStream				+= sizeof(STRUCT_TCPIPENVELOPE);
	dwTotLen				= 0;
	for(WORD wNumMsg=0;wNumMsg<pcsSTRUCT_TCPIPENVELOPE->wNumberOfMESSAGES;wNumMsg++)
	{
		pcsSTRUCT_TCPIPMSG = (STRUCT_TCPIPMSG*) pMsgStream;
		//
		// Verify Message Size
		//
		DWORD dwMsgSize = (DWORD) sizeof(STRUCT_TCPIPMSGHEADER) + (DWORD) pcsSTRUCT_TCPIPMSG->csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH;
		if(dwMsgSize > (DWORD) sizeof(STRUCT_TCPIPMSG))
		{
			TRACE(_T("CTCPIPServerClientFDXGT (%s) - Wrong Size While Receiving Message ID: %d Size: %d \n"),m_sChannel,pcsSTRUCT_TCPIPMSG->csSTRUCT_TCPIPMSGHEADER.wMsgID,dwMsgSize);
			return FALSE;
		}
		pMsgStream	+= dwMsgSize;
		dwTotLen	+= dwMsgSize;
	}

	// Check Total Size
	if(pcsSTRUCT_TCPIPENVELOPE->dwMessagesLENGTH != dwTotLen)
		return FALSE;

	// Check EOM
	pcsSTRUCT_TCPIPTAIL = (STRUCT_TCPIPTAIL*)pMsgStream;

	if(pcsSTRUCT_TCPIPTAIL->bySpare != 0 || pcsSTRUCT_TCPIPTAIL->wSpare != 0 || pcsSTRUCT_TCPIPTAIL->byEOM != TCPIPENVELOPE_EOM)
		return FALSE;
	
	return TRUE;
}

inline void CTCPIPServerClientFDXGT::NotifyKeepAliveMsg(void)
{
	STRUCT_TCPIPMSG	csSTRUCT_TCPIPMSG;


	::ZeroMemory(&csSTRUCT_TCPIPMSG,sizeof(csSTRUCT_TCPIPMSG));

	csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID		= m_wKeepAliveMsgID;
	csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH	= 0;

	if(m_pFlatQueueIn != NULL)
		m_pFlatQueueIn->PutItem((LPBYTE) &csSTRUCT_TCPIPMSG,(DWORD) (sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
}
