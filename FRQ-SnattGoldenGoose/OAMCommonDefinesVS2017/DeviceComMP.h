//
//	Class:		CDeviceComMP
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 6.0
//
//	Version:	See GetVersionC() or GetVersionI()
//
//	Created:	19/November/1999
//	Updated:	18/05/2004
//
//	Author:		Marco Piazza	marco.piazza@cml-ht.it
//
//
#ifndef _DEVICECOMMP_H_
#define _DEVICECOMMP_H_
//
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
//
// Errors Code
#define DEVICECOM_OK                  0
#define DEVICECOM_NOINIT              1
#define DEVICECOM_ALREADYINIT         2
#define DEVICECOM_WRONGCOMFLAGS       3
#define DEVICECOM_EVENTCREATIONERROR  4
#define DEVICECOM_WRONGCOMPORT        5
#define DEVICECOM_THRCREATIONERROR    6
#define DEVICECOM_ALLOCERROR          7
#define DEVICECOM_BUFFERTOOBIG        8
#define DEVICECOM_THRPARAMERROR       9
//
// Thread steps
#define DEVICECOM_THRSTEP_WAIT_RX     1
#define DEVICECOM_THRSTEP_RX          2
#define DEVICECOM_THRSTEP_TX          3
//
typedef struct _STRUCT_DCOM_THR_PARAM { // dtp
	//
	void*  pParent;        // Pointer to thread's class
	DWORD  dwThrExitCode;  // Error exit code for the thread
	DWORD  dwThreadID;     // ID from _beginthreadex
	HANDLE hExitEv;        // Event to stop the thread
	HANDLE hServiceEv;     // Event to check the thread start
	HANDLE hDeviceCom;     // Handler to the HW COM port
	HANDLE hThread;        // Handler to thread
	DWORD  dwBufferSize;   // Buffer size
	DWORD  dwPollingTime;  // To Call On TomeOut
	//
} STRUCT_DCOM_THR_PARAM;
//
#define SIZE_STRUCT_DCOM_THR_PARAM sizeof(_STRUCT_DCOM_THR_PARAM)
//
class CDeviceComMP  
{
public:
	//
	CDeviceComMP();
	virtual ~CDeviceComMP();
	//
	static const short GetVersionI(){return 12;};
	static const char* GetVersionC(){return "1.2";};
	//
	BOOL Init();
	BOOL Create(BYTE byComPort,DWORD dwBaudRate,char* szComFlags,DWORD dwBufferSize,DWORD dwPollingTime,DWORD dwRtsControl);
	BOOL Destroy(DWORD dwMilliseconds);
	//
	DWORD GetLastError();
	LPSTR GetLastErrorStr();
    DWORD SetLastError(DWORD dwError);
	//
private:
	//
	BOOL	m_bInit;           
	DWORD	m_dwErrorCode;
	STRUCT_DCOM_THR_PARAM m_dtpThreadParam;
	//
	static unsigned int __stdcall ThreadSerial(LPVOID pParam);
	//
protected:
	//
	virtual BOOL	OnTimeout() = 0;
	virtual void	OnReceive(LPBYTE lpbyData, DWORD dwDataLen) = 0;
	virtual void	OnError(DWORD dwError){};
	virtual DWORD	OnPrepareMsgToSend(LPBYTE lpbyBuffer, DWORD dwBufferLen) = 0;
	virtual void	OnEventCTS(void){};
};
//
#endif // !defined(AFX_DEVICECOM_H__B7601DEE_9B8D_11D3_BCD4_0060086296AC__INCLUDED_)
