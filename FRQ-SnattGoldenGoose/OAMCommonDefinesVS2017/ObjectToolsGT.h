// NAME:					CObjectToolsGT.h					//
// LAST MODIFICATION DATE:	24-Jul-2018							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Object Tools (NO GLOBAL ISTANCE)	//
//////////////////////////////////////////////////////////////////
#pragma once



class CObjectToolsGT
{
public:
	CObjectToolsGT(void);
	virtual ~CObjectToolsGT(void);

	char*	FromUINT16ToAsciiZ(UINT16 u16Value);
	char*	FromUINT32ToAsciiZ(UINT32 u32Value);
	char*	FromUINT64ToAsciiZ(UINT64 u64Value);

protected:
	int		m_nIxArrayBuffer;
	enum MyPrivateEnum
	{
		ARRAY_ELEMENTS = 255,
		DIM_ASCII_UINT16 = 5,
		DIM_ASCII_UINT32 = 10,
		DIM_ASCII_UINT64 = 20,
		DIM_BUFFER_SIZE	= 30 
	};
	char	m_szArrayBuffer[ARRAY_ELEMENTS][DIM_BUFFER_SIZE];

private:
	void	_SetArrayIndex(void);
};
