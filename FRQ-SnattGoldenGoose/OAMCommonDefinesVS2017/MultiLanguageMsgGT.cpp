//////////////////////////////////////////////////////////////////
//																//
// NAME:					CMultiLanguageMsgGT.cpp				//
// LAST MODIFICATION DATE:	22-May-2015							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Application							//
//////////////////////////////////////////////////////////////////
#include "pch.h"
#include "MultiLanguageMsgGT.h"
#include "XMLNodeWrapper.h"


CMultiLanguageMsgGT::CMultiLanguageMsgGT(void)
{
	::CoInitialize(NULL);
	pcsGUIMSG = NULL;
	m_DefaultLanguage = MULTILANGUAGEMSG_ZERO;
	m_bShowMsgId = FALSE;
}

CMultiLanguageMsgGT::~CMultiLanguageMsgGT(void)
{
	Destroy();
	pcsGUIMSG = NULL;
	::CoUninitialize();
}

BOOL CMultiLanguageMsgGT::Create(LPCTSTR lpszFilePath)
{
	CXmlDocumentWrapper m_xmlDoc;
	CString	sString;
	sString.Format(_T("%s"),lpszFilePath);
	if(_taccess(sString,0) != 0)
	{
		TRACE(_T("CMultiLanguageMsgGT::Create - Error Opening File %s \n"),sString);
		return FALSE;
	}
	try
	{
		if(m_xmlDoc.Load(sString) == TRUE)
		{
			m_bParsingSuccess = TRUE;
			bStringTableNodeHasBeenFound = FALSE;
			ParseXMLFile(m_xmlDoc.AsNode());
		}
	}
	catch(...)
	{
		TRACE(_T("CMultiLanguageMsgGT::Create - CATCH DETECTED %s \n"),sString);
		Destroy();
		return FALSE;
	}

	if (m_bParsingSuccess == FALSE)
	{
		m_xmlDoc.GetLastError();
		Destroy();
	}

	return 	m_bParsingSuccess;
}

void CMultiLanguageMsgGT::SetDefaultLanguage(ENUM_MULTILANGUAGEMSG eLANGUAGE)
{
	m_DefaultLanguage = eLANGUAGE;
}

void CMultiLanguageMsgGT::SetShowMsgId(BOOL bShowMsgId)
{
	m_bShowMsgId = bShowMsgId;
}

_TCHAR* CMultiLanguageMsgGT::GetMsgId(int nMsgId)
{
	STRUCT_GUIMSG*	pcsLoopGUIMSG;
	pcsLoopGUIMSG = pcsGUIMSG;

	if(pcsGUIMSG == NULL)
		return _T("MultiLanguageMsgGT::GetMsgId - Message Table Not Stored");
/*
	if(nMsgId <= 0)
		return _T("MultiLanguageMsgGT::GetMsgId - MsgId Must Be Greater Than ZERO");
	if(nMsgId >= m_NumNodes)
		return _T("MultiLanguageMsgGT::GetMsgId - MsgId greter Than Stored Messages");
	if(nMsgId > 0 && nMsgId < m_NumNodes)
		nMsgId--;
	else
		return _T("MultiLanguageMsgGT::GetMsgId - Invalid MsgId");
*/
	m_sReturnMsg.Format(_T("MultiLanguageMsgGT::GetMsgId - MsgId %d Not Found"),nMsgId);
	for(int nNumNode=0;nNumNode<m_NumNodes;nNumNode++)
	{
		if(pcsLoopGUIMSG->nMsgID == nMsgId)
		{
			for(int nLoop=0;nLoop<MAX_LANGUAGES;nLoop++)
			{
				if(nLoop == m_DefaultLanguage)
				{
					if(pcsLoopGUIMSG->pwszMsgText[nLoop] != NULL)
					{
						if(m_bShowMsgId == FALSE)
							m_sReturnMsg.Format(_T("%s"),pcsLoopGUIMSG->pwszMsgText[nLoop]);
						else
							m_sReturnMsg.Format(_T("[%04d] %s"),nMsgId,pcsLoopGUIMSG->pwszMsgText[nLoop]);
						//
						nNumNode = m_NumNodes;
						break;
					}
				}
			}
		}
		pcsLoopGUIMSG++;
	}
	return (_TCHAR*)(LPCTSTR)m_sReturnMsg;
}

_TCHAR* CMultiLanguageMsgGT::GetMsgIdByLanguage(int nMsgId,ENUM_MULTILANGUAGEMSG eLANGUAGE)
{
	STRUCT_GUIMSG*	pcsLoopGUIMSG;
	pcsLoopGUIMSG = pcsGUIMSG;

	if(pcsGUIMSG == NULL)
		return _T("MultiLanguageMsgGT::GetMsgIdByLanguage - Message Table Not Stored");
/*
	if(nMsgId <= 0)
		return _T("MultiLanguageMsgGT::GetMsgId - MsgId Must Be Greater Than ZERO");
	if(nMsgId >= m_NumNodes)
		return _T("MultiLanguageMsgGT::GetMsgId - MsgId greter Than Stored Messages");
	if(nMsgId > 0 && nMsgId < m_NumNodes)
		nMsgId--;
	else
		return _T("MultiLanguageMsgGT::GetMsgId - Invalid MsgId");
*/
	m_sReturnMsg.Format(_T("MultiLanguageMsgGT::GetMsgIdByLanguage - MsgId %d Not Found"),nMsgId);
	for(int nNumNode=0;nNumNode<m_NumNodes;nNumNode++)
	{
		if(pcsLoopGUIMSG->nMsgID == nMsgId)
		{
			for(int nLoop=0;nLoop<MAX_LANGUAGES;nLoop++)
			{
				if(nLoop == eLANGUAGE)
				{
					if(pcsLoopGUIMSG->pwszMsgText[nLoop] != NULL)
					{
						if(m_bShowMsgId == FALSE || true)
							m_sReturnMsg.Format(_T("%s"),pcsLoopGUIMSG->pwszMsgText[nLoop]);
						else
							m_sReturnMsg.Format(_T("[%04d] %s"),nMsgId,pcsLoopGUIMSG->pwszMsgText[nLoop]);
						//
						nNumNode = m_NumNodes;
						break;
					}
				}
			}
		}
		pcsLoopGUIMSG++;
	}
	return (_TCHAR*)(LPCTSTR)m_sReturnMsg;
}

void CMultiLanguageMsgGT::ParseXMLFile(IDispatch *pNode)
{
	CXmlNodeWrapper	XMLNode(pNode);
	CString	sNodeName;
	CString	sAttName;
	CString	sAttVal;
	char	szBuff[_MAX_PATH];
	
	int	nLoop;
	int nNumAtt;
	int	nAttFound=0;

	if(m_bParsingSuccess == FALSE)
		return;

	if(XMLNode.NodeType() == "element")
	{
		sNodeName = XMLNode.Name();
		TRACE(_T("Element %s \n"),sNodeName);

		if(sNodeName.CompareNoCase(_T("MsgTable")) == 0)
		{
			m_NumCurrentNode = -1;
			m_NumNodes = XMLNode.NumNodes();
			if(m_NumNodes > 1000)
			{
				TRACE(_T("CMultiLanguageMsgGT::ParseXMLFile - MORE THAN 1000 NODES HAS BEEN DEFINED %d \n"),m_NumNodes);
				m_bParsingSuccess = FALSE;
				return;
			}
			// Allocate Space
			UINT_PTR u64NumNodes = (UINT_PTR) m_NumNodes;
			u64NumNodes *= (UINT_PTR) sizeof(STRUCT_GUIMSG);
			pcsGUIMSG = (STRUCT_GUIMSG*) ::GlobalAlloc(GPTR,u64NumNodes);
			if(pcsGUIMSG == NULL)
			{
				TRACE(_T("CMultiLanguageMsgGT::ParseXMLFile - CANNOT ALLOCATE MEMORY %d \n"),u64NumNodes);
				m_bParsingSuccess = FALSE;
				return;
			}
			bStringTableNodeHasBeenFound = TRUE;
		}

		if(sNodeName.CompareNoCase(_T("MsgId")) == 0)
		{
			if(bStringTableNodeHasBeenFound == FALSE)
			{
				TRACE(_T("CMultiLanguageMsgGT::ParseXMLFile - Found 'MsgId' Node WITHOUT 'MsgTable' Node \n"));
				m_bParsingSuccess = FALSE;
				return;
			}
			nNumAtt = XMLNode.NumAttributes();
			VERIFY(nNumAtt == 1);
			sAttName = XMLNode.GetAttribName(0);
			sAttVal  = XMLNode.GetAttribVal(0);
			::WideCharToMultiByte(CP_ACP,NULL,sAttVal,-1,szBuff,sizeof(szBuff),NULL,NULL);
			int nMsgID = (int) atoi(szBuff);
			m_NumCurrentNode++;
			(pcsGUIMSG+m_NumCurrentNode)->nMsgID = nMsgID;
		}

		if(sNodeName.CompareNoCase(_T("Languages")) == 0)
		{
			if(bStringTableNodeHasBeenFound == FALSE)
			{
				TRACE(_T("CMultiLanguageMsgGT::ParseXMLFile - Found 'Languages' Node WITHOUT 'MsgTable' Node \n"));
				m_bParsingSuccess = FALSE;
				return;
			}
			nNumAtt = XMLNode.NumAttributes();
			VERIFY(nNumAtt == 2);
			int	nFrom=0;
			nAttFound=0;
			int	nLanguageId;
			for(nLoop=0;nLoop<nNumAtt;nLoop++)
			{
				sAttName = XMLNode.GetAttribName(nLoop);
				sAttVal  = XMLNode.GetAttribVal(nLoop);
				::WideCharToMultiByte(CP_ACP,NULL,sAttVal,-1,szBuff,sizeof(szBuff),NULL,NULL);
				if(sAttName.CompareNoCase(_T("LanguageID")) == 0)
				{
					::WideCharToMultiByte(CP_ACP,NULL,sAttVal,-1,szBuff,sizeof(szBuff),NULL,NULL);
					nLanguageId = (int) atoi(szBuff);
					if(nLanguageId < 0 || nLanguageId > MAX_LANGUAGES)
					{
						TRACE(_T("CMultiLanguageMsgGT::ParseXMLFile - 'LanguageID' Attribute Non Valid %d \n"),nLanguageId);
						m_bParsingSuccess = FALSE;
						return;
					}
					else
						nAttFound++;
				}
				if(sAttName.CompareNoCase(_T("TextID")) == 0)
				{
					nFrom = atoi(szBuff);
					nAttFound++;
				}
			}
			if(nAttFound != nNumAtt)
			{
				TRACE(_T("CMultiLanguageMsgGT::ParseXMLFile - Wrong Attribute Number In 'Languages' Node %d \n"),nAttFound);
				m_bParsingSuccess = FALSE;
				return;
			}
			else
			{
				int nLen = 1 + sAttVal.GetLength();
				(pcsGUIMSG+m_NumCurrentNode)->pwszMsgText[nLanguageId] = (_TCHAR*) new _TCHAR[nLen];
				::ZeroMemory((pcsGUIMSG+m_NumCurrentNode)->pwszMsgText[nLanguageId],nLen*sizeof(_TCHAR));
				::CopyMemory((pcsGUIMSG+m_NumCurrentNode)->pwszMsgText[nLanguageId],(_TCHAR*)(LPCTSTR)sAttVal,nLen*sizeof(_TCHAR));
			}
		}
	}
	else
	{
		CString str;
		str = XMLNode.GetText();
		TRACE(_T("NOT Element %s \n"),str);
	}
	for(int nNumNodes=0;nNumNodes<XMLNode.NumNodes();nNumNodes++)
	{
		ParseXMLFile(XMLNode.GetNode(nNumNodes));
	}
}
