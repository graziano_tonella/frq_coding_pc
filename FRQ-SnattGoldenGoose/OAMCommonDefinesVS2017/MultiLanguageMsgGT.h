//////////////////////////////////////////////////////////////////
//																//
// NAME:					CMultiLanguageMsgGT.h				//
// LAST MODIFICATION DATE:	22-May-2015							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Include								//
//////////////////////////////////////////////////////////////////
#pragma once
class CMultiLanguageMsgGT
{
public:
	CMultiLanguageMsgGT(void);
	virtual ~CMultiLanguageMsgGT(void);

	BOOL	Create(LPCTSTR lpszFilePath);

	enum ENUM_MULTILANGUAGEMSG
	{
		MULTILANGUAGEMSG_ZERO=0,
		MULTILANGUAGEMSG_ONE,
		MULTILANGUAGEMSG_TWO,
		MULTILANGUAGEMSG_THREE,
		MULTILANGUAGEMSG_FOUR,
		MULTILANGUAGEMSG_FIVE,
		MULTILANGUAGEMSG_SIX,
		MULTILANGUAGEMSG_SEVEN,
		MULTILANGUAGEMSG_EIGHT,
		MULTILANGUAGEMSG_NINE,
	};
	void	SetDefaultLanguage(ENUM_MULTILANGUAGEMSG eLANGUAGE);
	void	SetShowMsgId(BOOL bShowMsgId);
	_TCHAR*	GetMsgId(int nMsgId);
	_TCHAR*	GetMsgIdByLanguage(int nMsgId,ENUM_MULTILANGUAGEMSG eLANGUAGE);

private:
	BOOL	m_bShowMsgId;
	enum{MAX_LANGUAGES=MULTILANGUAGEMSG_NINE+1};
	#pragma pack(1)
	typedef struct _STRUCT_GUIMSG
	{
		int		nMsgID;
		_TCHAR*	pwszMsgText[MAX_LANGUAGES];
	} STRUCT_GUIMSG;
	#pragma pack()
	STRUCT_GUIMSG*	pcsGUIMSG;

	BOOL	m_bParsingSuccess;
	BOOL	bStringTableNodeHasBeenFound;
	void	ParseXMLFile(IDispatch *pNode);
	int			m_NumNodes;
	int			m_NumCurrentNode;
	int			m_DefaultLanguage;
	CString		m_sReturnMsg;
	
	void inline Destroy(void);
};

void inline CMultiLanguageMsgGT::Destroy(void)
{
	STRUCT_GUIMSG*	pcsLoopGUIMSG;
	pcsLoopGUIMSG = pcsGUIMSG;

	if(pcsGUIMSG != NULL)
	{
		for(int nNumNode=0;nNumNode<m_NumNodes;nNumNode++)
		{
			for(int nLoop=0;nLoop<MAX_LANGUAGES;nLoop++)
			{
				if(pcsLoopGUIMSG->pwszMsgText[nLoop] != NULL)
					delete pcsLoopGUIMSG->pwszMsgText[nLoop];
			}
			pcsLoopGUIMSG++;
		}
		::GlobalFree(pcsGUIMSG);
		pcsGUIMSG = NULL;
	}
}

