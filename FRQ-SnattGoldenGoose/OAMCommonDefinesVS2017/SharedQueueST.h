//
//	Class:		CSharedQueueST
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 5.0
//				Visual C++ 6.0
//
//	Version:	See GetVersionC() or GetVersionI()
//
//	Created:	06/July/1999
//	Updated:	17/November/1999
//
//	Author:		Davide Calabro'		davide_calabro@yahoo.com
//
#ifndef _SHAREDQUEUEST_H_
#define _SHAREDQUEUEST_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define QUEUEST_OK				0
#define QUEUEST_NOINIT			1
#define QUEUEST_ALREADYINIT		2
#define QUEUEST_EMPTY			3
#define QUEUEST_BADPARAM		4
#define QUEUEST_NOMEMORY		5
#define QUEUEST_FULL			6
#define QUEUEST_BUFFERTOOBIG	7

#define SIZE_DWORD				sizeof(DWORD)

class CSharedQueueST  
{
public:
	DWORD GetFirstItemSize();
	CSharedQueueST();
	virtual ~CSharedQueueST();

	DWORD Create(DWORD dwMaxItems, DWORD dwItemSize, HWND hWndParent = NULL, UINT nMsgParent = NULL, HANDLE hEventParent = NULL, HANDLE hThreadHandle = NULL);
	BOOL IsCreated();

	void Destroy();

	BOOL IsFull();
	BOOL WasFull();

	DWORD GetItemSize();
	
	DWORD PutItem(char* szpBuffer, DWORD dwBufferSize = 0);
	DWORD GetItem(char* szpBuffer, BOOL bRemove = TRUE, DWORD* dwpNumBytesCopied = NULL);

	DWORD GetCount();
	DWORD GetPeak();

	DWORD Empty();

	static short GetVersionI()		{return 21;}
	static LPCTSTR GetVersionC()	{return (LPCTSTR)_T("2.1");}

private:
	LPBYTE	m_pData;
	LPBYTE	m_pTop;
	LPBYTE	m_pBottom;
	LPBYTE	m_pPut;
	LPBYTE	m_pGet;

	DWORD				m_dwMaxItems;	// Max number of items can fit in queue
	DWORD				m_dwItemSize;	// Each item's max. size
	DWORD				m_dwNumItems;	// Number of items currently in queue
	DWORD				m_dwMaxHit;		// Max number of items was in queue

	BOOL				m_bWasFull;		// Queue was full

	CRITICAL_SECTION	m_cs_Wait;

	// Handle of the destination window that will receive the
	// notification message
	HWND m_hWndParent;			// NULL if not used
	// User message sent when a new item is inserted in queue
	// 0 (zero) if not used
	UINT m_nMsgParent;			// 0 (zero) if not used
	// Event that will be raised when a new item is inserted in queue
	HANDLE m_hEventParent;		// NULL if not used
	// Thread that will be resumed when a new item is inserted in queue
	HANDLE m_hThreadHandle;		// NULL if not used
};

#endif
