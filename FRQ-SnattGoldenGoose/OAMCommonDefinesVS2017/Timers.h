// Timers.h: interface for the CTimer55ms class.
//
// Author:		Giuseppe Alessio
//
//////////////////////////////////////////////////////////////////////

#ifndef _TIMERS_H_
#define _TIMERS_H_

#ifdef _CONSOLE
#include <windows.h>
#endif

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CTimer55ms  
{
public:
	CTimer55ms() {dwTempo=0; bSet=FALSE; }
	void  Clear();
	void  Start();
	DWORD Read();
	void  Stop();
	BOOL  IsStart();
	BOOL  IsElapsed(UINT delayMSec);

private:
	DWORD dwLast;
	DWORD dwTempo;
	BOOL  bSet;
};

#endif
