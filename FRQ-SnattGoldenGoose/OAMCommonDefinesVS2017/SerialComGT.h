//////////////////////////////////////////////////////////////////
//																//
//																//
// NAME:					SerialComGT.h						//
//																//
// LAST MODIFICATION DATE:	04/07/2008							//
//																//
// AUTHOR:					Graziano Tonella					//
//																//
// FILE TYPE:				Include C++							//
//																//
// PURPOSE:					Serial Port Device Management		//
//															    //
//																//
//////////////////////////////////////////////////////////////////
//
//
//
#ifndef __SERIALCOMGT_H__
#define __SERIALCOMGT_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
//

#include "DeviceComMP.h"
#include "Timers.h"
#include "CMLDefines.h"
#include "x64SharedFlatQueueGT.h"

class CSerialComGT : public CDeviceComMP
{
public:
	CSerialComGT();
	virtual ~CSerialComGT();
	//
	DWORD	Destroy(DWORD dwMilliseconds);
	BOOL	Create(BYTE byComPort,DWORD dwBaudRate,char* szComFlags,DWORD dwBufferSize,DWORD dwPollingTime,DWORD dwRtsControl,CSharedFlatQueueGT *pFlatQueueIn,DWORD dwSizeFlatQueueOut);

	BOOL	SetRXProperties(BYTE bySOM,BYTE byEOM,BYTE byMaxLen,WORD wTimeOut,WORD wMsgID);

	CSharedFlatQueueGT	m_FlatQueueOut;

	inline void	EnableReceive(WORD wMsgID=0);
	inline void	DisableReceive(WORD wMsgID=0);
	inline BOOL	IsReceivingEnabled(void);

private:
	BOOL	OnTimeout();
	void	OnReceive(LPBYTE lpbyData, DWORD dwDataLen);
	void	OnError(DWORD dwError);
	DWORD	OnPrepareMsgToSend(LPBYTE lpbyBuffer, DWORD dwBufferLen);

	CTimer55ms		m_tTimer55ms;

	LPBYTE				m_pMsgCML;
	CSharedFlatQueueGT	*m_pFlatQueueIn;

	DWORD	m_dwBufferSize;

	WORD	m_wNumBytes;
	BOOL	m_bEnableRX;
protected:
	BYTE	m_bySOM;
	BYTE	m_byEOM;
	BYTE	m_byMaxLen;
	WORD	m_wTimeOut;
	WORD	m_wMsgID;
	UINT	m_uErrorOnPutItem;
	BOOL	m_bWaitSOM;
	BOOL	m_bWaitEOM;
	inline void	WaitMessage(void);
	STRUCT_SINGLEMSGCML		m_csSINGLEMSGCML;
	SYSTEMTIME	m_csSystemTime;
	CString		m_sString;
};

inline void CSerialComGT::EnableReceive(WORD wMsgID)
{
	m_bEnableRX	= TRUE;
	//
	if(wMsgID)
	{
		m_csSINGLEMSGCML.csHeader.wMsgId	 = wMsgID;
		m_csSINGLEMSGCML.csHeader.wDataLen	 = 0;
		if(m_pFlatQueueIn->PutItem((LPBYTE)&m_csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+m_csSINGLEMSGCML.csHeader.wDataLen)) != QUEUEGT_OK)
			m_uErrorOnPutItem++;
	}
}

void CSerialComGT::DisableReceive(WORD wMsgID)
{
	m_bEnableRX	= FALSE;
	//
	m_tTimer55ms.Stop();
	//
	if(wMsgID)
	{
		m_csSINGLEMSGCML.csHeader.wMsgId	 = wMsgID;
		m_csSINGLEMSGCML.csHeader.wDataLen	 = 0;
		if(m_pFlatQueueIn->PutItem((LPBYTE)&m_csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+m_csSINGLEMSGCML.csHeader.wDataLen)) != QUEUEGT_OK)
			m_uErrorOnPutItem++;
	}
}

BOOL CSerialComGT::IsReceivingEnabled(void)
{
	return m_bEnableRX;
}

inline void	CSerialComGT::WaitMessage(void)
{
	m_wNumBytes	= 0;
	m_bWaitSOM	= (BOOL) (m_bySOM==0?FALSE:TRUE);
	m_bWaitEOM	= (BOOL) (m_byEOM==0?FALSE:TRUE);
	//
	m_tTimer55ms.Stop();
}
#endif
