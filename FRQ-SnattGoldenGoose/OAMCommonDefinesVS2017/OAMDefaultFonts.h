#pragma once
class COAMDefaultFonts
{
public:
	COAMDefaultFonts(void);
	~COAMDefaultFonts(void);

	class CFont*	GetXXXS(void);
	class CFont*	GetXXS(void);
	class CFont*	GetXS(void);
	class CFont*	GetS(void);
	class CFont*	GetM(void);
	class CFont*	GetL(void);
	class CFont*	GetXL(void);
	class CFont*	GetXXL(void);
	class CFont*	GetXXXL(void);
	class CFont*	GetEditL(void);
	class CFont*	GetEditXL(void);
	class CFont*	GetContainer(void);
	class CFont*	GetBtnOK(void);
	class CFont*	GetBatchList(void);
	class CFont*	GetAlarmOrWarningText(void);

private:
	CFont	m_fntXXXS;
	CFont	m_fntXXS;
	CFont	m_fntXS;
	CFont	m_fntS;
	CFont	m_fntM;
	CFont	m_fntL;
	CFont	m_fntXL;
	CFont	m_fntXXL;
	CFont	m_fntXXXL;
	CFont	m_fntEditL;
	CFont	m_fntEditXL;
	CFont	m_fntContainer;
	CFont	m_fntBtnOK;
	CFont	m_fntBatchList;
	CFont	m_fntAlarmOrWarningText;
};

