//////////////////////////////////////////////////////////////////
// NAME:					CSharedFlatQueue.cpp				//
// LAST MODIFICATION DATE:	30-Mar-2016							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Flat Queue Handling					//
//////////////////////////////////////////////////////////////////
#include "stdafx.h"

#include "x64SharedFlatQueueGT.H"


CSharedFlatQueueGT::CSharedFlatQueueGT()
{
	::InitializeCriticalSection(&m_cs_Wait);
	//
	m_pbyData = NULL;
	//
	Destroy();
}

CSharedFlatQueueGT::~CSharedFlatQueueGT()
{
	Destroy();
	//
	::DeleteCriticalSection(&m_cs_Wait);
}

// This function initializes the queue
//
// Parameters:
//		[in]	dwBufferSize
//				Maximum number of bytes that the queue can contains
//		[in]	hWndParent
//				Handle to a window that will receive a message each time a new item
//				is added to the queue. Can be NULL.
//		[in]	nMsgParent
//				Message to be sent to the window identified by hWndParent. Can be 0 (zero).
//		[in]	hEventParent
//				Handle of an event that will be raised each time a new item
//				is added to the queue. Can be NULL.
//		[in]	hThreadHandle
//				Handle of a thread that will be resumed each time a new item
//				is added to the queue. Can be NULL.
//
// Return value:
//		QUEUEGT_OK:				All ok
//		QUEUEGT_ALREADYINIT:	Queue already initialized
//		QUEUEGT_BADPARAM:		Input parameters not valid
//		QUEUEGT_NOMEMORY:		Cannot allocate memory
//
DWORD CSharedFlatQueueGT::Create(DWORD dwBufferSize, HWND hWndParent, UINT nMsgParent, HANDLE hEventParent, HANDLE hThreadHandle)
{
	if(m_pbyData != NULL)
		return QUEUEGT_ALREADYINIT;
	//
	if(dwBufferSize <= sizeof(DWORD))
		return QUEUEGT_BADPARAM;
	//	
	::EnterCriticalSection(&m_cs_Wait);
	//
	// Allocate memory
	//
	if((m_pbyData = (LPBYTE)::GlobalAlloc(GPTR,dwBufferSize)) == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_NOMEMORY;
	}
	//
	m_pbyTop			= m_pbyData;
	m_pbyPut			= m_pbyTop;
	m_pbyGet			= m_pbyTop;
	m_pbyBottom			= m_pbyTop + dwBufferSize;
	m_dwSpaceAvailable	= dwBufferSize;
	m_dwBufferSize		= dwBufferSize;
	m_dwMinFreeSpace	= dwBufferSize;
	m_hWndParent		= hWndParent;
	m_nMsgParent		= nMsgParent;
	m_hEventParent		= hEventParent;
	m_hThreadHandle		= hThreadHandle;
	//
	::LeaveCriticalSection(&m_cs_Wait);
	//
	return QUEUEGT_OK;
}

// This function returns if queue is initialized
//
// Return value:
//		TRUE:	Queue initialized
//		FALSE:	Queue NOT initialized
//
BOOL CSharedFlatQueueGT::IsCreated()
{
	BOOL bRetValue;

	::EnterCriticalSection(&m_cs_Wait);
	bRetValue = (m_pbyData != NULL);
	::LeaveCriticalSection(&m_cs_Wait);

	return bRetValue;
}

// This function de-initializes the queue
//
void CSharedFlatQueueGT::Destroy()
{
	::EnterCriticalSection(&m_cs_Wait);
	//
	// Free memory
	//
	if(m_pbyData != NULL)
		::GlobalFree((HGLOBAL)m_pbyData);
	//
	m_pbyData			= NULL;
	m_pbyTop			= NULL;
	m_pbyBottom			= NULL;
	m_pbyPut			= NULL;
	m_pbyGet			= NULL;
	//
	m_dwSpaceAvailable	= 0;
	m_dwBufferSize		= 0;
	m_dwNumItems		= 0;
	m_dwMaxItems		= 0;
	m_dwMinFreeSpace	= 0;
	m_hWndParent		= NULL;
	m_nMsgParent		= 0;
	m_hEventParent		= NULL;
	m_hThreadHandle		= NULL;
	//
	::LeaveCriticalSection(&m_cs_Wait);
}

// This function returns if queue has an Error
//
// Return value:
//		TRUE:	Queue has an error
//		FALSE:	Queue do not have error
//
BOOL CSharedFlatQueueGT::CheckError()
{
	BOOL bError = FALSE;


	
	if(m_pbyPut == m_pbyGet)
		if(m_dwSpaceAvailable != m_dwBufferSize)
			bError = TRUE;
	if(m_pbyPut > m_pbyGet)
		if((m_dwBufferSize - m_dwSpaceAvailable) != (DWORD) (m_pbyPut - m_pbyGet))
			bError = TRUE;
	if(m_pbyPut < m_pbyGet)
		if(m_dwSpaceAvailable != (DWORD) (m_pbyGet -m_pbyPut))
			bError = TRUE;

	if(m_dwNumItems > m_dwMaxItems)
		m_dwMaxItems = m_dwNumItems;

	if(m_dwSpaceAvailable < m_dwMinFreeSpace)
		m_dwMinFreeSpace = m_dwSpaceAvailable;

	return bError;
}

// This function returns the size of queue's item
//
// Return value:
//		DWORD:	Size of queue's bytes
//
DWORD CSharedFlatQueueGT::GetBufferSize()
{
	DWORD dwBufferSize;

	::EnterCriticalSection(&m_cs_Wait);
	dwBufferSize = m_dwBufferSize;
	::LeaveCriticalSection(&m_cs_Wait);

	return dwBufferSize;
}

// This function returns the size of queue's item
//
// Return value:
//		DWORD:	Size of Available Space in bytes
//
DWORD CSharedFlatQueueGT::GetSpaceAvailable()
{
	DWORD dwSpaceAvailable;

	::EnterCriticalSection(&m_cs_Wait);
	dwSpaceAvailable = m_dwSpaceAvailable;
	::LeaveCriticalSection(&m_cs_Wait);

	return dwSpaceAvailable;
}

// This function returns the size of the first item in queue (the item that will
// be returned on the next GetItem call)
//
// Return value:
//		The size (in bytes) of the first item in queue
//		If queue is empty or it is not initialized the return value is 0 (zero)
//
DWORD CSharedFlatQueueGT::GetFirstItemSize()
{
	DWORD	dwSize = 0;
	WORD	wIdx;
	LPBYTE	pMemRead;

	::EnterCriticalSection(&m_cs_Wait);
	//
	// If queue is initialized and one element is stored
	//
	if (m_pbyData != NULL && m_dwNumItems != 0)
	{
		for(wIdx=0,pMemRead=m_pbyGet;wIdx<sizeof(DWORD);wIdx++)
		{
			if(pMemRead >= m_pbyBottom)
				pMemRead = m_pbyTop;
			*((LPBYTE)&dwSize+wIdx) = (BYTE) *(pMemRead);
			pMemRead++;
		}
	}
	//
	::LeaveCriticalSection(&m_cs_Wait);
	//
	return dwSize;
}

// This function puts a new item in queue
//
// Parameters:
//		[in]	pbyBuffer
//				Pointer to buffer to copy from
//		[in]	dwBufferSize
//				Size of byte to copy from pszBuffer
//
// Return value:
//		QUEUEGT_OK
//			All ok
//		QUEUEGT_NOINIT
//			Queue not initialized
//		QUEUEGT_BADPARAM
//			Input parameters not valid
//		QUEUEGT_FULL
//			The Space Available is equal or less to  sizeof(DWORD)
//		QUEUEGT_BUFFERTOOBIG
//			ulBufferSize is too big
//		QUEUEGT_SOFWARE_ERROR
//			Software Bugs
//
DWORD CSharedFlatQueueGT::PutItem(LPBYTE pbyBuffer, DWORD dwBufferSize)
{
	DWORD	dwBuffSize;
	BOOL	bIsError;
	WORD	wIdx;
	LPBYTE	pbyMemBuffer;
	//
	//
	//
	::EnterCriticalSection(&m_cs_Wait);
	//
	// If queue is not initialized
	//
	if(m_pbyData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_NOINIT;
	}
	//
	// If bad parameters
	//
	if(dwBufferSize == 0 || pbyBuffer == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		 return QUEUEGT_BADPARAM;
	}
	//
	// If queue is full
	//
	if(m_dwSpaceAvailable <= sizeof(DWORD))
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_FULL;
	}
	//
	// If dwBufferSize is too big
	//
	if ((dwBufferSize+sizeof(DWORD)) > m_dwSpaceAvailable)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_BUFFERTOOBIG;
	}
	//
	// EnQueue Message
	//
	for(wIdx=0;wIdx<sizeof(DWORD);wIdx++)
	{
		if(m_pbyPut >= m_pbyBottom)
			m_pbyPut = m_pbyTop;
		*(m_pbyPut) = *((LPBYTE)&dwBufferSize+wIdx);
		m_pbyPut++;
	}
	//
	if(m_pbyPut >= m_pbyBottom)
		m_pbyPut = m_pbyTop;
	//
	pbyMemBuffer = pbyBuffer;
	dwBuffSize	 = dwBufferSize;
	//
	if((m_pbyPut+dwBufferSize) > m_pbyBottom)
	{
		dwBuffSize = (DWORD) (m_pbyBottom - m_pbyPut);
		::CopyMemory(m_pbyPut,pbyBuffer,dwBuffSize);
		m_pbyPut = m_pbyTop;
		pbyBuffer += dwBuffSize;
		dwBuffSize = dwBufferSize - dwBuffSize;
	}
	if(dwBuffSize != 0)
		::CopyMemory(m_pbyPut,pbyBuffer,dwBuffSize);
	//
	pbyBuffer = pbyMemBuffer;
	m_pbyPut += dwBuffSize;
	//
	// Update counters
	//
	m_dwNumItems++;
	m_dwSpaceAvailable -= (dwBufferSize + sizeof(DWORD));
	//
	// Send notification if required
	//
	if (m_hWndParent != NULL && m_nMsgParent != 0)
		::PostMessage(m_hWndParent, m_nMsgParent, 0, 0);
	if (m_hEventParent != NULL)
		::SetEvent(m_hEventParent);
	if (m_hThreadHandle != NULL)
		::ResumeThread(m_hThreadHandle);
	//
	// Check Errors
	//
	bIsError = CheckError();
	//
	::LeaveCriticalSection(&m_cs_Wait);
	//
	if(bIsError == TRUE)
		return QUEUEGT_SOFWARE_ERROR;
	//
	return QUEUEGT_OK;
}

// This function gets an item from queue
//
// Parameters:
//		[out]	pbyBuffer
//				Pointer to a buffer that will receive the item copied from queue.
//				This buffer must be large enough to accept the largest item
//				in queue.
//		[in]	bRemove
//				If bRemove == FALSE the item is copied 
//				into pszBuffer but is NOT removed from queue
//		[out]	pdwNumBytesCopied	
//				Pointer to a DWORD that will receive the
//				number of bytes copied in pbyBuffer.
//
// Return value:
//		QUEUEGT_OK
//			All ok
//		QUEUEGT_NOINIT
//			Queue not initialized
//		QUEUEGT_EMPTY
//			Queue is empty
//		QUEUEGT_BADPARAM
//			Input parameters not valid
//		QUEUEGT_SOFWARE_ERROR
//			Software Bugs
//
DWORD CSharedFlatQueueGT::GetItem(LPBYTE pbyBuffer, BOOL bRemove, DWORD* pdwNumBytesCopied)
{
	DWORD	dwBuffSize;
	DWORD	dwBytesCopied = 0;
	BOOL	bIsError;
	WORD	wIdx;
	LPBYTE	pbyMemRead;
	LPBYTE	pbyMemBuffer;


	::EnterCriticalSection(&m_cs_Wait);

	// If queue is not initialized
	if (m_pbyData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_NOINIT;
	}
	//
	// If bad parameters
	//
	if(pbyBuffer == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		 return QUEUEGT_BADPARAM;
	}
	// If queue is empty
	if (m_dwNumItems == 0)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_EMPTY;
	}

	for(wIdx=0,pbyMemRead=m_pbyGet;wIdx<sizeof(DWORD);wIdx++)
	{
		if(pbyMemRead >= m_pbyBottom)
			pbyMemRead = m_pbyTop;
		*((LPBYTE)&dwBytesCopied+wIdx) = (BYTE) *(pbyMemRead)++;
	}
	if(pbyMemRead >= m_pbyBottom)
		pbyMemRead = m_pbyTop;
	//
	pbyMemBuffer = pbyBuffer;
	dwBuffSize   = dwBytesCopied;
	//
	if((pbyMemRead+dwBytesCopied) > m_pbyBottom)
	{
		dwBuffSize = (DWORD) (m_pbyBottom - pbyMemRead);
		::CopyMemory(pbyBuffer,pbyMemRead,dwBuffSize);
		pbyMemRead = m_pbyTop;
		pbyBuffer += dwBuffSize;
		dwBuffSize = dwBytesCopied - dwBuffSize;
	}
	if(dwBuffSize != 0)
		::CopyMemory(pbyBuffer,pbyMemRead,dwBuffSize);
	//
	pbyBuffer = pbyMemBuffer;
	pbyMemRead += dwBuffSize;
	//
	// Update counters
	//
	if(bRemove == TRUE)
	{
		m_pbyGet = pbyMemRead;
		m_dwSpaceAvailable += (dwBytesCopied + sizeof(DWORD));
		m_dwNumItems--;
	}

	if(pdwNumBytesCopied != NULL)
		*pdwNumBytesCopied = dwBytesCopied;
	//
	// Check Errors
	//
	bIsError = CheckError();
	//
	::LeaveCriticalSection(&m_cs_Wait);
	//
	if(bIsError == TRUE)
		return QUEUEGT_SOFWARE_ERROR;
	//
	return QUEUEGT_OK;
}

// This function returns the number of items currently in queue
//
// Return value:
//		DWORD:	Number of item currently in queue
//
DWORD CSharedFlatQueueGT::GetCount()
{
	DWORD dwNumItems;
	
	::EnterCriticalSection(&m_cs_Wait);
	dwNumItems = m_dwNumItems;
	::LeaveCriticalSection(&m_cs_Wait);

	return dwNumItems;
}
// This function empties the queue
//
// Return value:
//		QUEUEGT_OK:		All ok
//		QUEUEGT_NOINIT:	Queue not initialized
//
DWORD CSharedFlatQueueGT::Empty()
{
	::EnterCriticalSection(&m_cs_Wait);

	// If queue is not initialized
	if (m_pbyData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_NOINIT;
	}

	// Empty
	m_dwNumItems		= 0;
	::ZeroMemory(m_pbyData, m_dwBufferSize);
	m_pbyPut			= m_pbyTop;
	m_pbyGet			= m_pbyTop;
	m_dwSpaceAvailable	= m_dwBufferSize;

	m_dwMaxItems		= 0;
	m_dwMinFreeSpace	= m_dwBufferSize;

	::LeaveCriticalSection(&m_cs_Wait);

	return QUEUEGT_OK;
}
//
// This function save the queue onto HD
//
// Return value:
//		QUEUEGT_OK:				All ok
//		QUEUEGT_SOFWARE_ERROR:	Error Saving Queue
//
DWORD CSharedFlatQueueGT::SaveQueue(LPCTSTR lpszFileName)
{
	DWORD	dwSize;
	//
	//
	//
	::EnterCriticalSection(&m_cs_Wait);
	//
	// If queue is not initialized
	//
	if (m_pbyData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_SOFWARE_ERROR;
	}
	//
	if(m_FileRW.Open(lpszFileName,CFile::modeCreate | CFile::modeWrite | CFile::shareExclusive | CFile::typeBinary,NULL) == FALSE)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_SOFWARE_ERROR;
	}
	//
	m_FileRW.Write(&m_dwBufferSize,sizeof(m_dwBufferSize));
	m_FileRW.Write(m_pbyData,m_dwBufferSize);
	m_FileRW.Write(&m_dwNumItems,sizeof(m_dwNumItems));
	m_FileRW.Write(&m_dwSpaceAvailable,sizeof(m_dwSpaceAvailable));
	dwSize = (DWORD) (m_pbyPut - m_pbyTop);
	m_FileRW.Write(&dwSize,sizeof(dwSize));
	dwSize = (DWORD) (m_pbyGet - m_pbyTop);
	m_FileRW.Write(&dwSize,sizeof(dwSize));
	//
	m_FileRW.Close();
	//
	// Empty Queue
	// 
	m_dwNumItems		= 0;
	::ZeroMemory(m_pbyData, m_dwBufferSize);
	m_pbyPut				= m_pbyTop;
	m_pbyGet				= m_pbyTop;
	m_dwSpaceAvailable	= m_dwBufferSize;
	//
	::LeaveCriticalSection(&m_cs_Wait);
	return QUEUEGT_OK;
}
//
// This function restore the queue onto HD
//
// Return value:
//		QUEUEGT_OK:				All ok
//		QUEUEGT_SOFWARE_ERROR:	Error Saving Queue
//
DWORD CSharedFlatQueueGT::RestoreQueue(LPCTSTR lpszFileName)
{
	DWORD	dwBuffSize;
	DWORD	dwSize;
	//
	//
	//
	::EnterCriticalSection(&m_cs_Wait);
	//
	// If queue is not initialized
	//
	if (m_pbyData == NULL)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_SOFWARE_ERROR;
	}
	//
	if(m_FileRW.Open(lpszFileName,CFile::modeRead | CFile::shareExclusive | CFile::typeBinary,NULL) == FALSE)
	{
		::LeaveCriticalSection(&m_cs_Wait);
		return QUEUEGT_SOFWARE_ERROR;
	}
	//
	m_FileRW.Read(&dwBuffSize,sizeof(dwBuffSize));
	if(m_dwBufferSize == dwBuffSize)
	{
		m_FileRW.Read(m_pbyData,m_dwBufferSize);
		m_FileRW.Read(&m_dwNumItems,sizeof(m_dwNumItems));
		m_FileRW.Read(&m_dwSpaceAvailable,sizeof(m_dwSpaceAvailable));
		m_FileRW.Read(&dwSize,sizeof(dwSize)); // m_pbyPut
		m_pbyPut += dwSize;
		m_FileRW.Read(&dwSize,sizeof(dwSize)); // m_pbyGet
		m_pbyGet += dwSize;
	}
	//
	m_FileRW.Close();
	//
	::DeleteFile(lpszFileName);
	//
	::LeaveCriticalSection(&m_cs_Wait);
	//
	return QUEUEGT_OK;
}
