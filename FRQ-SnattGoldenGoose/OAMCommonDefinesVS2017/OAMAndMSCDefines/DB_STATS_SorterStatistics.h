//
//	File:		DB_STATS_SorterStatistics.h
//
//	Compiler:	Visual C++ 2010
//
//	Tested on:	RTX v11.0.0 Build 10540 
//
//	Version:	1.0
//
//	Created:	24/September/2013
//	Updated:	
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	24-09-2013 - First release
//

#pragma once

// DEFINES
#define MAX_ISPC			40
#define MAX_DRIVE			100
#define MAX_TIME_SLOTS		60
#define MAX_VALUE_SLOTS		60
#define MAX_CHUTE			11
#define MAX_SPEED_SLOTS		800 // New value every 20ms -> 800 slot = 800*20 = 16000ms = 16 Seconds

//////////////////////////////////////////////////////////////////////////
//							STRUCT_STATS_SLOT
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
	//
	typedef struct _STRUCT_STATS_SLOT
	{
		//
		LARGE_INTEGER liFirstValue;
		LARGE_INTEGER liLastUpdate;
		double dValue;
		//
	} STRUCT_STATS_SLOT;
	//
#pragma pack()

#define SIZE_STRUCT_STATS_SLOT		sizeof(_STRUCT_STATS_SLOT)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_SORTER_STATS (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_SORTER_STATS
{
	// Counter to know that stats was updated ...
	WORD wStatsLoadUpdated;
	WORD wStatsSortUpdated;
	WORD wStatsTorqueUpdated;
	WORD wStatsSpeedUpdated;
	// Stats Slots
	STRUCT_STATS_SLOT	csObjectLoaded[MAX_ISPC][MAX_TIME_SLOTS];
	STRUCT_STATS_SLOT	csObjectSorted[MAX_CHUTE][MAX_TIME_SLOTS];
	short				shDriveTorque[MAX_DRIVE][MAX_VALUE_SLOTS];
	WORD				wSorterSpeed[MAX_SPEED_SLOTS];
	//
	// PRODUCTION TEST
	//
	BOOL				bUnderTest;
	STRUCT_STATS_SLOT	csLoadingTest[MAX_ISPC];
	STRUCT_STATS_SLOT	csSortingTest[MAX_CHUTE];
	//
}	STRUCT_SORTER_STATS;
#pragma pack()

#define SIZE_STRUCT_SORTER_STATS		sizeof(_STRUCT_SORTER_STATS)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////
