//
//	File:		device_wMSGID_Defines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	22/June/2010
//	Updated:	22/June/2010
//
//	Author:		Carmine Filella [+39-0331-665345 carmine.filella@fivesgroup.com]
//
//  History:
//
//	22-06-2010 - First release
//

#pragma once

#include "CMLDefines.h"

/**
	Structure for device event message (wMSGID_EVENT_DEVICE).
*/
struct STRUCT_EVENT_DEVICE
{
	/// Message identifier.
	/**
		Message identifier.
	*/
	WORD		wMsgId;
	
	/// Message data.
	/**
		Message data.
	*/ 
	BYTE				byData[SINGLEMSGCML_MAX_DATALEN - SIZE_STRUCT_HEADERMSGCML - sizeof(WORD)];
};
