//
//	File:		DB_MSC_ControlZone.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	30/June/2008
//	Updated:	30/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	30-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

// MAX control zone 
#define MAX_CONTROL_ZONE				40
// MAX Sensors for single control zone
#define MAX_CONTROL_ZONE_SENSORS		4

// Counter defines ...
#define CELL_CONUTER_TO_TEST_BLINK		4		// Number of cell to count before check blink (RE) counter
#define MAX_SENSOR_BLINK				7		// Number of RE into CELL_CONUTER_TO_TEST_BLINK to make alarm

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CTRLZONE_SENSOR
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CTRLZONE_SENSOR
{
	//
	WORD wPosition;						// Sensor NIF position
	WORD wHight;						// Reading position in mm ( 0 = see everything )
	BOOL bMustWork;						// TRUE means that if fault, control zone fault else only the sensor fault
	WORD wNifToFault;					// NIF always at 1 to fault
	WORD wDetectFilter;					// Object is detected if sensor stay at ON at least this NIF filter ( 0 = no filter)
	//
} STRUCT_CTRLZONE_SENSOR;
#pragma pack()

#define SIZE_STRUCT_CTRLZONE_SENSOR		sizeof(_STRUCT_CTRLZONE_SENSOR)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CONTROL_ZONE (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CONTROL_ZONE
{
	// Configuration fields ...
	BYTE	byControlID;				// Zone identifier
	BYTE	byControlType;				// Control zone type
	WORD	wControlFunctions;			// Functions flags
	//
	WORD	wResultPosition;			// Sorter NIF positions for result/notify functions
	//
	STRUCT_CTRLZONE_SENSOR	csSensors[MAX_CONTROL_ZONE_SENSORS];	// Sensors informations
	//
	STRUCT_DIAG_BLOCK	csDiag;			// Full Diagnostic
	//
} STRUCT_CONTROL_ZONE;
#pragma pack()

#define SIZE_STRUCT_CONTROL_ZONE	sizeof(_STRUCT_CONTROL_ZONE)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Values of byControlType
#define CTRL_ZONE_TYPE_HW		1		// Hardware, it means that has at least one sensor
#define CTRL_ZONE_TYPE_SW		2		// Software, it means that performe only logical functions

// Values of wControlFunctions
#define CTRL_ZONE_FUNC_NOTIFY_OUTCOME				0x0001	// Notify outcome codes
#define CTRL_ZONE_FUNC_NOTIFY_LOAD					0x0002	// Notify just loaded object
#define CTRL_ZONE_FUNC_NOTIFY_RECIRCULATION			0x0004	// Notify object recirculation
#define CTRL_ZONE_FUNC_CELL_REALLOCATION			0x0008	// Process reallocation
#define CTRL_ZONE_FUNC_FOUND						0x0010	// Check object found
#define CTRL_ZONE_FUNC_LOST							0x0020	// Check object lost
#define CTRL_ZONE_FUNC_ONGAP						0x0040	// Check object on gap
#define CTRL_ZONE_FUNC_NOTIFY_OBJECT_ON_CTRLZONE	0x0080	// Notify object is on Control Zone

// Inputs name define
#define DI_NAME_CTRLZONE_SENSOR		"CTRLZONE_SENSOR_"	// CTRLZONE_SENSOR_gn -> g = Control Zone ID n = Sensor (1..MAX_CONTROL_ZONE_SENSORS)

// Alarms bits ...
#define CTRL_ZONE_ALARM_GENERAL_FAULT		0	// Control Zone cannot work ...
#define CTRL_ZONE_ALARM_FAULT_SENSOR		1	// Sensor #1: faulted ... 
// ______ SENSOR # 2 _________________		2	// Sensor #2: faulted ... 
// ______ SENSOR # 3 _________________		3	// Sensor #3: faulted ... 
// ______ SENSOR # 4 _________________		4	// Sensor #4: faulted ... 

// Warnings bits ...
#define CTRL_ZONE_WARNING_FAULT_SENSOR		1	// Sensor #1: faulted ... 
// ______ SENSOR # 2 _________________		2	// Sensor #2: faulted ... 
// ______ SENSOR # 3 _________________		3	// Sensor #3: faulted ... 
// ______ SENSOR # 4 _________________		4	// Sensor #4: faulted ...
// Warning bit
#define CTRL_ZONE_WARNING_POSSIBLE_JAM		0	// Possible Sorter Jam

