//
//	File:		COMMON_MSC_Defines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	27/June/2008
//	Updated:	22/August/2013
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	22-08-2013 - Added Chute Type defines to 9 (1..9); No Read setted to 100
//	12-08-2013 - Added "ALARM_TYPE_DIAG_BY_CLASS" defines/enums
//	27-06-2008 - First release
//

#pragma once

//////////////////////////////////////////////////////////////////////////
// Chutes Types ...
//////////////////////////////////////////////////////////////////////////
enum _CHUTE_TYPE
{
	CHUTE_TYPE_1 = 0,
	CHUTE_TYPE_2,
	CHUTE_TYPE_3,
	CHUTE_TYPE_4,
	CHUTE_TYPE_5,
	CHUTE_TYPE_6,
	CHUTE_TYPE_7,
	CHUTE_TYPE_8,
	CHUTE_TYPE_9,
	//
	CHUTE_NO_READ,
	CHUTE_REJECT,
	//
	CHUTE_INVALID
	//
};

//////////////////////////////////////////////////////////////////////////
// Alarms Types ...
//////////////////////////////////////////////////////////////////////////
enum _ALARM_TYPE
{
	ALARM_TYPE_COMMON = 0,
	ALARM_TYPE_1,
	ALARM_TYPE_2,
	ALARM_TYPE_3,
	ALARM_TYPE_TM,
	ALARM_TYPE_EMERGENCY,
	ALARM_TYPE_SAFE_EMERGENCY,
	ALARM_TYPE_WARNING,
	ALARM_TYPE_ENERGYBOX,
	ALARM_TYPE_DIAG_BY_CLASS,
	ALARM_TYPE_OPERATIVE,
	ALARM_TYPE_ENVIRONMENTAL,
	ALARM_TYPE_PROCESS,
	ALARM_TYPE_TECHNICAL,
	ALARM_TYPE_WARNING_OPERATIVE,
	ALARM_TYPE_WARNING_ENVIRONMENTAL,
	ALARM_TYPE_WARNING_PROCESS,
	ALARM_TYPE_WARNING_TECHNICAL,
	QTY_ALARM_TYPE
};

//////////////////////////////////////////////////////////////////////////
// Warning Types ...
//////////////////////////////////////////////////////////////////////////
enum _WARNING_TYPE
{
	WARNING_TYPE_COMMON = 0,
	WARNING_TYPE_OPERATIVE,
	WARNING_TYPE_ENVIRONMENTAL,
	WARNING_TYPE_PROCESS,
	WARNING_TYPE_TECHNICAL,
	QTY_WARNING_TYPE
};

//////////////////////////////////////////////////////////////////////////
// Alarms Types Name ...
//////////////////////////////////////////////////////////////////////////
#define ALARM_NAME_LENGTH				9
#define ALARM_DAC_NAME_LENGTH			10
//										 01234567890123456
#define ALARM_TYPE_COMMON_NAME					"CABINETAL"
#define ALARM_TYPE_1_NAME						"CABINETT1"
#define ALARM_TYPE_2_NAME						"CABINETT2"
#define ALARM_TYPE_3_NAME						"CABINETT3"
#define ALARM_TYPE_TM_NAME						"CABINETTM"
#define ALARM_TYPE_EMERGENCY_NAME				"EMERGENCY"
#define ALARM_TYPE_WARNING_NAME					"CABINETWR"
#define ALARM_TYPE_DIAG_BY_CLASS_NAME			"CLASSDIAG"
#define ALARM_TYPE_SAFE_EMERGENCY_NAME			"SAFE_EMER"
// DAC v3.0 alarm/warning type
#define ALARM_TYPE_TECHNICAL_NAME				"CABINETALT"
#define ALARM_TYPE_OPERATIVE_NAME				"CABINETALO"
#define ALARM_TYPE_PROCESS_NAME					"CABINETALP"
#define ALARM_TYPE_ENVIRONMENTAL_NAME			"CABINETALE"
#define ALARM_TYPE_WARNING_TECHNICAL_NAME		"CABINETWRT"
#define ALARM_TYPE_WARNING_OPERATIVE_NAME		"CABINETWRO"
#define ALARM_TYPE_WARNING_PROCESS_NAME			"CABINETWRP"
#define ALARM_TYPE_WARNING_ENVIRONMENTAL_NAME	"CABINETWRE"

