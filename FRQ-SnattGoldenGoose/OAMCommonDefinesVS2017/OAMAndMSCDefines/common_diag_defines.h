//
//	File:		common_diag_defines.h
//
//  Shared by DDE, Plant, ISPC
//
//  History:
//
//  0.0.6		19-09-2016  ISPC group becomes a new Device Type 
//
//  0.0.5		16-09-2016  Removed offset of DIAG_IDs when sent by IODriver, it is not shared with DDE 
//
//	0.0.4		16-09-2016	Separation of really shared defines (here) and application customizable structures (in separate files)
//							ID defines becomes modular to be compatible with both IODriver and DDE
//							Added singulator device type
//
//	0.0.3		16-09-2016	Merge tentative with plant
//							Defines restyling
//
//	0.0.2		15-09-2016	Removed everything about TRI STATE
//							Added Strobe device
//
//  0.0.1		02-09-2016	First draft for testing

#pragma once

//////////////////////////////////////////////////////////////////////////

// Device type: induction or conveyor system
#define DEVICE_ID_IS					1	// induction line
#define DEVICE_ID_UP					2	// upstream
#define DEVICE_ID_SI					3	// singulator
#define DEVICE_ID_IG					4	// induction lines group

//////////////////////////////////////////////////////////////////////////

// Array ID define, used by ISPC and PLANT to send changes to EUIH/MSC
#define	DIAG_ID_CSCABINET			1
#define	DIAG_ID_CSZONE				2
#define	DIAG_ID_CSBELT				3
#define	DIAG_ID_CSMODULATOR  		4
#define	DIAG_ID_CSCOIL				5
#define	DIAG_ID_CSSENSOR			6
#define	DIAG_ID_CSSTROBEDEVICE		7

//////////////////////////////////////////////////////////////////////////

