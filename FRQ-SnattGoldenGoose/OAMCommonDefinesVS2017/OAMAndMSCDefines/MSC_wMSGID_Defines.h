//
//	Class:		MSC_wMSGID_Defines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//
//	Version:	1.0
//
//	Created:	05/June/2008
//	Updated:	05/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	05-06-2008 - First release
//

#pragma once

////////////////
// ID for MSG //
////////////////
enum _ENUM_WMSGID{

	wMSGID_EXIT=1,										// Generic message to kill the application
	wMSGID_EVENT_FROM_ENCODER,							// Event from Encoder
	wMSGID_EVENT_NOTIFY_ALARM,							// Event to notify a SW alarm
	wMSGID_EVENT_NEW_NIF,								// New NIF, used to wake-up all Threads that work by NIF ...
	wMSGID_EVENT_TIME,									// Event from 10ms main timer
	wMSGID_EVENT_NEW_SORTER_SPEED,						// Event from Sorter speed key selector (changed)
	wMSGID_EVENT_ALARM_RESET,							// Event from Reset key selector
	wMSGID_EVENT_SORT,									// Event from SortingManager -> UnloadCell
	wMSGID_EVENT_MOVE_FLAP,								// Event from SortingManager -> MoveFlap
	wMSGID_EVENT_CELL_IO_EXCHAGE,						// Event from SortingManager -> CellIOExchange
	wMSGID_EVENT_DEVICE,								// Event from an external device
	wMSGID_TRACE_LEVELS_UPDATE,							// Event from DDE/MIF with enabled levels ...
	wMSGID_EVENT_RESET_CLASSDIAG_IO,					// Event to seek and reset a CLASSDIAG Inputs
	wMSGID_EVENT_CELL_ACTION_RESULT,					// Event to Process Cell Action Result
	wMSGID_EVENT_MAINTENANCE_ACTION,					// Event to Process Maintenance over Coil
	wMSGID_EVENT_MAINTENANCE_ERASE_JOIN_ENTRIES,		// Event to Erase some Join Entries from the Join Table (After the Maintenance area access ...)
	//
	wMSGID_COORDINATOR_TO_RTX_INTERNAL_COMUNICATION,	// Internal message between Coordinator -> DDE -> MSC
	wMSGID_RTX_TO_COORDINATOR_INTERNAL_COMUNICATION,    // Internal message between MSC -> DDE -> Coordinator
	//
	wNUM_MSG_ID
	//
};

//////////////////////////////////////////////////////////////////////////
//						STRUCT_NOTIFY_ALARM_MSG
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_NOTIFY_ALARM_MSG
{
	//
	BYTE	byAlarmType;			// Type of SW event, see defines below !
	BYTE	byAlarmBit;				// Bit to set/reset
	BOOL	bMustBrake;				// Flag to stop the sorter using the brake mode
	//
} STRUCT_NOTIFY_ALARM_MSG;
#pragma pack()

#define SIZE_STRUCT_NOTIFY_ALARM_MSG sizeof(_STRUCT_NOTIFY_ALARM_MSG)

// Defines for byAlarmType of STRUCT_NOTIFY_ALARM_MSG
#define JUST_STOP_SORTER	0
#define WARNING_SW			1	// Set a SW warning
#define ALARM_SW	 		2	// Set a SW alarm
#define RESET_WARNING_SW	3	// Reset a SW warning
#define RESET_ALARM_SW 		4	// Reset a SW alarm

//////////////////////////////////////////////////////////////////////////
//					STRUCT_EVENT_RESET_CLASSDIAG_IO
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_EVENT_RESET_CLASSDIAG_IO
{
	//
	DWORD	dwClassID;
	WORD	wElement;		
	//
} STRUCT_EVENT_RESET_CLASSDIAG_IO;
#pragma pack()

#define SIZE_STRUCT_EVENT_RESET_CLASSDIAG_IO sizeof(_STRUCT_EVENT_RESET_CLASSDIAG_IO)

