///////////////////////////////////////////////////////////////////////////////////
///
/// 
/// \file			CustomData.h
///
/// \brief			header
///
///	\class			Structure STRUCT_CUSTOM_DATA
///
/// \brief			structure which defines specific plant data 
///
///	\version		4.0
///
///	\date    		07/May/2012
/// \update			26/June/2018
///
///	\author			Mauro Manca [+39-0331-665279 mauro.manca@fivesgroup.com]
///
////////////////////////////////////////////////////////////////////////////////

#pragma once

//
// Custom data as defined at chap.8 on document SPR99FOR00 made by G. Tonella
//

#define SIZEOF_OCSSCALE__WEIGHT				9
#define SIZEOF_OCSSCALE__MEASURE_UNIT		2
#define SIZEOF_OCSSCALE__ITEM_ATTRIBUTE		4
#define SIZEOF_OCSSCALE__ERROR_ID			3
#define SIZEOF_OCSSCALE__ERROR_MSG			50

#pragma pack(1)
typedef struct _STRUCT_CUSTOM_DATA
{
	//
	// All specific plant data
	// NOTE:
	// This data will be part of STRUCT_ISPC_MSC_DATA and will be transfered via DP/DP Coupler from ISPC to Sorter
	// The STRUCT_ISPC_MSC_DATA is also into Cell DB !
	// Keep in mind those two thing and think about memory size !
	char	szWeightInGramsZ[SIZEOF_OCSSCALE__WEIGHT + 1];
	char	szUnitZ[SIZEOF_OCSSCALE__MEASURE_UNIT + 1];
	char	szItemAttributeZ[SIZEOF_OCSSCALE__ITEM_ATTRIBUTE + 1];
	char	szIErrorIdentifierZ[SIZEOF_OCSSCALE__ERROR_ID + 1];
	char	szIErrorMessageZ[SIZEOF_OCSSCALE__ERROR_MSG + 1];
	DWORD	dwAlibiMemoryNumber;
	BOOL	bWeightingSystemBypassed;	// TRUE If Bypassed
	//
} STRUCT_CUSTOM_DATA;
#pragma pack()

#define SIZE_STRUCT_CUSTOM_DATA sizeof(_STRUCT_CUSTOM_DATA)

// ------------------------------------------------------------------


