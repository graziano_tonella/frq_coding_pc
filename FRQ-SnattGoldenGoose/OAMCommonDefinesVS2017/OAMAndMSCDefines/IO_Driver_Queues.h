//
//	File:		IO_Driver_Queues.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//
//	Version:	1.0
//
//	Created:	05/June/2008
//	Updated:	05/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	05-06-2008 - First Release
//

#pragma once

//////////////////////////
//	APPLICATION QUEUES	//
//////////////////////////
enum
{
	// Queue from W32 (INTERNAL)
	QUEUE_IO_DRIVER_FROM_W32_INT,
	// Queue from W32 (EXTERNAL)
	QUEUE_IO_DRIVER_FROM_W32_EXT,
	// Queue from other RTX applications ...
	QUEUE_IO_DRIVER_RTX_EXTERNAL,
	// Number of queues !
	IO_DRIVER_MAX_QUEUE
	//
};
//
//////////////////
// Queues Name  //
//////////////////
#define QUEUE_NAME_IO_DRIVER_FROM_W32_INT		_T("IOD_FROM_W32_INT")
#define QUEUE_NAME_IO_DRIVER_FROM_W32_EXT		_T("IOD_FROM_W32_EXT")
#define QUEUE_NAME_IO_DRIVER_RTX_EXTERNAL		_T("IOD_RTX_EXT")
