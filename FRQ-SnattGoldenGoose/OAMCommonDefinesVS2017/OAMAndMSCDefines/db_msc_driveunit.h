//
//	File:		DB_MSC_DriveUnit.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	30/June/2008
//	Updated:	30/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	30-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_INDUCTION_STATION (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_DRIVE_UNIT
{
	//
	BYTE	byDriveUnitID;			// Drive unit identification (Motor ID 1..X)
	BYTE	byDriveUnitType;		// Drive unit type (see defines!)
	BYTE	byDriveUnitMode;		// Drive unit mode (see defines!)
	WORD	wTorqueLimit;			// Torque limit
	//
	double	dDriveComputedSpeed;	// Motor speed calculated by drive
	WORD	wTemperature;			// Motor temperature
	double	dUsedForce;				// Force in Nm (should be -4 ..+4)
	//
	BOOL	bForceToque;			// Flag to enable MIF "force torque"
	short	shForcedTorque;			// Drive torque value to force
	//
	STRUCT_DIAG_BLOCK csDiag;		// Full Diagnostic
	//
} STRUCT_DRIVE_UNIT;
#pragma pack()

#define SIZE_STRUCT_DRIVE_UNIT sizeof(_STRUCT_DRIVE_UNIT)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Values of byDriveUnitType
#define DRIVE_UNIT_TYPE_LUST				1
#define DRIVE_UNIT_TYPE_ABB					2
#define DRIVE_UNIT_TYPE_ABB_300				3
#define DRIVE_UNIT_TYPE_LTI_SERVOONE		4
#define DRIVE_UNIT_TYPE_LTI_SERVOONE_JUNIOR	5

// Values of byDriveUnitMode
#define DRIVE_UNIT_MODE_NORMAL			1	// wTorqueLimit: 0 = no limit, else max value +/-
#define DRIVE_UNIT_MODE_BREAKER			2	// wTorqueLimit: fixed value given to motor
#define DRIVE_UNIT_MODE_BREAKER_AID		3	// wTorqueLimit: minimum motor value

// Quantity defines ...
#define MAX_DRIVE_UNIT				50
#define MAX_DRIVE_UNIT_MOTORS		2

// I/Os define ...
#define DU_IO_NAME_TORQUE			"GRUPPOMOTORE"
#define DU_IO_NAME_DRIVE_CTRL		"CTRLWORD"
#define DU_IO_NAME_DRIVE_STATE		"STATWORD"
#define DU_IO_NAME_MOTOR_STATE		"MOTOR_FAULT_"		// VI with motor fault state, useful for IO_Chains ...
#define DU_IO_NAME_LTI_INFO			"LTI_ACTFEEDBACK_"		// Used to retrive information from an LTI ServoOneJunior drive ...

// Alarms bit
#define DU_ALARM_BIT_DRIVE_FAULT			0	// Hw Fault on drive
												// (e.g.: for LTIServoOne Overspeed reached or Error from Codesys)
#define DU_ALARM_BIT_BUSFIELD_FAULT			1
#define DU_ALARM_BIT_NOT_READY				2	// Drive is not ready to apply Torque
												// (e.g.: for LTOServoOne it means the Finite State Machine doesn't complete the steps to be ready)
#define DU_ALARM_BIT_NO_POWER				3	// Power stage is not ok
#define DU_ALARM_BIT_LOST_COMMUNICATION		4	// No Fieldbus communication with the Drive
												// (e.g.: for LTOServoOne it means the Watchdog from Drive doesn't toggle)
