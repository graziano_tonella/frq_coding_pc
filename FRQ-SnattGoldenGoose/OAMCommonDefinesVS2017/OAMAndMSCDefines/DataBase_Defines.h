//
//	File:		DataBase_Defines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 6.0
//				RTX 4.3.2.1
//
//	Created:	29/November/2000
//	Updated:	31/January/2011
//  History:	- 31/January/2011: modified type of .dwEntrySize member of STRUCT_EL_DB_TABLE structure in DWORD.
//
//	Contributors:	Davide Calabro'
//
#ifndef _DataBase_Defines_H_
#define _DataBase_Defines_H_

// Max. number of tables the database can contains.
//
#define	EL_MAX_DB_TABLES			64

// Max. length (in TCHARs) of the name of a database table.
//
#define	EL_MAX_DB_TABLE_NAME_LEN	40

//////////////////////////////////////////////////////////////////////////
//							STRUCT_EL_DB_HEADER
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_EL_DB_HEADER
{
	BYTE	byNumTables;		// How many VALID tables the database contains
	DWORD	dwDBSize;			// Size (in bytes) of the whole database.
								// This size includes:	Database header
								//						Table descriptor
								//						Touched entries lookup table
								//						Actual database
	DWORD	dwTotalNumEntries;	// Total number of entries in the database.

	BYTE	byReserved[247];	// Reserved for future use
} STRUCT_EL_DB_HEADER;
#pragma pack()

#define SIZE_STRUCT_EL_DB_HEADER sizeof(_STRUCT_EL_DB_HEADER)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_EL_DB_TABLE
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_EL_DB_TABLE
{
	TCHAR		szName[EL_MAX_DB_TABLE_NAME_LEN];	// Name of the table
	BYTE		byTableIndex;						// Index of the table. This number is unique
													// for each table of the database and can be
													// from 0 to (EL_MAX_DB_TABLES - 1)
	WORD		wNumEntries;						// Number of entries in the table
	DWORD		dwEntrySize;						// Size (in bytes) of each entry of the table
	WORD		wNumValidEntries;					// How many entries have been added
	DWORD		dwSize;								// Total size (in bytes) of the table
	DWORD		dwOffset;							// Offset of the table in the database,
													// starting from database header
} STRUCT_EL_DB_TABLE;
#pragma pack()

#define SIZE_STRUCT_EL_DB_TABLE sizeof(_STRUCT_EL_DB_TABLE)

#endif