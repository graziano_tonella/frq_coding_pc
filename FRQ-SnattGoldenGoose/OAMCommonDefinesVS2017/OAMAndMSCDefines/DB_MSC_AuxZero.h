//
//	File:		DB_MSC_AuxZero.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	06/August/2008
//	Updated:	06/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	06-08-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_AUX_ZERO (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_AUX_ZERO
{
	//
	BYTE	byGroup;					// Group ID 1..9 (0 its the master cell zero phs on the encoder frame)
	WORD	wNifOffset;					// Sorter position to set (NIF) after a cell zero event ! Also used to check !
	BYTE	byCellZeroSetNif;			// NIF (absolute on the encoder) to wait for sorter cell zero sync.
	//
	STRUCT_DIAG_BLOCK		csDiag;		// Full Diagnostic
	//
} STRUCT_AUX_ZERO;
#pragma pack()
#define SIZE_STRUCT_AUX_ZERO	sizeof(_STRUCT_AUX_ZERO)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////
