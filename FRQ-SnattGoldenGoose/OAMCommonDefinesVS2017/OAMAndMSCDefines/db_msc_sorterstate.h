//
//	File:		DB_MSC_SorterState.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	26/June/2008
//	Updated:	07/February/2013
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	15-12-2014 - Added Alarm Bit 28: At least one cabinet in fault
//				 Added Alarm Bit 29: Possible Sorter Jam
//	07-02-2013 - Implemented possible signals from a Control Room (SYSTEM ENABLED, RESET, FAULT)
//	26-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

// Max number of stack lights ...
#define MAX_STACK_LIGHT		10

// Enumerate stack light ...
enum _STACK_LIGHT_INDEX
{
	//
	PRERUN_BUZZER = 0,
	GREEN_LIGHT,
	ORANGE_LIGHT,
	BLU_LIGHT,
	RED_LIGHT
	//
};

//////////////////////////////////////////////////////////////////////////
//					STRUCT_SORTER_STATE (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_SORTER_STATE
{
	//
	BYTE	byWorkingMode;						// Current sorter working mode
	//
	BYTE	byRunState;							// Current sorter running state
	//
	BYTE	bySpeedState;						// Current sorter speed state
	//
	WORD	wSpeedSetPoint;						// Current sorter speed set point (mm/sec)
	WORD	wControllerWorkingStep;				// Current sorter motion controller working step
	//
	short	shKeySpeedID;						// Cabinet Key Speed ID value	  (0 = never used, -1 = do not use, 1..4 possible speed ID)
	short	shCmdSpeedID;						// Network Command Speed ID value (0 = never used, -1 = do not use, 1..4 possible speed ID)
	//
	BYTE	byWantedSpeedID;					// Wanted Speed ID
	BYTE	byInUseSpeedID;						// Current Speed ID used on Sorter
	//
	WORD	wCurrentSpeed;						// Current sorter speed (mm/sec)
	short	shTorque;							// Current sorter torque (From PID, not on Drive !!!)
	WORD	wCurrentNIF;						// Current sorter position (NIFs)
	WORD	wLoadedCells;						// Number of current loaded cells
	BYTE	bySorterLap;						// Number of sorter laps
	//
	BYTE	bySorterStopMode;					// Select in wich way the sorter must stop
	//
	BYTE	bySorterCtrlFlags;					// Sorter control flags (bit used), defines below
	//
	BYTE	byEnableMode;						// Current sorter enable mode (bit used), defines below
	//
    WORD	wUnavailableCells;					// Number of cells that can't be loaded
    WORD	wLoadRateFromInductions[20];		// Parcels per hour (pph) - Max 20 inductions
	DWORD	dwUnloadRateToNormalChutes;			// Parcels per hour (pph)
	DWORD	dwUnloadRateToOtherChutes[10];		// Parcels per hour (pph) - Index [0] is for ALL reject chutes
	DWORD	dwTotalLoadRate;					// Actual sorter loading rate
	//
	BYTE	byStackLightState[MAX_STACK_LIGHT];	// Echo of the current state of Stack Lights
	//
	STRUCT_DIAG_BLOCK	csDiag;					// Full Diagnostic
	//
} STRUCT_SORTER_STATE;
#pragma pack()

#define SIZE_STRUCT_SORTER_STATE sizeof(_STRUCT_SORTER_STATE)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Values for byWorkingMode
#define	WORKING_MODE_UNKNOWN		0
#define WORKING_MODE_MAINTENANCE	1
#define	WORKING_MODE_AUTOMATIC		2

// Values for byRunState
#define SORTER_STOPPED		0
#define SORTER_FIRSTLAP		1
#define SORTER_RUNNING		2
#define SORTER_STOPPING		3
#define SORTER_BRAKING		4

// Values for bySpeedState
#define SORTER_SPEED_NULL_STATE		0
#define SORTER_SPEED_IS_CHANGING	1
#define SORTER_SPEED_IN_RANGE		2

// Values for bySorterStopMode
#define	SORTER_STOP_MODE_NONE			0
#define	SORTER_STOP_MODE_NORMAL			1
#define SORTER_STOP_MODE_WHEN_EMPTY		2
#define SORTER_STOP_MODE_BRAKING		3

// Values for byInUseSpeedID
#define SPEED_ID_DEFAULT			0
#define SPEED_ID_01					1
#define SPEED_ID_02					2
#define SPEED_ID_03					3
#define SPEED_ID_MAINTENANCE_SLOW	4
#define SPEED_ID_MAINTENANCE_HIGH	5

// Values (bits!!) for bySorterCtrlFlags
#define SORTER_CTRL_NORULES					0x00
#define SORTER_CTRL_BLOCK_LOCAL_START		0x01
#define SORTER_CTRL_BLOCK_REMOTE_START		0x02

// Values (bits!!) for byEnableMode
#define SORTER_ENABLE_USE_KEY					0x01
#define SORTER_ENABLE_USE_OAM					0x02
#define SORTER_ENABLE_SPARE_04					0x04
#define SORTER_ENABLE_SPARE_08					0x08
#define SORTER_ENABLE_FROM_KEY					0x10
#define SORTER_ENABLE_FROM_OAM					0x20
#define SORTER_ENABLE_SPARE_40					0x40
#define SORTER_ENABLE_SPARE_80					0x80

// I/O's name defines
#define DO_PRERUN_BUZZER				"SIRENA"				// All the run buzzer
#define DO_PRERUN_LAMP					"LAMPADA"				// All the run lamp
#define DO_ALARMS_LAMP					"ORANGELIGHT"			// All the alarms lamp
#define DO_EMERGENCY_LAMP				"REDLIGHT"				// All the emergency lamp
#define DO_MAINTENANCE_LAMP				"BLULIGHT"				// All the emergency lamp
//
#define DI_START_BTN					"MARCIA"				// Main cabinet start button
#define DI_STOP_BTN						"ARRESTO"				// Main cabinet stop button
#define DI_STOP_EMPTY_BTN				"ARSVUO"				// Main cabinet stop with empty button
#define DI_ALARMS_RESET_BTN				"RESAL"					// Main cabinet reset key/button
#define DI_TEST_LAMP_BTN				"TEST_LAMP"				// Main cabinet test lamp
//
#define DI_AUTOMATIC_KEY				"AUTOM"					// Main cabinet Automaitc key
#define DI_MAINTENANCE_KEY				"MANUAL"				// Main cabinet Maintenance key
#define DI_SPEEDID_0_KEY				"VELOC1"				// Main cabinet Speed bit 0 key
#define DI_SPEEDID_1_KEY				"VELOC2"				// Main cabinet Speed bit 1 key
//
#define DI_BLOCK_LOCAL_START_KEY		"BLOCK_LOCAL_START"		// Main cabinet Block Local Start key
#define DI_BLOCK_REMOTE_START_KEY		"BLOCK_REMOTE_START"	// Main cabinet Block Remote Start key
//
#define DI_RST_CTRLROOM					"RST_CTRLROOM_"			// Alarm reset from Control Room
#define DI_SYSENABLED_KEY				"SYSENABLED_KEY_"		// System Enable Key (from control room)
#define DI_SYSENABLED_OAM				"SYSENABLED_OAM_"		// Enable From OAM (Fake DI, its a VI managed with OAM message)
//
#define DO_SYSTEM_READY					"SYSTEM_READY_"			// System is ready (to control room)
#define DO_FAULT_ACTIVE					"FAULT_ACTIVE_"			// Fault Active (to control room)
//
#define DI_MAINTENANCE_START			"STARTM_"				// Maintenance start button one
#define DI_MAINTENANCE_SPEED			"HISPEM_"				// Maintenance speed key selector
#define DI_MAINTENANCE_RESET			"RESALM_"				// Maintenance reset alarm button
#define DI_MAINTENANCE_ENABLE_KEY		"ENABLM_"				// Maintenance enable key
//
#define DI_MAINTENANCE_PAD_LOADUNLOAD	"PAD_LOADUNLOAD_"		// Key selector to choose load or unload profile
#define DI_MAINTENANCE_PAD_JOG_DX		"PAD_JOG_DX_"			// Jog command for right movement
#define DI_MAINTENANCE_PAD_JOG_SX		"PAD_JOG_SX_"			// Jog command for left movement
#define DI_MAINTENANCE_FOOT_SWITCH		"FOOT_SWITCH_"			// Foot switch
//
#define DO_MAINTENANCE_ENABLE_LIGHT		"LAMPM_"				// Maintenance enable light
#define DO_MAINTENANCE_WAIT_DA_BOOT		"M_WAIT_DA_BOOT_"		// Maintenance Wait Digital Antenna Boot
#define DO_MAINTENANCE_DA_READY			"M_DA_READY_"			// Maintenance Digital Antenna Ready
//
#define DI_IO_MIRROR					"IO_MIRROR_"			// Input that must be mirrored to an output
//
#define DI_ALARM_BUZZER_QUIT			"ABUZZER_QUIT"			// Pushbutton to stop alarm buzzer (if any)
#define DO_ALARM_BUZZER					"ABUZZER"				// Output to activate the alarm buzzer (if any)
//
#define DO_HOUR_METER					"HOURMETER"				// Output to start the running hour meter
#define DO_WATCHDOG						"WATCHDOG"				// SLOW_FLASH after init !
#define DO_SORTER_IS_RUNNING			"SORTERISRUNNING_"		// Output to say that the sorter is running (maybe some external use)
#define DO_SORTER_IS_READY_TO_WORK		"SORTERISWORKING_"		// Output to say that the sorter is ready to work (Synch and correct speed)
#define DO_SORTER_AUTOMATIC_START		"SORTERAUTOSTART_"		// Pulse output to say that the sorter is starting in automatic mode (Jira MSC-67)

///////////////////////////////////////////////////////
// DIAG bits usage ...
///////////////////////////////////////////////////////

// Alarm's bits define

// Alarm's bits
#define SORTER_ALARM_PID_BAD_SPEED					0	// Speed set point error (never under 2% of error!)
#define SORTER_ALARM_PID_ENCODER_TIMEOUT			1	// Encoder timeout ...
#define SORTER_ALARM_BIT_BAD_AUTO_MAIN_KEY			5	// Bad Automatic/Maintenance selector inputs ...
#define SORTER_ALARM_MAINTENANCE_PAD_ENABLED		7	// Maintenance pad error. NB + Group (First used bit = 8)
//__________ Maintenance Pad #1 ____________		8
//__________ Maintenance Pad #2 ____________		9
//__________ Maintenance Pad #3 ____________		10
//__________ Maintenance Pad #4 ____________		11
//__________ Maintenance Pad #5 ____________		12
//
#define SORTER_ALARM_CHDAC_LINK_DOWN				15
#define SORTER_ALARM_CHR_LINK_DOWN					17
#define SORTER_ALARM_CHM_LINK_DOWN					18
#define SORTER_ALARM_CHECKSTATION_FAULT				20	// All Check Position are in fault
#define SORTER_ALARM_CONTROLZONE_FAULT				21	// All Control Zone are in fault
#define SORTER_ALARM_ENCODER_FAULT					22	// Encoder fault
#define SORTER_ALARM_AUXZERO_FAULT					23	// All Cell Zero (Master and Aux) are in fault ...
#define SORTER_ALARM_POWER_FAULT					24  // Energy box problems
#define SORTER_ALARM_SYNCH_FAULT					25	// Cell Zero not received after a full lap ...
#define SORTER_ALARM_SCANNER_FAULT					26	// All Scanner are in fault
#define SORTER_ALARM_MISSING_BLADE					27	// Missing blade detected
#define SORTER_ALARM_CABINET_FAULT					28	// At least one cabinet in fault
#define SORTER_ALARM_POSSIBLE_JAM					29	// Possible jam on Sorter

// Warning's bits define
#define SORTER_WARNING_SPEED_ERROR					0	// Speed error is over the setpoints' 2%
#define SORTER_WARNING_MISSING_BLADE_CHECK_DISABLED	1	// Miising blade control disabled by cabinet key
#define SORTER_WARNING_MAINTENANCE_PANELPC_FAULT	7	// Maint. Panel PC error. NB + Group (First used bit = 8)
//_____ Maintenance Panel PC #1 ____________		8
//_____ Maintenance Panel PC #2 ____________		9
//_____ Maintenance Panel PC #3 ____________		10
//_____ Maintenance Panel PC #4 ____________		11
//_____ Maintenance Panel PC #5 ____________		12
#define SORTER_WARNING_WAITING_VAHLE				13	// Waiting for Vahel inverter
#define SORTER_WARNING_CHDAC_LINK_DOWN				15
//
#define SORTER_WARNING_CHR_LINK_DOWN				17
#define SORTER_WARNING_CHM_LINK_DOWN				18
//
#define SORTER_WARNING_LINK_DOWN_CHI_G1				20 // Induction group 1
#define SORTER_WARNING_LINK_DOWN_CHI_G2				21 // Induction group 2
#define SORTER_WARNING_LINK_DOWN_CHI_G3				22 // Induction group 3
#define SORTER_WARNING_LINK_DOWN_CHI_G4				23 // Induction group 4
#define SORTER_WARNING_LINK_DOWN_CHI_G5				24 // Induction group 5
#define SORTER_WARNING_LINK_DOWN_CHI_G6				25 // Induction group 6
#define SORTER_WARNING_LINK_DOWN_CHI_G7				26 // Induction group 7
#define SORTER_WARNING_LINK_DOWN_CHI_G8				27 // Induction group 8
#define SORTER_WARNING_LINK_DOWN_CHI_G9				28 // Induction group 9

#define SORTER_WARNING_LINK_DOWN_CHU_G1             40 // Upstream group 1
#define SORTER_WARNING_LINK_DOWN_CHU_G2				31 // Upstream group 2
#define SORTER_WARNING_LINK_DOWN_CHU_G3				32 // Upstream group 3
#define SORTER_WARNING_LINK_DOWN_CHU_G4				33 // Upstream group 4
//
