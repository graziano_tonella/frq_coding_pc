//
//	File:		DB_MSC_CellTest.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	30/June/2008
//	Updated:	29/November/2017
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//  29-11-2017 - Added "No coil to use" alarm
//	28-05-2011 - Added support for even/odd cell inductors; position of magnetic block on even/odd cell
//	30-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"
#include "DB_IO_Signals.h"

// MAX Cell Test
#define MAX_CELL_TEST	4

//////////////////////////////////////////////////////////////////////////
//							STRUCT_CELL_TEST (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CELL_TEST
{
	//
	BYTE byCellTestID;				// Control ID
	//
	BYTE byCellTestType;			// See define below ...
	BYTE byCellTestMode;			// See define below ...
	WORD wCellTestFeatures;			// See define below ...
	BYTE byGatewayFieldbusType;		// Gateway type used for communication between fieldbus and RS485 Cell Test device.
									// It is valid only for Serial Cell Test.
	//
	WORD wInductor_Position_Even;	// Inductor position for even cell
	WORD wInductor_Position_Odd; 	// Inductor position for odd cell
	//
	WORD wInductorActionTime;		// Inductor activation time in ms (default 200ms)
	WORD wAntenna_Position;			// Antenna position (used to check cell test input)
	WORD wAntenna_CoveredNIF;		// Length of antenna (NIF)
	BYTE byMagnet_Position_Even;	// NIF magnet position on even cell
	BYTE byMagnet_Position_Odd;		// NIF magnet position on odd cell
	BYTE byMaxConsecutiveError;		// Max number of errors (0 = disabled!)
	//
	BYTE byRollerDiameter;			// Cell Roller diameter [mm]; valid only for Serial Cell Test. Used to convert Frequency detected into Linear Speed.
	BYTE byThicknessBelt;			// Thickness of Cell Belt [mm]; valid only for Serial Cell Test. Used to convert Frequency detected into Linear Speed.
	//
	WORD wThresholdMinimumOKSpeed;	// Minimum threshold speed to consider cell motion OK [mm/s]; valid only for Serial Cell Test.
	WORD wThresholdMaximumOKSpeed;	// Maximum threshold speed to consider cell motion OK [mm/s]; valid only for Serial Cell Test.
	//
	char szEvenExternalOutputCommand[SIZE_SIGNAL_ID+1];	// Even cell inductor output name (external move)
	char szEvenInternalOutputCommand[SIZE_SIGNAL_ID+1];	// Even cell inductor output name (internal move)
	char szOddExternalOutputCommand[SIZE_SIGNAL_ID+1];	// Odd cell inductor output name (external move)
	char szOddInternalOutputCommand[SIZE_SIGNAL_ID+1];	// Odd cell inductor output name (internal move)
	//
	// Runtime counters ...
	BYTE byNIFTestOK;
	BYTE byConsecutiveError;
	WORD wCoilToUseDBIndex_Service;	// DB Index of the coil to use for current service Cell Test action
	//
	STRUCT_DIAG_BLOCK	csDiag;		// Full Diagnostic
	//
}	STRUCT_CELL_TEST;
#pragma pack()

#define SIZE_STRUCT_CELL_TEST		sizeof(_STRUCT_CELL_TEST)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Define for byCellTestType
#define CELL_TEST_TYPE_MESA				1	// Old kind of cell test made by Mesa
#define CELL_TEST_TYPE_LOWCOST			2	// Low cost cell test by R. Macchi
#define CELL_TEST_TYPE_PLC_DIGITAL		3	// Beckhoff PLC made by V. Gattoni
#define CELL_TEST_TYPE_PLC_PROFIBUS		4	// Beckhoff PLC made by V. Gattoni
#define CELL_TEST_TYPE_SERIAL			5	// Serial Cell Test by M. Fumagalli

// Define for byCellTestMode
#define CELL_TEST_MODE_ALL				0	// Test all cells
#define CELL_TEST_MODE_ONLY_ODD			1	// Test only Odd Cell
#define CELL_TEST_MODE_ONLY_EVEN		2	// Test only Even Cell

// Define for byCellTestFeatures
#define CELL_TEST_FEATURES_NO_MOVE_TEST		0x0001
#define CELL_TEST_FEATURES_USE_COIL			0x0002
//#define CELL_TEST_FEATURES_???				0x0004
//#define CELL_TEST_FEATURES_???				0x0008
//#define CELL_TEST_FEATURES_???				0x0010
//#define CELL_TEST_FEATURES_???				0x0020
//#define CELL_TEST_FEATURES_???				0x0040
//#define CELL_TEST_FEATURES_???				0x0080
//#define CELL_TEST_FEATURES_???				0x0100
//#define CELL_TEST_FEATURES_???				0x0200
//#define CELL_TEST_FEATURES_???				0x0400
//#define CELL_TEST_FEATURES_???				0x0800
//#define CELL_TEST_FEATURES_???				0x1000
//#define CELL_TEST_FEATURES_???				0x2000
//#define CELL_TEST_FEATURES_???				0x4000
//#define CELL_TEST_FEATURES_???				0x8000

// Alarm's bit define
#define ALARM_CELL_TEST_HW_FAULT					0
#define ALARM_CELL_TEST_CONSECUTIVE_ERROR			1
#define ALARM_CELL_TEST_INDUCTOR_EVEN_FAULT			2
#define ALARM_CELL_TEST_INDUCTOR_ODD_FAULT			3
#define ALARM_CELL_TEST_NO_COIL_TO_USE				4

// I/O Name Define
#define DI_CELL_TEST_EVEN_INDUCTOR_FEEDBACK	"CELLTEST_EVN_IFB_"		// Input from inductor (HW feedback) even cell
#define DI_CELL_TEST_ODD_INDUCTOR_FEEDBACK	"CELLTEST_ODD_IFB_"		// Input from inductor (HW feedback) odd cell
#define DO_CELL_TEST_EVEN_INTERNAL			"CELLTEST_EVN_INT_CMD_"	// Even cell inductor command internal move
#define DO_CELL_TEST_EVEN_EXTERNAL			"CELLTEST_EVN_EXT_CMD_"	// Even cell inductor command external move
#define DO_CELL_TEST_ODD_INTERNAL			"CELLTEST_ODD_INT_CMD_"	// Odd cell inductor command internal move
#define DO_CELL_TEST_ODD_EXTERNAL			"CELLTEST_ODD_EXT_CMD_"	// Odd cell inductor command external move
//
#define DI_CELL_TEST_ANTENNA			"CELLTEST_FEEDBACK_"	// Input from cell test (CELL_TEST_TYPE_LOWCOST)
//
#define DO_CELL_TEST_CELL				"CELLTEST_NCELL_"		// Number of cell under test (CELL_TEST_TYPE_PLC_PROFIBUS)
#define DO_CELL_TEST_TOTAL_CELL			"CELLTEST_TCELL_"		// Total number of cells (CELL_TEST_TYPE_PLC_PROFIBUS)
#define DO_CELL_TEST_STROBE				"CELLTEST_STROBE_"		// Strobe output (CELL_TEST_TYPE_PLC_PROFIBUS)
#define DO_CELL_TEST_SIDE				"CELLTEST_SIDE_"		// Side run output (CELL_TEST_TYPE_PLC_PROFIBUS)
#define DI_CELL_TEST_RESULT_BIT0		"CELLTEST_PLC_RB0_"		// Result bit 0 (CELL_TEST_TYPE_PLC_PROFIBUS)
#define DI_CELL_TEST_RESULT_BIT1		"CELLTEST_PLC_RB1_"		// Result bit 1 (CELL_TEST_TYPE_PLC_PROFIBUS)

// Default inductor test action time (ms)
#define CELL_TEST_INDUCTOR_ACTION_MS	200

// Define for byGatewayFieldbusType
#define GATEWAY_TYPE_AB7000_ABC_PDP_HMS				1
#define GATEWAY_TYPE_TURCK_BANNER_SDPB_10S_0004		2
