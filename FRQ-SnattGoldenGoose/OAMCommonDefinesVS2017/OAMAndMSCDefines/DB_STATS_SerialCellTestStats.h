//
//	File:		DB_STATS_SerialCellTestStats.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 10.0 (2010)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//				RTX v8.1.2 Build 7818
//				RTX v8.1.2 Build 7818
//				RTX v11.0.0 Build 10540 (RTX2012)
//
//	Version:	1.3
//
//	Created:	21/Feb/2014
//	Updated:	21/Feb/2014
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	21-02-2014 - First release
//

#pragma once

//////////////////////////////////////////////////////////////////////////
//			STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST
{
	//
	WORD	wCellTestLastSpeedDetected;				// It contains the last speed detected from Serial Cell Test. This data must be expressed as
													// linear speed [mm/s].
	//
	WORD	wCellTestAverageSpeedDetected;			// It contains the average speed detected from Serial Cell Test. The average is computed considering
													// the wCellTestNumSampleAcquired samples. This data must be expressed as linear speed [mm/s].
	//
	BYTE	bCellTestNumSampleAcquired;				// It contains the quantity of sample acquired from Serial Cell Test
	//
	FLOAT	fCellTestStdevSpeedDetected;			// It contains the Standard Deviation of wCellTestNumSampleAcquired samples acquired.
	//
	FLOAT	fCellTestFaultPercSpeedDetected;		// It contains the Percentage of Failure computed considering the wCellTestNumSampleAcquired samples.
													// Range values [0..1]
	//
	BYTE	bGoodSamplesFromCoils;
	//
	BYTE	bFaultSamplesFromCoils;
	//
} STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST;
#pragma pack()
#define SIZE_STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST	sizeof(_STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST)

#define WINDOW_BUFFER_SIZE_CELL_TEST 50
#pragma pack(1)
	/**
		STRUCT dedicated to store data support useful for statistical computation
		related to the Test Cell run on each cell.
	*/
	typedef struct _STRUCT_CELL_DATA_SUPPORT {
		//
		DOUBLE	dCellTestAverageSpeedDetected; // Precision Average Speed, computed on DB_MSC_Cell.STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST.wCellTestNumSampleAcquired samples
											   // This value will be approximated and copied into DB_MSC_Cell.STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST.wCellTestAverageSpeedDetected
		//
		ULONG64	ul64CellTestSumOfSquaresSpeedDetected;	// It contains the Sum Of Squares of Speed Acquired from Serial Cell Test.
														// This data is necessary to compute the Standard Deviation value.
		//
		WORD wCellTestSpeedDetected[WINDOW_BUFFER_SIZE_CELL_TEST]; // It contains the last WINDOW_BUFFER_SIZE_CELL_TEST samples acquired from Cell Test
																   // The SumOfSquaresSpeedDetected is computed with this samples
		//
		BOOL bCellTestIsOkDirection[WINDOW_BUFFER_SIZE_CELL_TEST];
		//
		BYTE bIdxSample;
		//
		BYTE bIsArrayFull;
		//
		WORD wCounterSamplesInRange;
		//
	} STRUCT_CELL_DATA_SUPPORT;
	//
#pragma pack()
#define SIZE_STRUCT_CELL_DATA_SUPPORT sizeof(STRUCT_CELL_DATA_SUPPORT)

//////////////////////////////////////////////////////////////////////////
//				STRUCT_SERIALCELLTEST_STATS (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_SERIALCELLTEST_STATS
{
	//
	STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST	csInfoFromCellTestExtMov; // Info of belt speed (External movement) detected from Cell Test
	STRUCT_CELL_INFO_FROM_SERIAL_CELL_TEST	csInfoFromCellTestIntMov; // Info of belt speed (Internal movement) detected from Cell Test
	//
	// Main pointer to Cell Data Support STRUCT
	STRUCT_CELL_DATA_SUPPORT csCellDataSupportIntMov;
	STRUCT_CELL_DATA_SUPPORT csCellDataSupportExtMov;
	//
} STRUCT_SERIALCELLTEST_STATS;
#pragma pack()

#define SIZE_STRUCT_SERIALCELLTEST_STATS	sizeof(_STRUCT_SERIALCELLTEST_STATS)
