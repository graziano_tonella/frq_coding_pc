//
//	File:		DB_MSC_InductorsGroups.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//				RTX v8.1.2 Build 8640
//
//	Created:	11/January/2012
//	Updated:	11/january/2012
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//

#pragma once

//////////////////////////////////////////////////////////////////////////
//						STRUCT_INDUCTORS_GROUPS (DB)
//////////////////////////////////////////////////////////////////////////

#define MAX_INDUCTORS				9
#define MAX_INDUCTORS_OFFSET		MAX_INDUCTORS-1

#pragma pack(1)

typedef struct _STRUCT_INDUCTORS_GROUPS
{
	//
	WORD	wGroupID;									// Group ID (1..X)
	WORD	wInductorQty;								// Quantity of Inductors
	WORD	wWorkingMode;								// Working mode (see below defines ...)
	WORD	wCoveredArea;								// How much is the covered area by 1 inductor (NIF)
	WORD	wInductorOffset[MAX_INDUCTORS_OFFSET];		// Offset (NIF) between inductor; Max is 9 but first inductor pos. its in the chute !
	//
} STRUCT_INDUCTORS_GROUPS;

#pragma pack()

#define SIZE_STRUCT_INDUCTORS_GROUPS	sizeof(_STRUCT_INDUCTORS_GROUPS)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Values for wWorkingMode
#define	INDUCTOR_WORKING_MODE_AT_PITCH		1
#define	INDUCTOR_WORKING_MODE_SINGLE		2
