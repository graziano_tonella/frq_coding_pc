//
//	File:		MSC_Queues.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//
//	Version:	1.0
//
//	Created:	05/June/2008
//	Updated:	05/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	05-06-2008 - First Release
//

#pragma once

//////////////////////////
//	APPLICATION QUEUES	//
//////////////////////////
enum _ENUM_QUEUES
{
	QUEUE_MSC_TRACE_EXT,						// Queue Trace (EXTERNAL)
	QUEUE_MSC_FROM_W32_INT,						// Queue from W32 (INTERNAL)
	QUEUE_MSC_FROM_W32_EXT,						// Queue from W32 (EXTERNAL)
	QUEUE_MSC_TO_W32_INT,						// Queue to W32 (INTERNAL)
	QUEUE_MSC_TO_W32_EXT,						// Queue to W32 (EXTERNAL)
	QUEUE_MSC_THR_ENCODER,						// Main Queue of the Encoder Thread
	QUEUE_MSC_THR_PID_MANAGER,					// Main Queue of the PID Manager Thread
	QUEUE_MSC_THR_SYSTEM_MANAGER,				// Main Queue of the System Manager Thread
	QUEUE_MSC_THR_CHUTE_MANAGER,				// Main Queue of the Chute Manager Thread
	QUEUE_MSC_THR_LOADING_MANAGER,				// Main Queue of the Loading Manager Thread
	QUEUE_MSC_THR_SORTING_MANAGER,				// Main Queue of the Sorting Manager Thread
	QUEUE_MSC_THR_MAIN_TIMER,					// Main Queue of the Main Timer Manager Thread
	QUEUE_MSC_THR_DAC_MANAGER,					// Main Queue of the DAC manager thread
	//
	MSC_MAX_QUEUE								// Total queues ...
	//
};
//
//////////////////
// Queues Name  //
//////////////////
#define QUEUE_NAME_MSC_TRACE_INT			_T("MSC_TRACE_INT")
#define QUEUE_NAME_MSC_TRACE_EXT			_T("MSC_TRACE_EXT")
#define QUEUE_NAME_MSC_FROM_W32_INT			_T("MSC_FROM_W32_INT")
#define QUEUE_NAME_MSC_FROM_W32_EXT			_T("MSC_FROM_W32_EXT")
#define QUEUE_NAME_MSC_TO_W32_INT			_T("MSC_TO_W32_INT")
#define QUEUE_NAME_MSC_TO_W32_EXT			_T("MSC_TO_W32_EXT")
#define QUEUE_NAME_MSC_THR_ENCODER			_T("MSC_THR_ENCODER")
#define QUEUE_NAME_MSC_THR_PID_MANAGER		_T("MSC_THR_PIDMANAGER")
#define QUEUE_NAME_MSC_THR_SYSTEM_MANAGER	_T("MSC_THR_SYSMANAGER")
#define QUEUE_NAME_MSC_THR_CHUTE_MANAGER	_T("MSC_THR_CHUTEMANAG")
#define QUEUE_NAME_MSC_THR_LOADING_MANAGER	_T("MSC_THR_LOADMANAG")
#define QUEUE_NAME_MSC_THR_SORTING_MANAGER	_T("MSC_THR_SORTMANAG")
#define QUEUE_NAME_MSC_THR_MAIN_TIMER		_T("MSC_THR_MAINTMR")
#define QUEUE_NAME_MSC_THR_DAC_MANAGER		_T("MSC_THR_DACMANAG")