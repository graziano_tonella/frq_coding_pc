//
//	File:		DB_MSC_Upstream.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Created:	07/March/2011
//	Updated:	07/March/2011
//
//	Author:		Davide Calabr�
//
#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_UPSTREAM (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_UPSTREAM
{
	WORD	wAbsoluteID;			// Upstream absolute number
	BYTE	byGroupID;				// Upstream group
	BYTE	byUpstreamID;			// Upstream ID inside the group

	STRUCT_DIAG_BLOCK	csDiag;		// Full Diagnostic (RFU)
}	STRUCT_UPSTREAM;
#pragma pack()

#define SIZE_STRUCT_UPSTREAM	sizeof(_STRUCT_UPSTREAM)
