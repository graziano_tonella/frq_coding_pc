//
//	File:		DB_MSC_Encoder.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	27/June/2008
//	Updated:	15/March/2013
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//  15-03-2013 - Missing Blade alarm
//	27-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_ENCODER (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_ENCODER
{
	//
	WORD	wID;				// Encoder ID
	WORD	wEncoderCardType;	// Type of card, see defines below
	WORD	wEncoderIRQMode;	// IRQ mode, see defines below
	BYTE	byPHQty;			// Number of HW photocells
	BYTE	byCellZeroSetNif;	// NIF (absolute on the encoder) to wait for sorter cell zero sync.
	BOOL	bIsVirtual;			// If we have no HW card !
	BOOL	bIsReady;			// Flag to know if the encoder is synchronized !
	WORD	wTotalHwNif;		// Total sorter NIFs
	STRUCT_DIAG_BLOCK	csDiag;	// Full Diagnostic
	//
} STRUCT_ENCODER;
#pragma pack()

#define SIZE_STRUCT_ENCODER	sizeof(_STRUCT_ENCODER)

//////////////////////////////////////////////////////////////////////////
// All possible DEFINES
//////////////////////////////////////////////////////////////////////////

// Values for wEncoderCardType
#define ENCODER_CARD_TYPE_VIRTUAL			0
#define ENCODER_CARD_TYPE_TEWS600			1
#define ENCODER_CARD_TYPE_ADDIDATA1032		2

// Values for wEncoderIRQMode
#define ENCODER_IRQ_MODE_AUTO				0	// IRQ if available, polling otherwise
#define ENCODER_IRQ_MODE_POLLING			1	// Force polling mode

// Type of encoder alarms to check
#define CHECK_ENC_SYNC_ERROR		1
#define CHECK_ENC_RUNTIME_ERROR		2
#define CHECK_ENC_BLINKS			3
#define CHECK_ENC_ALL_RE_FE			4
#define CHECK_ENC_NOT_ALIVE			5

// Cell Zero possible Flags ...
#define ENC_CELL_ZERO_WAIT			0
#define ENC_CELL_ZERO_RECEIVED		1
#define ENC_CELL_ZERO_DONE			2

// Warnings bit define
#define ENC_FIRST_PH_FAULT_BIT		0
#define ENC_FIRST_PH_BLINK_BIT	   32

// Alarms bit define
#define ENC_SW_ALARM_BIT				0
#define ENC_SYNCH_ERROR_BIT				1
#define ENC_RUNTIME_ERROR_BIT			3
#define ENC_CELL0_1_NOT_RECEIVED_BIT	4
#define ENC_CELL0_2_NOT_RECEIVED_BIT	5
#define ENC_CELL0_BAD_EVENT_BIT			6
#define ENC_NOT_ALIVE					7

// I/O's name defines ...
#define DI_NAME_MASTER_CELL0			"CELLZERO_M_"			// "CELLZERO_M_gn"; Master set is on encoder ! g = 0 n = 1..2
#define DI_NAME_SLAVE_CELL0				"CELLZERO_S_"			// "CELLZERO_S_gn"; Slave set is around the machine ! g = x n = 1..2
#define MAX_QTY_SLAVE_CELL0_GROUP		9						// Max number of slave cell 0 group

//////////////////////////////////////////////////////////////////////////
//						STRUCT_ENCODER_EVENT
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ENCODER_EVENT
{
	// New encoder bits image
	DWORD	dwEncoderBits;
	// Time of the event
	LARGE_INTEGER	liEventTime;
	//
} STRUCT_ENCODER_EVENT;
#pragma pack()

#define SIZE_STRUCT_ENCODER_EVENT sizeof(_STRUCT_ENCODER_EVENT)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_ENCODER_EVENT_SPECIAL
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ENCODER_EVENT_SPECIAL
{
	// Real Event...
	STRUCT_ENCODER_EVENT csEncoderEvent;
	
	// Elaborated events...
	DWORD	dwRE_Mask;
	DWORD	dwFE_Mask;
	//
} STRUCT_ENCODER_EVENT_SPECIAL;
#pragma pack()

#define SIZE_STRUCT_ENCODER_EVENT_SPECIAL sizeof(_STRUCT_ENCODER_EVENT_SPECIAL)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_NIF_DATA_FOR_PID
//////////////////////////////////////////////////////////////////////////
//
#define	ENC_MAX_NIF_TIMING		4
//
#pragma pack(1)
typedef struct _STRUCT_NIF_DATA_FOR_PID
{
	// Number of NIF time stored
	WORD wNifTimeStored;
	// Nif Time stored
	DWORD dwNifTimes[ENC_MAX_NIF_TIMING];
	// Nif Time stored
	BYTE byAbsoluteNif[ENC_MAX_NIF_TIMING];
	//
} STRUCT_NIF_DATA_FOR_PID;
#pragma pack()

#define SIZE_STRUCT_NIF_DATA_FOR_PID sizeof(_STRUCT_NIF_DATA_FOR_PID)
//////////////////////////////////////////////////////////////////////////

