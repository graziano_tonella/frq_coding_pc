//
//	File:		DB_MSC_MODULATOR_COIL2.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 10.0 (2010)
//              RTX v12.0
//
//	Version:	1.0
//
//	Created:	06/October/2015
//	Updated:	08/September/2017
//
//	Author:		Andrea Cascarano [+39-0331-665349 andrea.cascarano@fivesgroup.com]
//				Matteo Fumagalli [+39-0331-665280 matteo.fumagalli@fivesgroup.com]
//				Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	08-09-2017 - Splitted possible Modulator feedback timeout
//	30/05/2017 - Added the flag byAuxZeroHW in order to manage the hw type.
//	19-04-2017 - Update: Coil to XChannel generalization; changes of some struct names and fields
//	27-03-2017 - Update: added new field byModulatorCfg useful to manage the Modulator as PARCEL_DETECTION device
//						 added field parameters useful to describe the Modulator FW version
//	26-01-2016 - Revision for final software
//	03-11-2015 - Update: Added the Digital Input useful for IO-Link channel check.
//						 Added ALARM bitmask (ALARM_MODULATOR_IO_LINK_CHANNEL_FAULT).
//	06-10-2015 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

// Number of Channel managed by Modulator
#define MAX_XCHANNEL_PER_MODULATOR		6

// DEI Data Size
#define MODULATOR_DEI_SIZE				32
// DEO Data Size
#define MODULATOR_DEO_SIZE				32

// Trace level to use ...
#define TRACE_LEVEL_MODULATOR	TRACE_LEVEL_15

// Default TX IDLE time ...
#define DEFAULT_TX_IDLE_TIME	30	// 30ms
#define PARAMETER_TX_IDLE_TIME	60	// 60ms

// Default Modulator Timeout Correction
#define DEFAULT_MODULATOR_TIMEOUT_CORRECTION	10 // ms

//////////////////////////////////////////////////////////////////////////
//							STRUCT_XCHANNEL_CFG
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_XCHANNEL_CFG
{
	//
	WORD	wEnabledFunction;
	int		nReferencePosition;		// Can be +/-, can be NIF or mm or ms, depending on function
	WORD	wXChannelDBIndex;
	//
} STRUCT_XCHANNEL_CFG;
#pragma pack()
// Size defines ...
#define SIZE_STRUCT_XCHANNEL_CFG	sizeof(_STRUCT_XCHANNEL_CFG)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_XCHANNEL_DATA
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_XCHANNEL_DATA
{
	//
	DWORD	dwModulatorID;				// Modulator ID
	WORD	wModulatorDBIndex;			// DB Index of Modulator
	BYTE	bySocket;					// XChannel Socket
	//
	WORD	wTimeoutCounter;			// Timeout counter, after 3 is alarm ...
	//
	BYTE	byEnabledForceCoil;			// Enabled Forced Coil Activation
	//
} STRUCT_XCHANNEL_DATA;
#pragma pack()
// Size defines ...
#define SIZE_STRUCT_XCHANNEL_DATA	sizeof(_STRUCT_XCHANNEL_DATA)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_XCHANNEL (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_XCHANNEL
{
	//
	STRUCT_XCHANNEL_CFG			csXChannelConfig;	// Configuration data (from Modulator CSV)
	//
	// Runtime fields
	//
	STRUCT_XCHANNEL_DATA		csRuntimeData;	// All runtime data
	STRUCT_DIAG_BLOCK			csDiag;			// Full Diagnostic
	//
} STRUCT_XCHANNEL;
#pragma pack()
// Size defines ...
#define SIZE_STRUCT_XCHANNEL sizeof(_STRUCT_XCHANNEL)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_MODULATOR_PARAMETER
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_MODULATOR_PARAMETER
{
	//
	WORD wBitTime_us;						// 
	WORD wIdleTime_us;						//
	WORD wDelayTimeAfterTrigger_ms;			//
	WORD wTimeout_ms;						//
	WORD wShowLedMagneticTrigger_ms;		//
	WORD wDelayTimeBetweenFeedbacks_ms;		//
	WORD wTimeTriggerError_ms;				//
	WORD wTimeAddedToFirstBit_us;			//
	WORD wTimeLongMagnet_ms;				//
	//
	// The following parameter are READ ONLY
	WORD wFwMajorVersion;					//
	WORD wFwMinorVersion;					//
	WORD wFwBuildVersion;					//
	WORD wFwDateVersion;					// Firmware Date Version (bit 11..15 DAY - bit 7..10  MONTH - bit 0..6   YEAR (starting from 2000 d.c.))
	//
} STRUCT_MODULATOR_PARAMETER;
#pragma pack()
// Size defines ...
#define SIZE_STRUCT_MODULATOR_PARAMETER	sizeof(_STRUCT_MODULATOR_PARAMETER)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_MODULATOR_COMMAND
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_MODULATOR_COMMAND
{
	//
	BOOL bUsedSlot;								// TRUE if filled slot, FALSE if is empty !
	BOOL bProcessDone;							// TRUE if action was done and we can compute the result
	//
	WORD wProcessTimeout;						// Process timeout (from modulator add to reply from coil)
	//
	BYTE byActionType;							// Action Type: 1 = Chute Unload 2 = Join ...
	BOOL bFireWhenTrigger;						// Coil must wait for a Cell ...
	WORD wRotation;								// Specify left or right for "TestCell" action type
	//
	WORD wCell;									// Cell under action
	BYTE byCellCount;							// 1 = Master, 2..X = slave
	WORD wChuteID;								// Chute under action
	WORD wChuteDBIndex;							// Chute DB Index for quick access
	WORD wCoilDBIndex;							// Coil DB Index for quick access
	WORD wModulatorDBIndex;						// Modulator DB Index for quick access
	//
	LARGE_INTEGER liRequestTime;				// Request time
	BYTE byCoilProcessStep;						// Coil process working step, see below defines ...
	BYTE byCoilResult;							// Coil Result: 1 = DONE 2 = Coil Reply Timeout 3 = Timeout from Coil ...
	//
	WORD wCellActionDelay;						// Time (ms) to wait before activate the Cell
	//
	WORD wCommandID;							// Command ID for Digital Antenna (please refers to the ENUM_COMMAND_TO_DIGITAL_ANTENNA_TYPE declared into the STD_Message_To_Digital_Antenna.h)
	WORD wGenericValue;							// A generic parameter used to store values or parameters (i.e. maintenance move time)
	//
} STRUCT_MODULATOR_COMMAND;
#pragma pack()
#define SIZE_STRUCT_MODULATOR_COMMAND sizeof(_STRUCT_MODULATOR_COMMAND)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_MODULATOR (DB)
//////////////////////////////////////////////////////////////////////////
//
enum _MaxModulatorTimeoutCounter{
	//
	MODULATOR_GLOBAL_FEEDBACK_TIMEOUT = 0,
	//
	MODULATOR_PARAMETER_FEEDBACK_TIMEOUT,
	MODULATOR_COMMAND_FEEDBACK_TIMEOUT,
	MODULATOR_MESSAGE_FEEDBACK_TIMEOUT,
	//
	MAX_MODULATOR_TIMEOUT_COUNTER
	//
};
//
#pragma pack(1)
typedef struct _STRUCT_MODULATOR
{
	//
	// CFG fields (from CSV)
	//
	DWORD					dwModulatorID;									// Modulator ID
	BYTE					byModulatorCfg;									// Modulator Configuration
	STRUCT_XCHANNEL_CFG		csXChannelConfig[MAX_XCHANNEL_PER_MODULATOR];	// Number of Channel managed by one modulator
	//
	// Runtime fields
	//
	STRUCT_MODULATOR_PARAMETER	csParameter;							// Parameters
	STRUCT_DIAG_BLOCK			csDiag;									// Full Diagnostic
	//
	WORD	wTimeoutCounter[MAX_MODULATOR_TIMEOUT_COUNTER];			// Timeout counter, after 3 is alarm ...
	//
	BYTE	byActivateMappingMode;										// Not Zero to activate mapping mode ...
	BYTE	byCurrentWorkingState;										// Current modulator working state
	//
} STRUCT_MODULATOR;
#pragma pack()
// Size defines ...
#define SIZE_STRUCT_MODULATOR	sizeof(_STRUCT_MODULATOR)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_ACTION_FEEDBACK
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_ACTION_FEEDBACK
{
	//
	BYTE	byActionType;
	BYTE	byActionResult;
	//
	WORD	wCell;
	WORD	wChuteID;
	//
	// Fields useful for unload filter (In Flight Objects manager)
	WORD	wActionDelay;				// Time Action Delay [ms]: time between Coil Mission and Cell movement
	WORD	wOnFullEventTimeout;		// Full Event Timeout [ms]: time between Coil Mission and Full Photoeye Rising Edge
	//	
} STRUCT_ACTION_FEEDBACK;
#pragma pack()
#define SIZE_STRUCT_ACTION_FEEDBACK sizeof(_STRUCT_ACTION_FEEDBACK)

enum ENUM_COIL_CLOSEST_AT_SIDE
{
	COIL_CLOSEST_AT_FROM_SIDE =						1,
	COIL_CLOSEST_AT_TO_SIDE
	//
};


//////////////////////////////////////////////////////////////////////////
// I/O Defines
//////////////////////////////////////////////////////////////////////////

// Digital Input useful for IO-Link channel check
#define DI_MODULATOR_IOLINK_ERROR			"MODULATOR_IOL_ERR_"	//			MODULATOR_IOL_xxxxx			xxxxx = Modulator ID [dwModulatorID]
// Data Exchange Input for 32 Input byte received from the Modulator devices
#define DEI_MODULATOR_MSG					"MODULATOR_DEI_"		//			MODULATOR_MSG_xxxxx			xxxxx = Modulator ID [dwModulatorID]
// Data Exchange Output for 32 Output byte sent to the Modulator devices
#define DEO_MODULATOR_MSG					"MODULATOR_DEO_"		//			MODULATOR_MSG_xxxxx			xxxxx = Modulator ID [dwModulatorID]

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Defines for wEnabledFunction of STRUCT_XCHANNEL_CFG
#define COIL_FUNCTION_CAN_JOIN							0x0001			// Coil can be used to join antennas
#define COIL_FUNCTION_CELL_ACTIVATION					0x0002			// Coil can be used to activate cell (cell movement)
#define COIL_FUNCTION_COORDINATOR						0x0004			// Coil can be used for Coordinator operation
#define COIL_FUNCTION_MAINTENANCE						0x0008			// Coil can be used for Maintenance area
#define COIL_FUNCTION_AUX_ZERO							0x0010			// Coil can be used to detect the Long Magnet installed on Zero Cell
// spare												0x0020
// spare												0x0040
// spare												0x0080
#define CHANNEL_FUNCTION_PH_SYNCH						0x0100			// Channel used for PH_SYNCH acquisition
#define CHANNEL_FUNCTION_PH_ON_GAP						0x0200			// Channel used for PH_ON_GAP acquisition
#define CHANNEL_FUNCTION_STROBE_DO						0x0400			// Channel used for STROBE_DO output
// spare												0x0800
// spare												0x1000
// spare												0x2000
// spare												0x4000
#define CHANNEL_FUNCTION_LINKED_CHECK_POSITION			0x8000			// Channel used as CheckPosition_ID where forward the Modulator Diagnostic

// Defines for byActionType of STRUCT_MODULATOR_COMMAND
#define COIL_ACTION_TYPE_UNLOAD				1				// Action "Unload cell"
#define COIL_ACTION_TYPE_JOIN				2				// Action "Join"
#define COIL_ACTION_TYPE_TESTCELL			3				// Action "Test Cell", run cell for test cell
#define COIL_ACTION_TYPE_RECENTERING		4				// Action "Recentering", run cell for recentering
#define COIL_ACTION_TYPE_SEND_COMMAND		5				// Action to send a command to digital antenna
#define COIL_ACTION_TYPE_MAINTENANCE		6				// Action to send a command to maintenance area
#define COIL_ACTION_TYPE_GENERATE_STROBE	7				// Action to generate a strobe, i.e. Tunnel trigger

// Defines for byCoilResult of STRUCT_MODULATOR_COMMAND
#define COIL_RESULT_DONE					1				// Coil Mission sent to Digital Antenna
#define COIL_RESULT_MODULATOR_TIMEOUT		2				// No reply from modulator
#define COIL_RESULT_DA_TIMEOUT				3				// Digital Antenna not detected by Coil or HW fault
#define COIL_RESULT_NOT_POSSIBLE			4				// Coil not available/fault/not enabled for a specific function
#define COIL_RESULT_TOO_LATE				5				// Coil Action too late
#define COIL_RESULT_SORTER_NOT_READY		6				// Sorter not ready
#define COIL_RESULT_MODULATOR_NOT_READY		7				// Modulator not ready (fault or in Map or not in Normal Mode)
#define COIL_RESULT_ANTENNA_NO_JOIN			8				// Target antenna has not completed the join procedure
#define COIL_RESULT_FAULT					9				// Modulator reported a feedback with error
#define COIL_RESULT_INTERNAL_SW_ERROR		10				// One of our function return FALSE ! (Should never happen !!!!)

// Define for byCoilProcessStep
#define COIL_PROCESS_STEP_CREATED				1			// Process created ...
#define COIL_PROCESS_STEP_IN_QUEUE				2			// Process added to modulator queue ...
#define COIL_PROCESS_STEP_WAIT_FEEDBACK			3			// Wrote to modulator and waiting for feedback ...

// Define for byModulatorCfg
#define MODULATOR_COIL_MANAGEMENT				1			// Standard Modulator device (it manages only Coil devices)
#define MODULATOR_PHT_MANAGEMENT				2			// Modulator used for Parcel Detection (it manages two photoeyes: PH_SYNCH and PH_ON_GAP)
#define MODULATOR_STROBE_MANAGEMENT				3			// Modulator used for Strobe (DO) generation (it manages: PH_SYNCH and STROBE_DO)
#define MODULATOR_PHT_AND_STROBE_MANAGEMENT		4			// Modulator used for Parcel Detection and Strobe (DO) generation

// Define for byActivateMappingMode
#define MODULATOR_NO_MAPPING					0			// Mapping function not active
#define MODULATOR_MAPPING_MODE					1			// Mapping function active
#define MODULATOR_MAPPING_AND_SAVE_MODE			2			// Mapping function with data save active

// Define for byCurrentWorkingState (MAX 7 values!)
#define MODULATOR_STATE_NOT_WORKING							0			// Modulator is not working
#define MODULATOR_STATE_NORMAL_MODE							1			// Modulator settled to normal mode
#define MODULATOR_STATE_MAPPING_MODE						2			// Modulator settled to mapping mode
#define MODULATOR_STATE_TEST_MODE							3			// Modulator settled to Test mode (NOT USED via MSC)
#define MODULATOR_STATE_PARCEL_DETECTION					4			// Modulator settled to Parcel Detection mode
#define MODULATOR_STATE_DEVICE_TRIGGER						5			// Modulator settled to Device Trigger (STROBE DO)
#define MODULATOR_STATE_PARCEL_DETECT_AND_DEVICE_TRG		6			// Modulator settled to Parcel Detection and Device Trigger (STROBE DO)

//////////////////////////////////////////////////////////////////////////
// All possible alarm/warning DEFINES (MODULATOR)
//////////////////////////////////////////////////////////////////////////

// Alarm's bit define
#define ALARM_MODULATOR_IO_LINK_CHANNEL_FAULT		0		// From IO-Link input (auto-resetting because coming from DI)
#define ALARM_MODULATOR_WATCHDOG_FAULT				1		// Fault from internal watchdog
#define ALARM_MODULATOR_QUEUE_FULL					2		// The internal queue towards the Modulator is full (Internal sw problem)
#define ALARM_MODULATOR_PAR_ACK_TIMEOUT				3		// Missing reply related to a Parameter message
#define ALARM_MODULATOR_CMD_ACK_TIMEOUT				4		// Missing reply related to a Command message
#define ALARM_MODULATOR_MSG_ACK_TIMEOUT				5		// Missing reply related to a Coil-Mission message
#define ALARM_MODULATOR_NO_RESPONSE					6		// After x missing reply, regardless if command, parameter or coil message
#define ALARM_MODULATOR_DISABLED_BY_SCADA			7		// Modulator was disabled by SCADA
//
#define ALARM_MODULATOR_PHT_SYNCH_ALWAYS_ON			10		// The Coil channel used to acquire the PH_SYNCH is ALWAYS_ON
#define ALARM_MODULATOR_PHT_SYNCH_ALWAYS_OFF		11		// The Coil channel used to acquire the PH_SYNCH is ALWAYS_OFF
#define ALARM_MODULATOR_PHT_ONGAP_ALWAYS_ON			12		// The Coil channel used to acquire the PH_ONGAP is ALWAYS_ON
//
#define ALARM_MODULATOR_REASON_UNKNOWN				31		// Default alarm for SW reasons...
//

// Warning's bit define
#define WARNING_MODULATOR_IO_LINK_CHANNEL_FAULT		0		// From IO-Link input (auto-resetting because coming from DI)
#define WARNING_MODULATOR_WATCHDOG_FAULT			1		// From internal watchdog
#define WARNING_MODULATOR_QUEUE_FULL				2		// The internal queue towards the Modulator is full (Internal sw problem)
#define WARNING_MODULATOR_PAR_ACK_TIMEOUT			3		// Missing reply related to a Parameter message
#define WARNING_MODULATOR_CMD_ACK_TIMEOUT			4		// Missing reply related to a Command message
#define WARNING_MODULATOR_MSG_ACK_TIMEOUT			5		// Missing reply related to a Coil-Mission message
#define WARNING_MODULATOR_NO_RESPONSE				6		// After x missing reply, regardless if command, parameter or coil message
#define WARNING_MODULATOR_DISABLED_BY_SCADA			7		// Modulator was disabled by SCADA
//
#define WARNING_MODULATOR_PHT_SYNCH_ALWAYS_ON		10		// The Coil channel used to acquire the PH_SYNCH is ALWAYS_ON
#define WARNING_MODULATOR_PHT_SYNCH_ALWAYS_OFF		11		// The Coil channel used to acquire the PH_SYNCH is ALWAYS_OFF
#define WARNING_MODULATOR_PHT_ONGAP_ALWAYS_ON		12		// The Coil channel used to acquire the PH_ONGAP is ALWAYS_ON
//
#define WARNING_MODULATOR_REASON_UNKNOWN			31		// Default alarm for SW reasons...
//

//////////////////////////////////////////////////////////////////////////
// All possible alarm/warning (COIL)
//////////////////////////////////////////////////////////////////////////

// Alarm's bit define
#define ALARM_COIL_OPEN_CIRCUIT					1		// HW problem
#define ALARM_COIL_SHORT_CIRCUIT				2		// HW problem
#define ALARM_COIL_BIT_ONE_MODULATION_FAULT		3		// Error during the Bit ONE Modulation
#define ALARM_COIL_BIT_ZERO_MODULATION_FAULT	4		// Error during the Bit ZERO Modulation
#define ALARM_COIL_NO_TRIGGER					5		// Magnetic trigger not received (NEVER USED YET)
#define ALARM_COIL_ERROR_TRIGGER				6		// ??? (not clear maybe not used)

#define ALARM_COIL_NO_RESPONSE					7		// Timeout on message

// Warning's bit define
#define WARNING_COIL_OPEN_CIRCUIT				1		// HW problem
#define WARNING_COIL_SHORT_CIRCUIT				2		// HW problem
#define WARNING_COIL_BIT_ONE_MODULATION_FAULT	3		// Error during the Bit ONE Modulation
#define WARNING_COIL_BIT_ZERO_MODULATION_FAULT	4		// Error during the Bit ZERO Modulation
#define WARNING_COIL_NO_TRIGGER					5		// Magnetic trigger not received
#define WARNING_COIL_ERROR_TRIGGER				6		// ??? (not clear maybe not used)
