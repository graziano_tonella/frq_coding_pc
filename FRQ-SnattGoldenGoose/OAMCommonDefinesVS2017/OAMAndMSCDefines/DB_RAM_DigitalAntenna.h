//
//	File:		DB_RAM_DigitalAntenna.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 10.0 (2010)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//				RTX v8.1.2 Build 7818
//				RTX v8.1.2 Build 7818
//				RTX v11.0.0 Build 10540 (RTX2012)
//
//	Version:	1.3
//
//	Created:	31/Mar/2016
//	Updated:	31/Mar/2016
//
//	Author:		Matteo Fumagalli [+39-0331-665280 matteo.fumagalli@fivesgroup.com]
//
//  History:
//
//	31-03-2016 - First release: This file contains the table definition for Digital_Antenna stored in DB_RAM
//

#pragma once

#define MAX_STATIC_PARAM 110
//////////////////////////////////////////////////////////////////////////
//			STRUCT_DIGITAL_ANTENNA_STATIC_PARAM (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_DIGITAL_ANTENNA_STATIC_PARAM
{
	// The ID_PARAMETER is the index of the array
	WORD	wParamTargetValue[MAX_STATIC_PARAM];
	WORD	wParamCurrentValue[MAX_STATIC_PARAM];
	BOOL	bMustBeEqual[MAX_STATIC_PARAM];

} STRUCT_DIGITAL_ANTENNA_STATIC_PARAM;
#pragma pack()

//////////////////////////////////////////////////////////////////////////
//			STRUCT_DIGITAL_ANTENNA_DIAGNOSTIC_DATA (DB)
//////////////////////////////////////////////////////////////////////////
//

enum _GENERATING_WHEEL_CONNECTIONS
{
	GEN_WHEEL_SERIES_CONNECTION = 0,
	GEN_WHEEL_PARALLEL_CONNECTION,
	GEN_WHEEL_RIGHT_DISCONNECTED,
	GEN_WHEEL_SERIES_CONNECTION_INHIBITED
};

#pragma pack(1)
typedef struct _STRUCT_DIGITAL_ANTENNA_DIAGNOSTIC_DATA
{
	// BOOL variable: TRUE when the MSC requests DIAG via Coil
	//				  FALSE when the DIAG is retrieved via Radio
	BYTE	by_RequestedDiag;
	//
	BYTE	by_DiagIsConsinstent;
	//
	BYTE	by_DiagCounters;
	// Bus Voltage
	WORD	w_BusVoltage;				// [0.1 V]
	//
	// Left Generating-Wheel Voltage
	BYTE	by_LeftGenWheel_Voltage;	// [V]
	BYTE	by_RightGenWheel_Voltage;	// [V]
	//
	// Frequency Left Generating-Wheel
	BYTE	by_LeftGenWheel_Freq;		// [Hz]
	//
	// Generating-Wheel diagnostic
	BOOL	b_HasGenWheel;
	BYTE	by_GenWheelConnectionType;	// please refers to the ENUM _GENERATING_WHEEL_CONNECTIONS
	BOOL	b_BreakingResistorInserted; // ON / OFF
	BYTE	by_ExtimatedParcelWeight;	// Value ZERO corresponds to cell empty (or parcel weight lower than 5Kg).
	// Value different from ZERO corresponds to 5Kg steps
	//
	// PCB Temperature
	signed short	sh_PCB_Temperature;	// Signed value
	//
	// Last motion mission received
	DWORD	dw_LastMission;
	//
	// FW Version
	WORD	w_FirmwareMajorVer;
	WORD	w_FirmwareMinorVer;
	WORD	w_FirmwareBuildVer;
	WORD	w_RepoVer;
	//
	// RF Radio-Channel
	BYTE	by_RF_Channel;	// Range value [11 .. 26]
	//
	// Cell, Plant and PresetParameter Number
	WORD	w_CellNumber;
	BYTE	by_PlantNumber;
	BYTE	by_PresetParameter;
	//
	// Fault bitmask
	DWORD	dw_Fault;
	//
	// Warning bitmask
	DWORD	dw_Warning;
	//
	// Speed and I_Mot
	WORD	w_DataSpeed;
	WORD	w_DataIMot;
	//
	// Counter Errors
	WORD	w_CounterError;
	WORD	w_CounterError2;
	WORD	w_CounterError5;
	//
	// Time of last Radio Diagnostic Message
	SYSTEMTIME	csTimeOfReceivingData;
	//

} STRUCT_DIGITAL_ANTENNA_DIAGNOSTIC_DATA;
#pragma pack()

#define SIZE_STRUCT_DIGITAL_ANTENNA_DIAGNOSTIC_DATA	sizeof(_STRUCT_DIGITAL_ANTENNA_DIAGNOSTIC_DATA)

//////////////////////////////////////////////////////////////////////////
//			STRUCT_DIGITAL_ANTENNA_RAM
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_DIGITAL_ANTENNA_RAM
{
	//
	STRUCT_DIGITAL_ANTENNA_STATIC_PARAM		csStaticParam;
	// Dynamic Parameters to be shown into the MIF tool....
	//
	// Dynamic values retrieved via Radio Communication
	STRUCT_DIGITAL_ANTENNA_DIAGNOSTIC_DATA	csDynamicData;
	//
	// Last Antenna Mission (24 bit mask) loaded from MSC or ISPC apps
	DWORD									dwMotionAntennaMission;
	//
	// Counter used to manage the fw reboot
	BYTE									byCounterForFwReboot;
	//
} STRUCT_DIGITAL_ANTENNA_RAM;
#pragma pack()

#define SIZE_STRUCT_DIGITAL_ANTENNA_RAM	sizeof(_STRUCT_DIGITAL_ANTENNA_RAM)
