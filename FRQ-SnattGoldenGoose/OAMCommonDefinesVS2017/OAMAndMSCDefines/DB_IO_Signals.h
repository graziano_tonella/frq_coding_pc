//
//	File:		DB_IO_Signals.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//				RTX v8.1.2 Build 8640
//
//	Version:	1.0
//
//	Created:	11/June/2008
//	Updated:	15/April/2011
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	15-04-2011 - Updated SIZE_SIGNAL_ID from 20 to 25
//  04-06-2010 - Added new signal type DEI, DEO to process Data Exchange with BlindMemoryX functions
//	11-06-2008 - First release
//

#pragma once

//////////////////////////////////////////////////////////////////////////
//						STRUCT_IO_SIGNAL (DB)
//////////////////////////////////////////////////////////////////////////

#define SIZE_SIGNAL_ID		25
#define	MAX_IO_INT_TIMERS	 4

#pragma pack(1)
typedef struct _STRUCT_IO_SIGNAL
{
	// Identification fields
	WORD	wDBIndex;
	char	szSignalId[SIZE_SIGNAL_ID+1];
	BYTE	byType;
	BYTE	byLogic;
	// Functionality fields
	BYTE	byMode;
	WORD	wModeTimerValue;
	WORD	wZeroToOneFilter;
	WORD	wOneToZeroFilter;
	DWORD	dwAlwaysOneFilter;
	DWORD	dwAlwaysZeroFilter;
	WORD	wEventMaskBit;
	BYTE	byQueueId;
	// Status fields
	short	shHwValue;
	short	shSwValue;
	short	shOldSwValue;
	BYTE	byFlags;
	// Hardware positions fields
	BYTE	byCardID;
	WORD	wNodeID;
	WORD	wOffset;
	BYTE	byBit;
	WORD	wDeoDeiSize;

private:
	// Links to busfield (internal)
	//
	// Since this pointer allows direct access to the DB_IO_NODES shared memory,
	// it is important that it only gets used by CIO_Manager itself, as this
	// class controls the access to the shared resource.
	LPBYTE	lpbyData;

friend class CIO_Manager;

public:
	BYTE	byMaskBit;
	// Utility fields
	BYTE	byTickCntFlag[MAX_IO_INT_TIMERS];
	DWORD	dwTickCntValue[MAX_IO_INT_TIMERS];

} STRUCT_IO_SIGNAL;
#pragma pack()

#define SIZE_STRUCT_IO_SIGNAL sizeof(_STRUCT_IO_SIGNAL)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_IO_EXTRA_INFO
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_IO_EXTRA_INFO
{
	// Hardware positions fields
	BYTE	byCardID;
	WORD	wNodeID;
	WORD	wOffset;
	BYTE	byBit;
	// Index into DB !
	WORD	wDBIndex;
	//
} STRUCT_IO_EXTRA_INFO;
#pragma pack()

#define SIZE_STRUCT_IO_EXTRA_INFO sizeof(_STRUCT_IO_EXTRA_INFO)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_IO_NOTIFY
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_IO_NOTIFY
{
	// Name of the signal
	char	szSignalId[SIZE_SIGNAL_ID+1];
	// DB Index
	WORD	wDBIndex;
	// Type of event
	WORD	wEvent;
	// Flag ...
	BYTE	byFlags;
	// Actual machine NIF position
	WORD	wCurrentNifPosition;
	// Event time; from RtGetClockTime
	LARGE_INTEGER	liEventTime;

} STRUCT_IO_NOTIFY;
#pragma pack()

#define SIZE_STRUCT_IO_NOTIFY sizeof(_STRUCT_IO_NOTIFY)

//////////////////////////////////////////////////////////////////////////
//								DEFINES
//////////////////////////////////////////////////////////////////////////

// byFlags Defines
#define SIGNAL_FLAG_RUNNING		0x01
#define SIGNAL_FLAG_USED   		0x02
#define SIGNAL_FLAG_USE_BRAKE	0x04
#define SIGNAL_FLAG_AUTO_RESET	0x10

// byType Defines
#define SIGNAL_TYPE_DI		  1		// Digital Input (single bit)
#define SIGNAL_TYPE_DO		  2		// Digital Output (single bit)
#define SIGNAL_TYPE_AI		  3		// Analog Input (16 bit)
#define SIGNAL_TYPE_AO		  4		// Analog Output (16 single bit)
#define SIGNAL_TYPE_VI		  5		// Fake signal on Input table
#define SIGNAL_TYPE_VO		  6		// Fake signal on Output table (filedbus not updated)
#define SIGNAL_TYPE_DEI		  7		// Data Exchange as Input (Used for BlindMemoryRead)
#define SIGNAL_TYPE_DEO		  8		// Data Exchange as Output (Used for BlindMemoryWrite)
#define SIGNAL_TYPE_VAI		  9		// Virtual Analog Input (16 bit)
#define SIGNAL_TYPE_VAO		  10	// Virtual Analog Output (16 single bit)

// byLogic Defines
#define SIGNAL_LOGIC_NO		  0
#define SIGNAL_LOGIC_NC		  1

// wEventMaskBit Defines
#define SIGNAL_EVENT_MASKBIT_RE		0x0001 // Rising Edge
#define SIGNAL_EVENT_MASKBIT_FE		0x0002 // Falling Edge
#define SIGNAL_EVENT_MASKBIT_A0		0x0004 // Always 0
#define SIGNAL_EVENT_MASKBIT_A1		0x0008 // Always 1
#define SIGNAL_EVENT_MASKBIT_MAP	0x0010 // Mapping
#define SIGNAL_EVENT_MASKBIT_FORCE0	0x0020 // Force DI/DO to 0 value
#define SIGNAL_EVENT_MASKBIT_FORCE1	0x0040 // Force DI/DO to 1 value

// shSwValue Defines valid for DO
#define OFF						0
#define ON						1
#define SLOW_FLASH				2
#define FAST_FLASH				3
#define CUSTOM_FLASH			4
#define PULSE					5
#define PULSE_TIME				6
#define CUSTOM_FLASH_TIME		7
#define PULSE_FLASH				8
#define TEST_ON					9
#define TEST_OFF				10

// Define of the wMSGID used for I/O event
#define wMSGID_EVENT_IO			0x00FF

// Trace level for I/O mapping events
#define IO_MAP_EVENT_TRACE_LEVEL	99
