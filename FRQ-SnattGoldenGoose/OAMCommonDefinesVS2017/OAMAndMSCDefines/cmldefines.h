//
//	File:		CMLDefines.h
//
//	Created:	04/Nov/1999
//	Updated:	23/Oct/2013
//				18/Jan/2017
//				25/Sep/2017
//				22/May/2018
//
//	Contributors:	Davide Calabro'
//					Graziano Tonella
//                  Marco Piazza
//
//	22/July/2004		Davide Calabro'		Changed STRUCT_ENVELOPECML::bySpare to STRUCT_ENVELOPECML::byFlags
//	23/July/2008		Davide Calabro'		Added RTXCMD_START, RTXCMD_STOP, RTXCMD_EMPTY
//	24/July/2008		Davide Calabro'		Changed RTXCMD_ABIL to RTXCMD_ENABLE. Changed RTXCMD_DISAB to RTXCMD_DISABLE
//  27/August/2010		Marco Piazza		Added RTXCMD_RESET_AS_KEY, used to send reset as the main cabinet key (full reset)
//	06/September/2012	Davide Calabro'		Added RTXCMD_CHANGE_SETPOINT
//	06/September/2012	Marco Piazza		Added RTXCMD_ESTOP_RESET to give a pulse to EMERGENCY_RST output
//	23/October/2013		Marco Piazza		Added RTXCMD_CHUTE_UNLOAD_RATE to set Chute Unload Rate from SCADA/OAM
//	18/January/2017		Davide Calabro'		Remove "#pragma once" preprocessor define and restored "#ifndef/#define" statements
//	25/September/2017	Davide Calabro'		Added CML_ACK and CML_NACK defines
//	22/May/2018			Davide Calabro'		Extended protocol implementation
//
//#pragma once
#ifndef	_CMLDEFINES_H_
#define	_CMLDEFINES_H_

#define	CML_SOM			0xFD
#define	CML_EOM			0xFE

#define	CML_ACK			1
#define	CML_NACK		3

// Commands to device
#define	RTXCMD_ENABLE				1
#define	RTXCMD_DISABLE				2
#define	RTXCMD_RESET				3
#define	RTXCMD_START				4
#define	RTXCMD_STOP					5
#define	RTXCMD_EMPTY				6
#define RTXCMD_RESET_AS_KEY			7
#define RTXCMD_CHANGE_SETPOINT		8
#define RTXCMD_ESTOP_RESET			9
#define	RTXCMD_INDUCTOR				10
#define	RTXCMD_CHUTE_UNLOAD_RATE	11

#define NEXT_PROG(p) \
{\
	p++;				\
	if (p > 9) p = 2;	\
}
//////////////////////////////////////////////////////////////////////////
//							STRUCT_ENVELOPECML
//////////////////////////////////////////////////////////////////////////
//
// Data block is encrypted -------------------------------+
//                                                        |
//				            +---+---+---+---+---+---+---+---+
//				            | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 | Bits
// 			                +---+---+---+---+---+---+---+---+
//						                 byFlags
//
#pragma pack(1)
typedef struct _STRUCT_ENVELOPECML
{
	BYTE		bySOM;
	BYTE		byClass;
	BYTE		bySourceNode;
	BYTE		byDestNode;
	BYTE		byProg;
	BYTE		byFlags;
	//WORD		wSpare;
	WORD		wLenHigh;
	WORD		wLen;
	WORD		wNumMsg;
} STRUCT_ENVELOPECML;
#pragma pack()

#define SIZE_STRUCT_ENVELOPECML sizeof(_STRUCT_ENVELOPECML)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_TAILCML
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_TAILCML
{
	BYTE		bySpare;
	WORD		wSpare;
	BYTE		byEOM;
} STRUCT_TAILCML;
#pragma pack()

#define SIZE_STRUCT_TAILCML sizeof(_STRUCT_TAILCML)

#define MSGCML_MAX_DATALEN			512*10
#define SINGLEMSGCML_MAX_DATALEN	512

//////////////////////////////////////////////////////////////////////////
//							STRUCT_HEADERMSGCML
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_HEADERMSGCML
{
	WORD		wMsgId;
	WORD		wDataLen;
} STRUCT_HEADERMSGCML;
#pragma pack()

#define SIZE_STRUCT_HEADERMSGCML sizeof(_STRUCT_HEADERMSGCML)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_HEADERMSGCML_EX
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
struct STRUCT_HEADERMSGCML_EX
{
	WORD		wMsgId;		// Fixed to 0xDEAD
	WORD		wDataLen;	// Fixed to 0xBEEF
	DWORD		dwMsgId;
	DWORD		dwDataLen;
};
#pragma pack()

#define SIZE_STRUCT_HEADERMSGCML_EX sizeof(STRUCT_HEADERMSGCML_EX)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_SINGLEMSGCML
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_SINGLEMSGCML
{
	STRUCT_HEADERMSGCML	csHeader;
	BYTE				byData[SINGLEMSGCML_MAX_DATALEN - SIZE_STRUCT_HEADERMSGCML];
} STRUCT_SINGLEMSGCML;
#pragma pack()

#define SIZE_STRUCT_SINGLEMSGCML sizeof(_STRUCT_SINGLEMSGCML)

#define MAX_SIZE_MSGCML			(SIZE_STRUCT_ENVELOPECML + MSGCML_MAX_DATALEN + SIZE_STRUCT_TAILCML)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_ENVELOPEOLDCML
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ENVELOPEOLDCML
{
	BYTE		bySOM;
	BYTE		byLen;
	BYTE		byDestNode;
	BYTE		byClass;
	BYTE		byProg;
    BYTE		bySourceNode;
	BYTE        byNumMsg; 

} STRUCT_ENVELOPEOLDCML;
#pragma pack()

#define SIZE_STRUCT_ENVELOPEOLDCML sizeof(_STRUCT_ENVELOPEOLDCML)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_TAILOLDCML
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_TAILOLDCML
{
	BYTE		byCks;
	BYTE		byEOM;

} STRUCT_TAILOLDCML;
#pragma pack()

#define SIZE_STRUCT_TAILOLDCML sizeof(_STRUCT_TAILOLDCML)

#define MSGOLDCML_MAX_DATALEN			252
#define SINGLEMSGOLDCML_MAX_DATALEN	    252

//////////////////////////////////////////////////////////////////////////
//							STRUCT_HEADERMSGOLDCML
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_HEADERMSGOLDCML
{
	BYTE		byMsgId;
	BYTE		byDataLen;

} STRUCT_HEADERMSGOLDCML;
#pragma pack()

#define SIZE_STRUCT_HEADERMSGOLDCML sizeof(_STRUCT_HEADERMSGOLDCML)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_SINGLEMSGOLDCML
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_SINGLEMSGOLDCML
{
	STRUCT_HEADERMSGOLDCML	csHeader;
	BYTE				    byData[SINGLEMSGOLDCML_MAX_DATALEN - SIZE_STRUCT_HEADERMSGOLDCML];

} STRUCT_SINGLEMSGOLDCML;
#pragma pack()

#define SIZE_STRUCT_SINGLEMSGOLDCML sizeof(_STRUCT_SINGLEMSGOLDCML)

#define MAX_SIZE_MSGOLDCML	(SIZE_STRUCT_ENVELOPEOLDCML + MSGOLDCML_MAX_DATALEN + SIZE_STRUCT_TAILOLDCML)

#endif
