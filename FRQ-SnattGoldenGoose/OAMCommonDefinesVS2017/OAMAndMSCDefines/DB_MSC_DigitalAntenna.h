//
//	File:		DB_MSC_DigitalAntenna.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 10.0 (2010)
//              RTX v12.0
//
//	Version:	1.0
//
//	Created:	02/Feb/2016
//	Updated:	02/Feb/2016
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	02-02-2016 - Revision for final software
//

#pragma once

#include "COMMON_Diag_Block.h"

// Size of the used IEEE code
#define	SIZE_MAC_ADDRESS_IEEE	8

//////////////////////////////////////////////////////////////////////////
//					STRUCT_DIGITAL_ANTENNA (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_DIGITAL_ANTENNA
{
	//
	STRUCT_DIAG_BLOCK csDiag;				// Full Diagnostic
	BYTE	byInstalled;					// Not zero if Cell has a Digital Antenna
	//
	WORD	wCell;							// Cell with Digital Antenna
	BYTE	byJoinState;					// See below defines
	BYTE	byIEEE[SIZE_MAC_ADDRESS_IEEE];	// MAC Address (aka IEEE)
	//
	WORD	wFirmwareMajorVer;				// Firmware Major Version Code
	WORD	wFirmwareMinorVer;				// Firmware Minor Version Code
	WORD	wFirmwareBuildVer;				// Firmware Build Version
	WORD	wFirmwareDateVer;				// Firmware Date Version (bit 11..15 DAY - bit 7..10  MONTH - bit 0..6   YEAR)
	//
	WORD	wCoilWrites;					// Number of messages wrote by a Coil 
	WORD	wCoilFeedback;					// Number of Coil feedback
	//
	WORD	wJoinTimeout;					// Timeout waiting for a response from Coordinator
	//
} STRUCT_DIGITAL_ANTENNA;
#pragma pack()
// Size defines ...
#define SIZE_STRUCT_DIGITAL_ANTENNA	sizeof(_STRUCT_DIGITAL_ANTENNA)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Defines for byJoinState
#define DIGANT_STATE_UNKNOWN				0
#define DIGANT_STATE_MUST_JOIN				1
#define DIGANT_STATE_JOIN_CMD_SENT			2
#define DIGANT_STATE_WAIT_JOIN_ACK			3
#define DIGANT_STATE_JOIN_DONE				4
#define DIGANT_STATE_REQUEST_PARAMETERS		5
#define DIGANT_STATE_SEND_PARAMETERS		6
#define DIGANT_STATE_READY_TO_WORK			7
#define DIGANT_STATE_MUST_REBOOT			8
// spare
#define DIGANT_STATE_READY_WITHOUT_COORD	0x80

// Alarm's bit define
#define ALARM_THERMOSWITCH_OPENED		0	// Thermo-switch protection activated
#define ALARM_MOTOR_FAIL				1	// Motor Failure - During the activation (motion) there was an unexpected current level
#define ALARM_ERROR_SIGNAL_5kHz			2	// Error on the 5 kHz signal
#define ALARM_ERROR_SIGNAL_50kHz		3	// Error on the 50 kHz signal
#define ALARM_ERROR_SUPPLY_15P			4	// Problem on the Power Supply level +15Vdc
#define ALARM_ERROR_SUPPLY_15N			5	// Problem on the Power Supply level -15Vdc
#define ALARM_ERROR_OFFSET_SENSE		6	// Problem during the offset acquiring (Problem during the on-board hw initialization)

// Warning's bit define
#define WARNING_COMM_RADIO_MODULE		0	// Some problem occurred during the communication with the radio module
#define WARNING_PCB_OVERTEMPERATURE		1	// Over-temperature acquired by the PCB sensor
#define WARNING_LEFT_GEN_WHEEL			2	// Problem affected the Left generating Wheel
#define WARNING_RIGHT_GEN_WHEEL			3	// Problem affected the Right generating Wheel
#define WARNING_UNDERVOLTAGE_UNLOAD		4	// Bus Under-voltage occurred during the last Unload Mission
#define WARNING_CMD_25BIT				5	// The Digital Antenna received via Coil a Mission composed by 25 bit (invalid mission)
#define WARNING_IDLE_TIME				6	// The Digital Antenna received via Coil a Mission with IdleTime duration not respected
#define WARNING_BIT_TIME				7	// The Digital Antenna received via Coil a Mission with BitTime duration not respected
#define WARNING_WORD_NOT_VALID			8	// The Digital Antenna received via Coil a Mission not valid (0x000000 or 0xFFFFFF)
#define WARNING_DOUBLE_CMD				9	// The Digital Antenna received via Coil at the same time two different bit (5kHz and 50 kHz)
#define WARNING_COIL_CMD_UNKNOWN		10	// The Digital Antenna received via Coil an Unknown Mission
#define WARNING_DECODE_FUNCTION			11	// The Digital Antenna encounters some problem during the decoding operation

// Warning's bitmask define
#define WARNING_BITMASK_COMM_RADIO_MODULE		0x00000001	// Some problem occurred during the communication with the radio module
#define WARNING_BITMASK_PCB_OVERTEMPERATURE		0x00000002	// Over-temperature acquired by the PCB sensor
#define WARNING_BITMASK_LEFT_GEN_WHEEL			0x00000004	// Problem affected the Left generating Wheel
#define WARNING_BITMASK_RIGHT_GEN_WHEEL			0x00000008	// Problem affected the Right generating Wheel
#define WARNING_BITMASK_UNDERVOLTAGE_UNLOAD		0x00000010	// Bus Under-voltage occurred during the last Unload Mission
#define WARNING_BITMASK_CMD_25BIT				0x00000020	// The Digital Antenna received via Coil a Mission composed by 25 bit (invalid mission)
#define WARNING_BITMASK_IDLE_TIME				0x00000040	// The Digital Antenna received via Coil a Mission with IdleTime duration not respected
#define WARNING_BITMASK_BIT_TIME				0x00000080	// The Digital Antenna received via Coil a Mission with BitTime duration not respected
#define WARNING_BITMASK_WORD_NOT_VALID			0x00000100	// The Digital Antenna received via Coil a Mission not valid (0x000000 or 0xFFFFFF)
#define WARNING_BITMASK_DOUBLE_CMD				0x00000200	// The Digital Antenna received via Coil at the same time two different bit (5kHz and 50 kHz)
#define WARNING_BITMASK_COIL_CMD_UNKNOWN		0x00000400	// The Digital Antenna received via Coil an Unknown Mission
#define WARNING_BITMASK_DECODE_FUNCTION			0x00000800	// The Digital Antenna encounters some problem during the decoding operation