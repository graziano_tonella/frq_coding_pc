//
//	File:		DB_MSC_CellsMoveProfiles.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//				RTX v8.1.2 Build 8640
//
//	Created:	03/May/2011
//	Updated:	03/May/2011
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CELLS_MOVE_PROFILES (DB)
//////////////////////////////////////////////////////////////////////////

#define	MAX_ELECTRICAL_CODE_SIZE		30

#pragma pack(1)

typedef struct _STRUCT_CELLS_MOVE_PROFILES
{
	//
	WORD	wAntennaType;									// Type 1..x
	char	szElectricalCode[MAX_ELECTRICAL_CODE_SIZE + 1];	// ECode, i.e. X28000??
	//
	WORD	wRightLeftMove;									// Right = 1 Left = 2	
	WORD	wTableRowIndex;									// Row index
	float	fTime;											// Time (ms)
	float	fSpace;											// Space (mm)
	//
} STRUCT_CELLS_MOVE_PROFILES;

#pragma pack()

#define SIZE_STRUCT_CELLS_MOVE_PROFILES	sizeof(_STRUCT_CELLS_MOVE_PROFILES)
