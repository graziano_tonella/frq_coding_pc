//
//	File:		DB_MSC_Cabinets.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 10.0 (2010)
//              RTX v12.0
//
//	Version:	1.0
//
//	Created:	29/Mar/2018
//	Updated:	
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	29-03-2018 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//					STRUCT_CABINETS_DIAG (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CABINETS_DIAG
{
	//
	STRUCT_DIAG_BLOCK	csDiag;		// Current cabinet diagnositcs
	//
	BOOL bDontStopSorter;			// Flag to consider or not this cabinet to stop Sorter; FALSE = Stop Sorter, TRUE = Ignored
	//
} STRUCT_CABINETS_DIAG;
#pragma pack()

// Size defines ...
#define SIZE_STRUCT_CABINETS_DIAG	sizeof(_STRUCT_CABINETS_DIAG)
