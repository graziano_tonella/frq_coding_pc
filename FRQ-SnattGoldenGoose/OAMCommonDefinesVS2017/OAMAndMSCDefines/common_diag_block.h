//
//	File:		COMMON_Diag_Block.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	27/June/2008
//	Updated:	27/June/2008
//				28/Jan/2016
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	27-06-2008 - First release
//	29-Jan-2016 - Added MAPPING status bit
//

#pragma once

//////////////////////////////////////////////////////////////////////////////
//						STRUCT_DIAG_BLOCK
//////////////////////////////////////////////////////////////////////////////

#define DWALARM_SIZE			2
#define DWWARNING_SIZE			2

#pragma pack(1)
typedef struct _STRUCT_DIAG_BLOCK
{
	// 32 bit of states ...
	DWORD	dwState;
	// 64 bit of alarms
	DWORD	dwAlarm[DWALARM_SIZE];
	// 64 bit of warnings
	DWORD	dwWarning[DWWARNING_SIZE];

} STRUCT_DIAG_BLOCK;
#pragma pack()

#define SIZE_STRUCT_DIAG_BLOCK sizeof(_STRUCT_DIAG_BLOCK)

#pragma pack(1)
typedef struct _STRUCT_TRISTATE {
	// 64 bit of alarms
	DWORD	dwAlarm[DWALARM_SIZE];
	// 64 bit of warnings
	DWORD	dwWarning[DWWARNING_SIZE];
	// 64 bit to know if Alarm bit is an Emergency ...
	DWORD dwEmergencyMask[DWALARM_SIZE];
	//
} STRUCT_TRISTATE;
#pragma pack()

#define SIZE_STRUCT_TRISTATE		sizeof(_STRUCT_TRISTATE)

///////////////////////////////////////////////////////////////////////////////
// Common dwState bit defines
///////////////////////////////////////////////////////////////////////////////

// State bits As Maskbit !
#define DIAG_MASK_STATE_RUNNING							0x00000001
#define DIAG_MASK_STATE_EMPTYNG							0x00000002
#define DIAG_MASK_STATE_STARTUP							0x00000004
#define DIAG_MASK_STATE_ENERGY_SAVING					0x00000008
#define DIAG_MASK_STATE_MANUAL							0x00000010
#define DIAG_MASK_STATE_MAINTENANCE						0x00000020
#define DIAG_MASK_STATE_AUTORECOVERY					0x00000040
#define DIAG_MASK_STATE_EMERGENCY						0x00000080
#define DIAG_MASK_STATE_EXTERNAL_FAULT					0x00000100
#define DIAG_MASK_STATE_SPEED_OK						0x00000200
#define DIAG_MASK_STATE_JAM								0x00000400
#define DIAG_MASK_STATE_FULL							0x00000800
#define DIAG_MASK_STATE_ALARM							0x00001000
#define DIAG_MASK_STATE_WARNING							0x00002000
#define DIAG_MASK_STATE_HALF_FULL						0x00004000
#define DIAG_MASK_STATE_BOOKED							0x00008000
#define DIAG_MASK_STATE_PREFILLING						0x00010000
#define DIAG_MASK_STATE_COMPLETING						0x00020000
#define DIAG_MASK_STATE_COMPLETED						0x00040000
#define DIAG_MASK_STATE_WAITING_LABEL					0x00080000
#define DIAG_MASK_STATE_DISABLE							0x00100000
#define DIAG_MASK_STATE_BLOCKED_BY_OAM					0x00200000
#define DIAG_MASK_STATE_AT_LEAST_ONE_COMPONENT_FAULT	0x00400000
#define DIAG_MASK_STATE_BLOCKED_BY_EXT					0x00800000
#define DIAG_MASK_STATE_BLOCKED_BY_TRACKING				0x01000000
#define DIAG_MASK_STATE_OPERATOR_PRESENCE				0x02000000
#define DIAG_MASK_STATE_RFU26							0x04000000
#define DIAG_MASK_STATE_RFU27							0x08000000
#define DIAG_MASK_STATE_MAPPING							0x10000000
// TRI-STATE as Maskbit ...
#define DIAG_MASK_STATE_EMERGENCY_TRISTATE				0x20000000
#define DIAG_MASK_STATE_ALARM_TRISTATE					0x40000000
#define DIAG_MASK_STATE_WARNING_TRISTATE				0x80000000

//////////////////////////////////////////////////////////////////////////////
// Flap State ...
//////////////////////////////////////////////////////////////////////////////
#define DIAG_MASK_STATE_FLAP_OPEN		DIAG_MASK_STATE_RUNNING
#define DIAG_MASK_STATE_FLAP_CLOSE		DIAG_MASK_STATE_STARTUP
#define DIAG_MASK_STATE_FLAP_TOUT_C		DIAG_MASK_STATE_ENERGY_SAVING
#define DIAG_MASK_STATE_FLAP_TOUT_O		DIAG_MASK_STATE_MANUAL
//////////////////////////////////////////////////////////////////////////////

// State As BIT number ...
#define DIAG_STATE_RUNNING						0
#define DIAG_STATE_EMPTYNG						1
#define DIAG_STATE_STARTUP						2
#define DIAG_STATE_ENERGY_SAVING				3
#define DIAG_STATE_MANUAL						4
#define DIAG_STATE_MAINTENANCE					5
#define DIAG_STATE_AUTORECOVERY					6
#define DIAG_STATE_EMERGENCY					7
#define DIAG_STATE_EXTERNAL_FAULT				8
#define DIAG_STATE_SPEED_OK						9
#define DIAG_STATE_JAM							10
#define DIAG_STATE_FULL							11
#define DIAG_STATE_ALARM						12
#define DIAG_STATE_WARNING						13
#define DIAG_STATE_HALF_FULL					14
#define DIAG_STATE_BOOKED						15
#define DIAG_STATE_PREFILLING					16
#define DIAG_STATE_COMPLETING					17
#define DIAG_STATE_COMPLETED					18
#define DIAG_STATE_WAITING_LABEL				19
#define DIAG_STATE_DISABLE						20
#define DIAG_STATE_BLOCKED_BY_OAM				21
#define DIAG_STATE_AT_LEAST_ONE_COMPONENT_FAULT	22
#define DIAG_STATE_BLOCKED_BY_EXT				23
#define DIAG_STATE_BLOCKED_BY_TRACKING			24
#define DIAG_STATE_OPERATOR_PRESENCE			25
#define DIAG_STATE_RFU_26						26
#define DIAG_STATE_RFU_27						27
#define DIAG_STATE_MAPPING						28
// TRI-STATE as BIT number ...
#define DIAG_STATE_EMERGENCY_TRISTATE			29
#define DIAG_STATE_ALARM_TRISTATE				30
#define DIAG_STATE_WARNING_TRISTATE				31

//////////////////////////////////////////////////////////////////////////////
// Flap State ...
//////////////////////////////////////////////////////////////////////////////
#define DIAG_STATE_FLAP_OPEN		DIAG_STATE_RUNNING
#define DIAG_STATE_FLAP_CLOSE		DIAG_STATE_STARTUP
#define DIAG_STATE_FLAP_TOUT_C		DIAG_STATE_ENERGY_SAVING
#define DIAG_STATE_FLAP_TOUT_O		DIAG_STATE_MANUAL
//////////////////////////////////////////////////////////////////////////////
//
