//
//	File:		DB_Diag.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	2.0
//
//	Created:	27/June/2008
//	Updated:	18/October/2016
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	18-10-2016	- Use of the new .H from Chierego
//  27-01-2016	- Modulators and coils added
//	27-06-2008	- First release

#pragma once

#include "COMMON_Diag_Block.h"
#include "COMMON_Diag_Defines.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_DIAG (DB)
//////////////////////////////////////////////////////////////////////////

#define DIAG_SIZE_CSCABINET			20
#define DIAG_SIZE_CSZONE			10
#define DIAG_SIZE_CSBELT			64
#define DIAG_SIZE_CSMODULATOR		2
#define DIAG_SIZE_CSCOIL			6

#pragma pack(1)
typedef struct _STRUCT_DIAG
{
	//
	STRUCT_DIAG_BLOCK	csCabinet [ DIAG_SIZE_CSCABINET ];			// Cabinet

	STRUCT_DIAG_BLOCK	csZone [ DIAG_SIZE_CSZONE ];				// Zone

	STRUCT_DIAG_BLOCK	csBelt [ DIAG_SIZE_CSBELT ];				// Belts

	STRUCT_DIAG_BLOCK	csModulator [ DIAG_SIZE_CSMODULATOR ];		// Modulator

	STRUCT_DIAG_BLOCK	csCoil [ DIAG_SIZE_CSCOIL ];				// Coil

} STRUCT_DIAG;
#pragma pack()

#define SIZE_STRUCT_DIAG	sizeof(STRUCT_DIAG)

// --------------------------------------------------------
// UPSTREAM DIAGNOSTIC BLOCKS OFFSET USED BY:
// 1. ISPC TO CAPTURE UPSTREAM DIAG BLOCK INSIDE DATABASE   
// --------------------------------------------------------
#define OFFSET_UPSTR_DIAG		10
// --------------------------------------------------------
//////////////////////////////////////////////////////////////////////////

