//
//	File:		DB_MSC_BusBreaker.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	11/Sep/2012
//	Updated:	13/Sep/2012
//
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//							STRUCT_BUS_BREAKER (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_BUS_BREAKER
{
	//
	BYTE	byType;					// Type of BUS Breaker (see define below)
	//
	WORD	wInstalledCell;			// Number of cell with this BUS Breaker
	//
	STRUCT_DIAG_BLOCK	csDiag;		// Full Diagnostic
	//
}	STRUCT_BUS_BREAKER;
#pragma pack()

#define SIZE_STRUCT_BUS_BREAKER		sizeof(_STRUCT_BUS_BREAKER)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// byType possible values ...
#define BUSBREAKER_TYPE_DEFAULT				1

// Alarm's bit define
#define	BUSBREAKER_ALARM_OPEN				0	// BUS Breaker open (by BUS Breaker test)
