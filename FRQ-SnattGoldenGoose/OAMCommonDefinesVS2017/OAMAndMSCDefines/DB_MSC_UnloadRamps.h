//
//	File:		DB_MSC_UnloadRamps.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 10.0 (2010)
//              RTX v12.0
//
//	Version:	1.0
//
//	Created:	05/Sep/2016
//	Updated:	05/Sep/2016
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	05-09-2016 - Revision for final software
//

#pragma once

//////////////////////////////////////////////////////////////////////////
//					STRUCT_UNLOAD_RAMP (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_UNLOAD_RAMP
{
	//
	BYTE	byRampID;										// Ramp ID 0..255
	BYTE	bySpeedID;										// Sorter Speed ID 0 = AnySpeed, 1..4 possible Working Speed
	//
	WORD	wUnloadSpeed;									// Unload Speed [mm/s] - It's useful only for GENI-BELT
	WORD	wUnloadAcc;										// Unload Acceleration [mm/s2] - It's useful only for GENI-BELT
	//
} STRUCT_UNLOAD_RAMP;
#pragma pack()
// Size defines ...
#define SIZE_STRUCT_UNLOAD_RAMP	sizeof(_STRUCT_UNLOAD_RAMP)
