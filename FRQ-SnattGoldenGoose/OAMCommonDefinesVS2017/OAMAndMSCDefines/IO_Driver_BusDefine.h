//
//	File:		IO_Driver_BusDefine.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 6.0
//              RTX v7.0 Build 4925
//
//	Version:	1.2
//
//	Created:	21/May/2008
//	Updated:	23/February/2017
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	23-02-2017 - Update PROFINET ID & String
//	08-07-2016 - Update list of device (nodes)
//				 Changed MAX quantity for BusFiledNodes and single card node considering also PROFINET
//	14-04-2009 - Update structure fields for diagnostic informations
//  21-05-2008 - First Release
//

#pragma once

#include <windows.h>

// Common define !
#define MAX_IO_CARD	4
#define DB_BUS_NAME			_T("DB_IO_NODES")
#define DBTABLE_HEADER		_T("IO_NODES_HEADER")
#define DBTABLE_BUSFIELD	_T("IO_NODES_TABLE")
#define DB_MAX_ENTRY		1000

// Name of the mutex that synchronizes access to STRUCT_IO_BUSFIELD array.
//
// When multiple instances of IODriver are enabled (by means of the
// corresponding configuration switch), the mutex name is appended with a
// two-digit zero-padded number, equal to the service manager ID.
// In this case the following format should be used, to build the mutex name:
//		_T("%s%02u"), FIELD_IO_MTX, wIODriverInstanceID
#define FIELD_IO_MTX		_T("FIELD_IO_MTX")

// Custom user events for RtTraceEvent (valid values are 1 to 1000)
#define FIELD_IO_MTXEVENT_BASE	100

//////////////////////////////////////////////////////////////////////////
//							STRUCT_IO_CARD_INFO (DB)
//////////////////////////////////////////////////////////////////////////

#define MAX_IO_CARD_INFO_SIZE			1023

#pragma pack(1)
typedef struct _STRUCT_IO_CARD_INFO
{
	BYTE byCardInfoType;				/**< I/O card type */
	BYTE csCardInfo[MAX_IO_CARD_INFO_SIZE];	/**< I/O card information, dependent on the I/O card type */
}
STRUCT_IO_CARD_INFO;
#pragma pack()

#define SIZE_STRUCT_IO_CARD_INFO sizeof(_STRUCT_IO_CARD_INFO)

// byCardInfoType (STRUCT_IO_CARD_INFO) Values Defines
#define NO_IO_CARD_INFO					0x00
// (Only I/O card types for which information is provided are listed.)
#define HILSCHER_CIFX_PNM_CARD_INFO		0x01
#define KUNBUS_DF_PNIO_CARD_INFO		0x02

//////////////////////////////////////////////////////////////////////////
//							STRUCT_IO_HEADER (DB)
//////////////////////////////////////////////////////////////////////////

#define MAX_FIELD_CONFIG_NAME			30

#pragma pack(1)
typedef struct _STRUCT_IO_HEADER
{
	/**
		IO cards cycle time (ms).
	*/
	double dCardsCycleTime[1 + MAX_IO_CARD];
	
	/**
		Virtual node manager cycle time (ms).
	*/
 	double dVNMCycleTime[1 + MAX_IO_CARD];

	/**
		I/O card information.
	*/
	STRUCT_IO_CARD_INFO csCardInfo[MAX_IO_CARD];

	/**
		Name of the currently active field configuration.
	*/
	char szFieldConfigName[MAX_FIELD_CONFIG_NAME];
} STRUCT_IO_HEADER;
#pragma pack()

#define SIZE_STRUCT_IO_HEADER sizeof(_STRUCT_IO_HEADER)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_IO_BUSFIELD (DB)
//////////////////////////////////////////////////////////////////////////

#define IO_QTY_BF_DIAG		2
#define IO_QTY_IN_DATA		1024
#define IO_QTY_OUT_DATA		1024

#pragma pack(1)
typedef struct _STRUCT_IO_BUSFIELD
{
	//
	// Node's General Informations
	//
	BYTE	byCardID;				// ID of the I/O board (1-8 for ApplicomIO)
	WORD	wNodeID;				// Node's ID on the I/O network (HW, 0-128 for Profibus)
	WORD	wLinkNodeID;			// If this node is a virtual DP/DP, this is the linked Node's ID
	DWORD	dwNodeTYPE;				// Node's Type, see defines below
	WORD	wFlags;					// Flags, see defines below
	//
	// Node's Diagnostics
	//
	WORD	wState;						// Node state, see defines below
	DWORD	dwDiagInfo[IO_QTY_BF_DIAG];	// (RFU) Possible errors, code ...
	WORD	wOutputTimeout;				// Safe timeout to reset outputs if not used ...
	//
	WORD	wReferenceIndex;			// Line number or conveyor system number, etc.
	WORD	wDbClassID;					// DB Class ID, to identify components
	WORD	wElement;					// Element of the components (Cells, belts, etc.)
	BYTE	byBit;						// Bit to set or reset (Number, not mask ! i.e. 0..xx)
	//
	// Node's Software/Hardware Informations
	//
	WORD	wExchangedInputBytes;			// Total number of INPUT bytes processed by this node
	WORD	wExchangedOutputBytes;			// Total number of OUTPUT bytes processed by this node
	BYTE	byHWDataIN[IO_QTY_IN_DATA];		// INPUT data exchanged with HW busfield
	BYTE	byHWDataOUT[IO_QTY_OUT_DATA];	// OUTPUT data exchanged with HW busfield
	BYTE	bySWDataIN[IO_QTY_IN_DATA];		// INPUT data exchanged with applications
	BYTE	bySWDataOUT[IO_QTY_OUT_DATA];	// OUTPUT data exchanged with applications
	//
} STRUCT_IO_BUSFIELD;
#pragma pack()

#define SIZE_STRUCT_IO_BUSFIELD sizeof(_STRUCT_IO_BUSFIELD)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_IO_DIAGNOSTIC_PROCESS
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_IO_DIAGNOSTIC_PROCESS
{
	// Old node state
	WORD*					pwOldState;		// Old node state ...
	STRUCT_IO_BUSFIELD*		pcsNodeInfo;	// Node information ...
	//
} STRUCT_IO_DIAGNOSTIC_PROCESS;
#pragma pack()

#define SIZE_STRUCT_IO_DIAGNOSTIC_PROCESS sizeof(_STRUCT_IO_DIAGNOSTIC_PROCESS)

//////////////////////////////////////////////////////////////////////////
//								DEFINES
//////////////////////////////////////////////////////////////////////////

// wFlags (STRUCT_IO_BUSFIELD) Bits Defines
#define BUS_FLAG_FROM_CSV				0x01	// Loaded from CSV
#define BUS_FLAG_HW_PRESENT				0x02	// Founded in the HW bus
#define BUS_FLAG_FORCE_ACTIVE			0x04	// Force mode, values can be changed not by HW
#define BUS_FLAG_VIRTUAL				0x08	// Processed by Virtual Node Manager
#define BUS_FLAG_WARNING_ON_FAULT		0x10	// 1 = set a warning if node fault instead of alarm
#define BUS_FLAG_IGNORE_ERROR			0x20	// Ignore Error status (but still consider Timeout) as a workaround,
												// only for Hilscher PROFINET IO Controller, in case of Devices with
												// extended diagnostics that cannot be disabled. This is a workaround
												// that should not be necessary anymore, when IOxS flags will be used to
												// determine the Error status. (See IOD-20 in JIRA.)

// wState (STRUCT_IO_BUSFIELD) Values Defines 
#define BUS_NODE_STATE_OK				0
#define BUS_NODE_STATE_FAILURE			1
#define BUS_NODE_STATE_NOT_INSTALLED	2
#define BUS_NODE_STATE_TIMEOUT			3
#define BUS_NODE_STATE_IOCARD_ERROR		4

// wNodeID (STRUCT_IO_BUSFIELD) Values Defines

// The status of the master is stored in the STRUCT_IO_BUSFIELD instance with wNodeID == 0
#define BUS_NODE_ID_MASTER				0

// dwNodeTYPE (STRUCT_IO_BUSFIELD) Values Defines

//
// PROFIBUS: we use slave module ident from GSD
//
#define BUS_ID_DP_MASTER				0x0000			// Its the HW board
#define BUS_ID_ET200B_16DI_16DO			0x000A			// 10
#define BUS_ID_ET200B_32DI				0x0004			// 4
#define BUS_ID_ET200B_32DO				0x000D			// 13
#define BUS_ID_ET200B_4AO_SPC3			0x8018			// 32792
#define BUS_ID_E2C						0x04DA			// 1242
#define BUS_ID_DP_AS_LINK20				0x804F			// 32847
#define BUS_ID_DP_DP_COUPLER			0x8070			// 32880
#define BUS_ID_VIRTUAL_DP_DP_COUPLER	0x029A			// 666
#define BUS_ID_ET200XBM141_8DI			0x803D			// 32829
#define BUS_ID_TOLEDO_JAGUAR			0x6713			// 26387
#define BUS_ID_SEW_MOVIMOT				0x6001			// 24577
#define BUS_ID_SEW_MOVIDRIVE			0x6002			// 24578
#define BUS_ID_SEW_MOVIFIT  			0x600A			// 24586
#define BUS_ID_SEW_MOVIPLC  			0x6007			// 24583
#define BUS_ID_ET200S_MULTI_IO			0x806A			// 32874
#define BUS_ID_IM153_MULTI_IO			0x801D			// 32797
#define BUS_ID_PHOENIX_MULT_IO			0x06CC			// 1740
#define BUS_ID_DP_AS_LINK20E			0x8098			// 32920
#define BUS_ID_BECKHOFF_BX3100			0x06FC			// 1788
#define BUS_ID_BECKHOFF_EL6731_0010		0x095F			// 2399
#define BUS_ID_BECKHOFF_BK3150			0xBECE			// 48846
#define BUS_ID_ABB_DIGIVECTOR			0x06B6			// 1718
#define BUS_ID_ABB_DGV300				0x08A4			// 2212
#define BUS_ID_ABB_PLUTO_GATEP2			0x0A2E			// 2606
#define BUS_ID_SICK_MSC800				0x0AE4			// 2788
#define BUS_ID_SICK_AG_MLG				0x095B			// 2395
#define BUS_ID_PESA_MBM_PKP				0x7510			// 29968
#define	BUS_ID_DATALOGIC_CBOX3X0		0x071E			// 1822
#define	BUS_ID_DATALOGIC_CBX			0x0BAC			// 2988
#define	BUS_ID_DATALOGIC_SC5000			0x0F13			// 3859
#define BUS_ID_LUST_CDE32				0x0564			// 1380
#define BUS_ID_LUMBERG_LIONS			0x09C9			// 2505
#define BUS_ID_LUMBERG_LIONM			0x09CA			// 2506
#define BUS_ID_ABC_DP_RS232				0x1803			// 6147
#define BUS_ID_ABC_X_GATEWAY			0x1831			// 6193
#define BUS_ID_ASI_DP_AC1375			0x07E5			// 2021
#define BUS_ID_ASI_IO_SLAVE				0xF000			// 61440
#define BUS_ID_LENZE_MOTOR				0x00DA			// 218
#define BUS_ID_LENZE_MOTEC_E84			0x0CB3			// 3251
#define BUS_ID_LEUZE_KONTURFLEX			0x05CE			// 1486
#define BUS_ID_TURCK_BLCDP				0xFF34			// 65332
#define BUS_ID_TURCK_BL20DP				0xFF35			// 65333
#define BUS_ID_TURCK_SDPB10S			0xFF29			// 65321
#define BUS_ID_TURCK_IOLINK_GATEWAY		0xFF2D			// 65325
#define BUS_ID_HMS_ANYBUS_COMPACTCOM	0x1811			// 6161
#define BUS_ID_PLC_SIEMENS_CPU313		0x80D0			// 32976
#define BUS_ID_SIEMENS_CP342_5			0x80D6			// 32982
#define BUS_ID_NORD_SKCU4				0x0BA8			// 2984
#define BUS_ID_LTI_SERVO_ONE			0x0A33			// 2611
#define BUS_ID_BALLUFF_BNI_PBS			0x0D3F			// 3391
#define BUS_ID_FAKE_NODE				0xFFFF			// 65535
#define BUS_ID_WEIDMUELLER				0x0DF4			// 3572
//
// PROFIBUS: Description
//
#define BUS_ID_SZ_DP_MASTER				_T("Master IO Card")
#define BUS_ID_SZ_ET200B_16DI_16DO		_T("ET200B 16DI 16DO")
#define BUS_ID_SZ_ET200B_32DI			_T("ET200B 32DI")
#define BUS_ID_SZ_ET200B_32DO			_T("ET200B 32DO")
#define BUS_ID_SZ_ET200B_4AO_SPC3		_T("ET200B 4AO SPC3")
#define BUS_ID_SZ_E2C					_T("LUMBERG E2C")
#define BUS_ID_SZ_DP_DP_COUPLER			_T("DP/DP Coupler")
#define BUS_ID_SZ_VIRTUAL_DP_DP_COUPLER	_T("Virtual DP/DP Coupler")
#define BUS_ID_SZ_ET200XBM141_8DI		_T("ET200 XBM141 8DI")
#define BUS_ID_SZ_TOLEDO_JAGUAR			_T("Toledo Jaguar")
#define BUS_ID_SZ_SEW_MOVIMOT			_T("SEW Movimot")
#define BUS_ID_SZ_SEW_MOVIDRIVE			_T("SEW Movidrive")
#define BUS_ID_SZ_SEW_MOVIFIT  			_T("SEW Movifit")
#define BUS_ID_SZ_SEW_MOVIPLC  			_T("SEW MoviPLC")
#define BUS_ID_SZ_ET200S_MULTI_IO		_T("ET200S Multi I/O")
#define BUS_ID_SZ_IM153_MULTI_IO		_T("IM153 Multi I/O")
#define BUS_ID_SZ_PHOENIX_MULT_IO		_T("Phoenix Multi I/O")
#define BUS_ID_SZ_BECKHOFF_BX3100		_T("Beckhoff BX3100")
#define BUS_ID_SZ_BECKHOFF_EL6731_0010	_T("Beckhoff EL6731-0010")
#define BUS_ID_SZ_BECKHOFF_BK3150		_T("Beckhoff BK3150")
#define BUS_ID_SZ_ABB_DIGIVECTOR		_T("ABB Digivector")
#define BUS_ID_SZ_ABB_DGV300			_T("ABB DGV300")
#define BUS_ID_SZ_ABB_PLUTO_GATEP2		_T("ABB PLUTO Gateway")
#define BUS_ID_SZ_SICK_MSC800			_T("Sick MSC800")
#define BUS_ID_SZ_SICK_AG_MLG			_T("Sick MLG")
#define BUS_ID_SZ_PESA_MBM_PKP			_T("Pesa MBM PKP")
#define	BUS_ID_SZ_DATALOGIC_CBOX3X0		_T("Datalogic CBOX")
#define	BUS_ID_SZ_DATALOGIC_CBX			_T("Datalogic CBX")
#define	BUS_ID_SZ_DATALOGIC_SC5000		_T("Datalogic SC5000")
#define BUS_ID_SZ_LUST_CDE32			_T("Lust Drive Motor")
#define BUS_ID_SZ_LUMBERG_LIONS			_T("Lumberg LioN-S")
#define BUS_ID_SZ_LUMBERG_LIONM			_T("Lumberg LioN-M")
#define BUS_ID_SZ_ABC_DP_RS232			_T("HMS Anybus Communicator")
#define BUS_ID_SZ_ABC_X_GATEWAY			_T("HMS Anybus X-Gateway")
#define BUS_ID_SZ_DP_AS_LINK20			_T("AS-i LINK20")
#define BUS_ID_SZ_DP_AS_LINK20E			_T("AS-i LINK20-E")
#define BUS_ID_SZ_DP_ASI20E				_T("DP AS-i/20E Link")
#define BUS_ID_SZ_ASI_DP_AC1375			_T("AS-i Smart Link DP AC1375")
#define BUS_ID_SZ_ASI_IO_SLAVE			_T("AS-i IO Slave")
#define BUS_ID_SZ_LENZE_MOTOR			_T("Lenze Drive Motor")
#define BUS_ID_SZ_LENZE_MOTEC_E84		_T("Lenze Motec E84")
#define BUS_ID_SZ_LEUZE_KONTURFLEX		_T("Leuze Konturflex Quattro")
#define BUS_ID_SZ_TURCK_BLCDP			_T("Turck BLC DP")
#define BUS_ID_SZ_TURCK_BL20DP			_T("Turck BL20 DP")
#define BUS_ID_SZ_TURCK_SDPB10S			_T("Turck SDPD-10S")
#define BUS_ID_SZ_TURCK_IOLINK_GATEWAY	_T("Turck IO-Link Gateway")
#define BUS_ID_SZ_HMS_ANYBUS_COMPACTCOM	_T("HMS Anybus CompactCom")
#define BUS_ID_SZ_PLC_SIEMENS_CPU313	_T("Siemens PLC CPU313")
#define BUS_ID_SZ_SIEMENS_CP342_5		_T("Siemens CP342-5 Cu Slave")
#define BUS_ID_SZ_NORD_SKCU4			_T("NORD SK CU4-PBR")
#define BUS_ID_SZ_LTI_SERVO_ONE			_T("LTI ServoOne Drive Motor")
#define BUS_ID_SZ_BALLUFF_BNI_PBS		_T("Balluff PB Master IO-LINK")
#define BUS_ID_SZ_FAKE_NODE				_T("Fake Node")
#define BUS_ID_SZ_WEIDMUELLER			_T("Weidmueller UR20 FBC PB DP")

//
// PROFINET: we use slave module "VendorID" Hex code + "DeviceID" Hex code from GSDML
//
#define BUS_ID_LTI_SERVO_ONE_JUNIOR					0x017C4E21
#define BUS_ID_SIEMENS_PNPN_COUPLER					0x002A0604
#define BUS_ID_PHOENIX_AXL_E_PN						0x00B00104
#define BUS_ID_WEIDMULLER_IO_UR_20_PN				0x013418C5
#define BUS_ID_IFM_AL1000							0x0136AC50
#define BUS_ID_IFM_AL1100							0x0136AC52
#define BUS_ID_IFM_AL1102							0x0136AC53
#define BUS_ID_IFM_AL1200							0x0136AC57
#define BUS_ID_IFM_AL1202							0x0136AC55
#define BUS_ID_BALLUFF_BNI005H						0x0378508A
#define BUS_ID_BALLUFF_BNI0092						0x0378507A
#define BUS_ID_SICK_CDF600_PN						0x01015110
#define BUS_ID_HMS_ANYBUS_COMPACTCOM_PN				0x010C0002
#define BUS_ID_SEW_MFE								0x010A0006
#define BUS_ID_BECKHOFF_EL6631_0010					0x012019E7
#define BUS_ID_COGNEX_DM							0x01370005
#define BUS_ID_IFM_AC140X_PN						0x0136AC01
#define BUS_ID_IM151_3PN							0x002A0301
#define BUS_ID_PEPPERL_FUCHS_ASI_GATEWAY_PN			0x005D0919
#define BUS_ID_IE200_8TC_G							0x0002017F
#define BUS_ID_MASI_BWU2729							0x01210778
#define BUS_ID_BECKHOFF_CX8093						0x01200028
#define BUS_ID_PROSOFT_PLX32_EIP_PND				0x01758202
#define BUS_ID_KUNBUS_DF_PNIO_DEVICE				0x016D0003
//
// PROFINET: Description
//
#define BUS_ID_SZ_LTI_SERVO_ONE_JUNIOR				_T("LTi Servo One PN")
#define BUS_ID_SZ_SIEMENS_PNPN_COUPLER				_T("Siemens PN/PN Coupler")
#define BUS_ID_SZ_PHOENIX_AXL_E_PN					_T("Phoenix AXL-E PN")
#define BUS_ID_SZ_WEIDMULLER_IO_UR_20_PN			_T("Weidmuller IO UR20 PN ")
#define BUS_ID_SZ_IFM_AL1000						_T("IFM AL1000 IO-Link")
#define BUS_ID_SZ_IFM_AL1100						_T("IFM AL1100 IO-Link")
#define BUS_ID_SZ_IFM_AL1102						_T("IFM AL1102 IO-Link")
#define BUS_ID_SZ_IFM_AL1200						_T("IFM AL1200 IO-Link")
#define BUS_ID_SZ_IFM_AL1202						_T("IFM AL1202 IO-Link")
#define BUS_ID_SZ_BALLUFF_BNI005H					_T("Balluff BNI005H IO-Link Master")
#define BUS_ID_SZ_BALLUFF_BNI0092					_T("Balluff BNI0092 IO-Link Master")
#define BUS_ID_SZ_SICK_CDF600_PN					_T("SICK CDF600 PN")
#define BUS_ID_SZ_HMS_ANYBUS_COMPACTCOM_PN			_T("HMS Anybus CompactCom PN")
#define BUS_ID_SZ_SEW_MFE							_T("SEW MFE")
#define BUS_ID_SZ_BECKHOFF_EL6631_0010				_T("Beckhoff EL6631-0010") // Recall Cell
#define BUS_ID_SZ_COGNEX_DM							_T("DataMan COGNEX Camera")
#define BUS_ID_SZ_IFM_AC140X_PN						_T("IFM AC140x Master AS-i")
#define BUS_ID_SZ_IM151_3PN							_T("IM151-3 PN Multi I/O")
#define BUS_ID_SZ_PEPPERL_FUCHS_ASI_GATEWAY_PN		_T("Pepperl Fuchs Master AS-i")
#define BUS_ID_SZ_IE200_8TC_G						_T("Cisco IE 2000-8TC-G-E")
#define BUS_ID_SZ_MASI_BWU2729						_T("BWU2729 DAP Master AS-i")
#define BUS_ID_SZ_BECKHOFF_CX8093					_T("Beckhoff CX8093 Smart Device")
#define BUS_ID_SZ_PROSOFT_PLX32_EIP_PND				_T("ProSoft PLX32 Ethernet/IP Gateway")
#define BUS_ID_SZ_KUNBUS_DF_PNIO_DEVICE				_T("Kunbus DF PNIO Device")
