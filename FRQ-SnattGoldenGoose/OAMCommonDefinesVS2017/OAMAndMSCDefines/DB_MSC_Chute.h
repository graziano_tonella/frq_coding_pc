//
//	File:		DB_MSC_Chute.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	30/June/2008
//	Updated:	30/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	30-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"
#include "COMMON_MSC_Defines.h"

// Max discharge offset (Single and multi)
#define	MAX_DISCHARGE_OFFSET		3

// Minimum parcel percentage hitting the last chute wall.
// Under this value, the unload is forbidden and avoided
#define MIN_PERC_PARCEL_HITTING_LAST_CHUTE_WALL 50

// Minimum chute width value, used for safe check on chute creation.
#define MIN_CHUTE_WIDTH_VALUE 500	// mm

//////////////////////////////////////////////////////////////////////////
//							STRUCT_CHUTE_DISCHARGE_VALUES
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CHUTE_DISCHARGE_VALUES
{
	//
	BYTE    byObjectDischargeMode;							// See define below ...
	//
	WORD	wMultiCellRotateFactor;							// Rotate factor for object on more than one cell (0 = move cell togheter on the same time)
	//
	WORD	wMultiCellBaseOffset[MAX_DISCHARGE_OFFSET];		// Discharge base NIF offset for object on more than one cell
	BYTE	byUseAlternateDischargeOnMultiCell;				// See below defines ...
	BYTE	byMultiAlternateIndex;							// Last used index for multiple discharge
	//
	WORD	wSingleCellBaseOffset[MAX_DISCHARGE_OFFSET];	// Discharge base NIF offset on single cell
	BYTE	byUseAlternateDischargeOnSingleCell;			// See below defines ...
	BYTE	bySingleAlternateIndex;							// Last used index for single discharge
	//
	BYTE	byAlternateInductor;							// Last used inductor (common single or multi cells)
	BYTE	bySCForceInductor;								// Forced inductor for single cell
	BYTE	byMCForceInductor;								// Forced inductor for multi cell
	//
	WORD	wInductorActivationTime;						// Inductor action time (ms). At 0 use the default value. NB: Min. is 150ms
	//
	WORD	wInductorGroupID;								// Inductor Group to use to discharge !
	WORD	wCoilToUseDBIndex;								// DB Index of the coil to use for this Chute
	//
	WORD	wSlideLength;									// Slide Length [mm] - Distance from sorter and the start of chute-side
	WORD	wChuteWidth;									// Chute width [mm] - Distance between the two wall sides, close to the sorter
	//
	BYTE	byDefaultRampID;								// Default Ramp ID to use to unload in this chute
	//
	WORD	wSafeUnloadMargin;								// Safe Unload margin referred to the chute start position [mm].
	//														// This parameter is meaningful only for the byObjectDischargeMode BY_BEGIN_CHUTE, BY_END_CHUTE, BY_CENTER_CHUTE.
	//
	WORD	wPercParcelHitting;								// Parcel % length hitting the chute end wall. Safe range is [100 .. 66].
	//														// This parameter is meaningful only for the byObjectDischargeMode BY_BEGIN_CHUTE, BY_END_CHUTE, BY_CENTER_CHUTE.
	//
}	STRUCT_CHUTE_DISCHARGE_VALUES;
#pragma pack()

#define SIZE_STRUCT_CHUTE_DISCHARGE_VALUES		sizeof(_STRUCT_CHUTE_DISCHARGE_VALUES)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_CHUTE_TRACKING_INFO
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CHUTE_TRACKING_INFO
{
	//
	WORD	wCell;						// Mission Cell
	BYTE	byTrackingFlags;			// See below defines ...
	WORD	wTrackingTimeout;			// Timeout counter
	WORD	wChuteDBIndex;				// Common info to process specail
	//
}	STRUCT_CHUTE_TRACKING_INFO;
#pragma pack()

#define SIZE_STRUCT_CHUTE_TRACKING_INFO		sizeof(_STRUCT_CHUTE_TRACKING_INFO)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_CHUTE_FLAP_INFO
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CHUTE_FLAP_INFO
{
	//
	BYTE	byRequiredFlapPosition;			// 0 = any, otherwise see below defines ...
	BYTE	byCurrentFlapPosition;			// See below defines ...
	BYTE	byCurrentFlapCommand;			// Last comand received (same defines of byCurrentFlapPosition)
	BYTE	byLastFlapCommand;				// Last comand processed (same defines of byCurrentFlapPosition)
	BOOL	bIsMoving;						// Flag to know that flap is moving and block chute
	BOOL	bBlockChute;					// Flag to know if the flap must block unload
	WORD	wNifElapsedFromLastObject;		// NIF timer, set 0 when object is unloaded ...
	WORD	wNifBeforeMove;					// Number of NIF before move (related to wNifElapsedFromLastObject)
	//
}	STRUCT_CHUTE_FLAP_INFO;
#pragma pack()

#define SIZE_STRUCT_CHUTE_FLAP_INFO		sizeof(_STRUCT_CHUTE_FLAP_INFO)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_CHUTE (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CHUTE
{
	//
	// Config fields ...
	//
	WORD	wDBIndex;					// DB record # (to performe qsort & bsearch !)
	WORD	wChuteID;					// Destination ID
										// First digit identify if we have flap or floors ...
										// i.e. ID = 100 its chute 10 with no flap
										//      ID = 221 its chute 22 upper level ...

	BYTE	byChuteType;				// See enum in COMMON_MSC_Defines.h
	WORD	wChuteConfig;				// Chute configuration field
	WORD	wChutePosition;				// NIF chute position
	WORD	wChuteSortActionPosition;	// NIF position of the first inductor / Coil
	WORD	wChuteFlapCmdAnticipation;	// ms before wChuteInductorPosition to send command to the flap
	WORD	wChuteIOAnticipation;		// ms before wChuteInductorPosition to perform I/O exchange
	BYTE	byCellUnloadRate;			// 0 = All Cell, x = 1 Cell each x
//
// MSC-124 _START_
// Improvement: In flight with objects size, not counting objects
//
	WORD	wMaxInflightObject;			// Max inflight object allowed (only with Modulator/Coil)
//
// MSC-124 _END_
//
	STRUCT_CHUTE_DISCHARGE_VALUES	csDischargeValues;	// All discharge parameters
	STRUCT_CHUTE_TRACKING_INFO		csTrackingInfo;		// All tracking info
	STRUCT_CHUTE_FLAP_INFO			csFlapInfo;			// All flap info

	//
	// Runtime fields ...
	//
	DWORD	dwObjUnloaded;				// Counter of objects unloaded into this chute
	BYTE	byUnloadKOCounter;			// Counter of objects not unloaded
	//
	WORD	wLastUnloadedCell;			// Last Cell unloaded
	BYTE	byLastUnloadedCellLap;		// Last Sorter lap on last Cell unloaded
	//
	DWORD	dwIOExchangeResult;				// I/O Exchange result, used by bits (see define below)
	DWORD	dwIOExchangeCustomNACKReason;	// I/O Exchange custom NACK reason
	//
	_TCHAR	szTextInfo[SIZEOF_MSC_DISPLAY_DATA+1];	// Text info comeing from OAM, used for supvs or display
	//
	STRUCT_DIAG_BLOCK	csDiag;			// Full Diagnostic
	//
}	STRUCT_CHUTE;
#pragma pack()

#define SIZE_STRUCT_CHUTE		sizeof(_STRUCT_CHUTE)

//////////////////////////////////////////////////////////////////////////
//					STRUCT_AFTER_COMMAND_PARAMETERS
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_AFTER_COMMAND_PARAMETERS
{
	//
	WORD	wChuteID;					// Target chute ID
	WORD	wDBIndex;					// Index of the chute inside DB ...
	BYTE	byCommand;					// Command done ...
	//
} STRUCT_AFTER_COMMAND_PARAMETERS;
#pragma pack()

#define SIZE_STRUCT_AFTER_COMMAND_PARAMETERS	sizeof(_STRUCT_AFTER_COMMAND_PARAMETERS)

//////////////////////////////////////////////////////////////////////////
//					STRUCT_BUILD_DATA_FOR_IOEXEC
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_BUILD_DATA_FOR_IOEXEC
{
	//
	WORD	wChuteID;					// Target chute ID
	WORD	wDBIndex;					// Index of the chute inside DB ...
	WORD	wCell;						// Referenced Cell
	BYTE	byData[240];				// Actual MAX size of a profibus node ...
	WORD	wDataLen;					// Number of used bytes of "byData"
	//
} STRUCT_BUILD_DATA_FOR_IOEXEC;
#pragma pack()

#define SIZE_STRUCT_BUILD_DATA_FOR_IOEXEC	sizeof(_STRUCT_BUILD_DATA_FOR_IOEXEC)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Values of byUseAlternateDischargeOnSingleCell and byUseAlternateDischargeOnMultipleCell
#define CHUTE_ALTERANTE_DISCHARGE_MODE_NONE					0	// Normal discharge mode
#define CHUTE_ALTERANTE_DISCHARGE_MODE_FLIP_FLOP_2OFS		1	// Flip Flop on two internal offset
#define CHUTE_ALTERANTE_DISCHARGE_MODE_RB_ON_INDUCTORS		2	// Round Robin on all inductor
#define CHUTE_ALTERANTE_DISCHARGE_MODE_FORCED_INDUCTOR		3	// Force to use one inductor
#define CHUTE_ALTERANTE_DISCHARGE_MODE_FLIP_FLOP_3OFS		4	// Flip Flop on three internal offset
//
#define CHUTE_ALTERANTE_DISCHARGE_MODE_MULTI_TRAJECTORY		5	// Multi trajectory (valid only for GENI-Belt)
																// The number of multi-trajectory will be stored into the wSingleCellBaseOffset[0] element
//

// Values of byTrackingFlags
#define CHUTE_TRACKING_FLAG_NO_EVENT		0	// No tracking event ...
#define CHUTE_TRACKING_FLAG_ON_EVENT		1	// Chute is processing tracking
#define CHUTE_TRACKING_FLAG_ERROR			2	// Tracking error (timeout)
#define CHUTE_TRACKING_FLAG_DONE			3	// Tracking done, parcel ok

// Values (bits!) of wChuteConfig
#define CHUTE_CFG_LEFT					0x0001	// 1 = left 0 = right; according to sorter move direction
#define CHUTE_CFG_FLAP					0x0002	// Destination with flap
#define CHUTE_CFG_FULL					0x0004	// Overflow sensor
#define CHUTE_CFG_EMPTY					0x0008	// Empty sensor
#define CHUTE_CFG_HALF_FULL				0x0010	// Half-full sensor
#define CHUTE_CFG_LAMP					0x0020	// Lamp present
#define CHUTE_CFG_TRACK					0x0040	// Tracking sensor present
#define CHUTE_CFG_JAM					0x0080	// Jam present
#define CHUTE_CFG_ACTIVE				0x0100	// Destination active management (basic data exchange with chute)
#define CHUTE_CFG_BLOCK					0x0200	// Block Input present
#define CHUTE_CFG_IOEXCHANGE			0x0400	// Chute must performe I/O Exchange
#define CHUTE_CFG_STATE_OUTPUT			0x0800	// Chute must provide an output with curent state
#define CHUTE_CFG_MAINTENANCE			0x1000	// Chute can be under maintenance condition
#define CHUTE_CFG_USE_COIL				0x2000	// Chute must use coil (no inductor !)
#define CHUTE_CFG_USE_BEST_COIL_ONLY	0x4000	// Chute must use coil (no inductor !)

// Value of byObjectDischargeMode
#define OBJECT_DISCHARGE_BY_HEAD	1
#define OBJECT_DISCHARGE_BY_TAIL	2
#define OBJECT_DISCHARGE_BY_CENTER	3
// The following values are meaningful only for GENI-Belt chute type ...
#define OBJECT_DISCHARGE_BY_BEGIN_CHUTE		4
#define OBJECT_DISCHARGE_BY_END_CHUTE		5
#define OBJECT_DISCHARGE_BY_CENTER_CHUTE	6

// Value of byCurrentFlapPosition and byRequiredFlapPosition
#define CHUTE_FLAP_POSITION_UNKNOWN		0
#define CHUTE_FLAP_IS_CLOSE				1
#define CHUTE_FLAP_IS_OPEN				2

 // Value for byCurrentFlapCommand
#define CHUTE_FLAP_NO_COMMAND			0
#define CHUTE_FLAP_CLOSE_COMMAND		1
#define CHUTE_FLAP_OPEN_COMMAND			2

// Values (bits!) of dwIOExchangeResult
#define CHUTE_IOEXC_TIMEOUT					0x00000000	// No replay (Timeout)
#define CHUTE_IOEXC_ACK						0x00000001	// ACK received, can sort
#define CHUTE_IOEXC_NACK_0					0x00000002	// NAK: E-Stop
#define CHUTE_IOEXC_NACK_1					0x00000004	// NAK: Full (or no space)
#define CHUTE_IOEXC_NACK_2					0x00000008	// NAK: Possible collision
#define CHUTE_IOEXC_NACK_3					0x00000010	// NAK: System not ready (Missing Auto/Manual)
#define CHUTE_IOEXC_NACK_4					0x00000020	// NAK: Type 4
#define CHUTE_IOEXC_NACK_5					0x00000040	// NAK: Type 5
#define CHUTE_IOEXC_NACK_6					0x00000080	// NAK: Type 6
#define CHUTE_IOEXC_NACK_7					0x00000100	// NAK: Type 7
#define CHUTE_IOEXC_NACK_8					0x00000200	// NAK: Type 8
#define CHUTE_IOEXC_NACK_9					0x00000400	// NAK: Type 9

// Flap nif counter limit ...
#define CHUTE_FLAP_MAX_NIF_COUNTER		20000

// Inductor action time (ms) if not configured into chute
#define CHUTE_BASE_INDUCTOR_ACTION_MS	150

// INPUTS name defines
#define DI_CHUTE_FULL					"DP"	//"CHUTE_FULL_"		// Full sensor I/O name			CHUTE_FULL_xxxxx			xxxxx = Chute ID
#define DI_CHUTE_HALF_FULL				"SP"	//"CHUTE_HFULL_"	// Half full sensor I/O name	CHUTE_FULL_xxxxx			xxxxx = Chute ID
#define DI_CHUTE_JAM					"DJ"	//"CHUTE_JAM_"		// Jam sensor I/O name			CHUTE_JAM_xxxxx				xxxxx = Chute ID
#define DI_CHUTE_BLOCK					"DB"	//"CHUTE_BLOCK_"	// Block chute					CHUTE_BLOCK_xxxxx			xxxxx = Chute ID
#define DI_CHUTE_TRACKING				"TK"	//"CHUTE_TRACK_"	// Tracking sensor I/O name		CHUTE_TRACK_xxxxx			xxxxx = Chute ID
#define DI_CHUTE_BUTTONS				"CHUTE_BTN_"				// Buttons I/O name				CHUTE_BTN_xx_yyyyy			xx = Button ID yyyyy = Chute ID
#define OFFSET_CHUTEID_BUTTONS			3							// Offset after DI_CHUTE_BUTTONS to find Chute ID
#define DI_CHUTE_INDUCTOR_FEEDBACK		"IN"//"CHUTE_IND_FB_"		// Inductor feedback			CHUTE_IND_FB_xyyyyy			x = Inductor # yyyyy = Chute ID
#define DI_CHUTE_FLAP_IS_OPEN			"CHUTE_FLAP_O_"				// Flap is Open					CHUTE_FLAP_O_xxxxx			xxxxx = Chute ID
#define DI_CHUTE_FLAP_IS_CLOSE			"CHUTE_FLAP_C_"				// Flap is Close				CHUTE_FLAP_C_xxxxx			xxxxx = Chute ID
#define DI_CHUTE_FLAP_TOUT				"CHUTE_FLAP_TOUT_"			// Flap move timeout			CHUTE_FLAP_TOUT_xxxxx		xxxxx = Chute ID
#define DI_CHUTE_IOEXC_TIMEOUT			"CHUTE_IOEXC_TOUT_"			// I/O Exchange Timeout			CHUTE_IOEXC_TOUT_xxxxx		xxxxx = Chute ID
#define DI_CHUTE_IOEXC_ACK				"CHUTE_IOEXC_ACK_"			// I/O Exchange ACK to sort		CHUTE_IOEXC_ACK_xxxxx		xxxxx = Chute ID
#define DI_CHUTE_IOEXC_NACK				"CHUTE_IOEXC_NACK_"			// I/O Exchange NAK to sort		CHUTE_IOEXC_NAK_yxxxxx		y = NAK type (0..9) xxxxx = Chute ID
#define DEI_CHUTE_IOEXC_NAK_CUSTOM_CODE	"CHUTE_IOEXC_CUSCOD_"		// I/O Exchange NACK Custom Cod CHUTE_IOEXC_CUSCOD_xxxxx	xxxxx = Chute ID
#define DI_CHUTE_RESET					"CHUTE_RST_"				// Chute reset as SCADA			CHUTE_RST_xxxxx				xxxxx = Chute ID
#define DI_CHUTE_MAINTENANCE			"CHUTE_MAINT_"				// Chute maintenance selector	CHUTE_MAINT_xxxxx			xxxxx = Chute ID
#define DI_CHUTE_SPECIAL				"CHUTE_SP_"					// Chute spacial I/O			CHUTE_SP_[anychar]

// OUTPUTS name defines
#define DO_CHUTE_LAMP					"LP"//"CHUTE_LAMP_"		// Lamp								CHUTE_LAMP_yxxxxx			y = Lamp # xxxxx = Chute ID
#define DO_CHUTE_FLAP_OPEN				"CHUTE_FLAP_O_"			// Flap Open Comand					CHUTE_FLAP_O_xxxxx			xxxxx = Chute ID
#define DO_CHUTE_FLAP_CLOSE				"CHUTE_FLAP_C_"			// Flap Close Comand				CHUTE_FLAP_C_xxxxx			xxxxx = Chute ID
#define DO_GROUP_CHUTE_LAMP				"XP"//"CHUTE_GLAMP_"	// Group Lamp						CHUTE_GLAMP_yxxxxx			y = Lamp # xxxxx = Chute ID
#define DO_CHUTE_INDUCTOR				"DE"//"CHUTE_IND_CMD_"	// Inductor comand					CHUTE_IND_CMD_xyyyyy		x = Inductor # yyyyy = Chute ID
#define DO_CHUTE_INCOMING_OBJECT		"CHUTE_IN_OBJ_"			// Chute incoming object			CHUTE_IN_OBJ_xxxxx			xxxxx = Chute ID
#define DO_CHUTE_INCOMING_OBJECT_DATA	"CHUTE_IN_DATA_"		// Chute incoming object data		CHUTE_IN_DAT_xxxxx			xxxxx = Chute ID
#define DO_CHUTE_IOEXC_LIFEBIT			"CHUTE_IOEXC_LIFEBIT_"	// I/O Exchange Lifebit				CHUTE_IOEXC_TOUT_x			x = free progressive number
#define DO_CHUTE_IOEXC_CHUTE_STATE		"CHUTE_IOEXC_STATE"		// I/O Exchange Chute State			CHUTE_IOEXC_STATEy_xxxxx	y = 0..9 xxxxx = Chute ID
#define DO_CHUTE_IOEXC_REQUEST_SORT		"CHUTE_IOEXC_REQS_"		// I/O Exchange REQuest to Sort		CHUTE_IOEXC_RSORT_xxxxx		xxxxx = Chute ID
#define DO_CHUTE_IOEXC_SORT				"CHUTE_IOEXC_SORT_"		// I/O Exchange Sort done			CHUTE_IOEXC_SORT_xxxxx		xxxxx = Chute ID
#define DO_CHUTE_IOEXC_NOT_SORT			"CHUTE_IOEXC_NSORT_"	// I/O Exchange Sort not done		CHUTE_IOEXC_SORT_xxxxx		xxxxx = Chute ID
#define DEO_CHUTE_IOEXC_DATA			"CHUTE_IOEXC_DATA_"		// I/O Exchange Data				CHUTE_IOEXC_DATA_xxxxx		xxxxx = Chute ID

// Enums for I/O !
enum _ENUM_CHUTE_IO{
	DI_CHT_FULL = 0,
	DI_CHT_HALF_FULL,
	DI_CHT_JAM,
	DI_CHT_TRACKING,
	DI_CHT_BLOCK,
	DI_CHT_BUTTONS,
	DI_CHT_FLAP_IS_OPEN,
	DI_CHT_FLAP_IS_CLOSE,
	DI_CHT_FLAP_TOUT,
	DI_CHT_INDUCTOR_FEEDBACK,
	DI_CHT_IOEXC_TIMEOUT,
	DI_CHT_IOEXC_ACK,
	DI_CHT_IOEXC_NACK,
	DI_CHT_RESET,
	DI_CHT_MAINTENANCE,
	DI_CHT_SPECIAL,
////////////////////////////////////////////////
	DO_CHT_LAMP,
	DO_CHT_FLAP_OPEN,
	DO_CHT_FLAP_CLOSE,
	DO_CHT_INCOMING_OBJECT,
	DO_CHT_IOEXC_LIFEBIT,
	DO_CHT_IOEXC_CHUTE_STATE,
	DO_CHT_IOEXC_REQUEST_SORT,
	DO_CHT_IOEXC_SORT

};

// Enums chute's fault reasons ...
enum _CHUTE_FAULT_REASON{

CHUTE_FAULT_REASON_NOTVALID = 0,
CHUTE_FAULT_REASON_DISABLED,
CHUTE_FAULT_REASON_JAM,
CHUTE_FAULT_REASON_OAM,
CHUTE_FAULT_REASON_TRACKING,
CHUTE_FAULT_REASON_EXTERN,
CHUTE_FAULT_REASON_ALARM,
CHUTE_FAULT_REASON_IOEXC_TIMEOUT,
CHUTE_FAULT_REASON_IOEXC_NAK,
CHUTE_FAULT_REASON_MAINTENANCE

};

// Alarms bit define
#define ALARM_CHUTE_LOGICAL_FAULT			0	// Many parcel unsorted ...
#define ALARM_CHUTE_BUSFIELD_FAULT			1	// Busfield fault
#define ALARM_CHUTE_INDUCTOR_FAULT			2	// Inductor 1 fault (also from Control Zone)
// Inductor #2 Fault ____________________	3
// Inductor #3 Fault ____________________	4
// Inductor #4 Fault ____________________	5
// Inductor #5 Fault ____________________	6
// Inductor #6 Fault ____________________	7
// Inductor #7 Fault ____________________	8
// Inductor #8 Fault ____________________	9
// Inductor #9 Fault ____________________	10
// [MSC-165] Improvement: If no Coil available for Unload operation, a new chute alarm or warning will be raised up
#define ALARM_CHUTE_NO_COIL					11	// No COIL to Sort in Chute!
//
#define ALARM_CHUTE_DISABLED				32	//
#define ALARM_CHUTE_FULL					33	//
#define ALARM_CHUTE_JAM						34	//
#define ALARM_CHUTE_OAM_BLOCK				35	//
#define ALARM_CHUTE_EXT_BLOCK				36	//
#define ALARM_CHUTE_FLAP_TOUT_OPEN			37	//
#define ALARM_CHUTE_FLAP_TOUT_CLOSE			38	//
#define ALARM_CHUTE_FLAP_BAD_POSITION		39	//
#define ALARM_CHUTE_IOEXC_TIMEOUT			40	//
#define ALARM_CHUTE_MAINTENANCE				41	//

// Warning bit defines
#define WARNING_CHUTE_HALF_FULL			    0	//
#define WARNING_CHUTE_TRACKING_BLOCK	    1	//
#define WARNING_CHUTE_INDUCTOR_FAULT		2	// Inductor 1 fault (warning)
// Inductor #2 Fault ____________________	3
// Inductor #3 Fault ____________________	4
// Inductor #4 Fault ____________________	5
// Inductor #5 Fault ____________________	6
// Inductor #6 Fault ____________________	7
// Inductor #7 Fault ____________________	8
// Inductor #8 Fault ____________________	9
// Inductor #9 Fault ____________________	10
// [MSC-165] Improvement: If no Coil available for Unload operation, a new chute alarm or warning will be raised up
#define WARNING_CHUTE_NO_COIL				11	// No COIL to Sort in Chute!
