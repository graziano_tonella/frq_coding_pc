///////////////////////////////////////////////////////////////////////////////////
///
/// 
/// \file			RecenteringSensor.h
///
/// \brief			header
///

///
///	\class			
///
/// \brief			New Recentering stations parameters 
///					
///
///	\version		1.0
///
///	\date    		10/December/2008
///
///	\author			Mauro Manca		[+39-0331-665279 mauro.manca@fivesgroup.com]
///					Marco Piazza	[+39-0331-665307 marco.piazza@fivesgroup.com]
///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#define NUM_SENSORS		8		// Num MAX sensors

enum _ENUM_RECENTERING_PH_TYPE
{
	NOT_PRESENT = 0,
	DIGITAL = 10,
	ANALOG = 20,
	IOLINK = 30
};

enum _ENUM_RECENTERING_PH_SIDE
{
	UNDEFINED = 0,
	LEFT = 10,
	RIGHT = 20

};

// --------------------------------------------------------
//	Legend :
//				Sorter direction
//					   /\					
//	LEFT SIDE         |  |         RIGHT SIDE
//											
//			 PHLx PHLx	  PHRx PHRx
//			  �	   �        �    � 				
//							  				
//			  --------------------				
//   ^    ^	  |		  CELL	     |  ^    ^				
//  PHLx PHLx -------------------- PHRx PHRx			
//                     |
// --------------------|------------------------------> x
//	negative distance  |    positive distance						
//
// --------------------------------------------------------

#pragma pack(1)

typedef struct _STRUCT_RECENTERING_SENSOR
{
	/// 
	_ENUM_RECENTERING_PH_TYPE nType;

	/// side referred to motion direction
	_ENUM_RECENTERING_PH_SIDE nSide;

	/// NIF Offset position respect to master recentering NIF position (signed)
	short shNIFOffsetPosition;

	/* Old declaration of shDetectedDistance variable (without sign)
	/// Distance between center of the cell and detected area.
	/// So, if cell length is 1000mm Center is 500mm; With a sensor with 300mm detection area must be set to 200mm (500-300) !
	WORD shDetectedDistance;
	*/

	/// Distance (signed) between center of the cell and detected area.
	/// So, if cell length is 1000mm Center is 500mm; With a sensor with 300mm detection area must be set to 200mm (500-300) !
	short shDetectedDistance;

	/// Distance (signed) between center of the cell when Analog Pht outputs the lower value (4 mA or 0V)
	/// This parameter is meaningful only if sensor is Analog type
	short shDistanceAtAnalogLowerValue;

	/// Distance (signed) between center of the cell when Analog Pht outputs the upper value (20 mA or 10V)
	/// This parameter is meaningful only if sensor is Analog type
	short shDistanceAtAnalogUpperValue;

	///// Gradient of the linear conversion from 16 bit acquired value to distance (mm) from cell center
	///// This parameter is meaningful only if sensor is Analog type
	//float fGradient;

	///// Intercept of the linear conversion from 16 bit acquired value to distance (mm) from cell center
	///// This parameter is meaningful only if sensor is Analog type
	//float fIntercept;

	/// Reserved for future use, please always put 0 !
	WORD wParcelOffset;

	/// Number of NIF with sensor at 1
	WORD wNIFatOn;

	/// It enable Failure Check on Pht that never detects expected objects
	BOOL bNoDetectionAlarmEnabled;

	/// Number of NIF with sensor at Off expected at On
	WORD wNIFatOff_ExpectedOn;

	// Default constructor
	_STRUCT_RECENTERING_SENSOR() {	nType = NOT_PRESENT;
									nSide = UNDEFINED;
									shNIFOffsetPosition = 0; 
									shDetectedDistance = 0; 
									shDistanceAtAnalogLowerValue = 0;
									shDistanceAtAnalogUpperValue = 0;
									wParcelOffset = 0; 
									wNIFatOn = 0;
									wNIFatOff_ExpectedOn = 0; };

} STRUCT_RECENTERING_SENSOR;

#pragma pack()

#define SIZE_STRUCT_RECENTERING_SENSOR		sizeof(_STRUCT_RECENTERING_SENSOR)
