//
//	File:		DB_MSC_EnergyBox_Test.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v8.1.2 Build 7818
//
//	Version:	1.0
//
//	Created:	28/March/2011
//	Updated:	23/February/2012
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	23-02-2012 - Energy Box and Ultrakap tests type will also performe master train test
//	28-03-2011 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//							STRUCT_ENERGYBOX_TEST (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_ENERGYBOX_TEST
{
	//
	BYTE	byType;						// Type of Energy box test (see define below)
	WORD	wTestPosition;				// Sorter position to performe test
	WORD	wConsecutiveFaultLimit;		// Number of consecutive fault to set alarm
	//
	WORD	wTestFlags;					// Flags used during runtime operation ... (see define below)
	BYTE	byConsecutiveFault;			// Number of consecutive bad test
	WORD	wWaitAfterStart;			// Timer after machine start (for Main Train test)
	//
	STRUCT_DIAG_BLOCK	csDiag;			// Full Diagnostic
	//
} STRUCT_ENERGYBOX_TEST;
#pragma pack()

#define SIZE_STRUCT_ENERGYBOX_TEST		sizeof(_STRUCT_ENERGYBOX_TEST)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// byType defines ...
#define ENERGYBOX_TEST_TYPE_ULTRAKAP							1	// Ultrakap Test for Energy Box and master train on sorter
#define ENERGYBOX_TEST_TYPE_EBOX								2	// Test for Energy Box and master train on sorter
#define ENERGYBOX_TEST_TYPE_WHEEL								3	// Test for Wheel
#define ENERGYBOX_TEST_TYPE_MASTERTRAIN							4	// Test ONLY for Master Train (Capotreno)
#define ENERGYBOX_TEST_TYPE_SLIDING_CONTACT						5	// Test for Sliding Contact
#define ENERGYBOX_TEST_TYPE_MASTERTRAIN_AND_BUSBREAKER			6	// Test for Master Train and BUS Breaker

// wTestFlag defines ...
#define ENERGYBOX_TEST_FLAG_IDLE		0x0000		// Nothing to do ...
#define ENERGYBOX_TEST_FLAG_WORKING		0x0001		// Test is in progress ...
#define ENERGYBOX_TEST_FLAG_OK			0x0002		// Feedback from DI recived

// Alarm's bit define
#define	ENERGYBOX_TEST_FAULT_SW			0			// Consecutive fault limit
#define	ENERGYBOX_TEST_FAULT_HW			1			// General HW fault (TBD)

// I/O Name Define
#define DI_ENERGYBOX_TEST_FEEDBACK	"EBOX_TEST_"	// Energy box test feedback 1 = OK, 0 = Error
