///////////////////////////////////////////////////////////////////////////////////
///
/// 
/// \file			RecenteringStation.h
///
/// \brief			header
///

///
///	\class			
///
/// \brief			New Recentering stations parameters 
///					
///
///	\version		1.0
///
///	\date    		10/December/2008
///
///	\author			Mauro Manca [+39-0331-665279 mauro.manca@fivesgroup.com]
///					Marco Piazza	[+39-0331-665307 marco.piazza@fivesgroup.com]
///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "RecenteringSensor.h"

#define DEFAULT_RECENTERING_THRESHOLD DEFAULT_MIN_SPACE_LOAD_UNLOAD_2X // Default threshold under which Recentering is not requested (25 mm)

enum _ENUM_RECENTERING_PARCEL_ALIGNMENT
{
	PARCEL_CENTER = 0,
	PARCEL_LEFT_SIDE,
	PARCEL_RIGHT_SIDE
};

#pragma pack(1)

typedef struct _STRUCT_RECENTERING_STATION
{
	//
	WORD	wBaseSensorNIFPosition;		// Base Recentering sensor position (NIF)
	WORD	wFirstInductorNIFPosition;	// Base Recentering inductor position (NIF)
	BYTE	byHowManyInductor;			// Number of inductors
//	BYTE	byLenInductor;				// Lenght inductor (NIF)	
	BYTE    byPitchInductor;			// Picth inductors (NIF)
	SHORT	shExtraTimeDx;				// Extra time to moving (side referred to motion direction)
	SHORT	shExtraTimeSx;				// Extra time to moving (side referred to motion direction)
	//
	BOOL	bIsUsedAsDisplacement;		// Enable the Displacement Function
	//
	BOOL	bRecenteringEnabled;		// Enable Recentering when sensor acquisition is valid from only one side
	//
	_ENUM_RECENTERING_PARCEL_ALIGNMENT nParcelAlignment;
	//
	short	shOffsetFromCenter;			// Offset from center cell (mm); used for parcel decentering
	//
	WORD	wMaxParcelLengthToRecenter;	// Max parcel lenght to recenter; over this size the recentering operation is disabled
	//
	WORD	wRecenteringThreshold;		// Threshold (mm) under which Recentering is not requested
	//
	WORD	wCoilToUseDBIndex_Service;	// DB Index of the coil to use for current service recentering cell action
	//
	STRUCT_RECENTERING_SENSOR cs_Sensors[ NUM_SENSORS ];
	//
}	STRUCT_RECENTERING_STATION;

#pragma pack()

#define SIZE_STRUCT_RECENTERING_POSITION		sizeof(_STRUCT_RECENTERING_STATION)
