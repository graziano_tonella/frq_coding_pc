//
//	Class:		MSC_Trace_Defines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//
//	Version:	1.0
//
//	Created:	11/July/2008
//	Updated:	11/July/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	11-07-2008 - First release
//

#pragma once

enum _ENUM_TRACE {

	TRACE_LEVEL_0 = 0,		// Generic level
	TRACE_LEVEL_1,			// Loading
	TRACE_LEVEL_2,			// Sorting
	TRACE_LEVEL_3,			// Scanner
	TRACE_LEVEL_4,			// Chute
	TRACE_LEVEL_5,			// PID information
	TRACE_LEVEL_6,			// Encoder
	TRACE_LEVEL_7,			// Smart Device
	TRACE_LEVEL_8,			// OAM
	TRACE_LEVEL_9,			// OAM Data
	TRACE_LEVEL_10,			// OPC
	TRACE_LEVEL_11,			// Cell Test
	TRACE_LEVEL_12,			// Sorter Power
	TRACE_LEVEL_13,			// DAC
	TRACE_LEVEL_14,			// Coordinator
	TRACE_LEVEL_15,			// Modulator/Coil
	TRACE_LEVEL_16,			// Recentering
	TRACE_LEVEL_17,			// Encoder NIF time
	TRACE_LEVEL_18,			// Spare
	TRACE_LEVEL_19,			// Spare
	MAX_TRACE_LEVEL

};

#define TRACE_ALWAYS		99
