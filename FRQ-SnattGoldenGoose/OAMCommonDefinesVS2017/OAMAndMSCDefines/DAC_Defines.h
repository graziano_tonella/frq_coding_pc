//
//	Header:		DAC_Defines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//				Visual C++ 2012
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//				RTX 2012 Build 10540
//
//	Version:	1.3
//
//	Created:	17/April/2012
//	Updated:	16/December/2013
//	Updated:	13/February/2014
//
//	Author:		Matteo Gardella
//	Update:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//	Update:		Mauro Manca  [+39-0331-665279 mauro.manca@fivesgroup.com]
//	Update:		Andrea Cascarano  [+39-0331-665349 andrea.cascarano@fivesgroup.com]
//
//  History:
//
//  19-02-2015 - Updated to new Spec.TrendViewer-3.0.doc 
//  13-02-2014 - Retrofit from ChronoPost of induction line _ENUM_CP_IN_INDICATORS: 
//               see Spec.TrendViewer-2.4.doc
//  16-12-2013 - New version merged with induction line
//	28-09-2013 - Added new indicators into CP_CELLSTATE
//				 Added P_POWER_SAVING into CP_SORTING
//	01-08-2013 - Revised some structure according to Spec.TrendViewer-2.3.doc
//	18-03-2013 - Removed two parameters not used (wFirstDest, wLastDest)
//				 Fixed all parts with "da sistemare" comments
//				 Fixed CP_CHUTE<x> Indicators calculation
//	17-04-2012 - First release
//

#pragma once

#define NODE_DAC				301
#define CHECK_POINT_NAME_SIZE	30
#define MAX_INDICATORS			75
#define MAX_DESTINATION			25
#define MAX_CHUTE_MSG14			1000
#define MAX_GENERAL				100
#define MAX_CELL				1000
#define MAX_MSG14_DEVICE		max(max( max(MAX_INDICATORS,MAX_CHUTE_MSG14),MAX_GENERAL),MAX_CELL)
#define MAX_MOTORS				200
#define MAX_SCANNERS			10
// -----------------------------------------
// --									  --
// -- S E C T I O N   DAC MESSAGES		  --
// --									  --
// -----------------------------------------
enum _ENUM_wMSGID_DAC
{
	wMSGID_DAC_MSG01_TRIGGER		= 21000,
	wMSGID_DAC_MSG02_UNLOADED,
	wMSGID_DAC_MSG03_REJECTED,
	wMSGID_DAC_MSG04_LOADED,
	wMSGID_DAC_MSG05_COUNTERS,
	wMSGID_DAC_MSG06_ITEM_HISTORY,
	wMSGID_DAC_MSG07_CHECK_POINT,
	wMSGID_DAC_MSG08_CUSTOM_COUNTERS,
	wMSGID_DAC_MSG09_ISPC_REJECTED_COUNTERS,
	wMSGID_DAC_MSG10_ISPC_DIAGNOSTIC_COUNTERS,
	wMSGID_DAC_MSG11,
	wMSGID_DAC_MSG12,
	wMSGID_DAC_MSG13,
	wMSGID_DAC_MSG14_DEVICES_COUNTERS,

	//
	LAST_DAC_MSGID
};
enum _ENUM_DAC_MASK_BIT_SEND
{
	 MASK_BIT_MSG00 = 1,	// bit 0
	 MASK_BIT_MSG01 = 2,	// bit 1
	 MASK_BIT_MSG02 = 4,	// bit 2
	 MASK_BIT_MSG03 = 8,	// bit 3
	 MASK_BIT_MSG04 = 16,	// bit 4
	 MASK_BIT_MSG05 = 32,	// bit 5
	 MASK_BIT_MSG06 = 64,	// bit 6
	 MASK_BIT_MSG07 = 128,	// bit 7
	 MASK_BIT_MSG08 = 256,	// bit 8
	 MASK_BIT_MSG09 = 512,	// bit 9
	 MASK_BIT_MSG10 = 1024,	// bit 10
	 MASK_BIT_MSG11 = 2048,	// bit 11
	 MASK_BIT_MSG12 = 4096,	// bit 12
	 MASK_BIT_MSG13 = 8192,	// bit 13
	 MASK_BIT_MSG14	= 16384,// bit 14
};
enum _ENUM_SUB_SYSTEM_TYPE
{
	SUBSYSTEM_SORTER = 1,
	SUBSYSTEM_INDUCTION_LINES,
	SUBSYSTEM_UPSTREAMS,
	SUBSYSTEM_SINGULATORS,
	SUBSYSTEM_DOWNSTREAM
};

enum _ENUM_DAC_TRIGGER_TYPE
{
	DATA_REQUEST = 0,
	RESET_COUNTERS,

	LAST_TYPE
};

// Device type 
enum _ENUM_DEVICE_TYPE
{
	DEVICE_TYPE_BELTDEVICE = 1,
	DEVICE_TYPE_SENSOR,
	DEVICE_TYPE_STROBE,
	DEVICE_TYPE_CELL,
	DEVICE_TYPE_CHUTE,
	DEVICE_TYPE_POWER,
	DEVICE_TYPE_ENCODER,
	DEVICE_TYPE_DRIVEUNIT,
	DEVICE_TYPE_ZONE
};

// Device sub-type 
enum _ENUM_SUB_DEVICE_TYPE
{
	// Sensor
	SUB_DEV_PHOTOCELL = 1,
	SUB_DEV_BARRIER,
	SUB_DEV_ZERO_SENSOR,
	SUB_DEV_RESYNCH,
	SUB_DEV_CONTROL_ZONE,
	SUB_DEV_CHECKPOSITION,
	// Strobe Device
	SUB_DEV_SCANNER = 1,
	SUB_DEV_VOLUME,
	SUB_DEV_CAMERA,
	SUB_DEV_RECENTERING,
	SUB_DEV_POWER_TEST,
	SUB_DEV_BUS_BREAKER,
	SUB_DEV_CELL_TEST,
	SUB_DEV_X_RAY,
	// Power
	SUB_DEV_POWERWEEL = 1,
	SUB_DEV_ENERGYBOX,
	SUB_DEV_VAHLE
};
#pragma pack(1)
typedef struct _STRUCT_INDICATOR
{
	//
	WORD	wIndicatorID;				// Indicator ID
	int		nIndicatorValue;			// Indicator counter
	//
} STRUCT_INDICATOR;
#define SIZE_STRUCT_INDICATOR	sizeof(_STRUCT_INDICATOR)
#pragma pack()
//////////////////////////////////////////////////////////////////////////
// MSG01
//////////////////////////////////////////////////////////////////////////

#pragma pack(1)
typedef struct _STRUCT_DAC_TRIGGER
{
	BYTE byTriggerType;		// See enum _ENUM_DAC_TRIGGER_TYPE
	DWORD dwMsgToSend;		// Bit mask for enable/disable msg send
} STRUCT_DAC_TRIGGER;
#define SIZE_STRUCT_DAC_TRIGGER	sizeof(_STRUCT_DAC_TRIGGER)
#pragma pack()

//////////////////////////////////////////////////////////////////////////
// /MSG01
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// MSG02
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_ITEM_UNLOADED
{
	//
	WORD wChuteID;			// Chute ID as per Sorter DB
	WORD wItemUnLoaded;		// Parcel sorted
	BYTE byNumOverload;		// Num overload
	WORD wTimeOverload;		// Duration overload in SEC
	WORD wTechnicalFault;	// Technical Fault
	WORD wOperativeFault;	// Operative Fault
	WORD wRun;				// Run (no fault !)
	//
} STRUCT_ITEM_UNLOADED;
#define SIZE_STRUCT_ITEM_UNLOADED	sizeof(_STRUCT_ITEM_UNLOADED)
#pragma pack()

#pragma pack(1)
typedef struct _STRUCT_DAC_UNLOADED // MSG02
{
	BYTE byPlantID; 
	STRUCT_ITEM_UNLOADED csItemUnloaded[MAX_DESTINATION];

} STRUCT_DAC_UNLOADED;
#define SIZE_STRUCT_DAC_UNLOADED	sizeof(_STRUCT_DAC_UNLOADED)
#pragma pack()
//////////////////////////////////////////////////////////////////////////
// \MSG02
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// MSG03
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_DAC_REJECTED
{
	BYTE byPlantID; 
	WORD wChuteID; 
	WORD wItemCounter[MAX_INDICATORS];

} STRUCT_DAC_REJECTED;
#define SIZE_STRUCT_DAC_REJECTED	sizeof(_STRUCT_DAC_REJECTED)
#pragma pack()
//////////////////////////////////////////////////////////////////////////
// \MSG03
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// MSG04
//////////////////////////////////////////////////////////////////////////

#pragma pack(1)
typedef struct _STRUCT_DAC_LOADED
{
	BYTE bySystemID; 
	BYTE bySubSystemType; // -------- SEE _ENUM_SUB_SYSTEM_TYPE ----------//
	WORD wSubSystemID;
	WORD wItemLoaded;

} STRUCT_DAC_LOADED;
#define SIZE_STRUCT_DAC_LOADED	sizeof(_STRUCT_DAC_LOADED)
#pragma pack()

//////////////////////////////////////////////////////////////////////////
// \MSG04
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// MSG07
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_CHECK_POINT_MSG07
{
	//
	BYTE bySystemID;										// System ID
	char szTimestamp[14];									// YYYYMMDDHHMMSS
	char szCP_Name[CHECK_POINT_NAME_SIZE];					// Check Point name
	WORD wNumIndicators;									// Total indicators
	STRUCT_INDICATOR csIndicator[MAX_INDICATORS];			// Values

} STRUCT_CHECK_POINT_MSG07;
#define SIZE_STRUCT_CHECK_POINT_MSG07	sizeof(_STRUCT_CHECK_POINT_MSG07)
#pragma pack()

//////////////////////////////////////////////////////////////////////////
// \MSG07
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// MSG08
//////////////////////////////////////////////////////////////////////////

#pragma pack(1)
typedef struct _STRUCT_CHECK_POINT_MSG08
{
	//
	BYTE bySystemID;										// System ID
	char szTimestamp[14];									// YYYYMMDDHHMMSS
	char szDeviceName[CHECK_POINT_NAME_SIZE];				// Device that generate the counters (ie: Control Zone)
	WORD wIndicatorsType;									// Type of indicators (ie: Lost, Found, On Gap)
	WORD wNumIndicators;									// Total indicators
	STRUCT_INDICATOR csIndicator[MAX_INDICATORS];			// Values
	//
} STRUCT_CHECK_POINT_MSG08;
#define SIZE_STRUCT_CHECK_POINT_MSG08	sizeof(_STRUCT_CHECK_POINT_MSG08)
#pragma pack()

//////////////////////////////////////////////////////////////////////////
// \MSG08
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// MSG09
//////////////////////////////////////////////////////////////////////////

#pragma pack(1)
typedef struct _STRUCT_ISPC_REJECTED_MSG09
{
	BYTE bySystemID; 
	WORD wSubSystemID;
	WORD wNumIndicators; 
	STRUCT_INDICATOR csIndicator[MAX_INDICATORS];

} STRUCT_ISPC_REJECTED_MSG09;
#define SIZE_STRUCT_ISPC_REJECTED_MSG09	sizeof(_STRUCT_ISPC_REJECTED_MSG09)
#pragma pack()

//////////////////////////////////////////////////////////////////////////
// \MSG09
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// MSG10
//////////////////////////////////////////////////////////////////////////

#pragma pack(1)
typedef struct _STRUCT_ISPC_DIAGNOSTIC_MSG10
{
	BYTE bySystemID; 
	WORD wSubSystemID;
	WORD wNumDevice;
	BYTE byData;
} STRUCT_ISPC_DIAGNOSTIC_MSG10;
#define SIZE_STRUCT_ISPC_DIAGNOSTIC_MSG10	sizeof(_STRUCT_ISPC_DIAGNOSTIC_MSG10)
#pragma pack()

//////////////////////////////////////////////////////////////////////////
// \MSG10
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// MSG14
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct _STRUCT_VARIABLE_MSG14
{
	WORD wDeviceID;
	BYTE byNumIndicators;
	STRUCT_INDICATOR csIndicator[MAX_INDICATORS];
} STRUCT_VARIABLE_MSG14;
#define SIZE_STRUCT_VARIABLE_MSG14	sizeof(_STRUCT_VARIABLE_MSG14)
#pragma pack()
//
#pragma pack(1)
typedef struct _STRUCT_DEVICE_DATA_MSG14
{
	// Fix part
	BYTE bySystemID;													// System ID ("sorter" id)
	WORD wDeltaT;														// Delta between 2 message 14
	char szTimestamp[14];												// YYYYMMDDHHMMSS
	BYTE bySubSystemType;												// Sub system type (sorter, induction ...)
	BYTE byGroupID;														// Working area of message sender (group 1 / group 2 ...)
	BYTE bySubSystemID;													// Relative ID in working area of subsystem 
	WORD wDeviceType;													// Device type
	WORD wDeviceSubType;												// Device subtype
	WORD wNumDevice;													// Device quantity in one message
	// Variable part according to wNumDevice
	STRUCT_VARIABLE_MSG14 csCustomInfo[MAX_MSG14_DEVICE];
	// WORD wDeviceID[MAX_MSG14_DEVICE];									// Identifier of the message
	// BYTE byNumIndicators;												// Num of indicators in one message
	// STRUCT_INDICATOR csIndicator[MAX_MSG14_DEVICE][MAX_INDICATORS];	// [max quantity of object] [max quantity of id and value for each object]
} STRUCT_DEVICE_DATA_MSG14;
#define SIZE_STRUCT_DEVICE_DATA_MSG14	sizeof(_STRUCT_DEVICE_DATA_MSG14)
#pragma pack()
//////////////////////////////////////////////////////////////////////////
// \MSG14
//////////////////////////////////////////////////////////////////////////