//
//	File:		DB_MSC_MODULATOR_COIL.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 10.0 (2010)
//              RTX v12.0
//
//	Version:	1.0
//
//	Created:	06/October/2015
//	Updated:	26/January/2016
//
//	Author:		Andrea Cascarano [+39-0331-665349 andrea.cascarano@fivesgroup.com]
//				Matteo Fumagalli [+39-0331-665280 matteo.fumagalli@fivesgroup.com]
//				Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	26-01-2016 - Revision for final software
//	03-11-2015 - Update: Added the Digital Input useful for IO-Link channel check.
//						 Added ALARM bitmask (ALARM_MODULATOR_IO_LINK_CHANNEL_FAULT).
//	06-10-2015 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"
#include "DB_MSC_Modulator_Coil2.h"
#include "DigitalAntenna\STD_Message_With_Modulator.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_COIL_MSG_QUEUE
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_COIL_MSG_QUEUE
{
	//
	STRUCT_MODULATOR_COMMAND* pcsMasterAction;
	//
	STRUCT_MISSION_FOR_COIL	csCoilMission;
	STRUCT_MODULATOR_COMMAND csAction;
	//
} STRUCT_COIL_MSG_QUEUE;
#pragma pack()
#define SIZE_STRUCT_COIL_MSG_QUEUE sizeof(_STRUCT_COIL_MSG_QUEUE)

