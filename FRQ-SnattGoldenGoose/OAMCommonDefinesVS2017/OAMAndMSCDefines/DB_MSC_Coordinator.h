//
//	File:		DB_MSC_Coordinator.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 10.0 (2010)
//              RTX v12.0
//
//	Version:	1.0
//
//	Created:	06/October/2015
//	Updated:	09/March/2016
//
//	Author:		Andrea Cascarano
//	Update		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	09-03-2016 - Re-design for target core project
//	06-10-2015 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

// Trace level to use ...
#define TRACE_LEVEL_COORDINATOR											TRACE_LEVEL_14
#define COORDINATOR_DEFAULT_MESSAGE_TIMEOUT								5000	// 5s
#define TIME_OFFSET_BETWEEN_SWITCH_ON_RADIO_AND_COORD_COMMUNICATION		1000	// [ms] Time between coil mission to SWITCH_ON_RADIO_MODULE and Coordinator request-send data

//////////////////////////////////////////////////////////////////////////
//				STRUCT_COORDINATOR_INFO
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_COORDINATOR_INFO
{

	//
	// Runtime info ...
	//
	WORD					wStatusRxCounter;				// RX Status message counter
	//
	WORD					wRSSI_Avg;						// RSSI (Received Signal Strength Indication)
	DWORD					dwUpTime_s;						// The time passed from the last switch-on of the Coordinator [s].
	WORD					wRadioModuleStatus;				// Bitmask that describe the Coordinator Wireless Module Status
	WORD					wRadioModuleChannelUsed;		// MODULE CHANNEL
	DWORD					dwRadioModuleFW_version;		// Radio module FW version (is a 32 bit number)
	//
	// Firmware informations ...
	//
	WORD					wApplicationFW_MajorVersion;
	WORD					wApplicationFW_MinorVersion;
	DWORD					dwApplicationFW_BuildVersion;
	WORD					wApplicationFW_RepoVersion;
	//
} STRUCT_COORDINATOR_INFO;
#pragma pack()
//
#define SIZE_STRUCT_COORDINATOR_INFO	sizeof(_STRUCT_COORDINATOR_INFO)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_COORDINATOR (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_COORDINATOR
{
	//
	// Config fields ...
	//
	BYTE					byCoordinatorID;				// Coordinator ID
	WORD					wNifPosition;					// Nif position of coordinator
	WORD					wCoveredArea;					// From antenna position, covered area before and after that (NIF)
	WORD					wJoinTimeoutValue;				// Time to wait for a Join ACK
	//
	// Connection status ...
	//
	BOOL					bConnected;						// Coordinator is connected to DDE
	BOOL					bRadioModuleON;					// Module radio is on for antenna communication
	BOOL					bInUse;							// Coordinator is currently the used one ... 
	BOOL					bPowerSupplyState;				// It keep trace about the Power Supply state (ON; OFF)
	//
	// Runtime fields ...
	//
	WORD wCoilToUseDBIndex_Service;							// DB Index of the coil to use for current service Coordinator action
	//
	STRUCT_DIAG_BLOCK		csDiag;							// Full Diagnostic
	STRUCT_COORDINATOR_INFO csInfo;							// All information from Coordinator status message
	//
} STRUCT_COORDINATOR;
#pragma pack()
//
#define SIZE_STRUCT_COORDINATOR	sizeof(_STRUCT_COORDINATOR)

//////////////////////////////////////////////////////////////////////////
// I/O Defines
//////////////////////////////////////////////////////////////////////////

// Digital Output that manage the Power Supply of the Coordinators
#define COORD_POWER_SUPPLY				"COORD_POWER_SUPPLY_"	//		COORD_POWER_SUPPLY_x	x = Coordinator ID

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Alarm's bit define
#define COORDINATOR_ALARM_TIMEOUT_ANSWER			0	// No answer after sending a message
#define COORDINATOR_ALARM_TCPIP_NO_CONNECTION		1	// TCP/IP link is down (auto-resetting according to socket state)
#define COORDINATOR_ALARM_NO_COIL_AVAILABLE			2	// No Coil (with features COIL_FUNCTION_COORDINATOR | COIL_FUNCTION_CAN_JOIN) available in the Covered Area

// Warning's bit define
#define COORDINATOR_WARNING_DISABLED_BY_SCADA		0	// Disabled by SCADA
