//
//	File:		DB_MSC_CheckPosition.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	30/June/2008
//	Updated:	30/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	30-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"
#include "RecenteringStation.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CHECK_POSITION (DB)
//////////////////////////////////////////////////////////////////////////

#pragma pack(1)
typedef struct _STRUCT_CHECK_POSITION
{
	//
	BYTE	byZoneID;								// Zone Identification
	//
	WORD	wPosPHCheckGap;							// PH Position (NIF)
	//
	WORD	wMinSensorHeight;						// Sensor Hight (for lost manager)
	//
	STRUCT_RECENTERING_STATION	csRecentering;		// Recentering information
	//
	STRUCT_DIAG_BLOCK	csDiag;						// Full Diagnostic
	//
}	STRUCT_CHECK_POSITION;
#pragma pack()

#define SIZE_STRUCT_CHECK_POSITION		sizeof(_STRUCT_CHECK_POSITION)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// I/O defines ...
#define DI_ON_GAP_PH						"FTFRACELLE"			// PH to performe check position and on gap functions + number
#define DI_ON_GAP_CS						"FTFRACELLECOL"			// PH to performe check position and on gap functions + number
#define DI_RECENTERING_SENSOR				"REC_SENSOR_"			// Recentering sensor + GN (group, number)
#define AI_RECENTERING_SENSOR				"REC_SENSOR_AI_"		// Analog Recentering sensor + GN (group, number)
#define IOLINK_RECENTERING_SENSOR			"REC_SENSOR_IOL_"		// IO-LINK Recentering sensor + GN (group, number)
#define IOLINK_RECENTERING_SENSOR_MAPPING	"REC_SENSOR_IOL_MAP_"	// IO-LINK Recentering sensor + GN (group, number)
#define IOLINK_RECENTERING_SENSOR_DI		"REC_SENSOR_IOL_DI_"	// IO-LINK Recentering sensor + GN (group, number)
#define DI_RECENTERING_INDUCTOR_FEEDBACK	"REC_IND_FB_"			// Recentering inductor feedback + GN (group, number)
//
#define DO_RECENTERING_INDUCTOR_LEFT_CMD	"REC_IND_L_CMD_"		// Recentering inductor command Left movement + GN (group, number)
#define DO_RECENTERING_INDUCTOR_RIGHT_CMD	"REC_IND_R_CMD_"		// Recentering inductor command Right movement + GN (group, number)

#define MASK_IOLINK_DISTANCE 0x1FFF

// Alarms bits ...
#define CHECK_POS_RECENTERING_SENSOR_FAULT					0	// First sensor (MAX 8) Bit 0 .. 7
// _________ Sensor #1 fault (always enabled)				0
// _________ Sensor #2 fault (always enabled)				1
// _________ Sensor #3 fault (always enabled)				2
// _________ Sensor #4 fault (always enabled)				3
// _________ Sensor #5 fault (always enabled)				4
// _________ Sensor #6 fault (always enabled)				5
// _________ Sensor #7 fault (always enabled)				6
// _________ Sensor #8 fault (always enabled)				7
#define CHECK_POS_ALARM_GAP_PH								8	// PH to perform GAP control
#define CHECK_POS_ALARM_GAP_CS								8	// Color Sensor (CS) to perform GAP control
#define CHECK_POS_RECENTERING_INDUCTOR_FAULT				9	// First inductor fault (MAX 4) Bit 9..12
// _________ Inductor #1 fault								9
// _________ Inductor #2 fault								10
// _________ Inductor #3 fault								11
// _________ Inductor #4 fault								12
#define CHECK_POS_RECENTERING_SENSOR_EVER_ENABLED_FAULT		13	// First sensor (MAX 8) Bit 0 .. 7
// _________ Sensor #1 fault (never enabled)				13	// ########
// _________ Sensor #2 fault (never enabled)				14	// ########
// _________ Sensor #3 fault (never enabled)				15  // This kind of alarm reports that corresponding sensor
// _________ Sensor #4 fault (never enabled)				16	// does not detect expected on-cell object for
// _________ Sensor #5 fault (never enabled)				17	// CHECK_POS_RECENTERING_SENSOR_NIF_TO_FAULT NIF position
// _________ Sensor #6 fault (never enabled)				18	// continuously
// _________ Sensor #7 fault (never enabled)				19	// ########
// _________ Sensor #8 fault (never enabled)				20	// ########
#define CHECK_POS_ALARM_FORWARDED_FROM_MODULATOR			21	// The Modulator that operates as CheckPosition device, is in alarm

// Warning bit
#define CHECK_POS_WARNING_POSSIBLE_JAM						 0	// Possible Sorter Jam
#define CHECK_POS_WARNING_SD_ALARM_GAP_PH					30	// GAP PH alarm if managed by SmartDevice

// Number of NIF to set sensor fault ...
#define CHECK_POS_RECENTERING_SENSOR_NIF_TO_FAULT			70

// MAX Check positions control
#define MAX_CHECK_POS_CONTROLS	20

// Return codes of CLoadingManager::CheckParcelOnGap
#define CHECK_ONGAP_NOERROR			0		// Parcel is NOT on gap
#define CHECK_ONGAP_SORTABLE		1		// Parcel is on gap, but we can sort to reject
#define CHECK_ONGAP_FOREVER			2		// Parcel is ONLY on gap and cannot be sorted !
