//
//	File:		OAMRTXDefines_Special.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 2010
//
//	Created:	xx/yy/20zz
//	Updated:	xx/yy/20zz
//
//	Author:
//

#pragma once

//
// Custom messages as defined into chap.9 of document SPR99FOR00 made by G. Tonella
//

// To be sent with a GENERIC_DATAEXCHANGE message
// From:	GENERIC_DATAEXCHANGE_MSC
// To:		GENERIC_DATAEXCHANGE_OAMPC
#define wMSGID_MSCTOOAMENG_ITEMTRANSFERREDFROMISPCTOMSC	0xBA00

#pragma pack(1)
typedef struct _STRUCT_MSCTOOAMENG_ITEMTRANSFERREDFROMISPCTOMSC
{
	//
	BYTE	byOperatorId;		// It Contains The Induction Line Number
	WORD	wCell;				// It Contains The Loaded Cell Number
	BYTE	byUsedCells;		// It Contains The Number Of Used Cell(s) 
	DWORD32	dw32ObjectID;		// It Contains The Unique Item ID Number
	//
} STRUCT_MSCTOOAMENG_ITEMTRANSFERREDFROMISPCTOMSC;
#pragma pack()
