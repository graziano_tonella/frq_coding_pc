//
//	File:		DB_MSC_Speeds.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	27/June/2008
//	Updated:	27/June/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	27-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"
#include "COMMON_MSC_Defines.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CHUTE_SPEED_PARAMETERS
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CHUTE_SPEED_PARAMETERS
{
	//
	CHAR	cUnloadOffset;				// Unload offset modifier
	WORD	wTrackingTimeout;			// Tracking timeout
	WORD	wNifFlapMoveDelay;			// NIF counter to allow move of the flap
	//
} STRUCT_CHUTE_SPEED_PARAMETERS;
#pragma pack()
//
#define SIZE_STRUCT_CHUTE_SPEED_PARAMETERS sizeof(_STRUCT_CHUTE_SPEED_PARAMETERS)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_SPEEDS (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_SPEEDS
{
	//
	BYTE	bySpeedID;						// Speed ID (1..4)

	WORD	wSpeedSetPoint;					// Sorter speed xxxx mm/s
 
	WORD	wPID_Pr;						// Proportional parameter |
	WORD	wPID_Der;						// Derivative parameter   -- Used during normal run
	WORD	wPID_Int;						// Integrative parameter  |

	WORD	wStartingTorque;				// Torque to apply to start the sorter
	WORD	wStartingRamp;					// Value of torque to add each PID timer (100ms) during starting phase
	WORD	wAfterStartingRamp;				// Value of torque to add each PID timer (100ms) after first speed sample

	BYTE	byTimeToStop;					// Time (seconds) to take torque to zero (Sorter stop)

	short	shBrakeTorque;					// Starting torque to brake (drives analogue value !)
	BYTE	byTimeToBrake;					// Time (seconds) to take torque from brake to zero (Sorter braking)

	short	shMinTorqueForSpeedChange;		// Minimum torque value while changing speed ...
	short	shMaxTorqueForSpeedChange;		// Maximum torque value while changing speed ...

	double	dSpeedToTorqueFactor;			// Factor to convert speed in torque ...

	// Discharge offset for all chute type (we use CHUTE_INVALID as qty, see enums in COMMON_MSC_Defines.h !)
	STRUCT_CHUTE_SPEED_PARAMETERS csChuteSpeedOffset[CHUTE_INVALID];

}	STRUCT_SPEEDS;
#pragma pack()

#define SIZE_STRUCT_SPEEDS	sizeof(_STRUCT_SPEEDS)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////
