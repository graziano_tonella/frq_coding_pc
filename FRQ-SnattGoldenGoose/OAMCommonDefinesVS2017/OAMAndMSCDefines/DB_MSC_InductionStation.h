//
//	File:		DB_MSC_InductionStation.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	30/June/2008
//	Updated:	25/March/2013
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	25-03-2013 - Added I/O to ask ISPC to stop loading
//	30-06-2008 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//		STRUCT_ISPC_LOAD_SIMULATOR (used by SorterMIFTools)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_ISPC_LOAD_SIMULATOR
{
	//
	BYTE	bySimulation;		// 0 = Not active, 1 = Simulation ON
	BYTE	byCellToLoad;		// 0 = ALL, 1 = Only ODD, 2 = Only EVEN, 3 = CUSTOM
	BYTE	byHowManyCell;		// 1..4, number of cell to load
	BYTE	byRandom;			// 1 = random choose of number of cells but still in "byHowManyCell" range
	BYTE	byRate;				// Rate, 1 cell each X ( 0 or 1 = ALL )
	//
	WORD	wLastCell;			// Last checked cell ...
	BYTE	byCellCounter;		// Cell from last action ...
	//
}	STRUCT_ISPC_LOAD_SIMULATOR;
#pragma pack()
#define SIZE_STRUCT_ISPC_LOAD_SIMULATOR	sizeof(_STRUCT_ISPC_LOAD_SIMULATOR)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_INDUCTION_STATION (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_INDUCTION_STATION
{
	//
	WORD	wAbsoluteID;						// Induction Absolute number
	BYTE	byGroupID;							// Induction Group
	BYTE	byLineID;							// Induction ID inside the group
	BYTE	byWorkMode;							// Type of Induction; Single or Group
	//
	WORD    wPositionNIF;						// NIF mapping
	WORD    wCellEmptyCheckPos;					// First position to check if the cell is empty
	WORD    wCellEmptyConfirmPos;				// Second position to confirm if the cell is empty
	//
	STRUCT_DIAG_BLOCK	csDiag;					// Full Diagnostic
	//
	// Runtime counters ...
	//
	DWORD	dwLoadedObject;	 					// Counter of loaded items
	//
	STRUCT_ISPC_LOAD_SIMULATOR csSimulator;
	//
}	STRUCT_INDUCTION_STATION;
#pragma pack()
#define SIZE_STRUCT_INDUCTION_STATION	sizeof(_STRUCT_INDUCTION_STATION)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Max IS groups
#define MAX_IS_GROUPS				24
#define MAX_IS_LINES				9

// Values for byWorkMode
#define IS_WORKING_MODE_SINGLE		2
#define IS_WORKING_MODE_GROUP		3

// I/O defines ...
#define DO_SORTER_SYNCH_OK		"SORTER_SYNCH_OK_"		// Sorter syncronized + progressive number
#define DO_SORTER_SPEED_OK		"SORTER_SPEED_OK_"		// Sorter speed ok  + progressive number
#define DO_EMPTY_REQUEST		"EMPTY_REQUEST_"		// Sorter request lines empting + progressive number
#define DO_EMERGENCY_ON			"EMERGENCY_ON_"			// Sorter emergency state + progressive number
#define DO_SORTER_STOP_LOADING	"SORTER_STOP_LOADING_"	// Sorter ask to stop loading at all induction lines + progressive number

#define DO_FIRST_CELL_STATE		"FIRST_CELL_STATE_"		// First cell state (old EMPTY1 !) + progressive number
#define DO_FIRST_CELL_NUMBER	"FIRST_CELL_NUM_"		// Cell number under first check point + progressive number
#define DO_FIRST_GAP_B1_STATE	"FIRST_GAP_B1_STATE_"	// First GAP portion before belt ... + progressive number
#define DO_FIRST_GAP_B2_STATE	"FIRST_GAP_B2_STATE_"	// Second GAP portion before belt ... + progressive number
#define DO_FIRST_BELT_STATE		"FIRST_BELT_STATE_"		// Belt state ... + progressive number
#define DO_FIRST_GAP_A1_STATE	"FIRST_GAP_A1_STATE_"	// First GAP portion after belt ... + progressive number
#define DO_FIRST_GAP_A2_STATE	"FIRST_GAP_A2_STATE_"	// Second GAP portion after belt ... + progressive number
#define DO_FIRST_CELL_CHUTE		"FIRST_CELL_CHUTE_"		// Cell target Chute ID (if any) + progressive number

#define DO_FINAL_CELL_STATE		"FINAL_CELL_STATE_"		// First cell state (old EMPTY2 !) + progressive number
#define DO_FINAL_CELL_NUMBER	"FINAL_CELL_NUM_"		// Cell number under final check point + progressive number
#define DO_FINAL_GAP_B1_STATE	"FINAL_GAP_B1_STATE_"	// First GAP portion before belt ... + progressive number
#define DO_FINAL_GAP_B2_STATE	"FINAL_GAP_B2_STATE_"	// Second GAP portion before belt ... + progressive number
#define DO_FINAL_BELT_STATE		"FINAL_BELT_STATE_"		// Belt state ... + progressive number
#define DO_FINAL_GAP_A1_STATE	"FINAL_GAP_A1_STATE_"	// First GAP portion after belt ... + progressive number
#define DO_FINAL_GAP_A2_STATE	"FINAL_GAP_A2_STATE_"	// Second GAP portion after belt ... + progressive number
#define DO_FINAL_CELL_CHUTE		"FINAL_CELL_CHUTE_"		// Cell target Chute ID (if any) + progressive number

#define DO_SORTER_SPEEDSETP		"SORTER_SPEEDSETP_"		// Current Sorter speed set point mm/s + progressive number
#define DO_SORTER_NIF			"SORTER_NIF_"			// Current Sorter NIF ... + progressive number

#define DO_SW_WD				"ISPC_SW_WD_"			// Software watchdog for DP/DP coupler ...  + progressive number
#define DO_LOAD_FAULT			"ISPC_LOAD_FAULT_"		// Load fault condition + Group number + Line number
#define DO_ECHO_MESSAGE			"ISPC_ECHO_MSG_"		// Echo message counter for ACK + Group number + Line number

#define DI_SW_WD				"ISPC_SW_WD_"			// Software watchdog for DP/DP coupler ...  + progressive number
#define DI_LINE_EMPTY			"ISPC_EMPTY_"			// Line gn is empty (no parcel on-board !)...  + GN group, line
#define DI_ISPC_LINE_LOADING	"ISPC_LINE_LOADING_"	// Line gn is not loading

// Single line data transfer
#define DI_NEW_MESSAGE			"ISPC_NEW_MSG_"			// New message counter, there's a new message on new value ! + Group number + Line number
#define DEI_MESSAGE_DATA		"ISPC_MSG_DATA_"		// Used as DEI information + Group number + Line number

// Group data transfer
#define DI_GROUP_NEW_MESSAGE	"ISPC_G_NEW_MSG_"		// New message counter, there's a new message on new value ! + Group number + Slot number (parallel transfer)
#define DEI_GROUP_MESSAGE_DATA	"ISPC_G_MSG_DATA_"		// Used as DEI information + Group number + Slot number (parallel transfer)
