//
//	File:		DB_Tables_Defines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              	RTX v7.0 Build 4925
//			RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	11/June/2008
//	Updated:	07/March/2011
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	07-March-2011 - Add DB_GENERIC, DB_UPSTREAM, SIZE_DB_GENERIC
//	11-06-2008 - First release
//

#pragma once

// MSC DB Tables names ...
#define	DB_GENERIC					_T("DB_GENERIC")
#define	DB_INPUTS					_T("DB_INPUTS")
#define	DB_OUTPUTS					_T("DB_OUTPUTS")
#define DB_CABINETS					_T("DB_CABINETS_DIAG")
#define DB_CELLS					_T("DB_CELLS")
#define DB_CELLTEST					_T("DB_CELLTEST")
#define DB_CHECKPOSITION			_T("DB_CHECKPOSITION")
#define DB_CHUTE					_T("DB_CHUTE")
#define DB_CONTROLZONE				_T("DB_CONTROLZONE")
#define DB_DRIVEUNIT				_T("DB_DRIVEUNIT")
#define DB_ENCODER					_T("DB_ENCODER")
#define DB_AUXZERO					_T("DB_AUXZERO")
#define DB_GENERALPARAMETERS		_T("DB_GENERALPARAMETERS")
#define DB_INDUCTIONSTATION			_T("DB_INDUCTIONSTATION")
#define DB_SCANNER					_T("DB_SCANNER")
#define DB_SORTERSTATE				_T("DB_SORTERSTATE")
#define DB_SPEEDS					_T("DB_SPEEDS")
#define DB_TRAINS					_T("DB_TRAINS")
#define DB_ENERGYBOX				_T("DB_EBOX")
#define DB_ENERGYBOX_TEST			_T("DB_EBOX_TEST")
#define DB_SMARTDEVICE				_T("DB_SMARTDEVICE")
#define DB_UPSTREAM					_T("DB_UPSTREAM")
#define	DB_ENCODER_RESYNC			_T("DB_ENC_RESYNC")
#define DB_CELLS_MOVE_PROFILES		_T("DB_CELLS_MOVE_PROFILES")
#define DB_INDUCTORS_GROUPS			_T("DB_INDUCTORS_GROUPS")
#define DB_BUS_BREAKER				_T("DB_BUS_BREAKER")
#define DB_SAFETY_DOORS				_T("DB_SAFETY_DOORS")
#define DB_BLADE_CHECK				_T("DB_BLADE_CHECK")
#define DB_MODULATOR				_T("DB_MODULATOR")
#define DB_COIL						_T("DB_COIL")
#define DB_DIGITAL_ANTENNA			_T("DB_DIGITAL_ANTENNA")
#define DB_COORDINATOR				_T("DB_COORDINATOR")
#define DB_UNLOAD_RAMPS				_T("DB_UNLOAD_RAMPS")

// STATS DB Tables names ...
#define DB_SORTER_STATS				_T("DB_SORTER_STATS")
#define DB_SERIALCELLTEST_STATS		_T("DB_SERIALCELLTEST_STATS")

// RAM DB Tables names ...
#define DB_DIGITAL_ANTENNA_RAM		_T("DB_DIGITAL_ANTENNA_RAM")

// Some tables size ...

//
// [MSC-23] New Feature: New flag into table "DB_CABINETS_DIAG" in order to choose if that "cabinet" must stop the Sorter
// 2018-06-06 DB_CABINETS qty will be into a CSV, if no CSV, we use SIZE_DB_CABINETS_DIAG
//
#define MAX_SIZE_DB_CABINETS_DIAG		150		// Default number of cabinets (diagnostic)
//
#define SIZE_DB_ENCODER					2		// Max number of encoders
#define	SIZE_DB_SPEEDS					6		// Max number of speeds
#define SIZE_DB_GENERIC					100		// Size of generic db ...
//