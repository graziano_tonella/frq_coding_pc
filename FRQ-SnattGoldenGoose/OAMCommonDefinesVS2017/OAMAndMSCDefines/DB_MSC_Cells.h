//
//	File:		DB_MSC_Cells.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	27/June/2008
//	Updated:	24/March/2017
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//				Matteo Fumagalli [+39-0331-665280 matteo.fumagalli@fivesgroup.com]
//
//  History:
//
//	27-03-2018 - Changed the wComputeNIFUnloadOffsetAnticipationBySpeed into sh_mm_UnloadOffsetAnticipationBySpeed: we prefer to keep trace about mm than NIF.
//				 Added the fields sh_mm_OffsetMinTrajectory, sh_mm_OffsetMaxTrajectory, in order to keep trace about the minimum and maximum trajectories to unload parcel in a chute.
//				 Added the fields byParcelHitsFirstChuteWall, byParcelPercHitsLastChuteWall, that keep trace some safety information related to the parcel unload in a chute.
//	07-02-2018 - Moved the array of chute parameters [wCurrentChuteID; wComputeNIFUnloadOffsetAnticipationBySpeed; cUnloadOffsetToUse] in a STRUCT (by Matteo F.)
//	24-03-2017 - Added support to sort into Chute with same Cell activation point
//  15-03-2013 - New field for special preserved flags
//               New Chute I/O Exchange counter
//				 Primary Chute for new OAM strategy
//				 Bus breaker alarm
//	27-06-2008 - First release
//

#pragma once

///////////////////////////////////////////////////////////////////////////////////////////
//
//		  SORTER ROTATION WAY
//			------------->
//	   A2   A1     Cell #1    B2   B1   
//   ----------------------------------
//   |    |    |            |    |    |
//   |    |    |            |    |    |
//   |    |    |            |    |    |
//   |    |    |            |    |    |
//   ----------------------------------
//	 |	 GAP   |    BELT    |   GAP   |
//
///////////////////////////////////////////////////////////////////////////////////////////


#include "COMMON_Diag_Block.h"
#include "OAMRTXDefines.h"
#include "COMMON_ISPC_MSC_Data.h"

#define MAX_CELL_CURRENT_CHUTE	(1 + SIZEOF_ARRAY_ALTERNATIVE_DESTINATIONS)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CELL_BOOK_INFO
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CELL_BOOK_INFO
{
	//
	BOOL	bIsMasterCell;			// TRUE if is a master cell (first cell of the object)
	BOOL	bNextCellIsLinked;		// TRUE if this cell is linked to the next one
	BOOL	bPrevCellIsLinked;		// TRUE if this cell is linked with the previous one
	//
} STRUCT_CELL_BOOK_INFO;
#pragma pack()
#define SIZE_STRUCT_CELL_BOOK_INFO	sizeof(_STRUCT_CELL_BOOK_INFO)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CELL_HW_INFO
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CELL_HW_INFO
{
	//
	BYTE	byAntennaType;			// See below defines
	BYTE	byAntennaOffset;		// Nif position of the antenna
	BYTE	byGapNif;				// Gap NIF size
	BYTE	byBeltNif;				// Belt NIF size
	WORD	wBladeLength;			// Blade Length (mm)
	WORD	wCellParameterPreset;	// Default HW Cell preset (only for Digital Antenna)
	//
} STRUCT_CELL_HW_INFO;
#pragma pack()
#define SIZE_STRUCT_CELL_HW_INFO	sizeof(_STRUCT_CELL_HW_INFO)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CELL_OBJECT_POSITION
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CELL_OBJECT_POSITION
{
	//
	short	shHeadPosition;		// Head position (mm); -1 if no head
	short	shTailPosition;		// Tail position (mm); -1 if no tail
	//
	BYTE	byObjFlags;			// Flags from smartdevice
	short	shObjectCenter;		// Center offset respect cell center (mm) + = right side - = left side
	short	shObjectWidth;		// Width (mm)
	//
} STRUCT_CELL_OBJECT_POSITION;
#pragma pack()
#define SIZE_STRUCT_CELL_OBJECT_POSITION	sizeof(_STRUCT_CELL_OBJECT_POSITION)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_CELL_CHUTE_INFO
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CELL_CHUTE_INFO
{
	//
	WORD	wCurrentChuteID;								// Actual possible Chute ID at which the item is sent ...
	//
	CHAR	cUnloadOffsetToUse;								// Unload Offset correction to use
	//
	SHORT	sh_mm_UnloadOffsetAnticipationBySpeed;			// mm unload offset anticipation, referred to the Chute Position
	//
	SHORT	sh_mm_OffsetMinTrajectory;						// [mm] Minimum unload offset, referred to the start chute position (parcel passes close to the first wall of the chute)
	SHORT	sh_mm_OffsetMaxTrajectory;						// [mm] Maximum unload offset, referred to the start chute position (75% of the parcel length hits the last wall of the chute)
	//
	BYTE	byParcelHitsFirstChuteWall;						// Flag value: TRUE means that the parcel hits the first chute wall
	//																	   FALSE means that the parcel will unload correctly	
	BYTE	byParcelPercHitsLastChuteWall;					// Parcel portion that hits the Last chute wall. Value range is [0 .. 100]
	//
} STRUCT_CELL_CHUTE_INFO;
#pragma pack()
#define SIZE_STRUCT_CELL_CHUTE_INFO	sizeof(_STRUCT_CELL_CHUTE_INFO)

//////////////////////////////////////////////////////////////////////////
//					STRUCT_CELL_OBJECT_FROM_RECENTERING
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CELL_OBJECT_FROM_RECENTERING
{
	//
	BYTE	bySensorDetectionFlags;			// If TRUE the values stored in struct are updated, meaningful
	short	shLeftmostDetectedDistance;		// The leftmost distance detected from cell center	(mm) + = right side - = left side
	short	shRightmostDetectedDistance;	// The rightmost distance detected from cell center	(mm) + = right side - = left side
	//
} STRUCT_CELL_OBJECT_FROM_RECENTERING;
#pragma pack()
#define SIZE_STRUCT_CELL_OBJECT_FROM_RECENTERING	sizeof(_STRUCT_CELL_OBJECT_FROM_RECENTERING)

// Values of byDetectionFlags
#define REC_DETECTION_LEFT_SIDE_OK			0x0001
#define REC_DETECTION_RIGHT_SIDE_OK			0x0002

//////////////////////////////////////////////////////////////////////////
//								STRUCT_CELLS (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_CELLS
{
	//
	STRUCT_CELL_HW_INFO	csCellHWInfo;												// Structure with Cell Hardware Info
	//
	WORD	wAllocationState;														// Current cell allocation state
	BYTE	byDeviceActive;															// Device activation state
    BYTE	byUsedGaps;																// GAP usage, see defines below ...
	BYTE	bySorterLap;															// Sorter lap for this cell ...
	DWORD	dwPreservedFlags;														// Some flags that are not cleared by CleanCell macro ...
	//
	BYTE	byControlZone_Load_Lap;													// Sorter lap when loaded ...
	BYTE	byControlZone_Load_ID;													// Control Zone that has notify the load ...
	//
	STRUCT_CELL_OBJECT_POSITION csObjPosition;										// Object position info
	STRUCT_CELL_BOOK_INFO	csBookInfo;												// Cell booking informations
	STRUCT_ISPC_MSC_DATA	csObjInfo;												// Info of the on-cell object (From ISPC)
	STRUCT_DIAG_BLOCK		csDiag;													// Full Diagnostic
	//
	BYTE	byScannerID;															// Last used Scanner ID
	BYTE	byOAMDestinationFlag;													// Destination Flag received from OAM ...
	BYTE	byRampID;																// RampID to use (0 = default)
	//
	STRUCT_CELL_CHUTE_INFO	csChuteInfo[MAX_CELL_CURRENT_CHUTE];					// Chute information for all the alternative destinations
	//
	WORD	wPrimaryChuteID;														// Primary Chute, used for special sorting checks ...
	BYTE	byIndexAlterantiveDestinationID;										// Index of the alternative destinations array (internal use!)
	BYTE	byTotalAlterantiveDestinationID;										// Total alternative destinations array (From OAM!)
	WORD	wAlterantiveDestinationID[SIZEOF_ARRAY_ALTERNATIVE_DESTINATIONS];		// Alternative destinations list
	BYTE	byReassignCounter;														// Counter of performed reassignments requests
	BYTE	byUnloadTried;															// Counter of unload try (used to unload rate limit check)
	//
	SHORT	shNifToChute;															// NIF counter for inductor actions
	SHORT	shNifToFlap;															// NIF counter for flap command
	SHORT	shNifToIOExchange;														// NIF counter for I/O exchange
	// MSC-147 New Feature: New Cell's Event NIF counter
	SHORT	shNifToEvent;															// NIF counter for Cell Event
	DWORD	dwEventMask;															// Event Identifier (bit used, see below define)
	//
	short	shStartDischargeOffset;													// Inductor case:		NIF offset to start unload action
	//																				// Modulator/Coil case: mm offset to start unload action
	WORD	wCoilToUseDBIndex_Chute;												// DB Index of the coil to use for current sort cell action
	WORD	wCoilToUseDBIndex_Service;												// DB Index of the coil to use for current service (join/testcell/recentering) cell action
	//
	WORD	wOutcomeReturn;															// Current outcome return code
	BYTE	byOutcomeAnomaly;														// Current outcome anomaly code
	BYTE	byOAMRejectAnomaly;														// Current reject anomaly from OAM
	//
	BYTE	byUnloadKOCounter;														// Qty of bad unload actions
	BYTE	byScannerRequestSent;													// Qty of Scanner MSG sent to OAM
	//
	// MSC-21: New Feature: Detect Cell rotation jam (always run) counting consecutive lost
	BYTE	byConsecutiveLost;														// Qty of consecutive lost on cell (maybe always run!)
	//
	WORD	wRecenteringFlag;														// See below defines
	WORD	wRecenteringSensor;														// See below defines
	WORD	wRecenteringNIFAction;													// Number of NIF for Recentering action
	WORD	wRecenteringSpaceCorrection;											// How many mm to correct the Parcel position carried by the Cell (used by the GENI-BELT sorter)
	STRUCT_CELL_OBJECT_FROM_RECENTERING csObjFromRecentering;						// Info of the on-cell object (From Recentering Station)
	//
	CHAR	cDestUnloadOffset;														// Primary Destination unload offset from OAM (NIF)
	CHAR	cAlternativeDestUnloadOffset[SIZEOF_ARRAY_ALTERNATIVE_DESTINATIONS];	// Alternative Destination unload offset from OAM (NIF)
	//
	BYTE	byCellTestFlags;														// Used by bits, see defines below
	BYTE	byCellTestResultFlags;													// Used by bits, see defines below
	//
	WORD	wControlZoneSensors;													// Used by bits, see define below
	DWORD	dwSensorsDetectionNif;													// Used by bits, 32 bits = 32 Nif from 0..31; Bit at 1 = sensor ON
	//
  	WORD	wRecircReason;															// Used from DacManager for determinate wich is the recirculating reason
} STRUCT_CELLS;
#pragma pack()

#define SIZE_STRUCT_CELLS	sizeof(_STRUCT_CELLS)

//////////////////////////////////////////////////////////////////////////
//								STRUCT_UNLOAD_CELL
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_UNLOAD_CELL
{
	//
	WORD	wCell;						// Unloaded cell number
	WORD	wChuteID;					// Target chute ID
	WORD	wDBIndex;					// Index of the chute inside DB ...
	BYTE	byAnomaly;					// If we have an anomaly setled ...
	STRUCT_ISPC_MSC_DATA	csObjInfo;	// Info of the on-cell object
	//
} STRUCT_UNLOAD_CELL;
#pragma pack()

#define SIZE_STRUCT_UNLOAD_CELL	sizeof(_STRUCT_UNLOAD_CELL)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Values of byAntennaType
#define CELLHWINFO_ANTENNA_TYPE_ANALOG		1		// Analog (Inductors)
#define CELLHWINFO_ANTENNA_TYPE_DIGITAL		2		// Digital (Coil)

// Values of wAllocationState
#define CELL_ALLOC_EMPTY					0x0000
#define CELL_ALLOC_LOADED					0x0001
#define CELL_ALLOC_CHK_UNLOAD				0x0002
#define CELL_ALLOC_REASSIGN					0x0004
#define CELL_ALLOC_NOTIFY					0x0008
#define CELL_ALLOC_MULTI_CELL				0x0010
#define CELL_ALLOC_OBJECT_FOUND				0x0020
#define CELL_ALLOC_INTERNAL_MOVE_FAULT		0x0040
#define CELL_ALLOC_EXTERNAL_MOVE_FAULT		0x0080
#define CELL_ALLOC_REAL_SPEED_ZERO_FAULT	0x0100	// RFU
#define CELL_ALLOC_UNLOAD_BOOKED			0x0200

// Values (bits!) of byDeviceActive
#define CELL_DEVICE_NOT_ACTIVE			0x00	// No device ...
#define CELL_DEVICE_ACTIVE_SCANNER		0x01	// Active Scanner
#define CELL_DEVICE_ACTIVE_VOLUME		0x02	// Active Volume barrier
#define CELL_DEVICE_ACTIVE_FAKE_SCANNER	0x04	// Send data as a scanner but without trigger it

// Values of wRecenteringFlag
#define CELL_REC_FLG_NONE				0x0000	// Parcel position ok, no action required
#define CELL_REC_FLG_RECENTER			0x0001	// Recentering required
#define CELL_REC_FLG_DECENTER			0x0002	// Decentering required
#define CELL_REC_FLG_DECENTER_CHECK		0x0004	// Decentering check required
#define CELL_REC_FLG_MOVE_RIGHT		    0x0100	// Right movement
#define CELL_REC_FLG_MOVE_LEFT			0x0200	// Left movement
#define CELL_REC_FLG_ACTION_DONE		0x0400	// Recentering/Decentering/Displacement action sent to Induction manager

// Values of wRecenteringSensor
#define CELL_REC_SENSOR_NONE			0x0000	// No sensor detected
#define CELL_REC_SENSOR_1				0x0001	// Sensor 1 detected
#define CELL_REC_SENSOR_2				0x0002	// Sensor 2 detected
#define CELL_REC_SENSOR_3				0x0004	// Sensor 3 detected
#define CELL_REC_SENSOR_4				0x0008	// Sensor 4 detected
#define CELL_REC_SENSOR_5				0x0010	// Sensor 5 detected
#define CELL_REC_SENSOR_6				0x0020	// Sensor 6 detected
#define CELL_REC_SENSOR_7				0x0040	// Sensor 7 detected
#define CELL_REC_SENSOR_8				0x0080	// Sensor 8 detected

// Values of csObjInfo.byUsedGaps and root byUsedGaps
#define CELL_LOAD_FLG_NONE				0x00	// No object on gap, only belt !
#define CELL_LOAD_FLG_GAPB1				0x01	// Object loaded on B1 gap fraction
#define CELL_LOAD_FLG_GAPB2				0x02	// Object loaded on B2 gap fraction
#define CELL_LOAD_FLG_GAPA1				0x04	// Object loaded on A1 gap fraction
#define CELL_LOAD_FLG_GAPA2				0x08	// Object loaded on A2 gap fraction

// Values (bits!) of byCellTestFlags
#define CELL_TEST_ACTIVE				0x01	// Cell test activated
#define CELL_TEST_INTERNAL_MOVE			0x02	// Cell test active for internal move
#define CELL_TEST_EXTERNAL_MOVE			0x04	// Cell test active for external move
#define CELL_TEST_WORKING				0x08	// Cell test is working (waiting antenna result)
#define CELL_TEST_DONE					0x10	// Cell test done !
#define CELL_TEST_USED_CELL				0x20	// Cell was just used (verified sort), no test needed
#define CELL_TEST_REAL_SPEED_ZERO		0x40	// Cell test active for real speed ZERO check
//#define ______________________		0x80

// Values (bits!) of byCellTestResultFlags
#define CELL_TEST_INTERNAL_MOVE_OK		0x01    // Cell test internal move OK
#define CELL_TEST_EXTERNAL_MOVE_OK		0x02	// Cell test external move OK

// Values (bits!) of wControlZoneSensors
#define CELL_CTRLZONE_CHECKED			0x8000	// Cell was check by a sensor
#define	CELL_CTRLZONE_SENSOR_1			0x0001	// Object detected by Sensor # 1
#define	CELL_CTRLZONE_SENSOR_2			0x0002	// Object detected by Sensor # 2
#define	CELL_CTRLZONE_SENSOR_3			0x0004	// Object detected by Sensor # 3
#define	CELL_CTRLZONE_SENSOR_4			0x0008	// Object detected by Sensor # 4

// Values (bits!) of dwPreservedFlags
#define CELL_PRESERVED_FLAG_INVENTORY_DONE			0x00000001	// Cell checked for inventory (processed by Control Zone)
#define CELL_PRESERVED_FLAG_OBJECT_ON_GAP			0x00000002	// Cell was marked as "On Gap" (fracelle !)
#define CELL_PRESERVED_FLAG_LOADABLE_CELL_E1		0x00000004	// Cell was marked as "Loadable" to ISPC group by "Empty 1"
#define CELL_PRESERVED_FLAG_LOADABLE_CELL_E2		0x00000008	// Cell was marked as "Loadable" to ISPC group by "Empty 2"

// Values (bits!) of dwEventMask
#define CELL_EVENTMASK_UNKNOWN						0x00000000	// No event!
#define CELL_EVENTMASK_CUSTOM_EVENT					0x00000001	// Cell need a custom event
#define CELL_EVENTMASK_REBOOT_DA					0x00000002	// Cell need a reboot to Digital Antenna

// Alarms bit define
#define ALARM_CELL_GENERAL_FAULT			0	// General fault (right now not used)
#define ALARM_CELL_ON_GAP_FAULT				1	// Object between cell (forever !)
#define ALARM_CELL_EXTERNAL_MOVE_FAULT		2	// From test cell
#define ALARM_CELL_INTERNAL_MOVE_FAULT		3	// From test cell
#define ALARM_CELL_CONSECUTIVE_UNLOAD_KO	4	// Number of consecutive ko unload reached
#define ALARM_CELL_ITEM_LOST				5	// Cell found with no item on (Special alarm NOT standard)
#define ALARM_CELL_NO_POWER					6	// Cell with no power (BUS Breaker)
#define ALARM_CELL_DIGITAL_ANTENNA_FAULT	7	// Cumulative Fault retrieved by the Digital Antenna Diagnostic
#define ALARM_CELL_ALWAYS_RUN				8	// Cell can be broken and always run

// Warnings bit define
#define WARNING_CELL_ON_GAP							0	// Object between cell
#define WARNING_CELL_MISSING_BLADE					1	// Possible missing blade
#define WARNING_CELL_DIGITAL_ANTENNA_NOT_READY		2	// Digital Antenna is not ready_to_work
// spare ...
#define WARNING_CELL_DIGITAL_ANTENNA				7	// Cumulative Warning retrieved by the Digital Antenna Diagnostic

// Recirc reason. New for dac. a.c.
#define CELL_RECIRC_FULL			0x0001
#define CELL_RECIRC_JAM				0x0002
#define CELL_RECIRC_CHUTE_KO		0x0004
#define CELL_RECIRC_CELL_KO			0x0008
#define CELL_RECIRC_CELL_DISABLED	0x0010
