//
//	File:		MSC_Maintenance_PanelPC.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//				RTX v8.1.2 Build 7818
//
//	Version:	1.0
//
//	Created:	28/September/2010
//	Updated:	28/September/2010
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	28-09-2010 - First Release
//

#pragma once

#define MAX_PANELPC_CELL_DATA		200

//////////////////////////////////////////////////////////////////////////
//						STRUCT_MPANELPC_TO_SORTER
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_MPANELPC_TO_SORTER
{
	//
	BYTE	byCommand;			// Command, see defines below ...
	BYTE	byInFlags;			// See defines below ...
	WORD	wCellToStop;		// Number of cell to stop
	//
} STRUCT_MPANELPC_TO_SORTER;
#pragma pack()
#define SIZE_STRUCT_MPANELPC_TO_SORTER sizeof(_STRUCT_MPANELPC_TO_SORTER)

//////////////////////////////////////////////////////////////////////////
//						STRUCT_SORTER_TO_MPANELPC
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_SORTER_TO_MPANELPC
{
	//
	BYTE	bySorterStatus;							// Sorter status ...
	BYTE	byEnablePanelAndSorterRun;				// See defines below ...
	BYTE	byCommandAnswer;						// Echo of received command as ACK, 0xFF as NACK !
	BYTE	bySorterSync;							// 1 = Sorter syncronized
	//
	BYTE	byByteNotUsed4;							// SPARE
	BYTE	byByteNotUsed5;							// SPARE
	//
	BYTE	byOutFlags;								// See defines below ...
	//
	BYTE	byByteNotUsed7;							// SPARE
	//
	BYTE	byFreeBytes[8];							// SPARE
	//
	WORD	wTotalCells;							// Total plants data ...
	WORD	wFirstCellId;							// Cell number of the first byCellState element
	//
	WORD	wQtyCellState;							// Number of elements copied into byCellState
	BYTE	byCellState[MAX_PANELPC_CELL_DATA];

	WORD	wEchoFirstCellID;						// Echo of wFirstCellId (used on Panel PC to read data !)
	//
} STRUCT_SORTER_TO_MPANELPC;
#pragma pack()

#define SIZE_STRUCT_SORTER_TO_MPANELPC sizeof(_STRUCT_SORTER_TO_MPANELPC)

// byInFlags values ...
#define MPANELPC_IN_FLAG_KEEP_ALIVE			0x01		// Heart beat !
#define MPANELPC_IN_FLAG_ACTION_READY		0x02		// Operator select a cell, sorter can start !

// byOutFlags values ...
#define MPANELPC_OUT_FLAG_KEEP_ALIVE		0x01		// Heart beat !

// I/O defines ...
#define DI_MAINTENANCE_ENABLE_PANELPC			"ENABLEMPC_"				// Maintenance enable Maintenance Panel PC (make it virtual to start in virtual mode)
#define DEI_MAINTENANCE_PANELPC					"MPANELPC_DEI_"				// Maintenance Panel PC Data input
#define DEO_MAINTENANCE_PANELPC					"MPANELPC_DEO_"				// Maintenance Panel PC Data output
#define VAI_MAINTENANCE_PANELPC_CELL			"MPANELPC_CELL_"			// Maintenance Panel PC Cell to call (Virtual Mode Only !)
#define VI_MAINTENANCE_PANELPC_RDATA			"MPANELPC_READ_"			// Maintenance Panel PC Trigger to read Cell to call (Virtual Mode Only !)
#define VAI_MAINTENANCE_PANELPC_CHANGE_SPEED	"MPANELPC_CHSP_NIFPOS_"		// Maintenance Panel PC Change Speed NIF Position (optional)

// byCommand defines ...
#define MPANELPC_CMD_SORTER_START		0x01
#define MPANELPC_CMD_SORTER_STOP		0xFE
#define MPANELPC_CMD_CLEARED			0xFF
#define MPANELPC_CMD_REFUSED			0xFF

// byEnablePanelAndSorterRun defines ...
#define MPANELPC_NOT_ENABLED						0		// Panel PC not enabled (from HW key !)
#define MPANELPC_ENABLED_AND_SORTER_STOP			1		// Key on enabled and sorter stopped
#define MPANELPC_ENABLED_AND_SORTER_RUN				2		// Key on enabled and sorter running

//
#define MPANELPC_KEEP_ALIVE_TIMEOUT				5000		// Timeout 5 sec. (SYSTEM_MANAGER_TICKs based)
