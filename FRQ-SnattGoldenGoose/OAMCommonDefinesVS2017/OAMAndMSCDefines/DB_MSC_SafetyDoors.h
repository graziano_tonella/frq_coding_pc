//
//	File:		DB_MSC_SafetyDoors.h
//
//	Compiler:	Visual C++ 2010
//
//	Tested on:	RTX v11.0.0 Build 10540 
//
//	Version:	1.0
//
//	Created:	24/September/2013
//	Updated:	
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	24-09-2013 - First release
//

#pragma once

//////////////////////////////////////////////////////////////////////////
//							STRUCT_SAFETY_DOORS (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_SAFETY_DOORS
{
	//
	WORD		wArea;						// Safety Door Area #
	WORD		wDoor;						// Safety Door #
	//
	BYTE		byOpenDoorRequestType;			// Open Door Request Type
	//
	char szAccessLamp[SIZE_SIGNAL_ID+1];	// Access Lamp (DO)
	char szOpenRequest[SIZE_SIGNAL_ID+1];	// Open Request/Safety Breaker (DI)
	char szOpenCommand[SIZE_SIGNAL_ID+1];	// Open Command (DO)
	char szDoorOpen[SIZE_SIGNAL_ID+1];		// Door Open (DI)
	char szPreReset[SIZE_SIGNAL_ID+1];		// Safety Pre Reset (DI)
	char szSafetyReset[SIZE_SIGNAL_ID+1];		// Safety Reset Command (DO)
	char szSafetyBlock[SIZE_SIGNAL_ID+1];		// Safety Block (DI)
	//
	WORD		wAlarmCabinetIndex;			// Alarm Cabinet Index (-1 not used)
	//
}	STRUCT_SAFETY_DOORS;
#pragma pack()

#define SIZE_STRUCT_SAFETY_DOORS		sizeof(_STRUCT_SAFETY_DOORS)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// Possible values of byOpenDoorRequestType
#define OPEN_DOOR_REQUEST_TYPE_PUSHBUTTON					1
#define OPEN_DOOR_REQUEST_TYPE_SAFETYBREAKER				2

// Alarms defines

// Each door use those bits :
#define SAFETYDOORS_ALARM_DOOR_OPEN							0
#define SAFETYDOORS_ALARM_SAFETY_BREAKER					1
#define SAFETYDOORS_ALARM_SAFETY_BLOCK						2
#define SAFETYDOORS_ALARM_PRE_RESET_REQ						3

// It means that in our standard Diag Block we can manage (64 / 4) 16 doors
#define MAX_SAFETY_DOORS	16

// Default door open timeout (ms)
#define SAFETYDOOR_OPEN_TIMEOUT		10000 // 10 second
