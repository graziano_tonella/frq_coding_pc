//
//	File:		DB_MSC_EnergyBox.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v8.1.2 Build 7818
//
//	Version:	1.0
//
//	Created:	17/August/2010
//	Updated:	01/October/2012
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//  01-10-2012 - Created the Energy Box without Ultracap define
//	29-03-2011 - Added defines for Pickup & Sliding Contact
//	17-08-2010 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//							STRUCT_ENERGYBOX (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_ENERGYBOX
{
	//
	BYTE	byType;						// Type of Energy box (see define below)
	//
	WORD	wInstalledCell;				// Number of cell with this energy box
	//
	DWORD	dwTrackFaultTestID;			// Energy Box Test ID fault tracking (1 bit for each Test)
	//
	STRUCT_DIAG_BLOCK	csDiag;			// Full Diagnostic
	//
}	STRUCT_ENERGYBOX;
#pragma pack()

#define SIZE_STRUCT_ENERGYBOX		sizeof(_STRUCT_ENERGYBOX)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// byType defines ...
#define ENERGYBOX_TYPE_CABINET			1			// Common energy box installed on cabinet
#define ENERGYBOX_TYPE_SORTER			2			// Common energy box installed on sorter
#define ENERGYBOX_TYPE_WHEEL			3			// Energy box on wheel !
#define ENERGYBOX_TYPE_PICKUP			4			// Pickup (Inducted energy)
#define ENERGYBOX_TYPE_SLIDING_CONTACT	5			// Sliding Contact
#define ENERGYBOX_TYPE_NO_ULTRAKAP		6			// Special energy box installed on sorter but without Ultrakap

// Alarm's bit define
#define	ENERGYBOX_FAULT_HW				0			// General HW fault (only for energy box in cabinet or from a DI)
#define	ENERGYBOX_FAULT_ULTRAKAP		1			// Ultrakap fault
#define	ENERGYBOX_FAULT_BY_TEST			2			// Fault by generator test

// I/O Name Define
#define DI_ENERGYBOX_FAULT		"EBOX_FAULT_"		// General HW fault of energy box; only in cabinet configuration !
