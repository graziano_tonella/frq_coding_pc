//
//	File:		MSC_Dedicated_Defines.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	11/June/2008
//	Updated:	29/November/2008
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//  29-11-2008 - First TNT Mega official release
//	11-06-2008 - First beta release
//

#pragma once

// Current project version as "https://semver.org/"
#define	PROJECT_MAJOR_VERSION		1
#define	PROJECT_MINOR_VERSION		1
#define	PROJECT_PATCH_VERSION		0
// Build/refernce date
#define PROJECT_BUILD_DATE			"2018-06-29"
// Build/refernce date
#define PROJECT_CUSTOM_NAME			"(FOR) PT Estonian v1.0.0"

// Common field separator
#define	SZ_SEPS		",;\t"

// Default paths ...
#define	DB_CSV_PATH			_T("C:\\PRODUCTION\\MSC\\DB_Csv\\")
#define	DB_BIN_PATH			_T("D:\\PRODUCTION\\Sorter_DataBase\\")
#define	DB_STATS_PATH		_T("D:\\PRODUCTION\\Sorter_DataBase\\")
#define STARTUP_PATH		_T("C:\\PRODUCTION\\MSC\\DB_Csv\\")

// Name of the safe database dump on disk
#define	DB_DUMP0_NAME			_T("RuntimeDB0.dat")
// Name of the database dump on disk
#define	DB_DUMP_NAME			_T("RuntimeDB.dat")

// Name of the safe database dump on disk
#define	DB_STATS_DUMP0_NAME		_T("StatsDB0.dat")
// Name of the database dump on disk
#define	DB_STATS_DUMP_NAME		_T("StatsDB.dat")

// DB IO Name
#define DB_IO_NAME				_T("MSC_DB_IO")
// DB MSC Name
#define DB_MSC_NAME				_T("MSC_DB")
// DB MSC STATS Name
#define DB_MSC_STATS_NAME		_T("MSC_STATS_DB")
// DB MSC STATS Name
#define DB_MSC_RAM_NAME			_T("MSC_RAM_DB")

// Internal time ticks ...
#define SYSTEM_MANAGER_TICK			100		// Time (ms) to wakeup System Manager Thread (ms)
#define PID_MANAGER_TICK			20		// Time (ms) to wakeup PID Manager Thread (ms)
#define CHUTE_MANAGER_TICK			10		// Time (ms) to wakeup Chute Manager Thread for chute tracking (ms)
#define DAC_MANAGER_TICK			100		// Time (ms) to wakeup DAC manager to update statistic
#define MAIN_TIMER_TICK				10		// Time (ms) of the main timer
