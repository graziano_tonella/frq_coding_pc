//
//	File:		DB_MSC_BladeCheck.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	03/March/2016/2008
//	Updated:	
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	03-03-16 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

//////////////////////////////////////////////////////////////////////////
//						STRUCT_BLADE_CHECK (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_BLADE_CHECK
{
	//
	WORD wNifPosition;				// Control position
	//
	short shCellWithMissingBlade;	// Last detected cell with missing blade (-1 = none)
	//
	STRUCT_DIAG_BLOCK	csDiag;		// Full Diagnostic
	//
} STRUCT_BLADE_CHECK;
#pragma pack()

#define SIZE_STRUCT_BLADE_CHECK	sizeof(_STRUCT_BLADE_CHECK)

//////////////////////////////////////////////////////////////////////////
// All possible DEFINES
//////////////////////////////////////////////////////////////////////////

#define DI_NAME_RESET_MISSING_BLADE			"MISSINGBLADE_RESET"	// Special reset to reset missing blade alarms
#define DI_NAME_BYPASS_MISSING_BLADE		"MISSINGBLADE_BYPASS"	// Input to bypass missing blade checks
#define DI_NAME_MISSINGBLADE_PH				"MISSINGBLADE_PH_"
#define DO_NAME_ALED_MISSING_BLADE			"MISSINGBLADE_ALED"		// Light to show state of alarm
#define DO_NAME_BYPLED_MISSING_BLADE		"MISSINGBLADE_BYPLED"	// Light to show state of alarm
//
#define	MAX_MISSINGBLADE_GROUP				100						// Max number of missing blade ph's group
#define	MAX_MISSINGBLADE_PH					2						// Max number of ph per group

// Alarms bit define
#define BLADE_CHECK_ALARM_MISSING_BLADE			0						// Possible blade problem detected

// Warning bit define
#define BLADE_CHECK_WARNING_DISABLED_BY_SCADA	0						// Control disabled by SCADA
