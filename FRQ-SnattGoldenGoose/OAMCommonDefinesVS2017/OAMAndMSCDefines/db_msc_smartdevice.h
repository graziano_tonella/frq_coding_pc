//
//	File:		DB_MSC_SmartDevice.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v7.0 Build 4925
//				RTX v7.1.0 Build 5306
//
//	Version:	1.0
//
//	Created:	22/June/2010
//	Updated:	21/April/2011
//
//	Author:		Carmine Filella [+39-0331-665345 carmine.filella@fivesgroup.com]
//	Updated:	Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	21-04-2010 - Added an "_" into I/O defines to better read ...
//	22-06-2010 - First release
//

#pragma once

// structures for ramp (movement)
#pragma pack(1)
struct STRUCT_SMART_DEVICE_MLOOKUPTABLEELEMCONFIG
{
	short				shMoveCmdTime;
	BYTE				bySpaceCovered;
	BYTE				byNotUsed;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_MLOOKUPTABLEELEMCONFIG sizeof(STRUCT_SMART_DEVICE_MLOOKUPTABLEELEMCONFIG)

// structures for ramp (movement and brake)
#pragma pack(1)
struct STRUCT_SMART_DEVICE_MBLOOKUPTABLEELEMCONFIG
{
	short				shMoveCmdTime;
	short				shPauseTime;
	short				shBrakeCmdTime;
	BYTE				bySpaceCovered;
	BYTE				byNotUsed;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_MLOOKUPTABLEELEMCONFIG sizeof(STRUCT_SMART_DEVICE_MLOOKUPTABLEELEMCONFIG)

// Smart device parameters class
// parameter class 1: general parameters
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_1
{
	short				shApplicationFlags;
	BYTE				byThresholdParcelUnbound;
	BYTE				byNotUsed;
	unsigned short		ushCorrectionMovement;
	BYTE				byRecenteringWndStartOffset;
	BYTE				byRecenteringWndEndOffset;
	unsigned short		ushRecenteringDeadzone;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_1 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_1)

// parameter class 2: sorter parameters
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_2
{
	unsigned short		ushSorterSpeed;
	short				shMaxCells;
	BYTE				byCellLength;
	BYTE				byCellWidth;
	BYTE				byBladeLength;
	BYTE				byBladeOffset;
	BYTE				byCarrierSize;
	BYTE				byAntennaLength;
	BYTE				byAntennaOffset;
	BYTE				bySmallGapLength;
	BYTE				byBigGapLength;
	BYTE				byBeltLength;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_2 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_2)

// parameter class 3: synchronism photoeye parameters
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_3
{
	unsigned short		ushTolleranceRange;
	unsigned short		ushSynchDoneThreshold;
	unsigned short		ushSynchNotPossibleThreshold;
	unsigned short		ushSynchReadyThreshold;
	unsigned short		ushNotUsed;
	unsigned short		ushSynchPhtPosition;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_3 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_3)

// parameter class 4: inductors and antennas parameters
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_4
{
	unsigned short		ushDistanceMoveAndBrakeInductor;
	short				shMoveInductorOffset;
	short				shCorrectionInductorOffset;
	BYTE				byInductorLengthForMovement;
	BYTE				byInductorLengthForBraking;
	BYTE				byInductorPitchForMovement;
	BYTE				byInductorPitchForBraking;
	BYTE				byNumMoveInductors;
	BYTE				byNumBrakeInductors;
	BYTE				byMinAntennaInductorOverlap;
	BYTE				bySwitchOnInductorsOverlap;
	BYTE				byNumCorrectionInductors;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_4 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_4)

// parameter class 5: shape barrier parameters 1
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_5
{
	WORD				wAnalogPhtOffset[8];
	BYTE				byNumAnalogPhts;
	BYTE				byMaxParcelWidth;
	BYTE				byUnboundPhtsOffset;
	BYTE				byThresholdAnalogPhtSeen;
	short				shBetweenCellsPhtOffset;
	unsigned short		ushThresholdPhtBlocked;
	BYTE				byObjectMinLength;
	BYTE				byMaxAnalogBrokenPhts;
	BYTE				byMaxDigitalBrokenPhts;
	BYTE				byMaxBrokenPhts;
	BYTE				byRangeLeftAnalogPhts;
	BYTE				byRangeRightAnalogPhts;
	short				shMinDistanceForLeftAnalogPhts;
	short				shMinDistanceForRightAnalogPhts;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_5 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_5)

// parameter class 6: shape barrier parameters 2
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_6
{
	WORD				wDigitalPhtsOffset[8];
	BYTE				byDigitalPhtsThreshold[8];
	BYTE				byNumDigitalLateralPhts;
	BYTE				byNumDigitalTopPhts;
	BYTE				byDigitalPhtsPresent;
	BYTE				byDigitalPhtsConfig;
	BYTE				byBetweenCellsConfig;
	BYTE				byDigitalPhtsFilter;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_6 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_6)

// parameter class 7: ramp parameters 1
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_7
{
	BYTE				byNumSampleLTForMove;
	BYTE				byNumSampleLTForMoveBrake;
	BYTE				byLookupSTOrder;
	BYTE				byNotUsed;
	STRUCT_SMART_DEVICE_MLOOKUPTABLEELEMCONFIG arrstLTForMove[4];
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_7 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_7)

// parameter class 8: ramp parameters 2
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_8
{
	STRUCT_SMART_DEVICE_MLOOKUPTABLEELEMCONFIG arrstLTForMove[4];
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_8 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_8)

// parameter class 9: ramp parameters 3
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_9
{
	STRUCT_SMART_DEVICE_MBLOOKUPTABLEELEMCONFIG arrstLTForMoveBrake[3];
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_9 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_9)

// parameter class 10: ramp parameters 4
#pragma pack(1)
struct STRUCT_SMART_DEVICE_PARAM_CLASS_10
{
	STRUCT_SMART_DEVICE_MBLOOKUPTABLEELEMCONFIG arrstLTForMoveBrake[3];
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE_PARAM_CLASS_10 sizeof(STRUCT_SMART_DEVICE_PARAM_CLASS_10)


//////////////////////////////////////////////////////////////////////////
//						STRUCT_SMART_DEVICE (DB)
//////////////////////////////////////////////////////////////////////////

#pragma pack(1)
struct STRUCT_SMART_DEVICE
{
	BOOL	bReset;											// Used to update parameters
	BYTE	byType;											// device type
	BYTE	byRecenteringIdx;								// recentering identifier (the index inside checkpositions database)
	BYTE	byDecenteringIdx;								// decentering identifier (the index inside checkpositions database)
	BYTE	byCameraSynchroIdx;								// camera synchro identifier (the index inside scanners database)
	short	shDeltaAxis;									// delta axis
	short	shDecenteringSpace;								// decentering space
	BYTE	byCameraSynchroActivationOffset;				// offset for activation signal of camera synchro (in cm, 0 < x < csSorterParams.byCellLength)
	int		nCameraSynchroActivationDelay;					// delay of camera synchro activation signal from synchronism photoeye (ms)	
	BYTE	byCameraSynchroDeltaActivation;					// delta activation: advance and postponed respect to length of parcel (cm)
	STRUCT_SMART_DEVICE_PARAM_CLASS_1	csGeneralParams;
	STRUCT_SMART_DEVICE_PARAM_CLASS_2	csSorterParams;
	STRUCT_SMART_DEVICE_PARAM_CLASS_3	csSynchPhtParams;
	STRUCT_SMART_DEVICE_PARAM_CLASS_4	csInductorAntennaParams;
	STRUCT_SMART_DEVICE_PARAM_CLASS_5	csShapeBarrierParams1;
	STRUCT_SMART_DEVICE_PARAM_CLASS_6	csShapeBarrierParams2;
	STRUCT_SMART_DEVICE_PARAM_CLASS_7	csRampParams1;
	STRUCT_SMART_DEVICE_PARAM_CLASS_8	csRampParams2;
	STRUCT_SMART_DEVICE_PARAM_CLASS_9	csRampParams3;
	STRUCT_SMART_DEVICE_PARAM_CLASS_10	csRampParams4;
};
#pragma pack()

#define SIZE_STRUCT_SMART_DEVICE			sizeof(STRUCT_SMART_DEVICE)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

// I/O defines ...
#define DI_DVC_INITIALIZATION_REQUEST			"DVC_INITREQ_"		// request to initialize external peripheral
#define DI_DVC_INITIALIZATION_ACK				"DVC_INITACK_"		// acknoledge for external peripheral initialization
#define DI_DVC_INITIALIZATION_ERROR				"DVC_INITERR_"		// error during external peripheral initialization
#define DI_DVC_TX_COUNT_BACK					"DVC_TXCOUNTB_"		// transmission couter back during initialization
#define DI_DVC_LIFE_BIT							"DVC_LIFEBIT_"		// life bit of external device
#define DI_DVC_STATUS							"DVC_STATUS_"		// Status word
#define DI_DVC_ALARMS1							"DVC_ALARMS1_"		// Alarms word 1
#define DI_DVC_ALARMS2							"DVC_ALARMS2_"		// Alarms word 2
#define DI_DVC_WARNINGS1						"DVC_WARNINGS1_"	// Warnings word 1
#define DI_DVC_WARNINGS2						"DVC_WARNINGS2_"	// Warnings word 2
// decentering
#define DI_DVC_DECENTERING_DONE					"DVC_DDONE_"		// decentering done
#define DI_DVC_DECENTERING_ABORT				"DVC_DABORT_"		// decentering abort
// camera synchronization
#define DI_DVC_CAMERASYNCHRO_DONE				"DVC_CDONE_"		// camera synchronization done
#define DI_DVC_CAMERASYNCHRO_SIGNAL				"DVC_CSIGNAL_"		// camera synchronization signal
// recentering
#define DI_DVC_RECENTERING_DONE					"DVC_RDONE_"		// recentering done
//
#define DO_DVC_INITIALIZATION_REQUEST			"DVC_INITREQ_"		// request to initialize external peripheral
#define DO_DVC_INITIALIZATION_ACK				"DVC_INITACK_"		// acknoledge for external peripheral initialization
#define DO_DVC_INITIALIZATION_DONE				"DVC_INITDN_"		// external peripheral initialization done
#define DO_DVC_SORTER_SYNCH						"DVC_SYNC_"			// sorter synchronized
#define DO_DVC_LIFE_BIT							"DVC_LIFEBIT_"		// life bit
#define DO_DVC_TX_COUNT							"DVC_TXCOUNT_"		// transmission couter during initialization
// decentering
#define DO_DVC_DECENTERING_ACTIVATION			"DVC_DACTIVE_"		// decentering activation
#define DO_DVC_DECENTERING_RESET_CMD			"DVC_DRSTCMD_"		// decentering reset command
#define DO_DVC_DECENTERING_ENABLE				"DVC_DENABLE_"		// decentering enable
// camera synchro
#define DO_DVC_CAMERASYNCHRO_ACTIVATION			"DVC_CACTIVE_"		// camera synchro activation
#define DO_DVC_CAMERASYNCHRO_RESET_CMD			"DVC_CRSTCMD_"		// camera synchro reset command
#define DO_DVC_CAMERASYNCHRO_ENABLE				"DVC_CENABLE_"		// camera synchro enable
// recentering
#define DO_DVC_RECENTERING_ACTIVATION			"DVC_RACTIVE_"		// recentering activation
#define DO_DVC_RECENTERING_RESET_CMD			"DVC_RRSTCMD_"		// recentering reset command
#define DO_DVC_RECENTERING_ENABLE				"DVC_RENABLE_"		// recentering enable

// Alarms bits ...

// Useful structure
/// Longitudinal (to sorter direction) object position structure.
/**
	Longitudinal (to sorter direction) object position structure.
*/
struct STRUCT_LONGITUDINAL_OBJECT_POSITION
{
	/// Check position DB record number ...
	WORD			wDBIndexChkPos;

	/// Object flags.
	/**
		Object flags (.byObjFlags defines):
			- bit 0: parcel found;
			- bit 1: cell empty.
	*/
	BYTE			byObjFlags;

	/// Object begin cell.
	/**
		Object begin cell.
	*/
	short			shBeginCell;

	/// Object end cell.
	/**
		Object end cell.
	*/
	short			shEndCell;

	/// Object begin offset (mm).
	/**
		Object begin offset (mm).
	*/
	short			shBeginOffset;

	/// Object end offset (mm).
	/**
		Object end offset (mm).
	*/
	short			shEndOffset;
};

/// Trasverse (to sorter direction) object position structure.
/**
	Trasverse (to sorter direction) object position structure.
*/
struct STRUCT_TRASVERSE_OBJECT_POSITION
{
	/// Check position DB record number ...
	WORD			wDBIndexChkPos;

	/// Object flags.
	/**
		Object flags (.byObjFlags defines):
			- bit 0: parcel decentered;
			- bit 1: estimated position;
			- bit 2: dimension invalid;
			- bit 3: parcel not found.
	*/
	BYTE			byObjFlags;

	/// Cell.
	/**
		Cell where the object stays.
	*/
	short			shCell;

	/// Object center (in mm, <0 on the left of cell center, >0 on the right of cell center).
	/**
		Object center (in mm, <0 on the left of cell center, >0 on the right of cell center).
	*/
	short			shObjCenter;

	/// Object width (mm).
	/**
		Object width (mm).
	*/
	short			shObjWidth;
};


struct STRUCT_CAM_SYNCHRO_STROBE
{
	/**
		Camera synchro index.
	*/
	WORD			wDBIndexScanner;

	/**
		Cell on which strobe has been opened/Close.
	*/
	WORD			wCell;
};

// .byObjFlags defines (longitudinal)
/// Parcel found.
/**
	Parcel found.
*/
#define PARCEL_FOUND		0x1

/// Cell empty.
/**
	Cell empty.
*/
#define CELL_EMPTY			0x2

// .byObjFlags defines (trasverse)
/// Parcel decentered.
/**
	Parcel decentered.
*/
#define PARCEL_DECENTERED	0x01

/// Estimated position.
/**
	Estimated position.
*/
#define ESTIMATED_POSITION	0x02

/// Invalid dimension.
/**
	Invalid dimension.
*/
#define INVALID_DIMENSION	0x04

/// Parcel not found.
/**
	Parcel not found.
*/
#define PARCEL_NOT_FOUND	0x08

// defines for wMSGID_EVENT_DEVICE sub-message (.wMsgId)
/// Smart device longitudinal to sorter direction parcel detection.
/**
	Sub message of wMSGID_EVENT_DEVICE. It identifies data for parcel detection longitudinal to sorter direction.
	In byData, the structure STRUCT_LONGITUDINAL_OBJECT_POSITION will be saved.
*/
#define wMSGID_SMARTDEV_LONGITUDINAL_PARCEL_DETECTION		1

/// Smart device trasverse to sorter direction parcel detection.
/**
	Sub message of wMSGID_EVENT_DEVICE. It identifies data for parcel detection trasverse to sorter direction.
	In byData, the structure STRUCT_TRASVERSE_OBJECT_POSITION will be saved.
*/
#define wMSGID_SMARTDEV_TRASVERSE_PARCEL_DETECTION			2

/// Smart device strobe open.
/**
	Sub message of wMSGID_EVENT_DEVICE. It identifies when the camera synchro has opened the strobe to scanner.
	In byData, the structure STRUCT_CAM_SYNCHRO_STROBE will be saved.
*/
#define wMSGID_SMARTDEV_STROBE_OPEN							3

/// Smart device strobe close.
/**
	Sub message of wMSGID_EVENT_DEVICE. It identifies when the camera synchro has close the strobe to scanner.
	In byData, the structure STRUCT_CAM_SYNCHRO_STROBE will be saved.
*/
#define wMSGID_SMARTDEV_STROBE_CLOSE						4

// [STRUCT_SMART_DEVICE_PARAM_CLASS_1].shApplicationFlags defines
/**
	Double cell sorter.
*/
#define DOUBLE_CELL_SORTER									0x0080

