//
//	File:		DB_MSC_Trains.h
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 8.0 (2005)
//              RTX v8.1.2 Build 7818
//
//	Version:	1.0
//
//	Created:	26/July/2010
//	Updated:	20/March/2013
//
//	Author:		Marco Piazza [+39-0331-665307 marco.piazza@fivesgroup.com]
//
//  History:
//
//	20-03-2013 - Added paramter for "Decay Factor" into STRUCT_POWER_COUNTERS
//	26-07-2010 - First release
//

#pragma once

#include "COMMON_Diag_Block.h"

#define	MAX_UNLOAD_CELL_HISOTRY			 400	// Last "x" NIF to keep history of unloaded cells; ie: ((50mm / 2500mm/s) * x) if x = 400 it means 8 seconds

//////////////////////////////////////////////////////////////////////////
//							STRUCT_POWER_COUNTERS
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_POWER_COUNTERS
{
	//
	BYTE	byPeakSpinningCells;
	double	dPeakSpinningDecayK;
	//
	BYTE	byAvarageSpinningCells;
	double	dAvarageSpinningDecayK;
	//
	BYTE	byPeakRunningCells;
	double	dPeakRunningDecayK;
	//
	BYTE	byAvarageRunningCells;
	double	dAvarageRunningDecayK;
	//
} STRUCT_POWER_COUNTERS;
#pragma pack()

#define SIZE_STRUCT_POWER_COUNTERS		sizeof(_STRUCT_POWER_COUNTERS)

//////////////////////////////////////////////////////////////////////////
//							STRUCT_TRAINS (DB)
//////////////////////////////////////////////////////////////////////////
//
#pragma pack(1)
typedef struct _STRUCT_TRAINS
{
	//
	BYTE	byTrainID;					// Main train ID
	BYTE	bySubTrainID;				// Sub-train ID (0 when master train)
	//
	WORD	wFromCell;					// First cell of the train
	WORD	wToCell;					// Last cell of the train
	//
	// MSC-156
	// Bug Fix: Trains managed Energy Box limit is to low
	// <refactor from BYTE to WORD>
	WORD	wInstalledEnergyBox;		// Total installed energy box in this train-subtrain
	WORD	wEnergyBoxInFault;			// Total number of faulted energy box  in this train-subtrain
	//
	// Energy limits counters
	//
	STRUCT_POWER_COUNTERS	csCfgPowerLimits;		// Configuratin values (from CSV)
	STRUCT_POWER_COUNTERS	csRunPowerLimits;		// Limit adjusted by runtime (Working Energy Box relation)
	//
	// Runtime values ...
	//
	WORD	wCurrentSpinningCells;
	WORD	wCurrentRunningCells;
	WORD	wUnloadedCellsHistory[MAX_UNLOAD_CELL_HISOTRY];
	WORD	wTempPeakCount;
	WORD	wTempAvarageCount;
	//
	// Runtime MAX values recorded...
	//
	WORD	wMaxSpinningCells;
	WORD	wMaxRunningCells;
	WORD	wMaxPeakCount;
	WORD	wMaxAvarageCount;
	//
	STRUCT_DIAG_BLOCK	csDiag;			// Full Diagnostic
	//
}	STRUCT_TRAINS;
#pragma pack()

#define SIZE_STRUCT_TRAINS		sizeof(_STRUCT_TRAINS)

//////////////////////////////////////////////////////////////////////////
// All possible fields DEFINES
//////////////////////////////////////////////////////////////////////////

#define ALL_TRAINS	(WORD)0xFFFF

// Alarm's bit define
#define	TRAIN_FAULT_LOW_POWER		0			// Number of faulted Energy Box is too high

// Warning's bit define
#define	TRAIN_WARNING_OPEN_LOOP		0			// Open loop (wiring problems) detected

// I/O Name Define
