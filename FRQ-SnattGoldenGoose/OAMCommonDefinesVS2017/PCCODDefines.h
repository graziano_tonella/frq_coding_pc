//
//	File:		PCCODDefines.h
//
//	Compiler:	Visual C++
//
//	Updated:	21-Oct-2021
//
//	Author:			Graziano Tonella
//
#ifndef __PCCODDEFINES_H__
#define __PCCODDEFINES_H__

#define CODE_UNKNOWN_ID			'$'

#define CODE_EAN13_LEN				13
#define CODE_EAN13_ID				'R'

#define CODE_CODE128_LEN			9
#define CODE_CODE128_ID_09DIGITS	'E'
#define CODE_CODE128_LEN_18DIGITS	18
#define CODE_CODE128_ID_18DIGITS	'Q'
#define CODE_CODE128_LEN_20DIGITS	20
#define CODE_CODE128_ID_20DIGITS	'Q'

#define CODE_EAN8_LEN				8
#define CODE_EAN8_ID				'A'

#define CODE_UPC_LEN				12
#define CODE_UPC_ID					'U'


#define CODE_CODEITF_LEN			14
#define CODE_CODEITF_ID				'I'

#define	MAX_RUNNING_BATCHES			10
#define	SIZEOF_PICKING				15	
#define	SIZEOF_MISSINGITEMS			10	


#define SIZEOF_BARCODE__DATA		50
#define SIZEOF_BARCODE__SYMBOLOGY	20
#define SIZEOF_BARCODE__AIM			 3

/*
	0x:	definisce il formato esadecimale.
	X:	identifica il nodo sorgente.
	Y:	identifica il nodo destinatario.
	XX:	progressivo messaggio della coppia XY.

	I possibili valori assegnabili ad X e Y sono: 
		A:	Order Assembly Manager PC (PCFO)
		B:	Macchina Smistatrice (MSC)
		C:	Linea Di Introduzione (ISPC)
		D:	PC Codifica Operatore (PCCOD)
		E:	Non Utilizzato
		F:	Non Utilizzato
*/


#pragma pack(1)
typedef struct _STRUCT_SNATTTOCODPC_KEEPALIVE
{
	WORD	wMsgId;

	wchar_t wzPickingNumberZ[MAX_RUNNING_BATCHES][SIZEOF_PICKING + 1];
	wchar_t wszMissingItemsZ[MAX_RUNNING_BATCHES][SIZEOF_MISSINGITEMS +1];
	int		nAvailableChutes;
} STRUCT_SNATTTOCODPC_KEEPALIVE;
#pragma pack( )


// Commands
enum{CODPCTOSNATT_COMMANDREQUEST__BARCODE_VALIDATION_REQUEST=0, CODPCTOSNATT_COMMANDREQUEST__PRINT_BARCODE_REQUEST=1};
#pragma pack(1)
typedef struct _STRUCT_CODPCTOSNATT_COMMANDREQUEST
{
	int		nCmdID;		// See enums	
	int		nUUID;
	//
	int		nBarcodeLength;
	char	szBarcodeDataZ[SIZEOF_BARCODE__DATA+1];
	char	szBarcodeSymbologyZ[SIZEOF_BARCODE__SYMBOLOGY+1];
	char	szBarcodeAIMCodeZ[SIZEOF_BARCODE__AIM+1];
} STRUCT_CODPCTOSNATT_COMMANDREQUEST;
#pragma pack( )

#pragma pack(1)
typedef struct _STRUCT_SNATTTOCODPC_COMMANDANSWER
{
	int		nCommandId;						// See enums	
	bool    bAccepted;
	CString sMsgToShow;
} STRUCT_SNATTTOCODPC_COMMANDANSWER;
#pragma pack( )

// --------------------------------------
// --								   --
// --  S E C T I O N   ISPC AND PCCOD  --
// --						           --
// --------------------------------------
enum _ENUM_wMSGID_ISPCANDPCCOD
{
	wMSGID_ISPCTOPCCOD_KEEPALIVE	=0xCD00,
	wMSGID_PCCODTOISPC_KEEPALIVE	=0xDC00,

	wMSGID_PCCODTOISPC_CODING		=0xDC01
};

#pragma pack(1)
typedef struct _STRUCT_ISPCTOPCCOD_KEEPALIVE
{
	WORD	wMsgId;
	BOOL	bIsLDCReady;					// TRUE=Yes
	BOOL	bIsEnabledManualCoding;			// TRUE=Yes
} STRUCT_ISPCTOPCCOD_KEEPALIVE;
#pragma pack( )

#pragma pack(1)
typedef struct _STRUCT_PCCODTOISPC_KEEPALIVE
{
	WORD	wMsgId;
	BOOL	bStopLoading;					// TRUE=Yes ; FALSE=No
} STRUCT_PCCODTOISPC_KEEPALIVE;
#pragma pack( )

#pragma pack(1)
typedef struct _STRUCT_PCCODTOISPC_CODING
{
	WORD	wMsgId;
	WORD	wItemLength;
	WORD	wItemWidth;
	BOOL	bManualCoded;
	BYTE	byBCType;
	BYTE	byBCLen;
	char	szBarcodeZ[SIZEOF_BARCODE__DATA +1];
} STRUCT_PCCODTOISPC_CODING;
#pragma pack( )

// --------------------------------------
// --								   --
// --  S E C T I O N   MSC AND PCCOD  --
// --						           --
// --------------------------------------
enum _ENUM_wMSGID_MSCANDPCCOD
{
	wMSGID_MSCTOPCCOD_KEEPALIVE	= 0xBD00,
	wMSGID_PCCODTOMSC_KEEPALIVE	= 0xDB00,
	wMSGID_PCCODTOMSC_CODING	= 0xDB01
};

#pragma pack(1)
typedef struct _STRUCT_MSCTOPCCOD_KEEPALIVE
{
	WORD	wMsgId;
	BOOL	bIsSorterReady;					// TRUE=Yes
	BOOL	bIsEnabledManualCoding;			// TRUE=Yes
} STRUCT_MSCTOPCCOD_KEEPALIVE;
#pragma pack( )

#pragma pack(1)
typedef struct _STRUCT_PCCODTOMSC_KEEPALIVE
{
	WORD	wMsgId;
	BOOL	bStopSorter;					// TRUE=Yes ; FALSE=No
} STRUCT_PCCODTOMSC_KEEPALIVE;
#pragma pack( )

#pragma pack(1)
typedef struct _STRUCT_PCCODTOMSC_CODING
{
	WORD	wMsgId;
	WORD	wItemLength;
	WORD	wItemWidth;
	BOOL	bManualCoded;
	BYTE	byBCType;
	BYTE	byBCLen;
	char	szBarcodeZ[SIZEOF_BARCODE__DATA +1];
} STRUCT_PCCODTOMSC_CODING;
#pragma pack( )



#endif