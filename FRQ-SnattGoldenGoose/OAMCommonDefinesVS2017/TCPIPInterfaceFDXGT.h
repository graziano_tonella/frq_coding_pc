//////////////////////////////////////////////////////////////////
// NAME:					TCPIPInterfaceFDXGT.h				//
// LAST MODIFICATION DATE:	19-Nov-2018							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					TCP/IP Full Duplex Connection with	//
//							Variable SOM and EOM values			//
//////////////////////////////////////////////////////////////////
#pragma once

#include "NetworkST.h"
#include "x64SharedFlatQueueGT.h"
//#include "CMLDefines.h"
#include "x64HighResolutionTimerGT.h"
#include "OAMCommonDefinesx64.h"

class CTCPIPInterfaceFDXGT : public CNetworkST
{
public:
	CTCPIPInterfaceFDXGT();
	virtual ~CTCPIPInterfaceFDXGT();
	//
	void		Destroy(DWORD dwMilliseconds);
	BOOL		Create(int nSocketType, LPCTSTR lpszIPAddress, int nPortNumber, DWORD dwBufferSize, DWORD dwPollingTime, UINT nRcvTimeout, CSharedFlatQueueGT *pFlatQueueIn, DWORD dwMemoryFlatQueueOut, LPCTSTR lpszChannel);
	BOOL		SetRXProperties(BYTE* pbySOM,BYTE* pbyEOM,WORD wMaxLen,WORD wTimeOut,WORD wMsgID,WORD wDiagMsgID);
	void		SetLogToFile(LPCTSTR lpszLogFilePath);

	void		SetPostMessage(HWND hwndID,UINT uMsgID, WORD wNetworkId = 0);
	BOOL		IsRunning(void);
	void		GetRGBConnectionState(COLORREF* pcrColor);
	WORD		GetNumberOfReceivedTelegrams(void);
	WORD		GetNumberOfTransmittedTelegrams(void);

	CSharedFlatQueueGT	m_FlatQueueOut;

private:
	void		OnChangeConnectionState(WPARAM wSocketType, LPARAM lNewState);
	DWORD		OnPrepareMsgToSend(LPBYTE pBuffer, DWORD dwBufferLen);
	BOOL		OnSendCompleted(BOOL bSendOk, int nErrorCode);
	void		OnReceive(LPBYTE pData, DWORD dwDataLen);
	BOOL		OnTimeout();

	CHighResolutionTimerGT		m_HRTimerRX;

	LPBYTE		m_pMsgTCPIP;
	CSharedFlatQueueGT	*m_pFlatQueueIn;

	HWND		m_hwndID;
	UINT		m_uMsgID;
	WORD		m_wNetworkId;
	BYTE		m_byConnectionState;
	COLORREF	m_crColor;
	WORD		m_wNumberOfReceivedTelegrams;
	WORD		m_wNumberOfTransmittedTelegrams;

	DWORD		m_dwNumBytes;

protected:
	UINT	m_uErrorOnPutItem;
	#pragma pack(1)
	typedef struct _STRUCT_PROPERTIES
	{
		size_t	uNumSOMElements;
		BYTE	byArraySOM[_MAX_PATH];
		size_t	uNumEOMElements;
		BYTE	byArrayEOM[_MAX_PATH];;
		WORD	wMaxLen;
		WORD	wTimeOut;
		WORD	wMsgID;
		WORD	wDiagMsgID;
		BOOL	bWaitSOM;
		BOOL	bWaitEOM;
		WORD	wSOMCounter;
		WORD	wEOMCounter;
	} STRUCT_PROPERTIES;
	#pragma pack()
	STRUCT_PROPERTIES	m_csSTRUCT_PROPERTIES;

	STRUCT_TCPIPMSG	m_csSTRUCT_TCPIPMSGIN;
	STRUCT_TCPIPMSG	m_csSTRUCT_TCPIPMSGOUT;

	inline void	WaitMessage(void);

	CString		m_sChannel;
	bool		m_bLogToFile;
	CString		m_sLogFilePath;

	SYSTEMTIME	m_csSystemTime;
	CString		m_sString;
	CFile		m_TraceFile;

};

inline void	CTCPIPInterfaceFDXGT::WaitMessage(void)
{
	m_dwNumBytes						= 0;

	m_csSTRUCT_PROPERTIES.wSOMCounter	= 0;
	m_csSTRUCT_PROPERTIES.wEOMCounter	= 0;

	m_csSTRUCT_PROPERTIES.bWaitSOM	= (BOOL) (m_csSTRUCT_PROPERTIES.uNumSOMElements==0?FALSE:TRUE);
	m_csSTRUCT_PROPERTIES.bWaitEOM	= (BOOL) (m_csSTRUCT_PROPERTIES.uNumEOMElements==0?FALSE:TRUE);

	m_HRTimerRX.StopCounter();
}

