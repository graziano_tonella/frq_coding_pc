//////////////////////////////////////////////////////////////////
//																//
// NAME:					CHighResolutionTimerGT.cpp			//
// LAST MODIFICATION DATE:	30-Mar-2016							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Include								//
//////////////////////////////////////////////////////////////////
#pragma once


//#define __HIGHRESOLUTIONTIMERGT_NODLL__
#ifndef __HIGHRESOLUTIONTIMERGT_NODLL__
//	#define	__GTXDLL_BUILDDLL__
	#ifdef	__GTXDLL_BUILDDLL__
		#define	HIGHRESOLUTIONTIMERGT_EXPORT	__declspec(dllexport)
	#else
		#define	HIGHRESOLUTIONTIMERGT_EXPORT	__declspec(dllimport)
	#endif
#else
	#define	HIGHRESOLUTIONTIMERGT_EXPORT
#endif

class HIGHRESOLUTIONTIMERGT_EXPORT CHighResolutionTimerGT
{
public:
	CHighResolutionTimerGT(void);
	~CHighResolutionTimerGT(void);
	BOOL	IsSupported(void);
	void	StopCounter(void);
	BOOL	StartCounter(void);
	BOOL	IsStarted(void);
	double	GetTimeInSeconds(void);
	double	GetTimeInMilliSeconds(void);
	double	GetTimeInMicroSeconds(void);

private:
	BOOL			m_bSupported;
	BOOL			m_bStarted;

protected:
	BOOL	_GetElapsedTime(void);
	LARGE_INTEGER	m_liFrequency;
	LARGE_INTEGER	m_liStartCounter;
	LARGE_INTEGER	m_liStopCounter;
	LARGE_INTEGER	m_liDiffCounter;
};
