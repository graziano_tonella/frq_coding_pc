#include "pch.h"

#include "NetworkST.h"
//#include "Macros.h"
#include "Timers.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#define JMP_STEP(s)			\
{							\
	/*::EnterCriticalSection(p->pcsWait);*/	\
	/*p->*/nStep = s;			\
	/*::LeaveCriticalSection(p->pcsWait);*/	\
};


CNetworkST::CNetworkST()
{
	m_bNoLinger = FALSE;
	::ZeroMemory(&m_csThread, sizeof(m_csThread));

	SetConnectedAddress(0, 0, 0, 0, 0);

/*
	m_paramThread.SockAddr.sin_family = AF_INET;
	m_paramThread.SockAddr.sin_port = 10000;
	m_paramThread.SockAddr.sin_addr.s_addr = ::inet_addr("127.0.0.1");
*/

	//::InitializeCriticalSection(&m_csWait);
}

CNetworkST::~CNetworkST()
{
	Destroy(1000);

	//::DeleteCriticalSection(&m_csWait);
}

// This function creates a socket
//
// Parameters:
//		[IN]	nSocketType
//				NETWORKST_SOCKET_SERVER
//					A server (accepting) socket will be created.
//				NETWORKST_SOCKET_CLIENT
//					A client (connecting) socket will be created.
//		[IN]	lpIPAddress
//				Pointer to an array of STRUCT_NETWORKST_IPADDRESS structures. Each element
//				of the array contains an IP address where try to connect. Connection will
//				be tried starting from the first element of the array. If connection is
//				refused it will be tried to the next element of the array and so on.
//				If nSocketType is NETWORKST_SOCKET_SERVER, only the first element of the array
//				will be considered and will be handled as the IP address and port number where
//				accept the connection.
//		[IN]	dwIPAddressCount
//				Number of structures pointed by lpIPAddress.
//		[IN]	dwBufferSize
//		[IN]	dwPollingTime
//				Indicates the minimum time (in milliseconds) the thread will call the OnPrepareMsgToSend
//				function to allow the parent class to send something.
//		[IN]	nRcvTimeout
//				Timeout value, in milliseconds, for incoming data.
//		[IN]	bRunAsMaster
//				If TRUE a minimum dwPollingTime will be granted between two writes.
//				If FALSE a write will be executed immediately after every receive.
//		[IN]	bHardwareKA
//				If TRUE the hardware Keep Alive feature of the NIC/Network driver
//				will be enabled.
//		[IN]	uHardwareSendBufferSize
//				Size of the hardware send buffer.
//				0 means no buffering.
//				-1L means use default value.
//		[IN]	uHardwareRcvBufferSize
//				Size of the hardware receive buffer.
//				0 means no buffering.
//				-1L means use default value.
//
// Return value:
//		NETWORKST_OK
//			Socket created
//		NETWORKST_NOINIT
//			Class not initialized
//		NETWORKST_ALREADYCREATED
//			Class is already handling a socket
//		NETWORKST_INVALIDIPADDR
//			IP address is not valid
//		NETWORKST_THREADKO
//			_beginthreadex has failed
//		NETWORKST_THREADPARENTKO
//			Pointer to parent class passed to thread is NULL
//		NETWORKST_THREADNETEVENTKO
//			The ::WSACreateEvent(...) call has failed
//		NETWORKST_THREADMEMKO
//			Thread has failed allocating memory
//		NETWORKST_INVALIDSOCKTYPE
//			Invalid socket type specified
//		NETWORKST_INVALID_BUFFERSIZE
//			Invalid dwBufferSize (must be > 0)
//		NETWORKST_MEMKO
//			Failed allocating temporary memory
//
DWORD CNetworkST::CreateSocket(	unsigned short nSocketType, 
								LPVOID lpIPAddress,
								DWORD dwIPAddressCount,
								DWORD dwBufferSize, 
								DWORD dwPollingTime, 
								UINT uRcvTimeout, 
								BOOL bRunAsMaster, 
								BOOL bHardwareKA, 
								UINT uHardwareSendBufferSize, 
								UINT uHardwareRcvBufferSize)
{
	if (m_csThread.hThreadHandle != NULL) return NETWORKST_ALREADYCREATED;
	::ZeroMemory(&m_csThread, sizeof(m_csThread));
	//
	if (dwBufferSize == 0) return NETWORKST_INVALID_BUFFERSIZE;
/*
	// Resolve IP address
	IN_ADDR AddrIn;
	AddrIn.s_addr = ::inet_addr(lpszIPAddress);
	// If failed
	if (AddrIn.s_addr == INADDR_NONE)
	{
		return NETWORKST_INVALIDIPADDR;
	}
*/

	// Prepare thread parameters
	// Check socket type
	switch (nSocketType)
	{
		case NETWORKST_SOCKET_SERVER:
		case NETWORKST_SOCKET_CLIENT:
			m_csThread.nSocketType = nSocketType;
			break;
		default:
			return NETWORKST_INVALIDSOCKTYPE;
	} // switch
	//
	if (lpIPAddress == NULL || dwIPAddressCount == 0)	return NETWORKST_INVALIDIPADDR;
	// Allocate memory for list of IP addresses
	m_csThread.lpIPAddress = (LPVOID)::GlobalAlloc(GPTR, SIZE_STRUCT_NETWORKST_IPADDRESS * dwIPAddressCount);
	if (m_csThread.lpIPAddress == NULL)	return NETWORKST_MEMKO;
	::CopyMemory(m_csThread.lpIPAddress, lpIPAddress, SIZE_STRUCT_NETWORKST_IPADDRESS * dwIPAddressCount);
	m_csThread.dwIPAddressCount = dwIPAddressCount;

	// Create exit event
	m_csThread.hExitEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	// If failed
	if (m_csThread.hExitEvent == NULL) 
	{
		::GlobalFree((HGLOBAL)m_csThread.lpIPAddress);
		return NETWORKST_NOINIT;
	} // if
	// Create service event
	m_csThread.hServiceEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	// If failed
	if (m_csThread.hServiceEvent == NULL)
	{
		::GlobalFree((HGLOBAL)m_csThread.lpIPAddress);
		::CloseHandle(m_csThread.hExitEvent);
		return NETWORKST_NOINIT;
	} // if

/*
	m_paramThread.SockAddr.sin_family = AF_INET;
	m_paramThread.SockAddr.sin_port = nPortNumber;
	m_paramThread.SockAddr.sin_addr.s_addr = AddrIn.s_addr;
*/
	m_csThread.pParent = this;
	//m_csThread.pcsWait = &m_csWait;
	m_csThread.dwBufferSize = dwBufferSize;
	m_csThread.dwPollingTime = dwPollingTime;
	m_csThread.uRcvTimeout = uRcvTimeout;
	m_csThread.bRunAsMaster = bRunAsMaster;
	m_csThread.bHardwareKA = bHardwareKA;
	m_csThread.uHardwareSendBufferSize = uHardwareSendBufferSize;
	m_csThread.uHardwareRcvBufferSize = uHardwareRcvBufferSize;
	m_csThread.bNoLinger = m_bNoLinger;
	m_csThread.bExit = FALSE;

	// Create thread
	::ResetEvent(m_csThread.hServiceEvent);
	m_csThread.hThreadHandle = (HANDLE) _beginthreadex(NULL,
								0, 
								&thrNetwork,
								(LPVOID)&m_csThread,
								0,
								(unsigned int*)&m_csThread.dwThreadId);
	// If failed
	if (m_csThread.hThreadHandle == NULL) 
	{
		::GlobalFree((HGLOBAL)m_csThread.lpIPAddress);
		::CloseHandle(m_csThread.hExitEvent);
		::CloseHandle(m_csThread.hServiceEvent);
		return NETWORKST_THREADKO;
	} // if

	// Wait end of thread's initialization
	::WaitForSingleObject(m_csThread.hServiceEvent, INFINITE);
	// Check thread's initialization return code
	if (m_csThread.nExitCode != NETWORKST_OK) 
	{
		::GlobalFree((HGLOBAL)m_csThread.lpIPAddress);
		::CloseHandle(m_csThread.hExitEvent);
		::CloseHandle(m_csThread.hServiceEvent);
		return m_csThread.nExitCode;
	} // if

	return NETWORKST_OK;
} // End of CreateSocket

// This function creates a socket.
//
// Parameters:
//		[IN]	nSocketType
//				NETWORKST_SOCKET_SERVER
//					A server (accepting) socket will be created.
//				NETWORKST_SOCKET_CLIENT
//					A client (connecting) socket will be created.
//		[IN]	lpszIPAddress
//				Points to a zero-terminated string in the Internet dotted form (ex. "127.0.0.1").
//				If server socket: specifies the address of the host machine.
//				If client socket: specifies the address of the remote machine to connect to.
//		[IN]	nPortNumber
//				Indicates the port number.
//				If server socket: specifies the port number where to listen for connection.
//				Id client socket: specifies the port number where to connect on the remote machine.
//		[IN]	dwBufferSize
//		[IN]	dwPollingTime
//				Indicates the minimum time (in milliseconds) the thread will call the OnPrepareMsgToSend
//				function to allow the parent class to send something.
//		[IN]	nRcvTimeout
//				Timeout value, in milliseconds, for incoming data.
//		[IN]	bRunAsMaster
//				If TRUE a minimum dwPollingTime will be granted between two writes.
//				If FALSE a write will be executed immediately after every receive.
//		[IN]	bHardwareKA
//				If TRUE the hardware Keep Alive feature of the NIC/Network driver
//				will be enabled.
//		[IN]	uHardwareSendBufferSize
//				Size of the hardware send buffer.
//				0 means no buffering.
//				-1L means use default value.
//		[IN]	uHardwareRcvBufferSize
//				Size of the hardware receive buffer.
//				0 means no buffering.
//				-1L means use default value.
//
// Return value:
//		NETWORKST_OK
//			Socket created
//		NETWORKST_NOINIT
//			Class not initialized
//		NETWORKST_ALREADYCREATED
//			Class is already handling a socket
//		NETWORKST_INVALIDIPADDR
//			IP address is not valid
//		NETWORKST_THREADKO
//			_beginthreadex has failed
//		NETWORKST_THREADPARENTKO
//			Pointer to parent class passed to thread is NULL
//		NETWORKST_THREADNETEVENTKO
//			The ::WSACreateEvent(...) call has failed
//		NETWORKST_THREADMEMKO
//			Thread has failed allocating memory
//		NETWORKST_INVALIDSOCKTYPE
//			Invalid socket type specified
//		NETWORKST_INVALID_BUFFERSIZE
//			Invalid dwBufferSize (must be > 0)
//		NETWORKST_MEMKO
//			Failed allocating temporary memory
//
DWORD CNetworkST::CreateSocket(	unsigned short nSocketType, 
								LPCTSTR lpszIPAddress, 
								int nPortNumber, 
								DWORD dwBufferSize, 
								DWORD dwPollingTime, 
								UINT uRcvTimeout, 
								BOOL bRunAsMaster, 
								BOOL bHardwareKA, 
								UINT uHardwareSendBufferSize, 
								UINT uHardwareRcvBufferSize)
{
	STRUCT_NETWORKST_IPADDRESS	csIPAddress;

	if (lpszIPAddress == NULL) return NETWORKST_INVALIDIPADDR;

	_tcsncpy_s(csIPAddress.szIPAddress,sizeof(csIPAddress.szIPAddress)/sizeof(_TCHAR), lpszIPAddress, (sizeof(csIPAddress.szIPAddress) / sizeof(TCHAR)));
	csIPAddress.wPortNumber = (WORD)nPortNumber;

	return CreateSocket(nSocketType, &csIPAddress, 1, dwBufferSize, dwPollingTime, uRcvTimeout, bRunAsMaster, bHardwareKA, uHardwareSendBufferSize, uHardwareRcvBufferSize);
} // End of CreateSocket

// This function destroys the socket and the associated thread.
// After that the class returns to a un-initialized state
//
// Return value:
//		NETWORKST_OK
//			[out]	Socket closed or no socket exists
//		NETWORKST_NOINIT
//			[out]	Class not initialized
DWORD CNetworkST::Destroy(DWORD dwMilliseconds)
{
	DWORD dwRetValue;

	// If class is not initialized
	// If class is not handling a socket
	if (m_csThread.hThreadHandle == NULL) return NETWORKST_OK;

	::ResetEvent(m_csThread.hServiceEvent);
	m_csThread.bExit = TRUE;

	::SetEvent(m_csThread.hExitEvent);

	dwRetValue = ::WaitForSingleObject(m_csThread.hServiceEvent, dwMilliseconds);

	// Release memory for list of IP addresses
	if (m_csThread.lpIPAddress)	::GlobalFree((HGLOBAL)m_csThread.lpIPAddress);

	// Close events
	::CloseHandle(m_csThread.hExitEvent);
	::CloseHandle(m_csThread.hServiceEvent);

	::ZeroMemory(&m_csThread, sizeof(m_csThread));

	if (dwRetValue != WAIT_OBJECT_0) 
	{
		ASSERT(FALSE);
		return NETWORKST_THREADKO;
	} // if

	return NETWORKST_OK;
} // End of Destroy

// This function returns the IP address and the port number the socket is currently connected to.
// For client sockets this is the IP/Port of the remote connection (same as specified in CreateSocket)
// For server sockets this is the IP/Port of the accepted connection
//
// Remarks:
// If there is no connection active this function will returns: Address = 0.0.0.0 Port = 0
//
// Return value:
//		NETWORKST_OK
//			Function executed successfully
//
DWORD CNetworkST::GetConnectedAddress(LPBYTE lpbyB1, LPBYTE lpbyB2, LPBYTE lpbyB3, LPBYTE lpbyB4, LPWORD lpwPortNumber)
{
	*lpbyB1 = m_byConnectedIP[0];
	*lpbyB2 = m_byConnectedIP[1];
	*lpbyB3 = m_byConnectedIP[2];
	*lpbyB4	= m_byConnectedIP[3];

	*lpwPortNumber = m_wConnectedPort;

	return NETWORKST_OK;
} // End of GetConnectedAddress

void CNetworkST::SetConnectedAddress(BYTE byB1, BYTE byB2, BYTE byB3, BYTE byB4, WORD wPortNumber)
{
	m_byConnectedIP[0] = byB1;
	m_byConnectedIP[1] = byB2;
	m_byConnectedIP[2] = byB3;
	m_byConnectedIP[3] = byB4;

	m_wConnectedPort = wPortNumber;
} // End of SetConnectedAddress

unsigned _stdcall CNetworkST::thrNetwork(LPVOID pParam)
{
	BOOL						bRetValue = FALSE;
	DWORD						dwRetValue = 0;
	int							nRetValue = 0;
	int							nError = 0;
	//BOOL						bExit = FALSE;
	STRUCT_NETWORKST_THRPARAM*	p = NULL;
	CNetworkST*					pParent = NULL;					// Pointer to parent class	
	unsigned short				nSocketType = 0;
	int							nOption = 0;
	LINGER						Linger;

	WSAOVERLAPPED				oOverlapped;					// Used in ::WSASend and ::WSARecv
	WSAEVENT					hNetEvent = WSA_INVALID_EVENT;	// Event for network activity
	WSAEVENT					hHandles[3];					// Used in ::WSAWaitForMultipleEvents
	WSANETWORKEVENTS			wsaNetworkEvents;				// Used in ::WSAEnumNetworkEvents

	DWORD						dwBufferSize = 0;
	DWORD						dwDataLen = 0;					// How many bytes send

	SOCKET						hSocketServer = INVALID_SOCKET;
	SOCKET						hSocket = INVALID_SOCKET;		// Socket on which work

	SOCKADDR_IN					NewSockAddrIn;					// Parameters of the incoming socket
	int							nLen = sizeof(SOCKADDR_IN);
	TCHAR						szIPPort[256];
	SOCKADDR_IN					csSockAddr;						// Socket IP/Port number
	STRUCT_NETWORKST_IPADDRESS*	lpIPAddress = NULL;				// Pointer to list of IP addresses
	DWORD						dwIPCount = 0;					// Number of IP addresses in list
	DWORD						dwIPIndex = 0;					// Index of current IP address

	DWORD						dwNumberOfBytesSent = 0;		// Number of bytes sent
	DWORD						dwNumberOfBytesRecvd = 0;		// number of bytes received
	DWORD						dwFlags = 0;
	WSABUF						wsaBuffer;						// Buffer for incoming/outcoming data

	BOOL						bMustClose = FALSE;				// Return value of the OnSendCompleted call

	CTimer55ms					tReadTimeout;					// Receive timeout timer
	UINT						uRcvTimeout = 0;				// Receive timeout value
	DWORD						dwPollingTime = 0;				// Minimum time between two writes
	BOOL						bRunAsMaster = FALSE;			// Indicates if socket is acting as Master or Slave

	int							nStep = NETWORKST_CREATE;		// Step of the thread

	p = (STRUCT_NETWORKST_THRPARAM*)pParam;
	// Create event for overlapped operations
	::ZeroMemory(&oOverlapped, sizeof(oOverlapped));
	oOverlapped.hEvent = ::WSACreateEvent();
	// If WSACreateEvent fails
	if (oOverlapped.hEvent == WSA_INVALID_EVENT)
	{
		p->nExitCode = NETWORKST_THREADNETEVENTKO;
		::SetEvent(p->hServiceEvent);
		return 0;
	} // if

	// Allocate buffer
	dwBufferSize = p->dwBufferSize;
	wsaBuffer.len = dwBufferSize;
	wsaBuffer.buf = (char*)::GlobalAlloc(GPTR, dwBufferSize);
	// If ::GlobalAlloc fails
	if (wsaBuffer.buf == NULL)
	{
		::WSACloseEvent(oOverlapped.hEvent);
		p->nExitCode = NETWORKST_THREADMEMKO;
		::SetEvent(p->hServiceEvent);
		return 0;
	} // if

	pParent = (CNetworkST*)p->pParent;
	// If invalid pointer
	if (pParent == NULL)
	{
		::WSACloseEvent(oOverlapped.hEvent);
		::GlobalFree((HGLOBAL)wsaBuffer.buf);
		p->nExitCode = NETWORKST_THREADPARENTKO;
		::SetEvent(p->hServiceEvent);
		return 0;
	} // if

	lpIPAddress = (STRUCT_NETWORKST_IPADDRESS*)p->lpIPAddress;
	dwIPCount = p->dwIPAddressCount;

	// Create network event
	hNetEvent = ::WSACreateEvent();
	// If WSACreateEvent fails
	if (hNetEvent == WSA_INVALID_EVENT)
	{
		::WSACloseEvent(oOverlapped.hEvent);
		::GlobalFree((HGLOBAL)wsaBuffer.buf);
		p->nExitCode = NETWORKST_THREADNETEVENTKO;
		::SetEvent(p->hServiceEvent);
		return 0;
	} // if

	// Get socket type
	nSocketType = p->nSocketType;

	// Get receive timeout-value
	uRcvTimeout = p->uRcvTimeout;

	// Get polling time
	dwPollingTime = p->dwPollingTime;

	// Get if socket is acting as Master or Slave
	bRunAsMaster = p->bRunAsMaster;

	TRACE1("thrNetwork: (%d) Enter \n", nSocketType);

	p->nExitCode = NETWORKST_OK;
	::SetEvent(p->hServiceEvent);

	// Prepare HANDLEs array
	hHandles[0] = oOverlapped.hEvent;
	hHandles[1] = p->hExitEvent;
	hHandles[2] = hNetEvent;

	// Update connection state
	pParent->OnChangeConnectionState(nSocketType, NETWORKST_CONNSTATE_STOP);

	// Main loop
	while (p->bExit == FALSE)
	{
		switch (/*p->*/nStep)
		{
			case NETWORKST_CREATE:
				hSocket = ::WSASocket(PF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
				if (hSocket == INVALID_SOCKET)
				{
					nError = ::WSAGetLastError();
					pParent->OnError((DWORD)nError);
					TRACE(_T("thrNetwork: WSASocket() = %d \n"), nError);
					ASSERT(FALSE);
				} // if
				hSocketServer = hSocket;
				// Set socket options
				// SO_KEEPALIVE
				if (p->bHardwareKA == TRUE)
				{
					nOption = 1;
					nRetValue = ::setsockopt(hSocket, SOL_SOCKET, SO_KEEPALIVE, (char*)&nOption, sizeof(int));
					if (nRetValue == SOCKET_ERROR)
					{
						nError = ::WSAGetLastError();
						pParent->OnError((DWORD)nError);
						TRACE(_T("thrNetwork: setsockopt() = %d \n"), nError);
						ASSERT(FALSE);
					} // if
				} // if
				// SO_REUSEADDR
				nOption = 1;
				nRetValue = ::setsockopt(hSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&nOption, sizeof(int));
				if (nRetValue == SOCKET_ERROR)
				{
					nError = ::WSAGetLastError();
					pParent->OnError((DWORD)nError);
					TRACE(_T("thrNetwork: setsockopt() = %d \n"), nError);
					ASSERT(FALSE);
				} // if
				// SO_LINGER
				if (p->bNoLinger == FALSE)
				{
					Linger.l_onoff = 1;
					Linger.l_linger = 0;
					nRetValue = ::setsockopt(hSocket, SOL_SOCKET, SO_LINGER, (char*)&Linger, sizeof(int));
					if (nRetValue == SOCKET_ERROR)
					{
						nError = ::WSAGetLastError();
						pParent->OnError((DWORD)nError);
						TRACE(_T("thrNetwork: setsockopt() = %d \n"), nError);
						ASSERT(FALSE);
					} // if
				} // if
				// TCP_NODELAY
				nOption = 1;
				nRetValue = ::setsockopt(hSocket, IPPROTO_TCP, TCP_NODELAY, (char*)&nOption, sizeof(int));
				if (nRetValue == SOCKET_ERROR)
				{
					nError = ::WSAGetLastError();
					pParent->OnError((DWORD)nError);
					TRACE(_T("thrNetwork: setsockopt() = %d \n"), nError);
					ASSERT(FALSE);
				} // if
				// SO_SNDBUF
				if (p->uHardwareSendBufferSize != -1L)
				{
					nOption = p->uHardwareSendBufferSize;
					nRetValue = ::setsockopt(hSocket, SOL_SOCKET, SO_SNDBUF, (char*)&nOption, sizeof(int));
					if (nRetValue == SOCKET_ERROR)
					{
						nError = ::WSAGetLastError();
						pParent->OnError((DWORD)nError);
						TRACE(_T("thrNetwork: getsockopt() = %d \n"), nError);
						ASSERT(FALSE);
					} // if
				} // if
				// SO_RCVBUF
				if (p->uHardwareRcvBufferSize != -1L)
				{
					nOption = p->uHardwareRcvBufferSize;
					nRetValue = ::setsockopt(hSocket, SOL_SOCKET, SO_RCVBUF, (char*)&nOption, sizeof(int));
					if (nRetValue == SOCKET_ERROR)
					{
						nError = ::WSAGetLastError();
						pParent->OnError((DWORD)nError);
						TRACE(_T("thrNetwork: getsockopt() = %d \n"), nError);
						ASSERT(FALSE);
					} // if
				} // if
				// Select network events
				nRetValue = ::WSAEventSelect(hSocket, hNetEvent, (nSocketType == NETWORKST_SOCKET_SERVER) ? FD_ACCEPT : FD_CONNECT);
				if (nRetValue == SOCKET_ERROR)
				{
					nError = ::WSAGetLastError();
					pParent->OnError((DWORD)nError);
					TRACE(_T("thrNetwork: WSAEventSelect() = %d \n"), nError);
					ASSERT(FALSE);
				} // if

//				JMP_STEP((nSocketType == NETWORKST_SOCKET_SERVER) ? NETWORKST_BIND : NETWORKST_CONNECT);
				if (nSocketType == NETWORKST_SOCKET_SERVER)
				{
					JMP_STEP(NETWORKST_BIND);
				} // if
				else
				{
					// Update connection state
					pParent->OnChangeConnectionState(nSocketType, NETWORKST_CONNSTATE_CONNECTING);
					// Start from first IP address in list
					dwIPIndex = 0;
					JMP_STEP(NETWORKST_CONNECT);
				} // else
				break;
			case NETWORKST_BIND:
				// Resolve IP address
				::wsprintf(szIPPort, _T("%s:%d"), lpIPAddress[0].szIPAddress, lpIPAddress[0].wPortNumber);
				//TRACE(_T("thrNetwork: Accept on %s \n"), szIPPort);
				nRetValue = ::WSAStringToAddress(szIPPort, AF_INET, NULL, (SOCKADDR*)&csSockAddr, &nLen);
				if (nRetValue == SOCKET_ERROR)
				{
					nError = ::WSAGetLastError();
					pParent->OnError((DWORD)nError);
					TRACE(_T("thrNetwork: WSAStringToAddress() = %d \n"), nError);

					// Take time
					::Sleep(250);
				} // if
				else
				{
					// Bind socket
					nRetValue = ::bind(hSocketServer, (SOCKADDR*)&csSockAddr/*&p->SockAddr*/, sizeof(SOCKADDR_IN));
					// If failed
					if (nRetValue == SOCKET_ERROR)
					{
						nError = ::WSAGetLastError();
						pParent->OnError((DWORD)nError);
						//TRACE(_T("thrNetwork: bind() = %d \n"), nError);

						// Take time
						::Sleep(250);
					} // if
					else
					{
						JMP_STEP(NETWORKST_LISTEN);
					} // else
				} // else
				break;
			case NETWORKST_LISTEN:
				// Put socket listening
				nRetValue = ::listen(hSocketServer, 1);
				// If failed
				if (nRetValue == SOCKET_ERROR)
				{
					nError = ::WSAGetLastError();
					pParent->OnError((DWORD)nError);
					TRACE(_T("thrNetwork: listen() = %d \n"), nError);
				} // if

				JMP_STEP(NETWORKST_ACCEPT);
				break;
			case NETWORKST_ACCEPT:
				// Update connection state
				pParent->OnChangeConnectionState(nSocketType, NETWORKST_CONNSTATE_ACCEPTING);

				::ZeroMemory(&NewSockAddrIn, sizeof(SOCKADDR_IN));
				dwRetValue = ::WSAWaitForMultipleEvents(2, &hHandles[1], FALSE, WSA_INFINITE, FALSE);
				switch (dwRetValue)
				{
					case WSA_WAIT_EVENT_0:		// Exit event
						//bExit = TRUE;
						break;
					case WSA_WAIT_FAILED:		// Some error
						JMP_STEP(NETWORKST_CLOSE);
						break;
					default:					// Network event (FD_ACCEPT)
						hSocket = ::WSAAccept(hSocketServer, (SOCKADDR*)&NewSockAddrIn, &nLen, NULL, 0);
						if (hSocket == INVALID_SOCKET)
						{
							nError = ::WSAGetLastError();
							switch (nError)
							{
								case WSAEWOULDBLOCK:	// No connection detected
									::WSAResetEvent(hNetEvent);
									break;
								default:				// Some error
									pParent->OnError((DWORD)nError);
									TRACE(_T("thrNetwork: WSAAccept() = %d \n"), nError);
									JMP_STEP(NETWORKST_CLOSE);
									break;
							} // switch
						} // if
						else	// nRetValue != INVALID_SOCKET	(Connection detected)
						{
							// Close server socket
							::shutdown(hSocketServer, SD_BOTH);
							::closesocket(hSocketServer);
							hSocketServer = INVALID_SOCKET;

							pParent->SetConnectedAddress(	NewSockAddrIn.sin_addr.S_un.S_un_b.s_b1,
															NewSockAddrIn.sin_addr.S_un.S_un_b.s_b2,
															NewSockAddrIn.sin_addr.S_un.S_un_b.s_b3,
															NewSockAddrIn.sin_addr.S_un.S_un_b.s_b4,
															lpIPAddress[0].wPortNumber);
							JMP_STEP(NETWORKST_PRERUN);
						} // else
						break;
				}
				break;
			case NETWORKST_CONNECT:
				// Resolve IP address
				::wsprintf(szIPPort, _T("%s:%d"), lpIPAddress[dwIPIndex].szIPAddress, lpIPAddress[dwIPIndex].wPortNumber);
				//TRACE(_T("thrNetwork: Try connect to %s \n"), szIPPort);
				nRetValue = ::WSAStringToAddress(szIPPort, AF_INET, NULL, (SOCKADDR*)&csSockAddr, &nLen);
				if (nRetValue == SOCKET_ERROR)
				{
					nError = ::WSAGetLastError();
					pParent->OnError((DWORD)nError);
					TRACE(_T("thrNetwork: WSAStringToAddress() = %d \n"), nError);
				} // if

				nRetValue = ::WSAConnect(hSocket, (SOCKADDR*)&csSockAddr/*&p->SockAddr*/, sizeof(SOCKADDR_IN), NULL, NULL, NULL, NULL);
				// If failed
				if (nRetValue == SOCKET_ERROR)
				{
					nError = ::WSAGetLastError();
					switch (nError)
					{
						case WSAEWOULDBLOCK:	// Connection in progress
							// Update connection state
							//pParent->OnChangeConnectionState(nSocketType, NETWORKST_CONNSTATE_CONNECTING);

							JMP_STEP(NETWORKST_WAITCONNECT);
							break;
						case WSAECONNREFUSED:	//	Connection not available at this moment (Server down?)
						case WSAENETUNREACH:
						case WSAETIMEDOUT:
							if (++dwIPIndex >= dwIPCount)	dwIPIndex = 0;	// Next IP address in list
							break;
						case WSAEISCONN:
							pParent->SetConnectedAddress(	csSockAddr.sin_addr.S_un.S_un_b.s_b1,
															csSockAddr.sin_addr.S_un.S_un_b.s_b2,
															csSockAddr.sin_addr.S_un.S_un_b.s_b3,
															csSockAddr.sin_addr.S_un.S_un_b.s_b4,
															lpIPAddress[dwIPIndex].wPortNumber);
							JMP_STEP(NETWORKST_PRERUN);
							break;
						default:				// Some error
							pParent->OnError((DWORD)nError);
							TRACE(_T("thrNetwork: WSAConnect() = %d \n"), nError);
							if (++dwIPIndex >= dwIPCount)	dwIPIndex = 0;	// Next IP address in list
							break;
					} // switch
				} // if
				else	// nRetValue == 0	(Connected successfully)
				{
					pParent->SetConnectedAddress(	csSockAddr.sin_addr.S_un.S_un_b.s_b1,
													csSockAddr.sin_addr.S_un.S_un_b.s_b2,
													csSockAddr.sin_addr.S_un.S_un_b.s_b3,
													csSockAddr.sin_addr.S_un.S_un_b.s_b4,
													lpIPAddress[dwIPIndex].wPortNumber);
					JMP_STEP(NETWORKST_PRERUN);
//					JMP_STEP(NETWORKST_WAITCONNECT);
				} // else
				break;
			case NETWORKST_WAITCONNECT:
				dwRetValue = ::WSAWaitForMultipleEvents(2, &hHandles[1], FALSE, WSA_INFINITE, FALSE);
				switch (dwRetValue)
				{
					case WSA_WAIT_EVENT_0:		// Exit event
						//bExit = TRUE;
						break;
					case WSA_WAIT_FAILED:		// Some error
						JMP_STEP(NETWORKST_CLOSE);
						break;
					default:					// Network event
						nRetValue = ::WSAEnumNetworkEvents(hSocket, hNetEvent, &wsaNetworkEvents);
						if (nRetValue == SOCKET_ERROR)
						{
							nError = ::WSAGetLastError();
							pParent->OnError((DWORD)nError);
							//TRACE(_T("thrNetwork: WSAEnumNetworkEvents() = %d \n"), nError);
							JMP_STEP(NETWORKST_CLOSE);
						} // if
						else	// Manage event
						{
							// If no error then connection is good
							if (wsaNetworkEvents.iErrorCode[FD_CONNECT_BIT] == 0)
							{
								pParent->SetConnectedAddress(	csSockAddr.sin_addr.S_un.S_un_b.s_b1,
																csSockAddr.sin_addr.S_un.S_un_b.s_b2,
																csSockAddr.sin_addr.S_un.S_un_b.s_b3,
																csSockAddr.sin_addr.S_un.S_un_b.s_b4,
																lpIPAddress[dwIPIndex].wPortNumber);
								JMP_STEP(NETWORKST_PRERUN);
							} // if
							else
							{
								// If connection refused (or other error)
								pParent->OnError((DWORD)wsaNetworkEvents.iErrorCode[FD_CONNECT_BIT]);
								//TRACE(_T("thrNetwork: (%d) WSAEnumNetworkEvents (FD_CONNECT) %d IPAddress %s \n"), nSocketType, wsaNetworkEvents.iErrorCode[FD_CONNECT_BIT],szIPPort);
								if (++dwIPIndex >= dwIPCount)	dwIPIndex = 0;	// Next IP address in list
								JMP_STEP(NETWORKST_CONNECT);
							} // else
						} // else
						break;
				} // switch
				break;
			case NETWORKST_PRERUN:
				// Select network events
				::WSAResetEvent(hNetEvent);
				nRetValue = ::WSAEventSelect(hSocket, hNetEvent, FD_READ | /*FD_WRITE |*/ FD_CLOSE);
				if (nRetValue == SOCKET_ERROR)
				{
					nError = ::WSAGetLastError();
					pParent->OnError((DWORD)nError);
					TRACE(_T("thrNetwork: WSAEventSelect() = %d \n"), nError);
					JMP_STEP(NETWORKST_CLOSE);
				} // if

				// Update connection state
				pParent->OnChangeConnectionState(nSocketType, NETWORKST_CONNSTATE_RUN);
				TRACE1("thrNetwork: (%d) Connected \n", nSocketType);

				// Start read-timeout
				tReadTimeout.Start();

				JMP_STEP(NETWORKST_RUN);
				break;
			case NETWORKST_RUN:
				dwRetValue = ::WSAWaitForMultipleEvents(2, &hHandles[1], FALSE, dwPollingTime /*WSA_INFINITE*/, FALSE);
				switch (dwRetValue)
				{
					case WSA_WAIT_EVENT_0:		// Exit event
						//bExit = TRUE;
						break;
					case WSA_WAIT_EVENT_0+1:	// Network event
						nRetValue = ::WSAEnumNetworkEvents(hSocket, hNetEvent, &wsaNetworkEvents);
						if (nRetValue == SOCKET_ERROR)
						{
							nError = ::WSAGetLastError();
							pParent->OnError((DWORD)nError);
							//TRACE(_T("thrNetwork: WSAEnumNetworkEvents() = %d \n"), nError);
							JMP_STEP(NETWORKST_CLOSE);
						} // if
						else	// Manage events
						{
							///--------------------
							// If other side has closed the connection
							if (wsaNetworkEvents.lNetworkEvents & FD_CLOSE)
							{
								TRACE1("thrNetwork: (%d) Connection closed by peer \n", nSocketType);
								JMP_STEP(NETWORKST_CLOSE);
								break;
							} // if FD_CLOSE
							///--------------------
							// If data to read
							if (wsaNetworkEvents.lNetworkEvents & FD_READ)
							{
								// If no error
								if (wsaNetworkEvents.iErrorCode[FD_READ_BIT] == 0)
								{
//									TRACE1("thrNetwork: (%d) Ready to read \n", nSocketType);
									JMP_STEP(NETWORKST_READ);
								} // if
								else
								{
									pParent->OnError((DWORD)wsaNetworkEvents.iErrorCode[FD_READ_BIT]);
									//TRACE2("thrNetwork: (%d) WSAEnumNetworkEvents (FD_READ) %d \n", nSocketType, wsaNetworkEvents.iErrorCode[FD_READ_BIT]);
									JMP_STEP(NETWORKST_CLOSE);
								} // else
								break;
							} // if FD_READ
						} // else
						break;
					case WSA_WAIT_TIMEOUT:
						// Read-Timeout elapsed ?
						if (tReadTimeout.IsElapsed(uRcvTimeout) == TRUE)
						{
							bMustClose = pParent->OnTimeout();
							if (bMustClose == TRUE)
							{
								JMP_STEP(NETWORKST_CLOSE);
								break;
							}
							tReadTimeout.Start();
						} // if

						JMP_STEP(NETWORKST_WRITE);

						break;
					case WSA_WAIT_FAILED:		// Some error
						JMP_STEP(NETWORKST_CLOSE);
						break;
				}
				break;
			case NETWORKST_WRITE:
					dwDataLen = pParent->OnPrepareMsgToSend((LPBYTE)wsaBuffer.buf, dwBufferSize);
					// If something to send
					if (dwDataLen != 0)
					{
						if (dwDataLen > dwBufferSize) dwDataLen = dwBufferSize;
						wsaBuffer.len = dwDataLen;
					} // if
					else
					{
						JMP_STEP(NETWORKST_RUN);
						break;
					} // else

					nRetValue = ::WSASend(hSocket, &wsaBuffer, 1, &dwNumberOfBytesSent, 0, &oOverlapped, NULL);
					if (nRetValue == SOCKET_ERROR)
					{
						nError = ::WSAGetLastError();
						switch (nError)
						{
							case WSA_IO_PENDING:	// Operation in progress
//								TRACE1("thrNetwork: (%d) WSASend (WSA_IO_PENDING) \n", nSocketType);
								JMP_STEP(NETWORKST_WRITEOVERLAPPING);
								break;
							case WSAEWOULDBLOCK:	// Take care
								// Write not ok
								bMustClose = pParent->OnSendCompleted(FALSE, nError);
								if (bMustClose == TRUE) {
									JMP_STEP(NETWORKST_CLOSE);}
								else {
									JMP_STEP(NETWORKST_RUN);}

								TRACE1("*** thrNetwork: (%d) WSASend (WSAEWOULDBLOCK) \n", nSocketType);
								break;
							default:	// Some error
								// Write not ok
								pParent->OnError((DWORD)nError);
								bMustClose = pParent->OnSendCompleted(FALSE, nError);
								if (bMustClose == TRUE) {
									JMP_STEP(NETWORKST_CLOSE);}
								else {
									JMP_STEP(NETWORKST_RUN);}

								TRACE2("thrNetwork: (%d) WSASend (%d) \n", nSocketType, nError);
								break;
						}
					}
					else	// Overlapped operation completed immediately
					{
						//TRACE1("thrNetwork: (%d) WSASend (overlapping not needed) \n", nSocketType);
						//JMP_STEP(NETWORKST_WRITEOVERLAPPING);
						JMP_STEP(NETWORKST_WRITEFINISHED);
					}
				break;
			case NETWORKST_WRITEOVERLAPPING:
				dwRetValue = ::WSAWaitForMultipleEvents(2, hHandles, FALSE, WSA_INFINITE, FALSE);
				switch (dwRetValue)
				{
					case WSA_WAIT_EVENT_0 + 1:	// Exit event
						//bExit = TRUE;
						break;
					case WSA_WAIT_EVENT_0:		// Overlapped event
						JMP_STEP(NETWORKST_WRITEFINISHED);
						break;
					case WSA_WAIT_FAILED:		// Some error
						nError = ::WSAGetLastError();
						pParent->OnError((DWORD)nError);
						bMustClose = pParent->OnSendCompleted(FALSE, nError);
						if (bMustClose == TRUE) {
							JMP_STEP(NETWORKST_CLOSE);}
						else {
							JMP_STEP(NETWORKST_RUN);}
						break;
				}
				break;
			case NETWORKST_WRITEFINISHED:
				bRetValue = ::WSAGetOverlappedResult(hSocket, &oOverlapped, &dwNumberOfBytesSent, FALSE, &dwFlags);
				// If operation completed successfully
				if (bRetValue == TRUE)
				{
					// Write ok
					bMustClose = pParent->OnSendCompleted(TRUE, 0);
					if (bMustClose == TRUE) {
						JMP_STEP(NETWORKST_CLOSE);}
					else {
						JMP_STEP(NETWORKST_RUN);}

//					TRACE2("thrNetwork: (%d) WSAGetOverlappedResult (Sent %lu bytes) \n", nSocketType, dwNumberOfBytesSent);
				}
				else	// Some error
				{
					nError = ::WSAGetLastError();
					// Write not ok
					pParent->OnError((DWORD)nError);
					bMustClose = pParent->OnSendCompleted(FALSE, nError);
					if (bMustClose == TRUE) {
						JMP_STEP(NETWORKST_CLOSE);}
					else {
						JMP_STEP(NETWORKST_RUN);}

//					TRACE2("thrNetwork: (%d) WSAGetOverlappedResult (%d) \n", nSocketType, nError);
				}
				break;
			case NETWORKST_READ:
					dwFlags = 0;
					wsaBuffer.len = dwBufferSize;
					nRetValue = ::WSARecv(hSocket, &wsaBuffer, 1, &dwNumberOfBytesRecvd, &dwFlags, &oOverlapped, NULL);
					if (nRetValue == SOCKET_ERROR)
					{
						nError = ::WSAGetLastError();
						switch (nError)
						{
							case WSA_IO_PENDING:	// Operation in progress
//								TRACE1("thrNetwork: (%d) WSARecv (WSA_IO_PENDING) \n", nSocketType);
								JMP_STEP(NETWORKST_READOVERLAPPING);
								break;
/*
							case WSAEWOULDBLOCK:	// Take care
								// Read not ok ?? (no, simply no data read)
								TRACE1("*** thrNetwork: (%d) WSARecv (WSAEWOULDBLOCK) \n", nSocketType);
								ASSERT(FALSE);
								break;
*/
							default:	// Some error
								// Read not ok
								pParent->OnError((DWORD)nError);
								TRACE2("thrNetwork: (%d) WSARecv (%d) \n", nSocketType, nError);
								JMP_STEP(NETWORKST_CLOSE);
								break;
						}
					}
					else	// Overlapped operation completed immediately
					{
						//TRACE1("thrNetwork: (%d) WSARecv (overlapping not needed) \n", nSocketType);
						//JMP_STEP(NETWORKST_READOVERLAPPING);
						JMP_STEP(NETWORKST_READFINISHED);
					}
				break;
			case NETWORKST_READOVERLAPPING:
				dwRetValue = ::WSAWaitForMultipleEvents(2, hHandles, FALSE, WSA_INFINITE, FALSE);
				switch (dwRetValue)
				{
					case WSA_WAIT_EVENT_0 + 1:	// Exit event
						//bExit = TRUE;
						break;
					case WSA_WAIT_EVENT_0:		// Overlapped event
						JMP_STEP(NETWORKST_READFINISHED);
						break;
					case WSA_WAIT_FAILED:		// Some error
						ASSERT(FALSE);
						JMP_STEP(NETWORKST_CLOSE);
						break;
				} // switch
				break;
			case NETWORKST_READFINISHED:
				bRetValue = ::WSAGetOverlappedResult(hSocket, &oOverlapped, &dwNumberOfBytesRecvd, FALSE, &dwFlags);
				// If operation completed succesfully
				if (bRetValue == TRUE)
				{
					// Read ok
					pParent->OnReceive((LPBYTE)wsaBuffer.buf, dwNumberOfBytesRecvd);
					//TRACE2("thrNetwork: (%d) WSAGetOverlappedResult (Received %lu bytes) \n", nSocketType, dwNumberOfBytesRecvd);
					if (bRunAsMaster == TRUE)
					{JMP_STEP(NETWORKST_RUN);}
					else
					{JMP_STEP(NETWORKST_WRITE);}
				} // if
				else	// Some error
				{
					nError = ::WSAGetLastError();
					// Read not ok
					pParent->OnError((DWORD)nError);
					TRACE2("thrNetwork: (%d) WSAGetOverlappedResult (%d) \n", nSocketType, nError);
					JMP_STEP(NETWORKST_CLOSE);
				} // else

				// Start read-timeout
				tReadTimeout.Start();
				break;
			case NETWORKST_CLOSE:
				::WSAEventSelect(hSocket, hNetEvent, 0);
				::WSAResetEvent(hNetEvent);
				// Close socket
				if (hSocket != INVALID_SOCKET)
				{
					::shutdown(hSocket, SD_BOTH);
					::closesocket(hSocket);
					hSocket = INVALID_SOCKET;
				} // if
				if (hSocketServer != INVALID_SOCKET)
				{
					::shutdown(hSocketServer, SD_BOTH);
					::closesocket(hSocketServer);
					hSocketServer = INVALID_SOCKET;
				} // if

				pParent->SetConnectedAddress(0, 0, 0, 0, 0);

				// Update connection state
				pParent->OnChangeConnectionState(nSocketType, NETWORKST_CONNSTATE_STOP);

				// Take time
				::Sleep(1000);

				JMP_STEP(NETWORKST_CREATE);
//				JMP_STEP((nSocketType == NETWORKST_SOCKET_SERVER) ? NETWORKST_ACCEPT : NETWORKST_CREATE);
				break;
		} // switch

		::Sleep(1);
	} // while

	pParent->SetConnectedAddress(0, 0, 0, 0, 0);

	// Update connection state
	pParent->OnChangeConnectionState(nSocketType, NETWORKST_CONNSTATE_STOP);

	::WSAEventSelect(hSocket, hNetEvent, 0);
	// Close socket(s)
	if (hSocket != INVALID_SOCKET)
	{
		::shutdown(hSocket, SD_BOTH);
		::closesocket(hSocket);
	}
	if (hSocketServer != INVALID_SOCKET)
	{
		::shutdown(hSocketServer, SD_BOTH);
		::closesocket(hSocketServer);
	}
	// Close network event
	::WSACloseEvent(hNetEvent);
	// Close overlapped event
	::WSACloseEvent(oOverlapped.hEvent);

	// Free buffer
	if (wsaBuffer.buf != NULL) ::GlobalFree((HGLOBAL)wsaBuffer.buf);

	::SetEvent(p->hServiceEvent);

	TRACE1("thrNetwork: (%d) Exit \n", nSocketType);

	return 0;
} // End of thrNetwork

void CNetworkST::OnError(DWORD dwErrorCode)
{
	dwErrorCode=0;
} // End of OnError

#undef JMP_STEP
