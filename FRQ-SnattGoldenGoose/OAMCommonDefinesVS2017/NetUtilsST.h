//
//	Class:		CNetUtilsST
//
//	Compiler:	Visual C++
//	Tested on:	Visual C++ 6.0 sp3
//
//	Version:	See GetVersionC() or GetVersionI()
//
//	Created:	06/March/2000
//	Updated:	20/April/2000
//
//	Author:		Davide Calabro'		davide_calabro@yahoo.com
//
#ifndef _NETUTILSST_H_
#define _NETUTILSST_H_

#pragma comment(lib, "ws2_32.lib")

#include <winsock2.h>

// Uncomment following line if you are using this class outside the DLL
#define _NETUTILSST_NODLL_

#ifndef _NETUTILSST_NODLL_
#ifndef	_CMLHTDLL_NOLIB_
	#ifdef _DEBUG
		#ifdef _UNICODE
			#pragma comment(lib, "CmlHTud.lib")
		#else
			#pragma comment(lib, "CmlHTd.lib")
		#endif
		#else
		#ifdef _UNICODE
			#pragma comment(lib, "CmlHTu.lib")
		#else
			#pragma comment(lib, "CmlHT.lib")
		#endif
	#endif
#endif
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Error codes
#define NETUTILSST_OK					0
#define NETUTILSST_NOK					1
#define NETUTILSST_WSAEFAULT			2
#define NETUTILSST_WSANOTINITIALISED	3
#define NETUTILSST_WSAENETDOWN			4
#define NETUTILSST_WSAEINPROGRESS		5
#define NETUTILSST_INVALIDADAPTERNUMBER	6
#define NETUTILSST_WSAHOST_NOT_FOUND	7
#define NETUTILSST_WSATRY_AGAIN			8
#define NETUTILSST_WSANO_RECOVERY		9
#define NETUTILSST_WSANO_DATA			10
#define NETUTILSST_NO32BITSADDR			11
#define NETUTILSST_BADPARAM				12

#ifndef _NETUTILSST_NODLL_
	#ifdef	_CMLHTDLL_BUILDDLL_
		#define	NETUTILSST_EXPORT	__declspec(dllexport)
	#else
		#define	NETUTILSST_EXPORT	__declspec(dllimport)
	#endif
#else
		#define	NETUTILSST_EXPORT
#endif

class NETUTILSST_EXPORT	CNetUtilsST  
{
public:
	CNetUtilsST();
	virtual ~CNetUtilsST();

	DWORD GetLocalHostName(LPTSTR lpszLocalHostName);
	DWORD GetLocalIPAddress(LPTSTR lpszAddr, UINT nAdapter = 0);
	DWORD GetLocalIPAddress(in_addr* pcsAddr, UINT nAdapter = 0);

	static short GetVersionI()		{return 10;}
	static LPCTSTR GetVersionC()	{return (LPCTSTR)_T("1.0");}
private:
	DWORD GetHostName_(char* lpszHostName, int nNameLen);
};

#endif
