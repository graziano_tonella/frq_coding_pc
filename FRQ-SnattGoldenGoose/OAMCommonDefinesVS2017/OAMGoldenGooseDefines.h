﻿//////////////////////////////////////////////////////////////////////
//																	//
// NAME:					OAMSamedayDefines.h						//
// LAST MODIFICATION DATE:	31-Jan-2022							//
// AUTHOR:					Graziano Tonella						//
// PURPOSE:					It collects definitions, structures,	//
//							enumerations and paths					//
//////////////////////////////////////////////////////////////////////
#pragma once

#include "..\..\OAMCommonDefinesVS2017\OAMCommonDefinesx64.h"

#include "OAMAndMSCDefines\CMLDefines.h"
#include "OAMAndMSCDefines\OAMRTXDefines.h"


#include "PCCODDefines.h"
#include "DB_Diag.h"

#define CARRIAGE_RETURN			0x0D
#define LINE_FEED				0x0A
#define CHARMAP_SOH				0x01		// Ascii CONTROL Characters START OF HEADER


#define	RGB_BLACK				RGB(0,0,0)
#define	RGB_RED					RGB(255,0,0)
#define	RGB_DARK_RED			RGB(128,0,0)
#define	RGB_MAGENTA				RGB(255,0,255)
#define	RGB_GREEN				RGB(0,255,0)
#define	RGB_YELLOW				RGB(255,255,0)
#define	RGB_BLUE				RGB(0,0,255)
#define	RGB_CYAN				RGB(0,200,255)
#define	RGB_WHITE				RGB(255,255,255)
#define	RGB_OCRA				RGB(255,128,64)
#define	RGB_GRAY				RGB(191,191,191)
#define	RGB_DARK_GRAY			RGB(80,80,80)
#define	RGB_LIGHT_OCRA			RGB(241,220,171)
#define	CINETIC_PURPLE_COLOR	RGB(175,0,124)
#define	RGB_DARK_BLUE_CHART		RGB(15,25,185)
#define	RBG_BLUE_SECONDARY		RGB(0,138,196)
#define	RGB_XXX_UNSELECT		RGB(192,192,192)
#define	RGB_XXX_BLUE			RGB(0,0,192)
#define	RGB_XXX_GREEN			RGB(0,150,0)
#define	RGB_XXX_PINK			RGB(128,0,128)
#define	RGB_XXX_GRAY			RGB(64,64,64)
#define	RGB_PURPLE				RGB(175,0,124)
//


//
//
//
//===========================================================================================================================================
//===========================================================================================================================================
//
// C O M M O N   D E F I N I T I O N        
//
//===========================================================================================================================================
//===========================================================================================================================================
//
//
//
#define PATH__XMLFILES								_T("..\\XMLFiles\\")
#define PATH__TXTFILES								_T("..\\TXTFiles\\")
#define PATH__TRACEFILES							_T("..\\TraceFiles\\")
