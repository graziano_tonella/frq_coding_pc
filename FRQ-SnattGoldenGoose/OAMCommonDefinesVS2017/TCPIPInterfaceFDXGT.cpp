//////////////////////////////////////////////////////////////////
// NAME:					TCPIPInterfaceFDXGT.cpp				//
// LAST MODIFICATION DATE:	19-Nov-2018							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					TCP/IP Full Duplex Connection with	//
//							Variable SOM and EOM values			//
//////////////////////////////////////////////////////////////////
//
#include "pch.h"
#include "TCPIPInterfaceFDXGT.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


CTCPIPInterfaceFDXGT::CTCPIPInterfaceFDXGT()
{
	m_byConnectionState				= 0;

	m_hwndID						= NULL;
	m_uMsgID						= 0;
	m_crColor						= RGB(255,255,255); // White
	m_wNumberOfReceivedTelegrams	= 0;
	m_wNumberOfTransmittedTelegrams	= 0;

	m_pMsgTCPIP					= NULL;

	::ZeroMemory(&m_csSTRUCT_PROPERTIES,sizeof(m_csSTRUCT_PROPERTIES));

	m_sChannel.Format(_T("No Channel"));
	m_bLogToFile = false;
	m_sLogFilePath.Empty();
}

CTCPIPInterfaceFDXGT::~CTCPIPInterfaceFDXGT()
{
	CNetworkST::Destroy(2000);

	if (m_pMsgTCPIP != NULL)
		::GlobalFree((HGLOBAL)m_pMsgTCPIP);
	m_pMsgTCPIP = NULL;
}

void CTCPIPInterfaceFDXGT::Destroy(DWORD dwMilliseconds)
{
	DWORD	dwRetValue;
	//
	//
	//
	dwRetValue = CNetworkST::Destroy(dwMilliseconds);
	VERIFY(dwRetValue == NETWORKST_OK);

	m_FlatQueueOut.Destroy();
}

BOOL CTCPIPInterfaceFDXGT::Create(int nSocketType, LPCTSTR lpszIPAddress, int nPortNumber, DWORD dwBufferSize, DWORD dwPollingTime, UINT nRcvTimeout, CSharedFlatQueueGT *pFlatQueueIn, DWORD dwMemoryFlatQueueOut, LPCTSTR lpszChannel)
{
	DWORD	dwRetValue;
	BOOL	bRunAsMaster = FALSE; // Read And Than Write


	if(m_HRTimerRX.IsSupported() == FALSE)
		return FALSE;

	// 22.02.2013 
	//dwRetValue = CNetworkST::CreateSocket(NETWORKST_SOCKET_SERVER,lpszIPAddress,nPortNumber,dwBufferSize,dwPollingTime,-1,bRunAsMaster,TRUE,0,-1);
	dwRetValue = CNetworkST::CreateSocket((unsigned short) nSocketType,lpszIPAddress,nPortNumber,dwBufferSize,dwPollingTime,nRcvTimeout,bRunAsMaster,TRUE,-1,-1);
	if(dwRetValue != NETWORKST_OK)
		return FALSE;
	//
	m_pFlatQueueIn	= pFlatQueueIn;
	//
	dwRetValue = m_FlatQueueOut.Create(dwMemoryFlatQueueOut,NULL,0,NULL,NULL);
	if(dwRetValue != QUEUEGT_OK)
		return FALSE;
	//
	// Allocate memory
	//
	m_pMsgTCPIP = NULL;
	m_pMsgTCPIP = (LPBYTE)::GlobalAlloc(GPTR, (SIZE_T) dwBufferSize);
	//
	if(m_pMsgTCPIP == NULL)
		return FALSE;

	m_sChannel.Format(_T("%s"),lpszChannel);

	WaitMessage();

	return TRUE;
}

BOOL CTCPIPInterfaceFDXGT::SetRXProperties(BYTE* pbySOM,BYTE* pbyEOM,WORD wMaxLen,WORD wTimeOut,WORD wMsgID,WORD wDiagMsgID)
{
	m_csSTRUCT_PROPERTIES.uNumSOMElements = strlen((const char*)pbySOM);
	if(m_csSTRUCT_PROPERTIES.uNumSOMElements == 0)
		;
	else
	{
		if(m_csSTRUCT_PROPERTIES.uNumSOMElements < MAX_PATH)
			::CopyMemory(&m_csSTRUCT_PROPERTIES.byArraySOM,pbySOM,m_csSTRUCT_PROPERTIES.uNumSOMElements);
		else
			return FALSE;
	}
	m_csSTRUCT_PROPERTIES.uNumEOMElements = strlen((const char*)pbyEOM);
	if(m_csSTRUCT_PROPERTIES.uNumEOMElements == 0)
		;
	else
	{
		if(m_csSTRUCT_PROPERTIES.uNumEOMElements < MAX_PATH)
			::CopyMemory(&m_csSTRUCT_PROPERTIES.byArrayEOM,pbyEOM,m_csSTRUCT_PROPERTIES.uNumEOMElements);
		else
			return FALSE;
	}

	m_csSTRUCT_PROPERTIES.wMaxLen		= wMaxLen;
	m_csSTRUCT_PROPERTIES.wTimeOut		= wTimeOut;
	m_csSTRUCT_PROPERTIES.wMsgID		= wMsgID;
	m_csSTRUCT_PROPERTIES.wDiagMsgID	= wDiagMsgID;
	//
	//
	if(wMaxLen != 0 && (m_csSTRUCT_PROPERTIES.uNumSOMElements != 0 || m_csSTRUCT_PROPERTIES.uNumEOMElements != 0))
		return FALSE;
	if(wMaxLen == 0 && m_csSTRUCT_PROPERTIES.uNumEOMElements == 0)
		return FALSE;
	if(m_csSTRUCT_PROPERTIES.wTimeOut == 0 || m_csSTRUCT_PROPERTIES.wMsgID == 0)
		return FALSE;
	//
	WaitMessage();
	//
	return TRUE;
}

void CTCPIPInterfaceFDXGT::SetLogToFile(LPCTSTR lpszLogFilePath)
{
	m_bLogToFile = true;
	m_sLogFilePath.Format(_T("%s"), lpszLogFilePath);
}

void CTCPIPInterfaceFDXGT::SetPostMessage(HWND hwndID,UINT uMsgID, WORD wNetworkId)
{
	m_hwndID = hwndID;
	m_uMsgID = uMsgID;
	m_wNetworkId = wNetworkId;
}

BOOL CTCPIPInterfaceFDXGT::IsRunning(void)
{
	return (BOOL) (m_byConnectionState == NETWORKST_CONNSTATE_RUN ? TRUE : FALSE);
}

void CTCPIPInterfaceFDXGT::GetRGBConnectionState(COLORREF* pcrColor)
{
	*pcrColor = m_crColor;
}

WORD CTCPIPInterfaceFDXGT::GetNumberOfReceivedTelegrams(void)
{
	return m_wNumberOfReceivedTelegrams;
}

WORD CTCPIPInterfaceFDXGT::GetNumberOfTransmittedTelegrams(void)
{
	return m_wNumberOfTransmittedTelegrams;
}

void CTCPIPInterfaceFDXGT::OnChangeConnectionState(WPARAM wSocketType, LPARAM lNewState)
{
	m_byConnectionState = (BYTE) lNewState;

	switch (lNewState)
	{
		case NETWORKST_CONNSTATE_STOP:
			m_crColor = RGB(255,0,0); // Red
			break;
		case NETWORKST_CONNSTATE_CONNECTING:
			m_crColor = RGB(255,255,0); // Yellow
			break;
		case NETWORKST_CONNSTATE_ACCEPTING:
			m_crColor = RGB(255,255,0); // Yellow
			break;
		case NETWORKST_CONNSTATE_RUN:
			m_crColor = RGB(0,255,0); // Green
			m_wNumberOfReceivedTelegrams	= 0;
			m_wNumberOfTransmittedTelegrams	= 0;
			WaitMessage();
			break;
		default:
			m_crColor = RGB(255,255,255); // White
			break;
	}
	//
	// Notify Message If Required
	//
	if(m_hwndID != NULL)
		::PostMessage(m_hwndID,m_uMsgID,(WPARAM)lNewState,(LPARAM)m_wNetworkId);
}

DWORD CTCPIPInterfaceFDXGT::OnPrepareMsgToSend(LPBYTE pBuffer, DWORD dwBufferLen)
{
	if(m_HRTimerRX.IsStarted() == TRUE)
	{
		//::GetLocalTime(&m_csSystemTime);
		//m_sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
		//TRACE(_T("%s CTCPIPInterfaceFDXGT::OnPrepareMsgToSend() - IsStart %s \n"),m_sString,(m_tTimer55ms.IsStart()==TRUE?_T("YES"):_T("NO")));
		//TRACE(_T("%s CTCPIPInterfaceFDXGT::OnPrepareMsgToSend() - IsElapsed %s \n"),m_sString,(m_tTimer55ms.IsElapsed(m_wTimeOut)==TRUE?_T("YES"):_T("NO")));
		if(m_HRTimerRX.GetTimeInMilliSeconds() >= (double) m_csSTRUCT_PROPERTIES.wTimeOut)
		{
			WaitMessage();
			//
			m_csSTRUCT_TCPIPMSGOUT.csSTRUCT_TCPIPMSGHEADER.wMsgID		= m_csSTRUCT_PROPERTIES.wDiagMsgID;
			m_csSTRUCT_TCPIPMSGOUT.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH	= 0;
			if(m_pFlatQueueIn->PutItem((LPBYTE)&m_csSTRUCT_TCPIPMSGOUT,(DWORD)(sizeof(STRUCT_TCPIPMSGHEADER)+m_csSTRUCT_TCPIPMSGOUT.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH)) != QUEUEGT_OK)
				m_uErrorOnPutItem++;
		}
	}
	DWORD	dwTotalMsgSize = 0;
	DWORD	dwFirstItemSize;
	//
	//
	//
	if(!m_FlatQueueOut.GetCount())
		return 0;
	// Get size of first item available in queue
	dwFirstItemSize = m_FlatQueueOut.GetFirstItemSize();
	if(dwFirstItemSize == 0 || dwFirstItemSize >= dwBufferLen || dwFirstItemSize >= (DWORD) sizeof(m_csSTRUCT_TCPIPMSGOUT.byMsgDATA))
		return 0;
	//
	if(m_FlatQueueOut.GetItem((LPBYTE)m_pMsgTCPIP,TRUE,NULL) != QUEUEGT_OK)
		return 0;
	::CopyMemory(&m_csSTRUCT_TCPIPMSGOUT,m_pMsgTCPIP,min(sizeof(m_csSTRUCT_TCPIPMSGOUT),dwFirstItemSize));
	::CopyMemory(pBuffer,&m_csSTRUCT_TCPIPMSGOUT.byMsgDATA,m_csSTRUCT_TCPIPMSGOUT.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH);
	dwTotalMsgSize += m_csSTRUCT_TCPIPMSGOUT.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH;
	//
	m_wNumberOfTransmittedTelegrams++;
	//
	if(m_bLogToFile == true)
	{
		::GetLocalTime(&m_csSystemTime);
		char szDateTime[100];
		sprintf_s(szDateTime,sizeof(szDateTime),"%02d/%02d/%02d %02d:%02d:%02d - ",m_csSystemTime.wDay,m_csSystemTime.wMonth,m_csSystemTime.wYear,m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond);
		m_sString.Format(_T("%s%s%s_%02d.%02d.%02d_%02d.TXT"), m_sLogFilePath, _T("OnPrepareMsgToSend"), m_sChannel,m_csSystemTime.wDay, m_csSystemTime.wMonth, m_csSystemTime.wYear, m_csSystemTime.wHour);
		if(m_TraceFile.Open(m_sString,CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
		{
			m_TraceFile.SeekToEnd();
			m_TraceFile.Write(szDateTime,strlen(szDateTime));
			m_TraceFile.Write(pBuffer,dwTotalMsgSize);
			m_TraceFile.Write("\r\n",2);
			m_TraceFile.Close();
		}
	}

	return dwTotalMsgSize;
}

BOOL CTCPIPInterfaceFDXGT::OnSendCompleted(BOOL bSendOk, int nErrorCode)
{
	if(bSendOk == FALSE)
		return TRUE;
	//
	//TRACE(_T("CTCPIPInterfaceFDXGT (%s) - OnSendCompleted returns %d Error %d \n"),m_sChannel,bSendOk,nErrorCode);
	return FALSE;
}

void CTCPIPInterfaceFDXGT::OnReceive(LPBYTE pData, DWORD dwDataLen)
{
	DWORD	dwIx;
	//
	// If a message arrived
	//
	if(dwDataLen == 0)
		return;
	//
	::GetLocalTime(&m_csSystemTime);
	m_sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
	//
	//
	if(m_bLogToFile == true)
	{
		if(m_sChannel.CompareNoCase(_T("GANTRY1")) == 0 || true)
		{
			SYSTEMTIME	csSystemTime;
			::GetLocalTime(&csSystemTime);
			CString sString;
			char szTmp[100];
			sprintf_s(szTmp,sizeof(szTmp),"%02d/%02d/%02d %02d:%02d:%02d - ",csSystemTime.wDay,csSystemTime.wMonth,csSystemTime.wYear,csSystemTime.wHour,csSystemTime.wMinute,csSystemTime.wSecond);
			//sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
			sString.Format(_T("%s%s%s_%02d.%02d.%02d_%02d.TXT"), m_sLogFilePath, _T("OnReceive"), m_sChannel, m_csSystemTime.wDay, m_csSystemTime.wMonth, m_csSystemTime.wYear, m_csSystemTime.wHour);
			if(m_TraceFile.Open(sString,CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
			{
				m_TraceFile.SeekToEnd();
				m_TraceFile.Write(szTmp,strlen(szTmp));
				m_TraceFile.Write(pData,dwDataLen);
				/*
				sString.Empty();
				for(DWORD dwIx=0;dwIx<dwDataLen;dwIx++)
				{
					sprintf_s(szTmp,sizeof(szTmp),"%02X ",pData[dwIx]);
					TraceFile.Write(szTmp,strlen(szTmp));
				}
				*/
				m_TraceFile.Write("\r\n",2);
				m_TraceFile.Close();
			}
		}
	}

//	TRACE(_T("%s CTCPIPInterfaceFDXGT::OnReceive - Received %d Bytes\n"),m_sString,dwDataLen);
	for(dwIx=0;dwIx<dwDataLen;dwIx++)
	{
		//TRACE(_T("%s CTCPIPInterfaceFDXGT::OnReceive - Processing Byte %d Containing %c\n"),m_sString,dwIx,pData[dwIx]);
		if(m_dwNumBytes >= sizeof(m_csSTRUCT_TCPIPMSGIN.byMsgDATA))
		{
			WaitMessage();
			continue;
		}
		//
		m_csSTRUCT_TCPIPMSGIN.byMsgDATA[m_dwNumBytes++] = (BYTE) pData[dwIx];
		//
		if(m_csSTRUCT_PROPERTIES.wMaxLen)
		{
			if(m_HRTimerRX.IsStarted() == FALSE)
				m_HRTimerRX.StartCounter();
			//
			if(m_dwNumBytes < m_csSTRUCT_PROPERTIES.wMaxLen)
				continue;
			//
			m_csSTRUCT_TCPIPMSGIN.csSTRUCT_TCPIPMSGHEADER.wMsgID		= m_csSTRUCT_PROPERTIES.wMsgID;
			m_csSTRUCT_TCPIPMSGIN.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH	= (WORD) m_dwNumBytes;
			if(m_pFlatQueueIn->PutItem((LPBYTE)&m_csSTRUCT_TCPIPMSGIN,(DWORD)(sizeof(STRUCT_TCPIPMSGHEADER)+m_csSTRUCT_TCPIPMSGIN.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH)) != QUEUEGT_OK)
				m_uErrorOnPutItem++;
			//
			WaitMessage();
			continue;
		}
		//
		if(m_csSTRUCT_PROPERTIES.bWaitSOM == TRUE)
		{
			if(pData[dwIx] != m_csSTRUCT_PROPERTIES.byArraySOM[m_csSTRUCT_PROPERTIES.wSOMCounter])
			{
				m_csSTRUCT_PROPERTIES.wSOMCounter = 0;
				continue;
			}
			if(++m_csSTRUCT_PROPERTIES.wSOMCounter < m_csSTRUCT_PROPERTIES.uNumSOMElements)
				continue;
			//
			m_csSTRUCT_PROPERTIES.bWaitSOM	= FALSE;
			if(false)	// True if exclude SOM
				m_dwNumBytes	= 0;
			//
			if(m_HRTimerRX.IsStarted() == FALSE)
				m_HRTimerRX.StartCounter();
		}
		//
		if(m_csSTRUCT_PROPERTIES.bWaitEOM == TRUE)
		{
			if(pData[dwIx] != m_csSTRUCT_PROPERTIES.byArrayEOM[m_csSTRUCT_PROPERTIES.wEOMCounter])
			{
				m_csSTRUCT_PROPERTIES.wEOMCounter = 0;
				continue;
			}
			if(++m_csSTRUCT_PROPERTIES.wEOMCounter < m_csSTRUCT_PROPERTIES.uNumEOMElements)
				continue;
			//
			m_csSTRUCT_TCPIPMSGIN.csSTRUCT_TCPIPMSGHEADER.wMsgID		= m_csSTRUCT_PROPERTIES.wMsgID;
			m_csSTRUCT_TCPIPMSGIN.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH	= (WORD) m_dwNumBytes;
			if(m_pFlatQueueIn->PutItem((LPBYTE)&m_csSTRUCT_TCPIPMSGIN,(DWORD)(sizeof(STRUCT_TCPIPMSGHEADER)+m_csSTRUCT_TCPIPMSGIN.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH)) != QUEUEGT_OK)
				m_uErrorOnPutItem++;
			//
			m_wNumberOfReceivedTelegrams++;
			//
			WaitMessage();
			//
			if(m_sChannel.CompareNoCase(_T("TNT")) == 0 && false)
			//if(m_sChannel.CompareNoCase(_T("GANTRY")) == 0)
			{
				SYSTEMTIME	csSystemTime;
				::GetLocalTime(&csSystemTime);
				CString sString;
				char szTmp[100];
				sprintf_s(szTmp,sizeof(szTmp),"%02d/%02d/%02d %02d:%02d:%02d - ",csSystemTime.wDay,csSystemTime.wMonth,csSystemTime.wYear,csSystemTime.wHour,csSystemTime.wMinute,csSystemTime.wSecond);
				//sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
				sString.Format(_T("%s_%02d.%02d.%04d_%02d.TXT"),_T("..\\TraceFiles\\OnReceive"),csSystemTime.wDay,csSystemTime.wMonth,csSystemTime.wYear,csSystemTime.wHour);
				CFile	TraceFile;
				if(TraceFile.Open(sString,CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
				{
					TraceFile.SeekToEnd();
					TraceFile.Write(szTmp,strlen(szTmp));
					sprintf_s(szTmp,sizeof(szTmp),"dwIx %d   dwDataLen %d   MsgLEN %d ",dwIx,dwDataLen,m_csSTRUCT_TCPIPMSGIN.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH);
					TraceFile.Write(szTmp,strlen(szTmp));
					/*
					sString.Empty();
					for(DWORD dwIx=0;dwIx<dwDataLen;dwIx++)
					{
						sprintf_s(szTmp,sizeof(szTmp),"%02X ",pData[dwIx]);
						TraceFile.Write(szTmp,strlen(szTmp));
					}
					*/
					TraceFile.Write("\r\n",2);
					TraceFile.Close();
				}
			}

			//TRACE(_T("%s CTCPIPInterfaceFDXGT::OnReceive - Msg Enqueued %d Bytes\n"),m_sString,m_csSTRUCT_TCPIPMSGIN.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH);
		}
	}
}
BOOL CTCPIPInterfaceFDXGT::OnTimeout()
{
//	::GetLocalTime(&m_csSystemTime);
//	m_sString.Format(_T("%02d:%02d:%02d-%03d"),m_csSystemTime.wHour,m_csSystemTime.wMinute,m_csSystemTime.wSecond,m_csSystemTime.wMilliseconds);
//	TRACE(_T("%s CTCPIPInterfaceFDXGT::OnTimeOut() - TimeStart %s \n"),m_sString,(m_tTimer55ms.IsStart()==TRUE?_T("YES"):_T("NO")));
	return TRUE;
}
