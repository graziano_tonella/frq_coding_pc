// MyADO.h: interface for the CMyADO class.
//
//////////////////////////////////////////////////////////////////////
// Last Modification: 30 March 2016 by Graziano Tonella

#define __MYADOGT_NODLL__
#ifndef __MYADOGT_NODLL__
	//#define	__GTXDLL_BUILDDLL__
	#ifdef	__GTXDLL_BUILDDLL__
		#define	MYADOGT_EXPORT	__declspec(dllexport)
	#else
		#define	MYADOGT_EXPORT	__declspec(dllimport)
	#endif
#else
	#define	MYADOGT_EXPORT
#endif


#import "C:\Program Files\Common Files\System\ado\msado15.dll" no_namespace rename( "EOF", "EndOfFile" )

class MYADOGT_EXPORT CMyADOException
{
public:
    CMyADOException(int nErr,LPCTSTR lpszDescription) {m_nErr=nErr;sString=lpszDescription;}
    CMyADOException(int nErr,const char* cszDescription) {m_nErr=nErr;sString=cszDescription;}
    ~CMyADOException() {};
	const int GetError() const {return m_nErr;}
    const LPCTSTR GetDescription() const {return sString;}
private:
	CString	sString;
	int		m_nErr;
};


class CMyADO  
{
public:
	CMyADO();
	virtual ~CMyADO();

	HRESULT OpenConnection( _bstr_t btConnectionString, _bstr_t btUserID, _bstr_t btPassword );
	HRESULT Close();

	HRESULT AddParameterReturnValue();
	HRESULT AddParameterInputLong( _bstr_t btParameterName, long lValue );
	HRESULT AddParameterInputText( _bstr_t btParameterName, _bstr_t btValue );

	HRESULT AddParameterInputOutputLong( _bstr_t btParameterName, long lValue );
	HRESULT AddParameterInputOutputText( _bstr_t btParameterName, _bstr_t btValue, DWORD dwMaxTextSize );

	HRESULT AddParameterOutputLong( _bstr_t btParameterName );
	HRESULT AddParameterOutputText( _bstr_t btParameterName, DWORD dwMaxTextSize );

	// original
	//HRESULT Execute();
	// new functions
	HRESULT	ExecuteStoredProcedure();
	HRESULT	ExecuteQuery(_bstr_t btCommandString);

	HRESULT GetFieldUINT16( _bstr_t btFieldName, UINT16* pu16Value );
	HRESULT GetFieldUINT32( _bstr_t btFieldName, UINT32* pu32Value );
	HRESULT GetFieldUINT64( _bstr_t btFieldName, UINT64* pu64Value );
	HRESULT GetFieldBigInt( _bstr_t btFieldName, __int64* pn64Value );
	HRESULT GetFieldLong( _bstr_t btFieldName, long* plValue );
	HRESULT GetFieldInt( _bstr_t btFieldName, int* pnValue );
	HRESULT GetFieldText( _bstr_t btFieldName, char* szText, DWORD dwMaxTextSize );
	HRESULT GetFieldLongVarBinary( _bstr_t btFieldName, unsigned char* pucBuffer, long lMaxBufferSize, long* plActualSize);

	HRESULT GetParameterReturnValue( long* plReturnValue );

	HRESULT GetParameterLong( _bstr_t btParameterName, long* plValue );
	HRESULT GetParameterText( _bstr_t btParameterName, char* szText, DWORD dwMaxTextSize );
	HRESULT GetParameterText( _bstr_t btParameterName, char* szText, DWORD dwMaxTextSize, bool* popBIsNull);

	// original
	//HRESULT Initialize( _bstr_t btStoredProcedureName );
	// new functions
	HRESULT InitializeStoredProcedure( _bstr_t btCommandStringOrStoredProcedureName);

	BOOL IsEOF();

	HRESULT MoveNext();
	HRESULT MoveFirst();
	HRESULT MoveLast();
	HRESULT MovePrevious();
	HRESULT	MoveToBegin();

	HRESULT GetRecordCount( long* lRecordCount );

protected:



	// modified
	HRESULT Execute(long lOptions);
	// modified
	HRESULT Initialize( _bstr_t btCommandStringOrStoredProcedureName, CommandTypeEnum CommandType);


private:

	HRESULT AddParameter( _bstr_t btParameterName, DataTypeEnum enDataType, ParameterDirectionEnum enParameterDirection, long lSize, _variant_t vtValue );
	HRESULT GetField( _variant_t vtFieldName, _variant_t& vtValue );
	HRESULT GetParameter( _variant_t vtParameterName, _variant_t& vtValue );
	HRESULT GetFieldDataType( _variant_t vtFieldName, _variant_t& vtValue );

	BOOL IsConnected();
	BOOL IsInitialized();


	_ConnectionPtr m_pConnectionPtr;
	_CommandPtr m_pCommandPtr;

	char	m_szLastError[_MAX_PATH];
	inline void CatchComError(char* pszFunction,_com_error& e,char* pszField=NULL);

	DataTypeEnum	m_DataTypeEnum;
public:
	_RecordsetPtr m_pRecordsetPtr;

public:
	inline char*	GetLastError(void) { return m_szLastError;};
};

inline void CMyADO::CatchComError(char* pszFunction,_com_error& e,char* pszField)
{
	//char szBuff[2000];
//	sprintf_s(szBuff,sizeof(szBuff),"%s: Error = %08lx\n",pszFunction,e.Error());
//	sprintf_s(szBuff,sizeof(szBuff),"ErrorInfo = %s\n", (char*) e.ErrorInfo());
//	sprintf_s(szBuff,sizeof(szBuff),"HelpContext = %08lx\n", (char*) e.HelpContext());
//	sprintf_s(szBuff,sizeof(szBuff),"HelpFile = %s\n", (char*) e.HelpFile());
//	TCHAR wszBuff[1000];
//	wsprintf(wszBuff,_T("ErrorMessage = %s\n"),e.ErrorMessage());
//	sprintf_s(szBuff,sizeof(szBuff),"Source = %s\n", (char*) e.Source());
//	sprintf_s(szBuff,sizeof(szBuff),"%s: Description = %s\n",pszFunction,(char*)e.Description());

	sprintf_s(m_szLastError,sizeof(m_szLastError),"%s: ErrorDescription = %s  (Field: %s)\n",pszFunction,(char*)e.Description(),pszField);

	//return;
	CString	sString;
	CFile	TraceFile;
	sString = _T("D:\\CatchComError.MSSQL.txt");

	SYSTEMTIME	csSystemTime;
	::GetLocalTime(&csSystemTime);
	char szDateTime[50];
	sprintf_s(szDateTime,sizeof(szDateTime),"%02d/%02d/%02d %02d:%02d:%02d - ",csSystemTime.wDay,csSystemTime.wMonth,csSystemTime.wYear,csSystemTime.wHour,csSystemTime.wMinute,csSystemTime.wSecond);
	if(TraceFile.Open(sString,CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
	{
		TraceFile.SeekToEnd();
		TraceFile.Write(szDateTime,strlen(szDateTime));
		TraceFile.Write(m_szLastError,strlen(m_szLastError));
		TraceFile.Write("\r\n",2);
		TraceFile.Close();
	}


/*

				long nCount = m_pConnectionPtr->Errors->Count;
				_TCHAR	wszErrorMsg[256];
				_sntprintf_s(wszErrorMsg,sizeof(wszErrorMsg ),_T("Error In CMyADO::Open() - %s\n"), e.ErrorMessage());
				OutputDebugString(wszErrorMsg );

#define __GTX_TEST__
#ifdef __GTX_TEST__
//Define Other Variables
HRESULT hr = S_OK;
_bstr_t strError;
ErrorPtr pErr = NULL;

CString strErrNum , strErrDescript , strErrSource , strSQLState , strNative ;
CString strHelpFile , strHelpContext ;

try
{
// Enumerate Errors collection and display
// properties of each Error object.
long nCount = m_pConnectionPtr->Errors->Count;

// Collection ranges from 0 to nCount - 1.
for(long i = 0; i < nCount; i++)
{
pErr = m_pConnectionPtr->Errors->GetItem(i);

strErrNum.Format ( _T("0x%08X") , pErr->Number ) ;

strErrNum.Format ( _T("%d  0x%08X") , pErr->Number , HRESULT_CODE(pErr->Number) ) ;

strErrNum.Format ( _T("%d  0x%08X") , e.WCode() , e.WCode() ) ;



strErrDescript.Format ( _T("%s") , (LPCSTR)pErr->Description ) ;
strErrSource.Format ( _T("%s") , (LPCSTR)pErr->Source ) ;
strSQLState.Format ( _T("%s") , (LPCSTR)pErr->SQLState ) ;
strNative.Format ( _T("%d") , (LPCSTR)pErr->NativeError ) ;

if ((LPCSTR)pErr->GetHelpFile() == NULL)
{
OutputDebugString ( _T("No help file available") ) ;
}
else
{
CString strHelpFile , strHelpContext ;

strHelpFile.Format ( _T(" %s ") , pErr->HelpFile ) ;
strHelpContext.Format ( _T(" %s ") , pErr->HelpContext ) ;

}
}
}
catch(_com_error &e)
{

// Notify the user of errors if any.
OutputDebugString ( _T("PrintComError(e);")) ;
}

#endif



*/
}
