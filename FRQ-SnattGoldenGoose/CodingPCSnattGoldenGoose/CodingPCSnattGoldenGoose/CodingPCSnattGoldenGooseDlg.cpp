
// CodingPCSnattGoldenGooseDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "CodingPCSnattGoldenGoose.h"
#include "CodingPCSnattGoldenGooseDlg.h"
#include "afxdialogex.h"

#include "..\..\OAMCommonDefinesVS2017\BtnSt.h"
#include "EXTERN.H"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



/*
class CAlmGrid : public CListCtrl
{
	afx_msg void OnDrawItem(NMHDR* pNMHDR, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()
}

void CAlmGrid::OnDrawItem(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int nRow = lplvcd->nmcd.dwItemSpec; // get row number
	if (nRow == 0)
		lplvcd->clrText = RGB(255,0,255); // set text color
	else
		lplvcd->clrText = RGB(0,255,0); // set text color

	*pResult = CDRF_NEWFONT | CDRF_NOTIFYSUBITEMDRAW;
}
*/

// CCodingPCSnattGoldenGooseDlg dialog



CCodingPCSnattGoldenGooseDlg::CCodingPCSnattGoldenGooseDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CODINGPCSNATTGOLDENGOOSE_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCodingPCSnattGoldenGooseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LISTBOX_ALARMS, m_ListBoxALARMS);
}

BEGIN_MESSAGE_MAP(CCodingPCSnattGoldenGooseDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_MOVING()
	ON_BN_CLICKED(IDOK, &CCodingPCSnattGoldenGooseDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCodingPCSnattGoldenGooseDlg::OnBnClickedCancel)
	ON_MESSAGE(ON_MYWM_UPDATE_DATEANDTIME, On_MYWM_UPDATE_DATEANDTIME)
	ON_MESSAGE(ON_MYWM_SHOW_ISPCALARM_MESSAGE, On_MYWM_SHOW_ISPCALARM_MESSAGE)
	ON_MESSAGE(ON_MYWM_UPDATE_CODING, On_MYWM_UPDATE_CODING)
	ON_EN_CHANGE(IDC_EDIT_SKU, &CCodingPCSnattGoldenGooseDlg::OnEnChangeEditSku)
	ON_BN_CLICKED(IDC_BTN_LABELS, &CCodingPCSnattGoldenGooseDlg::OnBnClickedBtnLabels)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CCodingPCSnattGoldenGooseDlg message handlers

BOOL CCodingPCSnattGoldenGooseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	ShowWindow(SW_MAXIMIZE);

	// TODO: Add extra initialization here

	// BackGround Color Code EDIT
	m_bkBackGroundCODFIELDS.DeleteObject();
	m_crBackGroundCODFIELDS = RGB_BLACK;
	m_bkBackGroundCODFIELDS.CreateSolidBrush(m_crBackGroundCODFIELDS);





	G_hInterfaceDlg = this->m_hWnd;

	ShowWindow(SW_NORMAL);

	((CWnd*)GetDlgItem(IDOK))->EnableWindow(FALSE);
	((CWnd*)GetDlgItem(IDOK))->ShowWindow(SW_HIDE);
	((CWnd*)GetDlgItem(IDCANCEL))->EnableWindow(FALSE);
	((CWnd*)GetDlgItem(IDCANCEL))->ShowWindow(SW_HIDE);

	// SYSTEMTIME DATA
	m_stcDATA_SYSTEMTIME.SubclassDlgItem(IDC_DATA_SYSTEMTIME,this);
	m_stcDATA_SYSTEMTIME.SetTextColor(RGB_YELLOW);
	m_stcDATA_SYSTEMTIME.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA_SYSTEMTIME.SetFont(G_CoreAppHnd.m_CustomFonts.GetXS(),TRUE);
	// SYSTEMDATE DATA
	m_stcDATA_SYSTEMDATE.SubclassDlgItem(IDC_DATA_SYSTEMDATE,this);
	m_stcDATA_SYSTEMDATE.SetTextColor(RGB_YELLOW);
	m_stcDATA_SYSTEMDATE.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA_SYSTEMDATE.SetFont(G_CoreAppHnd.m_CustomFonts.GetXS(),TRUE);

	/*
	// EUIHandler TEXT
	m_stcTEXT__CHN_EUIH.SubclassDlgItem(IDC_TEXT__CHN_EUIH, this);
	m_stcTEXT__CHN_EUIH.SetTextColor(RGB_YELLOW);
	m_stcTEXT__CHN_EUIH.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT__CHN_EUIH.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT__CHN_EUIH.SetWindowText(_T("EUIHandler:"));
	// EUIHandler DATA
	m_stcDATA__CHN_EUIH.SubclassDlgItem(IDC_DATA__CHN_EUIH, this);
	m_stcDATA__CHN_EUIH.SetTextColor(RGB_BLACK);
	m_stcDATA__CHN_EUIH.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA__CHN_EUIH.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcDATA__CHN_EUIH.SetWindowText(_T("S:00 - R:00"));
	*/


	// SNATT Channel TEXT
	m_stcTEXT__CHN_SNATT.SubclassDlgItem(IDC_TEXT__CHN_SNATT, this);
	m_stcTEXT__CHN_SNATT.SetTextColor(RGB_YELLOW);
	m_stcTEXT__CHN_SNATT.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT__CHN_SNATT.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT__CHN_SNATT.SetWindowText(_T("SNATT Channel:"));
	// SNATT Channel DATA
	m_stcDATA__CHN_SNATT.SubclassDlgItem(IDC_DATA__CHN_SNATT, this);
	m_stcDATA__CHN_SNATT.SetTextColor(RGB_BLACK);
	m_stcDATA__CHN_SNATT.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA__CHN_SNATT.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcDATA__CHN_SNATT.SetWindowText(_T("S:00 - R:00"));


	// ISPCHandler TEXT
	m_stcTEXT__CHN_ISPC.SubclassDlgItem(IDC_TEXT__CHN_ISPC, this);
	m_stcTEXT__CHN_ISPC.SetTextColor(RGB_YELLOW);
	m_stcTEXT__CHN_ISPC.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT__CHN_ISPC.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT__CHN_ISPC.SetWindowText(_T("ISPC Channel:"));
	// ISPCHandler DATA
	m_stcDATA__CHN_ISPC.SubclassDlgItem(IDC_DATA__CHN_ISPC, this);
	m_stcDATA__CHN_ISPC.SetTextColor(RGB_BLACK);
	m_stcDATA__CHN_ISPC.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA__CHN_ISPC.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcDATA__CHN_ISPC.SetWindowText(_T("S:00 - R:00"));

	// MSSQL TEXT
	m_stcTEXT_MSSQL.SubclassDlgItem(IDC_TEXT_MSSQL,this);
	m_stcTEXT_MSSQL.SetTextColor(RGB_YELLOW);
	m_stcTEXT_MSSQL.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT_MSSQL.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT_MSSQL.SetWindowText(_T("SQL Server:"));
	m_stcTEXT_MSSQL.ShowWindow(SW_HIDE);
	// MSSQL DATA
	m_stcDATA_MSSQL.SubclassDlgItem(IDC_DATA_MSSQL,this);
	m_stcDATA_MSSQL.SetTextColor(RGB_BLACK);
	m_stcDATA_MSSQL.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA_MSSQL.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcDATA_MSSQL.SetWindowText(_T(""));
	m_stcDATA_MSSQL.ShowWindow(SW_HIDE);

	// BOX BATCH
	m_stcBOX_BATCH.SubclassDlgItem(IDC_BOX_BATCH,this);
	m_stcBOX_BATCH.SetTextColor(RGB_WHITE);
	m_stcBOX_BATCH.SetBkColor(MAINDLG_BKCOLOR);
	m_stcBOX_BATCH.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcBOX_BATCH.SetWindowText(_T("PICKING List"));
	// TEXT PICKING
	m_stcTEXT_PICKING.SubclassDlgItem(IDC_TEXT_PICKING,this);
	m_stcTEXT_PICKING.SetTextColor(RGB_WHITE);
	m_stcTEXT_PICKING.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT_PICKING.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT_PICKING.SetWindowText(_T("Picking"));
	// TEXT MISSING
	m_stcTEXT_MISSING.SubclassDlgItem(IDC_TEXT_MISSING,this);
	m_stcTEXT_MISSING.SetTextColor(RGB_WHITE);
	m_stcTEXT_MISSING.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT_MISSING.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT_MISSING.SetWindowText(_T("Qta Mancante"));
	
	// BATCH
	for(int nLoop=0;nLoop< MAX_RUNNING_BATCHES;nLoop++)
	{
		m_stcDATA_PICKING[nLoop].SubclassDlgItem(IDC_DATA_PICKING01+nLoop,this);
		m_stcDATA_PICKING[nLoop].SetTextColor(RGB_YELLOW);
		m_stcDATA_PICKING[nLoop].SetBkColor(MAINDLG_BKCOLOR);
		m_stcDATA_PICKING[nLoop].SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
		m_stcDATA_PICKING[nLoop].SetWindowText(_T("--------"));
	}

	// MISSING
	for(int nLoop=0;nLoop< MAX_RUNNING_BATCHES;nLoop++)
	{
		m_stcDATA_MISSING[nLoop].SubclassDlgItem(IDC_DATA_MISSING01+nLoop,this);
		m_stcDATA_MISSING[nLoop].SetTextColor(RGB_YELLOW);
		m_stcDATA_MISSING[nLoop].SetBkColor(MAINDLG_BKCOLOR);
		m_stcDATA_MISSING[nLoop].SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
		m_stcDATA_MISSING[nLoop].SetWindowText(_T("-"));
	}


	// BOX CHUTE
	m_stcBOX_CHUTE.SubclassDlgItem(IDC_BOX_CHUTE,this);
	m_stcBOX_CHUTE.SetTextColor(RGB_WHITE);
	m_stcBOX_CHUTE.SetBkColor(MAINDLG_BKCOLOR);
	m_stcBOX_CHUTE.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcBOX_CHUTE.SetWindowText(_T("N. Uscite Libere"));
	// DATA CHUTE
	m_stcDATA_CHUTE.SubclassDlgItem(IDC_DATA_CHUTE,this);
	m_stcDATA_CHUTE.SetTextColor(RGB_YELLOW);
	m_stcDATA_CHUTE.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA_CHUTE.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcDATA_CHUTE.SetWindowText(_T("-"));

	// BOX COLLIPRINT
	m_stcBOX_COLLIPRINT.SubclassDlgItem(IDC_BOX_COLLIPRINT,this);
	m_stcBOX_COLLIPRINT.SetTextColor(RGB_WHITE);
	m_stcBOX_COLLIPRINT.SetBkColor(MAINDLG_BKCOLOR);
	m_stcBOX_COLLIPRINT.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcBOX_COLLIPRINT.SetWindowText(_T("N. Ultimi Colli Da Stampare"));
	m_stcBOX_COLLIPRINT.ShowWindow(SW_HIDE);
	// DATA COLLIPRINT
	m_stcDATA_COLLIPRINT.SubclassDlgItem(IDC_DATA_COLLIPRINT,this);
	m_stcDATA_COLLIPRINT.SetTextColor(RGB_YELLOW);
	m_stcDATA_COLLIPRINT.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA_COLLIPRINT.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcDATA_COLLIPRINT.SetWindowText(_T(""));
	m_stcDATA_COLLIPRINT.ShowWindow(SW_HIDE);

	// Button Print Labels
	m_btnLABELS.SubclassDlgItem(IDC_BTN_LABELS,this);
	//m_btnLABELS[nLoop].SetAlign(CButtonST::ST_ALIGN_VERT);
	//m_btnLABELS[nLoop].SetShade(CShadeButtonST::SHS_HARDBUMP,8,10,0,0);
	//m_btnLABELS[nLoop].SetIcon(IDI_ICON_F1+nLoop);
	m_btnLABELS.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_btnLABELS.SetWindowText(_T("Richesta Stampa Label"));
	m_btnLABELS.ShowWindow(SW_HIDE);

	// Cmd Done
	m_stcTEXT_CMDDONE.SubclassDlgItem(IDC_TEXT_CMDDONE,this);
	m_stcTEXT_CMDDONE.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT_CMDDONE.ShowWindow(SW_HIDE);

	// TEXT SKU
	m_stcTEXT_SKU.SubclassDlgItem(IDC_TEXT_SKU,this);
	m_stcTEXT_SKU.SetTextColor(RGB_WHITE);
	m_stcTEXT_SKU.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT_SKU.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT_SKU.SetWindowText(_T("Barcode:"));
	// DATA SKU
	m_stcDATA_SKU.SubclassDlgItem(IDC_EDIT_SKU,this);
	m_stcDATA_SKU.SetTextColor(RGB_BLUE);
	m_stcDATA_SKU.SetBkColor(RGB_YELLOW);
	m_stcDATA_SKU.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXXL(), TRUE);
	m_stcDATA_SKU.SetWindowText(_T(""));
	((CWnd*)GetDlgItem(IDC_EDIT_SKU))->SetFocus();
	//		
	//
	// INFO OPE MANAGEMENT
	//
	//
	m_stcTEXT_INFO.SubclassDlgItem(IDC_TEXT_INFO,this);
	m_stcTEXT_INFO.SetTextColor(RGB_GREEN);
	m_stcTEXT_INFO.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT_INFO.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT_INFO.SetWindowText(_T("Informazioni:"));
	//
	m_stcDATA_INFO.SubclassDlgItem(IDC_DATA_INFO,this);
	m_stcDATA_INFO.SetTextColor(RGB_WHITE);
	m_stcDATA_INFO.SetBkColor(RGB_DARK_GRAY);
	m_stcDATA_INFO.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcDATA_INFO.SetWindowText(_T(""));
	//m_stcDATA_INFO.ShowWindow(SW_HIDE);

	//		
	//
	// ALARMS MANAGEMENT
	//
	//
	m_stcTEXT_ALARM.SubclassDlgItem(IDC_TEXT_ALARM,this);
	m_stcTEXT_ALARM.SetTextColor(RGB_RED);
	m_stcTEXT_ALARM.SetBkColor(MAINDLG_BKCOLOR);
	m_stcTEXT_ALARM.SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	m_stcTEXT_ALARM.SetWindowText(_T("Allarmi:"));
	//
	m_ListBoxALARMS.ResetContent();
	((CWnd*)GetDlgItem(IDC_LISTBOX_ALARMS))->SetFont(G_CoreAppHnd.m_CustomFonts.GetXXS(), TRUE);
	//for(int nLoop=0;nLoop<G_CoreAppHnd.m_pcsMapCORE->nNumElementIncsSTRUCT_RSA;nLoop++)
	{
		//m_sString.Format(_T("%S"),(char*)&G_CoreAppHnd.m_pcsMapCORE->csSTRUCT_RSA[nLoop]);
		CString	sString;
		sString = _T("Waiting for info ........");
		m_ListBoxALARMS.InsertString(0, sString);
	}

	::PostMessage(G_hInterfaceDlg, ON_MYWM_UPDATE_DATEANDTIME, ON_MYWM_UPDATE_DATEANDTIME, 0);

	m_uTimerID = SetTimer(IDT_CODINGDLG_DIALOG,700,NULL);


	//
	// Hide Process And ShellNotify
	//
	#define	TMP_MSG	_T("February,14h 2022 - 09:10")
	CString	sTmp;
	#ifdef _DEBUG
		sTmp.Format(_T("CODPC(D): %s"),TMP_MSG);
	#else
		sTmp.Format(_T("CODPC(R): %s"),TMP_MSG);
	#endif
	#undef	TMP_MSG
	// APPLICATION NAME
	CString	sString;
	sString.Format(_T("%s %03d %s"), G_CoreAppHnd.m_csSTRUCT_CFGTABLE.sCODPC_Description,G_CoreAppHnd.m_csSTRUCT_CFGTABLE.nCODPC_Node,sTmp);
	this->SetWindowText(sString);
/*
	if(MyTaskBarAddIcon(this->m_hWnd,MY_TASKBAR,(_TCHAR*)(LPCTSTR)sString) == FALSE)
		return FALSE;
//	::PostMessage(this->m_hWnd,ON_MYWM_HIDEWINDOW,ON_MYWM_HIDEWINDOW,0);
	//
*/

	#ifndef _DEBUG
		HWND hwnd = ::FindWindow(_T("Shell_traywnd"), NULL);
		::ShowWindow(hwnd,SW_HIDE);

		DisableTaskKeys(TRUE,FALSE);


		#define HKCU HKEY_CURRENT_USER
		// Magic registry key/value for "Remove Task Manager" policy.
		//
		LPCTSTR KEY_DisableTaskMgr = _T("Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System");
		HKEY hk;
		if(RegOpenKey(HKCU,KEY_DisableTaskMgr,&hk) == ERROR_SUCCESS)
		{
			DWORD val=1;
			LPCTSTR VAL_DisableTaskMgr = _T("DisableTaskMgr");
			RegSetValueEx(hk,VAL_DisableTaskMgr,NULL,REG_DWORD,(BYTE*)&val, sizeof(val));
		}
	#endif

	G_CoreAppHnd.m_bSentManualCodingRequestToSNATT = false;


	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCodingPCSnattGoldenGooseDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this);
		CRect rect;
		GetClientRect(rect);
		dc.FillSolidRect(rect, MAINDLG_BKCOLOR);

		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCodingPCSnattGoldenGooseDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CCodingPCSnattGoldenGooseDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	//	CDialog::OnOK();
}

void CCodingPCSnattGoldenGooseDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	#ifdef _DEBUG
		CDialog::OnCancel();
	#endif
}

void CCodingPCSnattGoldenGooseDlg::OnTimer(UINT nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent != (UINT)IDT_CODINGDLG_DIALOG)
		return;
	//
	UpdateOnTimer();

	::PostMessage(G_hInterfaceDlg, ON_MYWM_UPDATE_CODING, ON_MYWM_UPDATE_CODING, 0);

	m_stcDATA_SKU.SetFocus();

	CDialog::OnTimer(nIDEvent);

}

void CCodingPCSnattGoldenGooseDlg::OnMoving(UINT fwSide, LPRECT pRect)
{
	CDialogEx::OnMoving(fwSide, pRect);

	// TODO: Add your message handler code here
	// Find Current location of the dialog

	return;



	CRect CurRect;
	GetWindowRect(&CurRect);

	// Set current location as the moving location
	pRect->left = CurRect.left;
	pRect->top = CurRect.top;
	pRect->right = CurRect.right;
	pRect->bottom = CurRect.bottom;
}

void CCodingPCSnattGoldenGooseDlg::OnSystemExit()
{
	KillTimer(m_uTimerID);
	//
	//		MyTaskBarDeleteIcon();
	//
#ifndef _DEBUG	
	HWND hwnd = ::FindWindow(_T("Shell_traywnd"), NULL);
	::ShowWindow(hwnd, SW_SHOW);
	DisableTaskKeys(FALSE, FALSE);
#define HKCU HKEY_CURRENT_USER
	// Magic registry key/value for "Remove Task Manager" policy.
	//
	LPCTSTR KEY_DisableTaskMgr = _T("Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System");
	HKEY hk;
	if (RegOpenKey(HKCU, KEY_DisableTaskMgr, &hk) == ERROR_SUCCESS)
	{
		DWORD val = 0;
		LPCTSTR VAL_DisableTaskMgr = _T("DisableTaskMgr");
		RegSetValueEx(hk, VAL_DisableTaskMgr, NULL, REG_DWORD, (BYTE*)&val, sizeof(val));
	}
#endif
	CDialog::OnCancel();
}

//
void CCodingPCSnattGoldenGooseDlg::UpdateOnTimer()
{
	static int	nCicle = 0;
	static BOOL	bFlipFlop;
	CString		sString;
	COLORREF	crColor;
	/*
	//
	// Update NotifyIcon
	//
	if(++m_nIconIdx>=2)
	m_nIconIdx=0;

	if(G_CoreAppHnd.IsEveryThingOK() == FALSE)
	{
	m_nIconIdx=2;
	}
	m_csNotifyIconData.hIcon = m_hIconsArray[m_nIconIdx];
	Shell_NotifyIcon(NIM_MODIFY, &m_csNotifyIconData);
	*/
	/*
	//
	// EUIH Link
	//
	G_CoreAppHnd.m_EUIHNetworkLink.GetRGBConnectionState(&crColor);
	m_stcDATA__CHN_EUIH.SetBkColor(crColor);
	sString.Format(_T("S:%5d(%3d) R:%5d(%3d)"),
		G_CoreAppHnd.m_EUIHNetworkLink.GetSentMsgID(), G_CoreAppHnd.m_EUIHNetworkLink.GetSentPrgID(),
		G_CoreAppHnd.m_EUIHNetworkLink.GetReceMsgID(), G_CoreAppHnd.m_EUIHNetworkLink.GetRecePrgID());
	m_stcDATA__CHN_EUIH.SetWindowText(sString);
	*/

	//
	// SNATT Link
	//
	G_CoreAppHnd.m_TCPIPInterfaceSNATTServerFDX.GetRGBConnectionState(&crColor);
	m_stcDATA__CHN_SNATT.SetBkColor(crColor);
	sString.Format(_T("S:%5d(%3d) R:%5d(%3d)"),
		G_CoreAppHnd.m_EUIHNetworkLink.GetSentMsgID(), G_CoreAppHnd.m_EUIHNetworkLink.GetSentPrgID(),
		G_CoreAppHnd.m_EUIHNetworkLink.GetReceMsgID(), G_CoreAppHnd.m_EUIHNetworkLink.GetRecePrgID());
	m_stcDATA__CHN_SNATT.SetWindowText(sString);


	//
	// ISPC Link
	//
	G_CoreAppHnd.m_ISPCNetworkLinkFDXGT.GetRGBConnectionState(&crColor);
	m_stcDATA__CHN_ISPC.SetBkColor(crColor);
	sString.Format(_T("S:%5d(%3d) R:%5d(%3d)"),
		G_CoreAppHnd.m_ISPCNetworkLinkFDXGT.GetSentMsgID(), G_CoreAppHnd.m_ISPCNetworkLinkFDXGT.GetSentPrgID(),
		G_CoreAppHnd.m_ISPCNetworkLinkFDXGT.GetReceMsgID(), G_CoreAppHnd.m_ISPCNetworkLinkFDXGT.GetRecePrgID());
	m_stcDATA__CHN_ISPC.SetWindowText(sString);

	//
	// MSSQL Link
	//
	/*
	if (G_CoreAppHnd.OpenSQLConnection() == TRUE)
	{
		m_stcDATA_MSSQL.SetBkColor(RGB_GREEN);
		m_stcDATA_MSSQL.SetWindowText(_T("Connesso"));
	}
	else
	{
		m_stcDATA_MSSQL.SetBkColor(RGB_RED);
		m_stcDATA_MSSQL.SetWindowText(_T("AVARIA"));
	}
	CallExecuteGET_LABELS_TO_PRINT_QUANTITY(FALSE);
	//
	// reset Cmd Done
	//
	if (m_nResetCmdDone)
	{
		if (--m_nResetCmdDone == 0)
			m_stcTEXT_CMDDONE.ShowWindow(SW_HIDE);
	}
	*/
/*
	//
	// If Operator Not Detected ... Wait
	//
	if (G_CoreAppHnd.IsEveryThingOK() == FALSE)
	{
		//m_stcDATA_ALARM.SetTextColor(RGB_WHITE);
		//m_stcDATA_ALARM.SetBkColor(RGB_RED);
		//m_stcDATA_ALARM.SetWindowText(_T("*** AVARIA COMUNICAZIONE CON SISTEMA PCFO ***"));
		//m_stcDATA_ALARM.ShowWindow(SW_SHOW);
		return;
	}
*/
	//
	switch (nCicle++)
	{
	case 0: // Display Data
		if (false)
			break;

		G_CoreAppHnd.m_cs_DataBase.EnterCriticalSection();
			// Picking
			for (int nLoop = 0; nLoop < MAX_RUNNING_BATCHES; nLoop++)
			{
				sString.Format(_T("%s"), G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE.wzPickingNumberZ[nLoop]);
				m_stcDATA_PICKING[nLoop].SetWindowText(sString);
			}
			// Missing
			for (int nLoop = 0; nLoop < MAX_RUNNING_BATCHES; nLoop++)
			{
				if (G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE.wzPickingNumberZ[nLoop][0] != 0)
					sString.Format(_T("%s"), G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE.wszMissingItemsZ[nLoop]);
				else
					sString = _T("");
				m_stcDATA_MISSING[nLoop].SetWindowText(sString);
			}

			// Chutes
			sString.Format(_T("%d"), G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE.nAvailableChutes);
			m_stcDATA_CHUTE.SetWindowText(sString);

			// Num Ultimi Colli Da Stampare
			sString.Format(_T("%d"), G_CoreAppHnd.m_lLabels);
			m_stcDATA_COLLIPRINT.SetWindowText(sString);
		G_CoreAppHnd.m_cs_DataBase.LeaveCriticalSection();

		// Alarms	
		INFOAlarmsHandling();
		break;
	case 1:
		ISPCAlarmsHandling();
		break;
	default:
		nCicle = 0;
		break;
	}
}

LRESULT CCodingPCSnattGoldenGooseDlg::On_MYWM_UPDATE_DATEANDTIME(WPARAM wParam, LPARAM lParam)
{
	CString	sString;
	UINT uID;

	uID = (UINT)wParam;
	if (uID != ON_MYWM_UPDATE_DATEANDTIME)
		return 0;

	//
	// Update Date And Time
	//
	sString.Format(_T("%02d:%02d:%02d"), G_CoreAppHnd.m_csActualTime.GetHour(), G_CoreAppHnd.m_csActualTime.GetMinute(), G_CoreAppHnd.m_csActualTime.GetSecond());
	m_stcDATA_SYSTEMTIME.SetWindowText(sString);

	static WORD wGetDay = 99;
	if (G_CoreAppHnd.m_csActualTime.GetDay() != wGetDay)
	{
		wGetDay = G_CoreAppHnd.m_csActualTime.GetDay();
		sString.Format(_T("%02d/%02d/%04d"), G_CoreAppHnd.m_csActualTime.GetDay(), G_CoreAppHnd.m_csActualTime.GetMonth(), G_CoreAppHnd.m_csActualTime.GetYear());
		m_stcDATA_SYSTEMDATE.SetWindowText(sString);
	}
	return 0;
}

LRESULT CCodingPCSnattGoldenGooseDlg::On_MYWM_SHOW_ISPCALARM_MESSAGE(WPARAM wParam, LPARAM lParam)
{
	CString	sString;

	wchar_t	wszRow[512];


	::ZeroMemory(wszRow, sizeof(wszRow));
	//
	/*
	if (lParam >= 0 && lParam <MAX_ISPCALARMS)
		//sString.Format(_T("Alm %ld      -->  %s"),lParam,G_CoreAppHnd.m_wszArrayAlm[lParam]);
		sString.Format(_T("%s"), G_CoreAppHnd.m_wszArrayAlm[lParam]);
	else
		sString.Format(_T("Alm %ld E R R O R E   G R A V E"), lParam);
	*/
	//m_stcDATA_ALARM.SetWindowText(sString);
	//m_stcDATA_ALARM.ShowWindow(SW_SHOW);

	return 0;
}

LRESULT CCodingPCSnattGoldenGooseDlg::On_MYWM_UPDATE_CODING(WPARAM wParam, LPARAM lParam)
{
	CString	sString;
	UINT uID;

	uID = (UINT)wParam;
	if (uID != ON_MYWM_UPDATE_CODING)
		return 0;

	static BYTE byXX = 0;


	BOOL bExecute = FALSE;
	if (G_CoreAppHnd.m_bSentManualCodingRequestToSNATT == true)
	{
		m_stcDATA_SKU.EnableWindow(FALSE);

		if (G_CoreAppHnd.m_nTimerWaitingAnswerFromSNATT > 0)
		{
			G_CoreAppHnd.m_nTimerWaitingAnswerFromSNATT--;
			if (G_CoreAppHnd.m_nTimerWaitingAnswerFromSNATT == 0)
				bExecute = TRUE;

			#if GTXSTDAFX_SIMULATE_SNATT_CODING_ACCEPTED
			{
				G_CoreAppHnd.m_bCommandAnswerTelegramReceived = true;
				G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.bAccepted = true;
			}
			#endif


			if (G_CoreAppHnd.m_bCommandAnswerTelegramReceived == true)
			{
				bExecute = TRUE;
				if (G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.bAccepted == true)
				{
					if (G_CoreAppHnd.m_csSTRUCT_CFGTABLE.nISPC_Node == 11 || true)
						G_CoreAppHnd.SendManualCodingToISPC();
					//else if (G_CoreAppHnd.m_csSTRUCT_CFGTABLE.nISPCNode == 21)
					//	G_CoreAppHnd.SendManualCodingToMSC();
				}
			}
		}
		if (bExecute == TRUE)
		{
			m_stcDATA_SKU.SetTextColor(RGB_BLUE);
			m_stcDATA_SKU.SetBkColor(RGB_YELLOW);
			m_stcDATA_SKU.EnableWindow(TRUE);
			if (G_CoreAppHnd.m_bCommandAnswerTelegramReceived == true && G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.bAccepted == true)
				m_stcDATA_SKU.SetWindowText(_T(""));
			else
				m_stcDATA_SKU.SetWindowText(_T("")); // DO IT ANYWAY
			G_CoreAppHnd.m_bSentManualCodingRequestToSNATT = false;
		}
	}
	else
	{
		if (G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE.bIsEnabledManualCoding == TRUE || (G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE.bIsSorterReady == TRUE && G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE.bIsEnabledManualCoding == TRUE))
		{
			m_stcTEXT_SKU.ShowWindow(SW_SHOW);
			m_stcDATA_SKU.ShowWindow(SW_SHOW);
			m_stcDATA_SKU.EnableWindow(TRUE);
		}
		else
		{
			m_stcTEXT_SKU.ShowWindow(SW_HIDE);
			m_stcDATA_SKU.ShowWindow(SW_HIDE);
			m_stcDATA_SKU.EnableWindow(FALSE);
		}
	}

	return 0;
}


void CCodingPCSnattGoldenGooseDlg::INFOAlarmsHandling(void)
{
	static int	nIxAlm = 0;
	static int	nIxBit = 1;
	static int	nNumBit = 0;
	static bool	bAtLeastOneFound = false;

	LPBYTE	pbyINFODIAG = (LPBYTE)&G_CoreAppHnd.m_byINFODIAG;

	// KeepAlive ISPC
	if (G_CoreAppHnd.m_wISPCTOCODPCKeepAliveTimer == 0)
		G_CoreAppHnd.m_byINFODIAG |= 0x01;
	else
		G_CoreAppHnd.m_byINFODIAG &= ~0x01;

	// KeepAlive MSC
	if (G_CoreAppHnd.m_wMSCTOCODPCKeepAliveTimer == 0)
		G_CoreAppHnd.m_byINFODIAG |= 0x02;
	else
		G_CoreAppHnd.m_byINFODIAG &= ~0x02;

	// KeepAlive SNATT
	if (G_CoreAppHnd.m_nSNATTToCODPCKeepAliveTimer <= 0)
		G_CoreAppHnd.m_byINFODIAG |= 0x04;
	else
		G_CoreAppHnd.m_byINFODIAG &= ~0x04;


	G_CoreAppHnd.m_byINFODIAG = 0;


	// Check Messages
CheckAgain:
	BOOL bFound = FALSE;
	for (; nIxAlm < sizeof(G_CoreAppHnd.m_byINFODIAG); nIxAlm++)
	{
		for (; nIxBit < 256; nIxBit <<= 1, nNumBit++)
		{
			if (*(pbyINFODIAG + nIxAlm) & nIxBit)
			{
				bFound = TRUE;
				LONG lIdx = (LONG)((nIxAlm * 8) + nNumBit);

				if (lIdx == 0)
					m_stcDATA_INFO.SetWindowText(_T("m_wISPCTOCODPCKeepAliveTimer"));
				else if (lIdx == 1)
					m_stcDATA_INFO.SetWindowText(_T("m_wMSCTOCODPCKeepAliveTimer"));
				else if (lIdx == 2)
					m_stcDATA_INFO.SetWindowText(_T("m_wSNATTToCODPCKeepAliveTimer"));

				m_stcDATA_INFO.ShowWindow(SW_SHOW);

				bAtLeastOneFound = true;

				nIxBit <<= 1;
				nNumBit++;
				break;
			}
		}
		if (bFound == TRUE)
			break;
		nIxBit = 1;
		nNumBit = 0;
	}

	if (bAtLeastOneFound == false)
	{
		m_stcDATA_INFO.SetWindowText(_T(""));
		//m_stcDATA_INFO.ShowWindow(SW_HIDE);
	}

	if (nIxAlm >= sizeof(G_CoreAppHnd.m_byINFODIAG))
	{
		nIxAlm = 0;
		nIxBit = 1;
		nNumBit = 0;
		if (bAtLeastOneFound == true)
		{
			bAtLeastOneFound = false;
			goto CheckAgain;
		}
	}
}

void CCodingPCSnattGoldenGooseDlg::ISPCAlarmsHandling(void)
{
	bool bIsThereAnyAlarm = false;
	WORD wWarnings = 0;

	//G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[0].dwAlarm[0] = 0xFFFFFFFF;
	//G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[0].dwAlarm[1] = 0xFFFFFFFF;

	// GROUPCABINETS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSCABINET; nLoop++)
	{
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[nLoop].dwAlarm[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[nLoop].dwAlarm[1])
			bIsThereAnyAlarm = true;
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[nLoop].dwWarning[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[nLoop].dwWarning[1])
			bIsThereAnyAlarm = true;
	}
	// CABINETS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSCABINET; nLoop++)
	{
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nLoop].dwAlarm[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nLoop].dwAlarm[1])
			bIsThereAnyAlarm = true;
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nLoop].dwWarning[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nLoop].dwWarning[1])
			bIsThereAnyAlarm = true;
	}
	// ZONE
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSZONE; nLoop++)
	{
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_ZONE[nLoop].dwAlarm[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_ZONE[nLoop].dwAlarm[1])
			bIsThereAnyAlarm = true;
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_ZONE[nLoop].dwWarning[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_ZONE[nLoop].dwWarning[1])
			bIsThereAnyAlarm = true;
	}
	// BELTS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSBELT; nLoop++)
	{
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_BELT[nLoop].dwAlarm[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_BELT[nLoop].dwAlarm[1])
			bIsThereAnyAlarm = true;
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_BELT[nLoop].dwWarning[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_BELT[nLoop].dwWarning[1])
			bIsThereAnyAlarm = true;
	}
	// MODULATORS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSMODULATOR; nLoop++)
	{
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_MODULATOR[nLoop].dwAlarm[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_MODULATOR[nLoop].dwAlarm[1])
			bIsThereAnyAlarm = true;
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_MODULATOR[nLoop].dwWarning[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_MODULATOR[nLoop].dwWarning[1])
			bIsThereAnyAlarm = true;
	}
	// COILS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSCOIL; nLoop++)
	{
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_COIL[nLoop].dwAlarm[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_COIL[nLoop].dwAlarm[1])
			bIsThereAnyAlarm = true;
		if (G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_COIL[nLoop].dwWarning[0] || G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_COIL[nLoop].dwWarning[1])
			bIsThereAnyAlarm = true;
	}

	if(bIsThereAnyAlarm == false && false)
	{
		m_ListBoxALARMS.ResetContent();
		m_ListBoxALARMS.ShowWindow(SW_HIDE);
	}
	else
	{
		m_ListBoxALARMS.ResetContent();
		m_ListBoxALARMS.ShowWindow(SW_SHOW);
		BuildAlarmOrWarningList();
		G_CoreAppHnd.TRACEDIAG(_T("CCodingPCSnattGoldenGooseDlg::ISPCAlarmsHandling    ENTER \n"));
	}
}

void CCodingPCSnattGoldenGooseDlg::BuildAlarmOrWarningList(void)
{
	CString sItemID;



	// CHECK GROUP CABINET ALARMS & WARNINGS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSCABINET; nLoop++)
	{
		sItemID.Format(_T("GROUPCABINETAL%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[nLoop].dwAlarm[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayGROUPCABINETAL[nLoop]);
		sItemID.Format(_T("GROUPCABINETWR%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[nLoop].dwWarning[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayGROUPCABINETWR[nLoop]);
	}

	// CHECK CABINET ALARMS & WARNINGS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSCABINET; nLoop++)
	{
		sItemID.Format(_T("CABINETAL%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nLoop].dwAlarm[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayCABINETAL[nLoop]);
		sItemID.Format(_T("CABINETWR%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nLoop].dwWarning[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayCABINETWR[nLoop]);
	}

	// CHECK ZONE ALARMS & WARNINGS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSZONE; nLoop++)
	{
		sItemID.Format(_T("ZONEAL%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_ZONE[nLoop].dwAlarm[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayZONEAL[nLoop]);
		sItemID.Format(_T("ZONEWR%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_ZONE[nLoop].dwWarning[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayZONEWR[nLoop]);
	}

	// CHECK BELT ALARMS & WARNINGS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSBELT; nLoop++)
	{
		CString sItemID;
		sItemID.Format(_T("BELTAL%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_BELT[nLoop].dwAlarm[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayBELTAL[nLoop]);
		sItemID.Format(_T("BELTWR%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_BELT[nLoop].dwWarning[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayBELTWR[nLoop]);
	}

	// CHECK MODULATOR ALARMS & WARNINGS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSMODULATOR; nLoop++)
	{
		CString sItemID;
		sItemID.Format(_T("MODULATORAL%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_MODULATOR[nLoop].dwAlarm[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayMODULATORAL[nLoop]);
		sItemID.Format(_T("MODULATORWR%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_MODULATOR[nLoop].dwWarning[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayMODULATORWR[nLoop]);
	}

	// CHECK COIL ALARMS & WARNINGS
	for (int nLoop = 0; nLoop < DIAG_SIZE_CSCOIL; nLoop++)
	{
		CString sItemID;
		sItemID.Format(_T("COILAL%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_COIL[nLoop].dwAlarm[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayCOILAL[nLoop]);
		sItemID.Format(_T("COILWR%02d"), nLoop + 1);
		for (int nIxALM = 0; nIxALM < DWALARM_SIZE; nIxALM++)
			InsertAlarmOrWarningIntoCLISTBOX(G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_COIL[nLoop].dwWarning[nIxALM], nIxALM, sItemID, G_CoreAppHnd.m_sArrayCOILWR[nLoop]);
	}

	int nID;
	while ((nID = m_ListBoxALARMS.GetCount()) > 200 && false)
		m_ListBoxALARMS.DeleteString(m_ListBoxALARMS.GetCount() - 1);
}

void CCodingPCSnattGoldenGooseDlg::InsertAlarmOrWarningIntoCLISTBOX(DWORD dwAlarm, int nOfs, LPCTSTR lpszAlarmType, CString* psAlarmMsg)
{
	int nNumBit = 0;
	for (DWORD64 dw64Bit = 1; dw64Bit < 0xFFFFFFFF; dw64Bit <<= 1, nNumBit++)
	{
		if (dwAlarm & dw64Bit)
		{
			//G_CoreAppHnd.TRACEDIAG(_T("nIdx %d   dw64Bit %032I64u   nNumBit %d\n"),nLoop,dw64Bit,nNumBit);
			//G_CoreAppHnd.TRACEDIAG(_T("nIdx %d   dw64Bit %016X       nNumBit %d\n"),nLoop,dw64Bit,nNumBit);
			int nIdx = (int)((nOfs * 32) + nNumBit);
			nIdx %= 64;
			CString sDateTime;
			//sDateTime.Format(_T("%02d/%02d/%04d %02d:%02d:%02d"),G_CoreAppHnd.m_csActualTime.GetDay(),G_CoreAppHnd.m_csActualTime.GetMonth(),G_CoreAppHnd.m_csActualTime.GetYear(),
			//				 G_CoreAppHnd.m_csActualTime.GetHour(),G_CoreAppHnd.m_csActualTime.GetMinute(),G_CoreAppHnd.m_csActualTime.GetSecond());
			sDateTime.Format(_T("%02d:%02d:%02d"), G_CoreAppHnd.m_csActualTime.GetHour(), G_CoreAppHnd.m_csActualTime.GetMinute(), G_CoreAppHnd.m_csActualTime.GetSecond());
			CString sString;
			sString.Format(_T("%s %s[%02d] - %s"),sDateTime,lpszAlarmType,nIdx,psAlarmMsg[nIdx]);
			//sString.Format(_T("%s: %s"), sDateTime, psAlarmMsg[nIdx]);

			m_ListBoxALARMS.InsertString(-1, sString);
			G_CoreAppHnd.TRACEDIAG(_T("CCodingPCSnattGoldenGooseDlg::InsertAlarmOrWarningIntoCLISTBOX   %s \n"), sString);
		}
	}
}


void CCodingPCSnattGoldenGooseDlg::SendManualCodingToSNATT(void)
{
	if (G_CoreAppHnd.m_bSentManualCodingRequestToSNATT == true)
		return;

	int nBCLen = m_stcDATA_SKU.GetWindowTextLength();
	char cCodeID;
	cCodeID = CODE_UNKNOWN_ID;

	if (nBCLen < 4)
		return;
	/*
	switch (nBCLen)
	{
	case CODE_EAN8_LEN:
		cCodeID = CODE_EAN8_ID;
		break;
	case CODE_EAN13_LEN:
		cCodeID = CODE_EAN13_ID;
		break;
	case CODE_CODE128_LEN:
		cCodeID = CODE_CODE128_ID_09DIGITS;
		break;
	case CODE_CODE128_LEN_18DIGITS:
		cCodeID = CODE_CODE128_ID_18DIGITS;
		break;
	case CODE_CODE128_LEN_20DIGITS:
		cCodeID = CODE_CODE128_ID_20DIGITS;
		break;
	case CODE_CODEITF_LEN:
		cCodeID = CODE_CODEITF_ID;
		break;
	default:
		if (nBCLen >= 8 && nBCLen <= 15)
			cCodeID = CODE_UPC_ID;
		else
			cCodeID = CODE_UNKNOWN_ID;
		break;
	}
	if (cCodeID == CODE_UNKNOWN_ID)
		return;
	*/

	::ZeroMemory(&G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST, sizeof(G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST));
	G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST.nCmdID = CODPCTOSNATT_COMMANDREQUEST__BARCODE_VALIDATION_REQUEST;
	G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST.nUUID = 1;
	G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST.nBarcodeLength = nBCLen;
	if (nBCLen > SIZEOF_BARCODE__DATA)
		return;
	CString	sString;
	m_stcDATA_SKU.GetWindowText(sString);
	::WideCharToMultiByte(CP_ACP, NULL, sString, -1, (char*)&G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST.szBarcodeDataZ, SIZEOF_BARCODE__DATA, NULL, NULL);
	strncpy_s(G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST.szBarcodeSymbologyZ, SIZEOF_BARCODE__SYMBOLOGY, "Code128cooooooooooooooooooooooooooooooooo", _TRUNCATE);
	strncpy_s(G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST.szBarcodeAIMCodeZ, SIZEOF_BARCODE__AIM, "]C????ooooooooooooooooooooooooooooooooo", _TRUNCATE);

	//
	G_CoreAppHnd.m_nTimerWaitingAnswerFromSNATT = 7;
	G_CoreAppHnd.m_bSentManualCodingRequestToSNATT = true;
	G_CoreAppHnd.m_bCommandAnswerTelegramReceived = false;

	#if GTXSTDAFX_SIMULATE_SNATT_CODING_ACCEPTED
	{
		G_CoreAppHnd.m_bCommandAnswerTelegramReceived = true;
		G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.bAccepted = true;
	}
	#endif

	G_CoreAppHnd.SendCommandRequestToSNATT(G_CoreAppHnd.m_csCODPCTOSNATT_COMMANDREQUEST);

	//	m_stcDATA_SKU.SetWindowText(_T(""));
	m_stcDATA_SKU.SetTextColor(RGB_BLACK);
	m_stcDATA_SKU.SetBkColor(MAINDLG_BKCOLOR);
	m_stcDATA_SKU.EnableWindow(FALSE);
}

BOOL CCodingPCSnattGoldenGooseDlg::PreTranslateMessage(MSG* pMsg)
{
	BOOL bRet = TRUE;
	BYTE byCase = 0;
	CString	sCode;

	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		byCase++;
		break;
	case WM_KEYUP:
		byCase++;
		break;
	case WM_CHAR:
		byCase++;
		break;
	case WM_DEADCHAR:
		byCase++;
		break;
	case WM_SYSKEYDOWN:
		byCase++;
		break;
	case WM_SYSKEYUP:
		byCase++;
		break;
	case WM_SYSCHAR:
		byCase++;
		break;
	case WM_SYSDEADCHAR:
		byCase++;
		break;
	case WM_KEYLAST:
		byCase++;
		break;
	}
	if (pMsg->message == WM_CHAR)
	{
		TRACE(_T("WM_CHAR: wParam %X %X \n"), pMsg->wParam, pMsg->lParam);
		/*
		// selezione numerica
		if((pMsg->lParam & 0xff0000) >  0x10000 && (pMsg->lParam & 0xff0000) <  0xC0000)
		{
		pMsg->lParam = 0;
		pMsg->wParam = 0;
		}
		// codifica alfabetica
		if((pMsg->wParam  >  0x60 && pMsg->wParam <  0x7B) ||(pMsg->wParam  >  0x40 && pMsg->wParam <  0x5B))
		{
		pMsg->lParam = 0;
		pMsg->wParam = 0;
		}
		*/
	}
	else if (pMsg->message == WM_KEYDOWN)
	{
		TRACE(_T("WM_KEYDOWN: wParam %X %X \n"), pMsg->wParam, pMsg->lParam);

		G_CoreAppHnd.KEYPADPreTranslateMessage(pMsg);

		if (pMsg->wParam == VK_RETURN)
		{
			pMsg->wParam = 0;
			pMsg->lParam = 0;

			if (G_CoreAppHnd.IsEveryThingOK() == TRUE)
			{
				if (G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE.bIsEnabledManualCoding == TRUE || (G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE.bIsSorterReady == TRUE && G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE.bIsEnabledManualCoding == TRUE))
					SendManualCodingToSNATT();
			}
		}
		switch (pMsg->wParam)
		{
		case VK_UP:  // Arrow UP
			break;
		case VK_DOWN:  // Arrow DOWN
			break;
		case VK_LEFT:  // Arrow LEFT
		case VK_RIGHT:  // Arrow RIGHT
			break;
		case VK_DELETE:  // "Canc"
			break;
		case VK_DIVIDE:  // ":"
			break;
		case VK_MULTIPLY:  // "x"
			break;
		case VK_ADD:  // "+"
			break;
		case VK_INSERT:  // "Ins"
			break;
		case VK_HOME:  // "Home"
			break;
		case VK_TAB:  // "Tab"
		case VK_NEXT: // "Pagedown"
			break;
		case VK_PRIOR:	// "Pageup"
			break;
		case VK_SPACE:  // "Space"
			m_stcDATA_SKU.SetWindowText(_T(""));
			break;
		case VK_PAUSE:  // "Pause"
			break;
		case VK_END:  // "End"
			break;
		case VK_BACK:  // "BackSpace" 
			break;
		case VK_RETURN:  // "Return"
			break;
		case VK_F1:   // Function key F1
		case VK_F2:   // Function key F2
		case VK_F3:   // Function key F3
		case VK_F4:   // Function key F4
		case VK_F5:   // Function key F5
		case VK_F6:   // Function key F6
		case VK_F7:   // Function key F7
		case VK_F8:   // Function key F8
		case VK_F9:   // Function key F9
		case VK_F10:  // Function key F10
		case VK_F11:  // Function key F11
		case VK_F12:  // Function key F12
			byCase--;
			break;
		}
	}
	else if (pMsg->message == WM_SYSKEYDOWN)
	{
		TRACE(_T("WM_SYSKEYDOWN: wParam %X %X \n"), pMsg->wParam, pMsg->lParam);

		if (pMsg->wParam == 0x79 && pMsg->lParam == 0x20440001)  // ALT+F10 "ShutDown"
		{
			TRACE(_T("ShutDown \n"));
			pMsg->wParam = 0;
			pMsg->lParam = 0;
#ifndef _DEBUG
			G_CoreAppHnd.ShutDownWINDOWSNT(TRUE);
#endif
		}
		if (pMsg->wParam == 0x7B && pMsg->lParam == 0x20580001)  // ALT+F12 "SystemExit"
		{
			TRACE(_T("SystemExit \n"));
			pMsg->wParam = 0;
			pMsg->lParam = 0;
			OnSystemExit();
		}
	}
	return CWnd::PreTranslateMessage(pMsg);
} // End of PreTranslateMessage



void CCodingPCSnattGoldenGooseDlg::OnEnChangeEditSku()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	int nLen = m_stcDATA_SKU.GetWindowTextLength();
	if (nLen <= 0)
		return;
}

void CCodingPCSnattGoldenGooseDlg::CallExecuteGET_LABELS_TO_PRINT_QUANTITY(BOOL bExecuteImmediately)
{
	/*
	if (++G_CoreAppHnd.m_nCallSP > 40 || bExecuteImmediately == TRUE)
	{
		G_CoreAppHnd.m_nCallSP = 0;
		if (G_CoreAppHnd.ExecuteGET_LABELS_TO_PRINT_QUANTITY() == TRUE)
			m_stcDATA_COLLIPRINT.SetTextColor(RGB_GREEN);
		else
			m_stcDATA_COLLIPRINT.SetTextColor(RGB_RED);
	}
	*/
}

void CCodingPCSnattGoldenGooseDlg::OnBnClickedBtnLabels()
{
	// TODO: Add your control notification handler code here
/*
	BOOL bRet;

	bRet = G_CoreAppHnd.ExecutePRINT_LABELS();
	if (bRet == TRUE)
	{
		m_stcTEXT_CMDDONE.SetTextColor(RGB_GREEN);
		m_stcTEXT_CMDDONE.SetBkColor(MAINDLG_BKCOLOR);
		m_stcTEXT_CMDDONE.SetWindowText(_T("OK"));
		m_stcTEXT_CMDDONE.ShowWindow(SW_SHOW);
	}
	else
	{
		m_stcTEXT_CMDDONE.SetTextColor(RGB_WHITE);
		m_stcTEXT_CMDDONE.SetBkColor(RGB_RED);
		m_stcTEXT_CMDDONE.SetWindowText(_T("KO"));
		m_stcTEXT_CMDDONE.ShowWindow(SW_SHOW);
	}
	m_nResetCmdDone = 4;
*/
}


HBRUSH CCodingPCSnattGoldenGooseDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here

	// TODO:  Return a different brush if the default is not desired
	switch (nCtlColor)
	{
	case CTLCOLOR_STATIC:
		/*
		switch (pWnd->GetDlgCtrlID())
		{
		case IDC_DATA_CODLINKOAMENG:
		case IDC_DATA_CODLINKISPC:
		case IDC_DATA_CODLINKZEBRAZM400BCR:
		case IDC_DATA_CODSYSREADY:
		case IDC_BOX_CODCIO:
		case IDC_TEXT_CODCIO:
		case IDC_TEXT_CODCIOMSG:
		case IDC_BOX_CODINFORMACION:
		case IDC_TEXT_CODRECHAZODESC:
		case IDC_TEXT_CODRECHAZONUM:
		case IDC_BOX_CODCE:
		case IDC_TEXT_CODCPORIGEN:
		case IDC_TEXT_CODCPDESTINO:
			//case IDC_DATA_CODSYSREADY:
			break;
		default:
			hbr = m_bkBackGroundColor;
			pDC->SetBkColor(m_crBackGroundColor);
			pDC->SetTextColor(GUI_MAINDLG_BKCOLOR);
			break;
		}
		*/
		break;
	case CTLCOLOR_EDIT:
	case CTLCOLOR_MSGBOX:
		/*
		switch (pWnd->GetDlgCtrlID())
		{
		case IDC_EDIT_CODCIO:
		case IDC_EDIT_CODCPDESTINO:
		case IDC_EDIT_CODCPORIGEN:
			hbr = m_bkBackGroundCODFIELDS;
			pDC->SetBkColor(m_crBackGroundCODFIELDS);
			pDC->SetTextColor(RGB_BLACK);
			break;
		}
		*/
		break;

		/*
				switch(pWnd->GetDlgCtrlID())
				{
				case IDC_TEXT_OPMODE:
					if(m_byGGG==0)
						pDC->SetTextColor(RGB(0,128,0));
					else if(m_byGGG==1)
						pDC->SetTextColor(RGB(128,0,64));
					else
						pDC->SetTextColor(RGB(0,0,200));
					break;
				}
				break;
		*/
		/*
					case IDC_CHECK_STATISTICS_SORTER:
					case IDC_CHECK_STATISTICS_ISPC11:
					case IDC_CHECK_STATISTICS_ISPC12:
					case IDC_EXPORT_STATISTICS:
						pDC->SetBkColor(m_clrBgDlgColor);
						pDC->SetTextColor(m_clrFgColor);
		//	m_brBgDlgColor.CreateSolidBrush(m_clrBgDlgColor);
		//	m_brFgDlgColor.CreateSolidBrush(RGB_RED);
						hbr = (HBRUSH) m_brBgDlgColor;
					break;
					default:
					break;
				}*/
		break;
		/*
			case CTLCOLOR_DLG:
				return static_cast<HBRUSH> (m_bkBackGroundColor.GetSafeHandle());
		*/
		/*
			case CTLCOLOR_BTN:
				pDC->SetTextColor(RGB(255,255,255));
		*/
		break;
		//		case CTLCOLOR_EDIT:
		//		case CTLCOLOR_LISTBOX:
		//		case CTLCOLOR_MSGBOX:
		//		case CTLCOLOR_SCROLLBAR:
		//		case CTLCOLOR_STATIC:
					//pDC->SetTextColor(RGB(0, 255, 0));
					//pDC->SetBkColor(RGB(0, 0, 0));
					//return (HBRUSH)(m_bkBrush.GetSafeHandle());
		/*
			case CTLCOLOR_EDIT:
			case CTLCOLOR_MSGBOX:
				switch(pWnd->GetDlgCtrlID())
				{
					// first CEdit control ID
					//case IDC_EDIT_SYSPARX0:
						pDC->SetBkColor(m_clrBgDlgColor);	// change the background
						pDC->SetTextColor(m_clrFgColor);	// white change the text color
						hbr = (HBRUSH) m_brBgDlgColor;		// apply the blue brush
						break;
				}
				break;
		*/
	case CTLCOLOR_LISTBOX:
		hbr = m_bkBackGroundCODFIELDS;
		pDC->SetBkColor(m_crBackGroundCODFIELDS);
		pDC->SetTextColor(RGB_WHITE);
		break;
	default:
		break;
	}



	return hbr;
}
