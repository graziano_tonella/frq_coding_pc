//////////////////////////////////////////////////////////////////
//																//
// NAME:					ThreadCLOCK.cpp						//
// LAST MODIFICATION DATE:	07-Nov-2017							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					System Clock Timers					//
//////////////////////////////////////////////////////////////////
#include "pch.h"
#include "EXTERN.H"



UINT ThreadCLOCK(LPVOID)
{
	BOOL				bIsThreadRunning = TRUE;
	static STRUCT_SINGLEMSGCML	csSINGLEMSGCML;
	static STRUCT_TCPIPMSG		csSTRUCT_TCPIPMSG;
	DWORD				dwBytesCopied;
	DWORD				dwWaitStatus;
	int					nSeconds=0;
	int					nEndShift=0;
	CHighResolutionTimerGT	HRTimer;
	

	while(bIsThreadRunning)
	{
		enum{WAIT_TIMEOUT_THREAD_CLOCK=100};
		dwWaitStatus = ::WaitForSingleObject(G_CoreAppHnd.m_hHandleEvents[HANDLE_THREAD__CLOCK],WAIT_TIMEOUT_THREAD_CLOCK);
		switch(dwWaitStatus)
		{
		case WAIT_OBJECT_0:
			// Check MailBox Messages
			while(G_CoreAppHnd.m_FlatQueueCLOCK.GetCount())
			{
				if(G_CoreAppHnd.m_FlatQueueCLOCK.GetItem((LPBYTE)&csSINGLEMSGCML,TRUE,&dwBytesCopied) != QUEUEGT_OK)
					continue;
				switch(csSINGLEMSGCML.csHeader.wMsgId)
				{
				case wMSGID_GENERIC__KILLTHREAD:
					csSINGLEMSGCML.csHeader.wMsgId = wMSGID_GENERIC__KILLTHREAD;
					csSINGLEMSGCML.csHeader.wDataLen = 0;
					G_CoreAppHnd.m_FlatQueueAPPHND.PutItem((LPBYTE)&csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
					//
					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_GENERIC__KILLTHREAD;
					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH	= 0;
					G_CoreAppHnd.m_FlatQueueTRACEHND.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));

					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_GENERIC__KILLTHREAD;
					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = 0;
					G_CoreAppHnd.m_FlatQueueTRACEHND.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
					G_CoreAppHnd.m_FlatQueueSNATT.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
					bIsThreadRunning = FALSE;
					break;
				default:
					break;
				}
			}
			break; 
		case WAIT_TIMEOUT:
			// WakeUp Management
			csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_WAKEUP__100_MILLISECONDS;
			csSINGLEMSGCML.csHeader.wDataLen = 0;

			// Update System Clock
			G_CoreAppHnd.m_csActualTime = CTime::GetCurrentTime();

			//
			// One Second
			//
			static int nGetSecond  = -1;
			if(G_CoreAppHnd.m_csActualTime.GetSecond() != nGetSecond)
			{
				nGetSecond = G_CoreAppHnd.m_csActualTime.GetSecond();
				nSeconds++;
				//
				::PostMessage(G_hInterfaceDlg, ON_MYWM_UPDATE_DATEANDTIME, ON_MYWM_UPDATE_DATEANDTIME, 0);

				csSINGLEMSGCML.csHeader.wMsgId = wMSGID_WAKEUP__ONE_SECOND;
				csSINGLEMSGCML.csHeader.wDataLen = 0;
				G_CoreAppHnd.m_FlatQueueAPPHND.PutItem((LPBYTE)&csSINGLEMSGCML, (DWORD)(SIZE_STRUCT_HEADERMSGCML + csSINGLEMSGCML.csHeader.wDataLen));

				csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_WAKEUP__ONE_SECOND;
				csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = 0;
				G_CoreAppHnd.m_FlatQueueSNATT.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));

				//
				// Timers Based On OneSecond Clock
				if((nSeconds % 2) == 0)
				{
					csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_WAKEUP__TWO_SECONDS;
					csSINGLEMSGCML.csHeader.wDataLen = 0;

				}
				if((nSeconds % 3) == 0)
				{
					csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_WAKEUP__THREE_SECONDS;
					csSINGLEMSGCML.csHeader.wDataLen = 0;
					//G_CoreAppHnd.m_FlatQueueAPPHND.PutItem((LPBYTE)&csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));

					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_WAKEUP__THREE_SECONDS;
					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = 0;
					//G_CoreAppHnd.m_FlatQueueSNATT.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
				}
				if((nSeconds % 5) == 0)
				{
					csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_WAKEUP__FIVE_SECONDS;
					csSINGLEMSGCML.csHeader.wDataLen = 0;
					//G_CoreAppHnd.m_FlatQueueAPPHND.PutItem((LPBYTE)&csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
					//G_CoreAppHnd.m_FlatQueueRETURN.PutItem((LPBYTE)&csSINGLEMSGCML, (DWORD)(SIZE_STRUCT_HEADERMSGCML + csSINGLEMSGCML.csHeader.wDataLen));
					//
					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_WAKEUP__FIVE_SECONDS;
					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = 0;
					G_CoreAppHnd.m_FlatQueueSNATT.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
				}
				if((nSeconds % 30) == 0)
				{
					nSeconds = 0;
					// Update OAMGUI Statistics
					//csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_UPDATE__OAMGUI_STATISTICS_EVERY30SECONDS;
					csSINGLEMSGCML.csHeader.wDataLen = 0;
					//G_CoreAppHnd.m_FlatQueueMISSION.PutItem((LPBYTE)&csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
				}
			}
			//
			// Every Five Minutes
			//
			static bool bFiveMinutes = false;
			if((G_CoreAppHnd.m_csActualTime.GetMinute() == 0 || ((G_CoreAppHnd.m_csActualTime.GetMinute()%5) == 0)) && bFiveMinutes == false)
			{
				bFiveMinutes = true;
				//
				/*
				tm cstm;
				cstm.tm_mday = (int) G_CoreAppHnd.m_csActualTime.GetDay();
				cstm.tm_mon = (int) G_CoreAppHnd.m_csActualTime.GetMonth();
				cstm.tm_year = (int) G_CoreAppHnd.m_csActualTime.GetYear();
				cstm.tm_hour = (int) G_CoreAppHnd.m_csActualTime.GetHour();
				cstm.tm_min = (int) G_CoreAppHnd.m_csActualTime.GetMinute();
				cstm.tm_sec = (int) G_CoreAppHnd.m_csActualTime.GetSecond();
				*/
				//
				// Update Statistics
				if(true)
				{
					//csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_WAKEUP_EVERY_FIVEMINUTES;
					//csSINGLEMSGCML.csHeader.wDataLen = 0;
					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_WAKEUP__EVERY_FIVEMINUTES;
					csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = 0;
					//G_CoreAppHnd.m_FlatQueueOAMGUI.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER)+csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
					//G_CoreAppHnd.m_FlatQueueOUTPUT.PutItem((LPBYTE)&csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
				}
				//TRACE(_T("G_CoreAppHnd.m_csActualTime TRUE %d \r\n"),G_CoreAppHnd.m_csActualTime.GetMinute());
			}
			if(bFiveMinutes == true && G_CoreAppHnd.m_csActualTime.GetMinute() > 0 && ((G_CoreAppHnd.m_csActualTime.GetMinute()%5) !=0))
			{
				bFiveMinutes = false;
				//TRACE(_T("G_CoreAppHnd.m_csActualTime FALSE %d \r\n"),G_CoreAppHnd.m_csActualTime.GetMinute());
			}
			//
			// Every Minute
			//
			static int nGetMinute = -1;
			if(G_CoreAppHnd.m_csActualTime.GetMinute() != nGetMinute)
			{
				nGetMinute = G_CoreAppHnd.m_csActualTime.GetMinute();
				//
				//csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_WAKEUP__EVERY_MINUTE;
				//csSINGLEMSGCML.csHeader.wDataLen = 0;
				csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_WAKEUP__EVERY_MINUTE;
				csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = 0;
				//G_CoreAppHnd.m_FlatQueueAPPHND.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
				//
				//
				// Handle Change SHIFT
				//
				//
				nEndShift =  G_CoreAppHnd.m_csActualTime.GetHour() * 60;
				nEndShift += G_CoreAppHnd.m_csActualTime.GetMinute();
			}
			//
			// Every Hour
			//
			static int nGetHour = -1;
			if(G_CoreAppHnd.m_csActualTime.GetHour() != nGetHour)
			{
				nGetHour = G_CoreAppHnd.m_csActualTime.GetHour();
				//
				csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_WAKEUP__EVERY_HOUR;
				csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = 0;
				G_CoreAppHnd.m_FlatQueueTRACEHND.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
			}
			break;
		}
	}
	return 0;
}
