//////////////////////////////////////////////////////////////////
// NAME:					CoreAppHnd.cpp						//
// AUTHOR:					Graziano Tonella					//
// LAST MODIFICATION DATE:	31-Jan-2022							//
//																//
// PURPOSE:					Application							//
//////////////////////////////////////////////////////////////////
//
//
//
#include "pch.h"

#include "EXTERN.H"
#include <io.h>

#pragma once
//
//
//
CCoreAppHnd::CCoreAppHnd()
{
	m_wISPCTOCODPCKeepAliveTimer = 0;
	::ZeroMemory(&m_csISPCTOPCCODKEEPALIVE,sizeof(m_csISPCTOPCCODKEEPALIVE));

	m_wMSCTOCODPCKeepAliveTimer = 0;
	::ZeroMemory(&m_csMSCTOPCCODKEEPALIVE, sizeof(m_csMSCTOPCCODKEEPALIVE));

	m_nSNATTToCODPCKeepAliveTimer = 0;

	m_byINFODIAG = 0;

	::ZeroMemory(&m_csSTRUCT_DIAG_ISPC, sizeof(m_csSTRUCT_DIAG_ISPC));
	
	m_nCallSP = 9999;

	m_csISPCTOPCCODKEEPALIVE.bIsEnabledManualCoding = FALSE;
}
//
CCoreAppHnd::~CCoreAppHnd()
{
	::CoUninitialize();

}
//	- This Function Init The External Unit Interface Application
//
// INPUT  PARAMETERS:
//	- Nothing
//
// OUTPUT PARAMETERS:
//	- Nothing
//
// RETURN:
//	- TRUE	Init Succesfully Executed
//	- FALSE	Init Failed
//
BOOL CCoreAppHnd::InitApplication()
{
	DWORD			dwRetVal;
	WSADATA			wsaData;
	CString			sString;
	CToolsLibraryGT	ToolsLibrary;

	//
	// Create Mutex In Order To Detect MySelf
	//
	#define	TMP_APPLICATION_NAME _T("CodingPCGoldenGoose VS2017")
	m_hMyMutex = ::CreateMutex(NULL,TRUE,TMP_APPLICATION_NAME);
	if(::GetLastError() == ERROR_ALREADY_EXISTS)
	{
		::MessageBox(NULL,_T("Program Already Active"),TMP_APPLICATION_NAME,MB_ICONINFORMATION);
		return FALSE;
	}
	#undef	TMP_APPLICATION_NAME


	// 
	// VERIFY / CREATE APPLICATION DIRECTORIES
	//
	if (ToolsLibrary.CheckDirectory(PATH__XMLFILES) == FALSE)
	{
		sString.Format(_T("Directory %s Not Accessible Or Not Found"), PATH__XMLFILES);
		::AfxMessageBox(sString, MB_ICONSTOP);
		return FALSE;
	}
	if (ToolsLibrary.CheckDirectory(PATH__TXTFILES) == FALSE)
	{
		sString.Format(_T("Directory %s Not Accessible Or Not Found"), PATH__TXTFILES);
		::AfxMessageBox(sString, MB_ICONSTOP);
		return FALSE;
	}
	if (ToolsLibrary.CheckDirectory(PATH__TRACEFILES) == FALSE)
	{
		sString.Format(_T("Directory %s Not Accessible Or Not Found"), PATH__TRACEFILES);
		::AfxMessageBox(sString, MB_ICONSTOP);
		return FALSE;
	}

	//
	// LOAD CONFIGURATION FILE
	//
	if (LoadCODPCApplicationCfgTable() == FALSE)
	{
		::AfxMessageBox(_T("Error Parsing LoadCODPCApplicationCfgTable.xml File"), MB_ICONSTOP);
		return FALSE;
	}


	::CoInitialize(NULL);

	//
	// Start NetWork
	//
	if(::WSAStartup(MAKEWORD(2,0),&wsaData) != 0)
	{
		::AfxMessageBox(_T("Error On WSAStartup"),MB_ICONSTOP);
		return FALSE;
	}

	// Create Handles
	for (int nLoop = 0; nLoop<NUM_APPLICATION__HANDLES; nLoop++)
	{
		m_hHandleEvents[nLoop] = ::CreateEvent(NULL, FALSE, FALSE, NULL);
		if (m_hHandleEvents[nLoop] != NULL)
			continue;
		::AfxMessageBox(_T("Error Creating Handles"), MB_ICONSTOP);
		return FALSE;
	}

	// Load Application Language Messages
	sString.Format(_T("%s%s"), PATH__XMLFILES, _T("CODPCApplicationMsg.xml"));
	if (m_MLAppMsg.Create(sString) == FALSE)
	{
		::AfxMessageBox(_T("Error Parsing CODPCApplicationMsg.xml"), MB_ICONSTOP);
		return FALSE;
	}
	m_MLAppMsg.SetDefaultLanguage(CMultiLanguageMsgGT::MULTILANGUAGEMSG_ZERO);
	G_CoreAppHnd.m_MLAppMsg.SetShowMsgId(FALSE);

	//
	// Load ISPC ILGRP Alarms File
	//
	if (LoadSWPFiles(true) == FALSE)
	{
		AfxMessageBox(_T("Error Loading ILGRPXY TXT Files"), MB_ICONSTOP);
		return FALSE;
	}

	//
	// Load ISPC IL01 Alarms File
	//
	if (LoadSWPFiles(false) == FALSE)
	{
		AfxMessageBox(_T("Error Loading ILXY TXT Files"), MB_ICONSTOP);
		return FALSE;
	}

	// Thread CLOCK
	m_pArrayTHREADS[THREAD__CLOCK] = AfxBeginThread(ThreadCLOCK, 0, THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
	if (m_pArrayTHREADS[THREAD__CLOCK] == NULL)
	{
		AfxMessageBox(_T("Error Creation ThreadCLOCK !"), MB_ICONSTOP);
		return FALSE;
	}
	m_pArrayTHREADS[THREAD__CLOCK]->m_bAutoDelete = FALSE;

	// Thread APPLICATION HANDLER
	m_pArrayTHREADS[THREAD__APPHND] = AfxBeginThread(ThreadAPPHND, 0, THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
	if (m_pArrayTHREADS[THREAD__APPHND] == NULL)
	{
		AfxMessageBox(_T("Error Creation ThreadAPPHND !"), MB_ICONSTOP);
		return FALSE;
	}
	m_pArrayTHREADS[THREAD__APPHND]->m_bAutoDelete = FALSE;

	// Thread TRACE HANDLER
	m_pArrayTHREADS[THREAD__TRACEHND] = AfxBeginThread(ThreadTRACEHND, 0, THREAD_PRIORITY_BELOW_NORMAL, 0, CREATE_SUSPENDED);
	if (m_pArrayTHREADS[THREAD__TRACEHND] == NULL)
	{
		AfxMessageBox(_T("Error Creation ThreadTRACEHND !"), MB_ICONSTOP);
		return FALSE;
	}
	m_pArrayTHREADS[THREAD__TRACEHND]->m_bAutoDelete = FALSE;

	// Thread SNATT HANDLER
	m_pArrayTHREADS[THREAD__SNATT] = AfxBeginThread(ThreadSNATT, 0, THREAD_PRIORITY_BELOW_NORMAL, 0, CREATE_SUSPENDED);
	if (m_pArrayTHREADS[THREAD__SNATT] == NULL)
	{
		AfxMessageBox(_T("Error Creation ThreadSNATT !"), MB_ICONSTOP);
		return FALSE;
	}
	m_pArrayTHREADS[THREAD__SNATT]->m_bAutoDelete = FALSE;

	//
	//
	// Create Application Queues
	//
	//
	if (m_FlatQueueCLOCK.Create(MSGCML_MAX_DATALEN * 3, NULL, 0, m_hHandleEvents[HANDLE_THREAD__CLOCK], NULL) != QUEUEGT_OK)
	{
		AfxMessageBox(_T("FlatQueueCLOCK Creation Failed !"), MB_ICONSTOP);
		return FALSE;
	}

	if (m_FlatQueueAPPHND.Create(MSGCML_MAX_DATALEN * 10, NULL, 0, NULL, m_pArrayTHREADS[THREAD__APPHND]->m_hThread) != QUEUEGT_OK)
	{
		AfxMessageBox(_T("m_FlatQueueAPPHND Creation Failed !"), MB_ICONSTOP);
		return FALSE;
	}

	if (m_FlatQueueTRACEHND.Create((sizeof(STRUCT_TCPIPENVELOPE) + sizeof(STRUCT_TCPIPMSG) + sizeof(STRUCT_TCPIPTAIL)) * 3, NULL, 0, NULL, m_pArrayTHREADS[THREAD__TRACEHND]->m_hThread) != QUEUEGT_OK)
	{
		AfxMessageBox(_T("FlatQueueTRACEHND Creation Failed !"), MB_ICONSTOP);
		return FALSE;
	}

	if (m_FlatQueueSNATT.Create((sizeof(STRUCT_TCPIPENVELOPE) + sizeof(STRUCT_TCPIPMSG) + sizeof(STRUCT_TCPIPTAIL)) * 1, NULL, 0, NULL, m_pArrayTHREADS[THREAD__SNATT]->m_hThread) != QUEUEGT_OK)
	{
		AfxMessageBox(_T("m_FlatQueueSNATT Creation Failed !"), MB_ICONSTOP);
		return FALSE;
	}

	//
	//
	// Resume Threads
	//
	//
	//
	m_pArrayTHREADS[THREAD__CLOCK]->ResumeThread();
	m_pArrayTHREADS[THREAD__APPHND]->ResumeThread();
	m_pArrayTHREADS[THREAD__TRACEHND]->ResumeThread();
	m_pArrayTHREADS[THREAD__SNATT]->ResumeThread();

/*
	//
	// EUIH Network
	//
	#define OAMPC_NODE	101
	m_EUIHNetworkLink.SetProperties(m_csSTRUCT_CFGTABLE.nCODPC_Node, OAMPC_NODE);
	#undef OAMPC_NODE
	m_EUIHNetworkLink.ExcludeProtocolControls(FALSE,FALSE,FALSE,FALSE);
	dwRetVal = m_EUIHNetworkLink.Create(NETWORKST_SOCKET_CLIENT,m_csSTRUCT_CFGTABLE.sDDE_IPAddress, m_csSTRUCT_CFGTABLE.nDDE_IPPort, MSGCML_MAX_DATALEN, 100, 9000, &m_FlatQueueAPPHND, MSGCML_MAX_DATALEN * 10, 5000, _T("CHOAM"));
	if(dwRetVal != NETWORKST_OK)
	{
		AfxMessageBox(_T("m_EUIHNetworkLink Socket Creation Failed !"),MB_ICONSTOP);
		return FALSE;
	}
*/
	//
	// ISPC Network Link
	//
	m_ISPCNetworkLinkFDXGT.SetProperties((BYTE)m_csSTRUCT_CFGTABLE.nCODPC_Node, (BYTE)m_csSTRUCT_CFGTABLE.nISPC_Node);
	m_ISPCNetworkLinkFDXGT.ExcludeProtocolControls(FALSE, FALSE, FALSE, FALSE);
	#if GTXSTDAFX_SIMULATE_SNATT_CODING_ACCEPTED
		dwRetVal = m_ISPCNetworkLinkFDXGT.Create(NETWORKST_SOCKET_SERVER, _T("172.24.2.26"), m_csSTRUCT_CFGTABLE.nISPC_IPPort, MSGCML_MAX_DATALEN, 10, 10000, &m_FlatQueueAPPHND, MSGCML_MAX_DATALEN * 100, 5000, _T("ISPC"));
	#else
		dwRetVal = m_ISPCNetworkLinkFDXGT.Create(NETWORKST_SOCKET_CLIENT, m_csSTRUCT_CFGTABLE.sISPC_IPAddress, m_csSTRUCT_CFGTABLE.nISPC_IPPort, MSGCML_MAX_DATALEN, 10, 10000, &m_FlatQueueAPPHND, MSGCML_MAX_DATALEN * 100, 5000, _T("ISPC"));
	#endif
	if (dwRetVal != NETWORKST_OK)
	{
		AfxMessageBox(_T("m_ISPCNetworkLinkFDXGT Socket Creation Failed !"), MB_ICONSTOP);
		return FALSE;
	}

	//
	// SNATT Network Link
	//
	VERIFY(m_TCPIPInterfaceSNATTServerFDX.SetRXProperties((BYTE*)"\x02", (BYTE*)"\x03", 0, 900, wMSGID_SNATT__TELEGRAM, wMSGID_SNATT__DIAGNOSTIC) == TRUE);
	m_TCPIPInterfaceSNATTServerFDX.SetLogToFile(_T("C:\\FRQ-SnattGoldenGoose\\CodingPCSnattGoldenGoose\\TraceFiles\\"));
	UINT uSNATT_ReceiveTimeout = 150000;
	//if (m_TCPIPInterfaceSNATTServerFDX.Create(NETWORKST_SOCKET_SERVER, _T("10.10.5.230"), 30011, sizeof(STRUCT_TCPIPMSG), 50, uSNATT_ReceiveTimeout, &m_FlatQueueSNATT, sizeof(STRUCT_TCPIPMSG), _T("SNATT")) == FALSE)
	if (m_TCPIPInterfaceSNATTServerFDX.Create(NETWORKST_SOCKET_SERVER, m_csSTRUCT_CFGTABLE.sSNATT_IPAddress, m_csSTRUCT_CFGTABLE.nSNATT_IPPort, sizeof(STRUCT_TCPIPMSG), 50, uSNATT_ReceiveTimeout, &m_FlatQueueSNATT, sizeof(STRUCT_TCPIPMSG), _T("SNATT")) == FALSE)
	{
		AfxMessageBox(_T("m_TCPIPInterfaceSNATTServerFDX Socket Creation Failed !"), MB_ICONSTOP);
		return FALSE;
	}


	return TRUE;
}

//	- This Function Destroy The Current Application
//
// INPUT  PARAMETERS:
//	- Nothing
//
// OUTPUT PARAMETERS:
//	- Nothing
//
// RETURN:
//	- Nothing
//
void CCoreAppHnd::DestroyApplication()
{
	int		nLoop;
	CString	sString;
	//
	// Release My Identification Name
	//
	if(::ReleaseMutex(m_hMyMutex) == FALSE) // I'm Not The Owner
		::CloseHandle(m_hMyMutex);
	//
	// Application Exit
	//
	STRUCT_SINGLEMSGCML	csSINGLEMSGCML;
	csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_GENERIC__KILLTHREAD;
	csSINGLEMSGCML.csHeader.wDataLen = 0;
	m_FlatQueueCLOCK.PutItem((LPBYTE)&csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
	//
	// Stop Threads
	//
	try
	{
		for(nLoop=0;nLoop<NUM_APPLICATION__THREADS;nLoop++)
		{
			switch(::WaitForSingleObject(m_pArrayTHREADS[nLoop]->m_hThread,4000))
			{
			case WAIT_OBJECT_0: // Means OK...
				VERIFY(TRUE);
				break;
			default:
				{
					CString	sString;
					CFile	TraceFile;
					sString = _T("D:\\WaitForSingleObject.FOR.FALSE.txt");
					SYSTEMTIME	csSystemTime;
					::GetLocalTime(&csSystemTime);
					char szDateTime[50];
					sprintf_s(szDateTime, sizeof(szDateTime), "%02d/%02d/%02d %02d:%02d:%02d - ", csSystemTime.wDay, csSystemTime.wMonth, csSystemTime.wYear, csSystemTime.wHour, csSystemTime.wMinute, csSystemTime.wSecond);
					char szBuff[50];
					sprintf_s(szBuff, sizeof(szBuff), "Thread %d", nLoop);
					if (TraceFile.Open(sString, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
					{
						TraceFile.SeekToEnd();
						TraceFile.Write(szDateTime, strlen(szDateTime));
						TraceFile.Write(szBuff, strlen(szBuff));
						TraceFile.Write("\r\n", 2);
						TraceFile.Close();
					}
				}
				VERIFY(FALSE);
				break;
			}
	 	}
		m_EUIHNetworkLink.Destroy(2000);
		m_ISPCNetworkLinkFDXGT.Destroy(2000);
		m_TCPIPInterfaceSNATTServerFDX.Destroy(2000);
		//
		// Delete Threads
		//
		for(nLoop=0;nLoop<NUM_APPLICATION__THREADS;nLoop++)
			delete m_pArrayTHREADS[nLoop];
		//
		// Save Queues Values
		//
		//sString.Format(_T("%s%s"),_T(PATH_DATABASE),_T("QueuePLC.DAT"));
		//m_FlatQueuePLC.SaveQueue(sString);
		//
		// Delete Application Queues
		//
		m_FlatQueueCLOCK.Destroy();
		m_FlatQueueAPPHND.Destroy();
		m_FlatQueueTRACEHND.Destroy();
		m_FlatQueueSNATT.Destroy();

		// Release Handles
		for (int nLoop = 0; nLoop<NUM_APPLICATION__HANDLES; nLoop++)
		{
			if (m_hHandleEvents[nLoop] == NULL)
				continue;
			::CloseHandle(m_hHandleEvents[nLoop]);
		}

	}
	catch(...)
	{
		::WSACleanup();
		::ExitProcess(-1);
	}
	::WSACleanup();
}

//	- This Function Generates A Trace Message Towards RTX System
//
// INPUT  PARAMETERS:
//	- Argument List As a sprintf
//
// OUTPUT PARAMETERS:
//	- Nothing
//
// RETURN:
//	- Nothing
//
void CCoreAppHnd::TRACEDIAG(LPCTSTR lpszFormat, ...)
{
	va_list	args;
	va_start(args, lpszFormat);

	static STRUCT_TCPIPMSG		csSTRUCT_TCPIPMSG;
	int		nBuf;
	_TCHAR	szBuffer[1024];

	SYSTEMTIME	csSystemTime;
	CString		sString;

	m_cs_TraceDiag.EnterCriticalSection();
		try
		{
			nBuf = _vsntprintf_s(szBuffer, sizeof(szBuffer) / sizeof(TCHAR), _TRUNCATE, lpszFormat, args);
			if (nBuf < 0)
				_tcscpy_s(szBuffer, sizeof(szBuffer) / sizeof(_TCHAR), _T("_TRUNCATE"));

			::GetLocalTime(&csSystemTime);
			sString.Format(_T("%02d:%02d:%02d-%03d --> %s"), csSystemTime.wHour, csSystemTime.wMinute, csSystemTime.wSecond, csSystemTime.wMilliseconds, szBuffer);
			//TRACE(sString);

			::ZeroMemory(&csSTRUCT_TCPIPMSG, sizeof(csSTRUCT_TCPIPMSG));
			::WideCharToMultiByte(CP_ACP, NULL, sString, -1, (char*)&csSTRUCT_TCPIPMSG.byMsgDATA, sizeof(csSTRUCT_TCPIPMSG.byMsgDATA) - 1, NULL, NULL);
			csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = wMSGID_TRACEHND__TRACEDIAG;
			csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = (WORD)strlen((char*)&csSTRUCT_TCPIPMSG.byMsgDATA);
			G_CoreAppHnd.m_FlatQueueTRACEHND.PutItem((LPBYTE)&csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
		}
		catch (...)
		{
			m_cs_TraceDiag.LeaveCriticalSection();
			va_end(args);
			return;
		}
	m_cs_TraceDiag.LeaveCriticalSection();

	va_end(args);
}


//	- This Function ShutDown The Windows NT Operating system
//
// INPUT  PARAMETERS:
//	- Argument List As a sprintf
//
// OUTPUT PARAMETERS:
//	- Nothing
//
// RETURN:
//	- Nothing
//
void CCoreAppHnd::ShutDownWINDOWSNT(BOOL bPowerOFF)
{
	if(bPowerOFF == FALSE)
	{
		BOOL				bRetValue;
		HANDLE				hProcessHandle;
		HANDLE				hTokenHandle;
		TOKEN_PRIVILEGES	NewState;
		//
		//
		//
		hProcessHandle	= ::GetCurrentProcess();
		hTokenHandle	= NULL;
		//		
		bRetValue = ::OpenProcessToken(hProcessHandle,TOKEN_ADJUST_PRIVILEGES,&hTokenHandle);
		if(bRetValue == 0)
			return;
		NewState.PrivilegeCount = 1;
		::LookupPrivilegeValue(NULL,SE_SHUTDOWN_NAME,&NewState.Privileges[0].Luid);
		NewState.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		//
		bRetValue = ::AdjustTokenPrivileges(hTokenHandle,FALSE,&NewState,NULL,NULL,NULL);
		if(bRetValue == 0)
			return;
		::InitiateSystemShutdown(NULL,_T("System SHUT-DOWN Started"),7,TRUE,FALSE);
	}
	else
	{
		::Sleep(1500);

		HANDLE				hToken; 
		TOKEN_PRIVILEGES	tkp; 

		// Get a token for this process. 
		if(!OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) 
		{
			//error("OpenProcessToken"); 
			return;
		}

		// Get the LUID for the shutdown privilege. 
		LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,&tkp.Privileges[0].Luid); 
		tkp.PrivilegeCount = 1;  // one privilege to set    
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
 
		// Get the shutdown privilege for this process. 
		AdjustTokenPrivileges(hToken,FALSE,&tkp,0,(PTOKEN_PRIVILEGES)NULL, 0); 
 
		// Cannot test the return value of AdjustTokenPrivileges. 
		if(GetLastError() != ERROR_SUCCESS) 
		{
			//error("AdjustTokenPrivileges"); 
			return;
		}
 
		// Shut down the system and force all applications to close. 
 
		if(!ExitWindowsEx(EWX_POWEROFF | EWX_FORCEIFHUNG, 0)) 
		//if(!ExitWindowsEx(EWX_SHUTDOWN | EWX_FORCE, 0)) 
		{
			//error("ExitWindowsEx"); 
			return;
		}
	}
}

BOOL CCoreAppHnd::LoadSWPFiles(bool bGroup)
{
	CStdioFile		StdUsersFile;
	CFileException	ex;
	CString			sString;
	BOOL			bSuccess = TRUE;
	BOOL			bRetCode = FALSE;
	int				nLine = 0;
	int				nExpectedToken = 11;

	if(bGroup == false)
		sString.Format(_T("%s%s%1d.txt"), PATH__TXTFILES, _T("Alarms_RO_IL0"), m_csSTRUCT_CFGTABLE.nCODPC_Node%10);
	else
		sString.Format(_T("%s%s.txt"), PATH__TXTFILES, _T("Alarms_RO_ILGRP01"));
	try
	{
		//if (StdUsersFile.Open(sString, CFile::sha::shareExclusive | CFile::modeNoTruncate | CFile::modeRead | CFile::typeText | CFile::typeUnicode, &ex) == FALSE)
		if (StdUsersFile.Open(sString, CFile::shareDenyNone | CFile::modeNoTruncate | CFile::modeRead | CFile::typeText | CFile::typeUnicode, &ex) == FALSE)
		{
			G_CoreAppHnd.TRACEDIAG(_T("CCoreAppHnd::LoadSWPFiles - StdUsersFile.Open returns FALSE \n"));
			return FALSE;
		}

		CString	sLine;
		while (StdUsersFile.ReadString(sLine))
		{
			nLine++;

			CStringTokenizerGT	StringTokenizer;
			if (StringTokenizer.Tokenizer(sLine, _T(",")) == FALSE)
			{
				G_CoreAppHnd.TRACEDIAG(_T("CCoreAppHnd::LoadSWPFiles - StringTokenizer.Tokenizer returns FALSE \n"));
				bSuccess = FALSE;
				break;
			}
			if (StringTokenizer.GetCountTokens() != nExpectedToken)
			{
					G_CoreAppHnd.TRACEDIAG(_T("CCoreAppHnd::LoadSWPFiles - StringTokenizer.GetCountTokens   Expected[%d]   returned[%d] At Line[%d] \n"), nExpectedToken, StringTokenizer.GetCountTokens(), nLine);
					bSuccess = FALSE;
					break;
			}

/*
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_CABINET[];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_ZONE[];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_BELT[];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_MODULATOR[];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_COIL[];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_GROUPCABINET[DIAG_SIZE_CSCABINET];





*/

			CString	sKEY;
			CString	sAL_WR;
			CString	sELEMENT;
			CString	sBIT;
			int		nIdx;
			int		nELEMENT;
			int		nBIT;

			sKEY = StringTokenizer.GetTokenAt(1);
			if ((nIdx = sKEY.Find(_T("ZONE"), 0)) != -1)
			{
				sAL_WR = sKEY.GetAt(nIdx + 4);
				sAL_WR += sKEY.GetAt(nIdx + 5);
				sELEMENT = sKEY.GetAt(nIdx + 6);
				sELEMENT += sKEY.GetAt(nIdx + 7);
				sBIT = sKEY.GetAt(nIdx + 8);
				sBIT += sKEY.GetAt(nIdx + 9);

				nELEMENT = (int)_wtoi(sELEMENT) % DIAG_SIZE_CSZONE;
				if (nELEMENT >= 1)
					nELEMENT--;
				else
					VERIFY(FALSE);

				nBIT = (int)_wtoi(sBIT) % 64;

				if (sAL_WR == _T("AL"))
					m_sArrayZONEAL[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
				else if (sAL_WR == _T("WR"))
					m_sArrayZONEWR[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
			}
			else if ((nIdx = sKEY.Find(_T("CABINET"), 0)) != -1)
			{
				sAL_WR = sKEY.GetAt(nIdx + 7);
				sAL_WR += sKEY.GetAt(nIdx + 8);
				sELEMENT = sKEY.GetAt(nIdx + 9);
				sELEMENT += sKEY.GetAt(nIdx + 10);
				sELEMENT += sKEY.GetAt(nIdx + 11);
				sBIT = sKEY.GetAt(nIdx + 12);
				sBIT += sKEY.GetAt(nIdx + 13);

				nELEMENT = (int)_wtoi(sELEMENT) % DIAG_SIZE_CSCABINET;

				if (sAL_WR == _T("AL") || sAL_WR == _T("WR"))
				{
					if (nELEMENT >= 1)
						nELEMENT--;
					else
						VERIFY(FALSE);

					nBIT = (int)_wtoi(sBIT) % 64;

					if (sAL_WR == _T("AL"))
					{
						if (bGroup == false)
							m_sArrayCABINETAL[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
						else
							m_sArrayGROUPCABINETAL[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
					}
					else if (sAL_WR == _T("WR"))
					{
						if (bGroup == false)
							m_sArrayCABINETWR[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
						else
							m_sArrayGROUPCABINETWR[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
					}
				}
			}
			else if ((nIdx = sKEY.Find(_T("BELT"), 0)) != -1)
			{
				sAL_WR = sKEY.GetAt(nIdx + 4);
				sAL_WR += sKEY.GetAt(nIdx + 5);
				sELEMENT = sKEY.GetAt(nIdx + 6);
				sELEMENT += sKEY.GetAt(nIdx + 7);
				sBIT = sKEY.GetAt(nIdx + 8);
				sBIT += sKEY.GetAt(nIdx + 9);

				nELEMENT = (int)_wtoi(sELEMENT) % DIAG_SIZE_CSBELT;
				if (nELEMENT >= 1)
					nELEMENT--;
				else
					VERIFY(FALSE);

				nBIT = (int)_wtoi(sBIT) % 64;

				if (sAL_WR == _T("AL"))
					m_sArrayBELTAL[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
				else if (sAL_WR == _T("WR"))
					m_sArrayBELTWR[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
			}
			else if ((nIdx = sKEY.Find(_T("MODULATOR"), 0)) != -1)
			{
				sAL_WR = sKEY.GetAt(nIdx + 9);
				sAL_WR += sKEY.GetAt(nIdx + 10);
				sELEMENT = sKEY.GetAt(nIdx + 11);
				sELEMENT += sKEY.GetAt(nIdx + 12);
				sBIT = sKEY.GetAt(nIdx + 13);
				sBIT += sKEY.GetAt(nIdx + 14);

				nELEMENT = (int)_wtoi(sELEMENT) % DIAG_SIZE_CSMODULATOR;
				if (nELEMENT >= 1)
					nELEMENT--;
				else
					VERIFY(FALSE);

				nBIT = (int)_wtoi(sBIT) % 64;

				if (sAL_WR == _T("AL"))
					m_sArrayMODULATORAL[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
				else if (sAL_WR == _T("WR"))
					m_sArrayMODULATORWR[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
			}
			else if ((nIdx = sKEY.Find(_T("COIL"), 0)) != -1)
			{
				sAL_WR = sKEY.GetAt(nIdx + 4);
				sAL_WR += sKEY.GetAt(nIdx + 5);
				sELEMENT = sKEY.GetAt(nIdx + 6);
				sELEMENT += sKEY.GetAt(nIdx + 7);
				sBIT = sKEY.GetAt(nIdx + 8);
				sBIT += sKEY.GetAt(nIdx + 9);

				nELEMENT = (int)_wtoi(sELEMENT) % DIAG_SIZE_CSCOIL;
				if (nELEMENT >= 1)
					nELEMENT--;
				else
					VERIFY(FALSE);

				nBIT = (int)_wtoi(sBIT) % 64;

				if (sAL_WR == _T("AL"))
					m_sArrayCOILAL[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
				else if (sAL_WR == _T("WR"))
					m_sArrayCOILWR[nELEMENT][nBIT] = StringTokenizer.GetTokenAt(9);
			}
		}
		StdUsersFile.Close();
	}
	catch (CFileException *ex)
	{
		int		nErrorCode;
		CString	sErrorDescription;

		nErrorCode = ex->m_cause;
		ex->GetErrorMessage(sErrorDescription.GetBuffer(sErrorDescription.GetLength()), _MAX_PATH);
		sErrorDescription.ReleaseBuffer();

		G_CoreAppHnd.TRACEDIAG(_T("CCoreAppHnd::LoadSWPFiles - CFileException Code[%d - %s] \n"), nErrorCode, sErrorDescription);

		ex->Delete();
		return FALSE;
	}
	catch (...)
	{
		G_CoreAppHnd.TRACEDIAG(_T("CCoreAppHnd::LoadSWPFiles - CATCH DETECTED \n"));
		return FALSE;
	}
	if (bSuccess == TRUE)
	{
		//G_MMFDB.m_pcsMapDTB->csSTRUCT_GEDEPHY_$200.byRoutesFileOK = 1;
		//G_MMFDB.m_pcsMapDTB->csSTRUCT_GEDEPHY_$200.byRoutesFileErrorCode = 0; // No Error Code
		return TRUE;
	}

	return FALSE;
}
