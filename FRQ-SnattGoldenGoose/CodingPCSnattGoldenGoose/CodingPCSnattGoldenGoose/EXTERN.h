//////////////////////////////////////////////////////////////////
// NAME:					Extern.h							//
// AUTHOR:					Graziano Tonella					//
// LAST MODIFICATION DATE:	27-Sep-2021							//
//																//
// PURPOSE:					It collects all definition and		//
//							include files useful to both class	//
//							thread definition                   //
//////////////////////////////////////////////////////////////////
#pragma once

//
// SYSTEM INCLUDES
//
#include <stdio.h>
#include <process.h>
#include <tchar.h>
//#include <direct.h>
//#include <errno.h>
//#include <memory.h>
//#include <string.h>
//#include <stdlib.h>
//#include <malloc.h>
#include <conio.h>
#include <time.h>
#include <assert.h>
//#include <io.h>

//
//////////////////////////
//	WINDOWS WM_USER IDs	//
//////////////////////////
enum
{
	ON_MYWM_NOTIFYICON	= WM_USER + 1,
	ON_MYWM_HIDEWINDOW,
	//
	ON_MYWM_SHOW_ISPCALARM_MESSAGE,
	ON_MYWM_SHOW_INFOALARM_MESSAGE,
	//
	ON_MYWM_UPDATE_DATEANDTIME,
	ON_MYWM_UPDATE_CODING
};

#include "..\..\OAMCommonDefinesVS2017\OAMCommonDefinesx64.h"

//
// APPLICATION INCLUDES
//
#include "..\..\OAMCommonDefinesVS2017\OAMDefaultFonts.h"
#include "..\..\OAMCommonDefinesVS2017\x64ToolsLibraryGT.h"
#include "..\..\OAMCommonDefinesVS2017\ObjectToolsGT.h"
#include "..\..\OAMCommonDefinesVS2017\x64HighResolutionTimerGT.h"
#include "..\..\OAMCommonDefinesVS2017\x64CriticalSectionGT.h"
#include "..\..\OAMCommonDefinesVS2017\x64MappedFileST.h"
#include "..\..\OAMCommonDefinesVS2017\x64SharedFlatQueueGT.h"
#include "..\..\OAMCommonDefinesVS2017\ServerClientFDXGT.h"
#include "..\..\OAMCommonDefinesVS2017\TCPIPInterfaceFDXGT.h"
#include "..\..\OAMCommonDefinesVS2017\SharedFileGT.h"
#include "..\..\OAMCommonDefinesVS2017\SharedQueueST.h"
#include "..\..\OAMCommonDefinesVS2017\MultiLanguageMsgGT.h"
#include "..\..\OAMCommonDefinesVS2017\x64MyADO.h"
#include "..\..\OAMCommonDefinesVS2017\XMLDocument.h"
#include "..\..\OAMCommonDefinesVS2017\XMLNodeWrapper.h"
#include "..\..\OAMCommonDefinesVS2017\StringTokenizerGT.h"
#include "..\..\OAMCommonDefinesVS2017\TCPIPServerClientFDXGT.h"

#include "..\..\OAMCommonDefinesVS2017\OAMGoldenGooseDefines.h"

/*
//
// APPLICATION INCLUDES
//
////#include "CommonPL3.h"
#include "CMLDefines.h"
#include "OAMRTXDefines.h"
#include "PCCODDefines.h"

#include "SharedFlatQueueGT.H"
#include "SharedFileGT.H"
#include "HighResolutionTimerGT.H"
#include "CriticalSectionGT.H"
/*
#include "SharedQueueST.h"
#include "MappedFileST.h"

#include "ServerClientFDXGT.h"
#include "SerialComGT.h"

#include "NetUtilsST.h"
#include "RegistryST.h"

#include "Msg.h"

#include "MyADO.h"

*/
#include "CoreAppHnd.H"

//
/////////////////
// BASE CLOCKs //
/////////////////
#define TICK_BASE_50MS				50
#define TIMER_WAKEUP_100MSEC		(100/TICK_BASE_50MS)
#define TIMER_WAKEUP_300MSEC		(300/TICK_BASE_50MS)
#define TIMER_WAKEUP_500MSEC		(500/TICK_BASE_50MS)
#define TIMER_WAKEUP_ONE_SEC		(1000/TICK_BASE_50MS)
#define TIMER_WAKEUP_THREE_SECS		(3000/TICK_BASE_50MS)
#define TIMER_WAKEUP_FIVE_SECS		(5000/TICK_BASE_50MS)
#define TIMER_WAKEUP_ONE_MINUTE		(60000/TICK_BASE_50MS)
//
//#define MY_TASKBAR	112233
//////////////////////////
//	WINDOWS TIMERS IDs	//
//////////////////////////
enum
{
	IDT_CODINGDLG_DIALOG	= 1000,
//	IDT_DEBUG_DIALOG,
//	IDT_MANCOD_DIALOG,
//	IDT_LOGIN_DIALOG,
//	IDT_LISTALOTTI_DIALOG,
	IDT_NEXT_TIMER
};
//
//////////////////////////
//	QUEUE MESSAGE IDs	//
//////////////////////////
enum
{
	wMSGID_EVENT__GENERIC = 1000,

	wMSGID_GENERIC__KILLTHREAD,

	wMSGID_TRACEHND__TRACEDIAG,

	wMSGID_WAKEUP__100_MILLISECONDS,
	wMSGID_WAKEUP__ONE_SECOND,
	wMSGID_WAKEUP__TWO_SECONDS,
	wMSGID_WAKEUP__THREE_SECONDS,
	wMSGID_WAKEUP__FIVE_SECONDS,
	wMSGID_WAKEUP__EVERY_MINUTE,
	wMSGID_WAKEUP__EVERY_FIVEMINUTES,
	wMSGID_WAKEUP__EVERY_HOUR,
	wMSGID_WAKEUP__EIGHTOCLOCK,

	wMSGID_SNATT__TELEGRAM,
	wMSGID_SNATT__DIAGNOSTIC,


	wMSGID_AVAILABLE
};
//
//#define	MAINDLG_BKCOLOR		RGB(192,192,192) // WINDOWS DEFAULT
//#define	MAINDLG_BKCOLOR		RGB(56,140,70)
#define	MAINDLG_BKCOLOR		RGB(0,0,150) // BLUE
#define	INFODLG_BKCOLOR		RGB(14,205,182)
//#define	CODINGDLG_BKCOLOR	RGB(111,149,168)
#define	CODINGDLG_BKCOLOR	RGB(14,205,182)
//#define	YESNO_BKCOLOR	RGB(111,149,168)
#define	YESNO_BKCOLOR		RGB(14,205,182)
#define	LOGIN_BKCOLOR		RGB(111,149,168)
#define	LISTALOTTI_BKCOLOR	RGB(80,80,80)
//
#define	RGB_BLACK			RGB(0,0,0)
#define	RGB_RED				RGB(255,0,0)
#define	RGB_DARK_RED		RGB(128,0,0)
#define	RGB_MAGENTA			RGB(255,0,255)
#define	RGB_GREEN			RGB(0,255,0)
#define	RGB_YELLOW			RGB(255,255,0)
#define	RGB_BLUE			RGB(0,0,255)
#define	RGB_CYAN			RGB(0,200,255)
#define	RGB_WHITE			RGB(255,255,255)
#define	RGB_OCRA			RGB(255,128,64)
#define	RGB_DARK_GRAY		RGB(80,80,80)
#define	RGB_LIGHT_OCRA		RGB(241,220,171)
//
//
#define CARRIAGE_RETURN		0x0D
#define LINE_FEED			0x0A


extern	CCoreAppHnd		G_CoreAppHnd;
extern	HWND			G_hInterfaceDlg;

extern  UINT			ThreadCLOCK(LPVOID);
extern  UINT			ThreadAPPHND(LPVOID);
extern  UINT			ThreadTRACEHND(LPVOID);
extern  UINT			ThreadSNATT(LPVOID);
