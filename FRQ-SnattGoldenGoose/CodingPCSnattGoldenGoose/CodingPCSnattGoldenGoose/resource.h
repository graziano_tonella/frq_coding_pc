//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CodingPCSnattGoldenGoose.rc
//
#define IDD_CODINGPCSNATTGOLDENGOOSE_DIALOG 102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     130
#define IDB_BITMAP2                     132
#define IDC_DATA_SYSTEMTIME             1000
#define IDC_DATA_SYSTEMDATE             1001
#define IDC_DATA__CHN_SNATT             1002
#define IDC_TEXT__CHN_SNATT             1003
#define IDC_EDIT_SKU                    1004
#define IDC_BOX_BATCH                   1005
#define IDC_TEXT_MSSQL                  1006
#define IDC_DATA_MSSQL                  1007
#define IDC_BTN_LABELS                  1008
#define IDC_TEXT_CMDDONE                1009
#define IDC_DATA_INFO                   1011
#define IDC_TEXT_INFO                   1012
#define IDC_TEXT_ALARM                  1013
#define IDC_BOX_CHUTE                   1014
#define IDC_DATA_CHUTE                  1015
#define IDC_TEXT_SKU                    1016
#define IDC_TEXT_PICKING                1017
#define IDC_TEXT_MISSING                1018
#define IDC_BOX_COLLIPRINT              1020
#define IDC_DATA_COLLIPRINT             1021
#define IDC_TEXT__CHN_ISPC              1023
#define IDC_DATA__CHN_ISPC              1024
#define IDC_DATA_PICKING01              1031
#define IDC_DATA_PICKING02              1032
#define IDC_DATA_PICKING03              1033
#define IDC_DATA_PICKING04              1034
#define IDC_DATA_PICKING05              1035
#define IDC_DATA_PICKING06              1036
#define IDC_DATA_PICKING07              1037
#define IDC_DATA_PICKING08              1038
#define IDC_DATA_PICKING09              1039
#define IDC_DATA_PICKING10              1040
#define IDC_DATA_MISSING01              1051
#define IDC_DATA_MISSING02              1052
#define IDC_DATA_MISSING03              1053
#define IDC_DATA_MISSING04              1054
#define IDC_DATA_MISSING05              1055
#define IDC_DATA_MISSING06              1056
#define IDC_DATA_MISSING07              1057
#define IDC_DATA_MISSING08              1058
#define IDC_DATA_MISSING09              1059
#define IDC_DATA_MISSING10              1060
#define IDC_LISTBOX_ALARMS              1080

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1081
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
