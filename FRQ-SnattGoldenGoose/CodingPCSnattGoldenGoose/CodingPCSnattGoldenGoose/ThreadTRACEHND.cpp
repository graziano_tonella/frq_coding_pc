//////////////////////////////////////////////////////////////////
//																//
// NAME:					ThreadTRACEHND.cpp					//
// LAST MODIFICATION DATE:	05-Nov-2018							//
// AUTHOR:					Graziano Tonella					//
// PURPOSE:					Trace Handler						//
//////////////////////////////////////////////////////////////////
#include "pch.h"
#include "EXTERN.H"

UINT ThreadTRACEHND(LPVOID)
{
	BOOL				bIsThreadRunning = TRUE;
	static STRUCT_TCPIPMSG		csSTRUCT_TCPIPMSG;
	DWORD				dwBytesCopied;

//	int				nLoop;
	CString			sString;
	CFile			TraceFile;
//	char			szBuffer[_MAX_PATH];
	CToolsLibraryGT	ToolsLibrary;



	while(bIsThreadRunning)
	{
		//
		// Wait MailBox Messages
		//
		if(!G_CoreAppHnd.m_FlatQueueTRACEHND.GetCount())
			::SuspendThread(::GetCurrentThread());
		//
		if (G_CoreAppHnd.m_FlatQueueTRACEHND.GetItem((LPBYTE)&csSTRUCT_TCPIPMSG, TRUE, &dwBytesCopied) != QUEUEGT_OK)
			continue;
		//
		switch (csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID)
		{
		case wMSGID_GENERIC__KILLTHREAD:
			bIsThreadRunning = FALSE;
			break;
		case wMSGID_TRACEHND__TRACEDIAG:
			sString.Format(_T("%s%s%02d%02d%04d.%02d.txt"), PATH__TRACEFILES, _T("TraceDiag"), G_CoreAppHnd.m_csActualTime.GetDay(), G_CoreAppHnd.m_csActualTime.GetMonth(), G_CoreAppHnd.m_csActualTime.GetYear(), G_CoreAppHnd.m_csActualTime.GetHour());
			if (TraceFile.Open(sString, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == FALSE)
				break;
			TraceFile.SeekToEnd();
			TraceFile.Write(&csSTRUCT_TCPIPMSG.byMsgDATA, csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH - 1);
			TraceFile.Write("\r\n", 2);
			TraceFile.Close();
			break;
		case wMSGID_WAKEUP__EVERY_HOUR:
			ToolsLibrary.DeleteFiles(PATH__TRACEFILES, _T("*.*"), 3, 0, 0);
			break;
		default:
			G_CoreAppHnd.TRACEDIAG(_T("ThreadTRACEHND - Unknown Message ID %d - Len %d \n"), csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID, csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH);
			break;
		}
	}
	return 0;
}
