//////////////////////////////////////////////////////////////////
// NAME:					CoreAppHnd.cpp						//
// AUTHOR:					Graziano Tonella					//
// LAST MODIFICATION DATE:	22-Oct-2021							//
//																//
// PURPOSE:					External Unit Interface Handler		//
//							Must Be a Global Object				//
//////////////////////////////////////////////////////////////////
#pragma once

//
// MFC DECLARATION
//
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
//
//#include <Message.h>
#include <ostream>

//
//////////////////////////
//	APPLICATION THREADS	//
//////////////////////////
enum
{
	THREAD__CLOCK,
	THREAD__APPHND,
	THREAD__SNATT,
	THREAD__TRACEHND,
	//
	NUM_APPLICATION__THREADS
};

//////////////////////////
//	APPLICATION HANDLES	//
//////////////////////////
enum
{
	HANDLE_THREAD__CLOCK,
	//
	NUM_APPLICATION__HANDLES
};


class CCoreAppHnd
{
public:
	CCoreAppHnd();
	~CCoreAppHnd();
	BOOL	InitApplication();
	void	DestroyApplication();

	void	TRACEDIAG(LPCTSTR lpszFormat, ...);
	void	ShutDownWINDOWSNT(BOOL bPowerOFF=TRUE);

	CSharedFlatQueueGT		m_FlatQueueCLOCK;
	CSharedFlatQueueGT		m_FlatQueueAPPHND;
	CSharedFlatQueueGT		m_FlatQueueTRACEHND;
	CSharedFlatQueueGT		m_FlatQueueSNATT;

	CTime					m_csActualTime;

	CWinThread*				m_pArrayTHREADS[NUM_APPLICATION__THREADS];
	HANDLE					m_hHandleEvents[NUM_APPLICATION__HANDLES];

	CServerClientFDXGT		m_EUIHNetworkLink;
	CServerClientFDXGT		m_ISPCNetworkLinkFDXGT;
	CTCPIPInterfaceFDXGT	m_TCPIPInterfaceSNATTServerFDX;

	CMultiLanguageMsgGT		m_MLAppMsg;
	COAMDefaultFonts		m_CustomFonts;

	CCriticalSectionGT		m_cs_CoreAppHnd;

private:
	HANDLE					m_hMyMutex;
	CCriticalSectionGT		m_cs_TraceDiag;
	STRUCT_TCPIPMSG			m_csSTRUCT_TCPIPMSG;


	BOOL					LoadSWPFiles(bool bGroup);

protected:
	BOOL inline LoadCODPCApplicationCfgTable(void);
	BOOL inline ParseCODPCApplicationCfgTable(IDispatch *pNode);

public:
	CCriticalSectionGT		m_cs_DataBase;

	bool					m_bSentManualCodingRequestToSNATT;
	int						m_nTimerWaitingAnswerFromSNATT;

	inline	BOOL			IsEveryThingOK();

	inline	void			SendHeartBeatRequestToSNATT(void);

	inline	void			SendCommandRequestToSNATT(STRUCT_CODPCTOSNATT_COMMANDREQUEST &rcsCODPCTOSNATT_COMMANDREQUEST);

	inline	void			SendKeepAliveToISPC();

	inline	void			SendKeepAliveToMSC();

	inline	void			SendManualCodingToISPC();

	inline	void			SendManualCodingToMSC();

	inline	void			KEYPADPreTranslateMessage(MSG* pMsg);

	WORD								m_wISPCTOCODPCKeepAliveTimer;
	STRUCT_ISPCTOPCCOD_KEEPALIVE		m_csISPCTOPCCODKEEPALIVE;

	WORD								m_wMSCTOCODPCKeepAliveTimer;
	STRUCT_MSCTOPCCOD_KEEPALIVE			m_csMSCTOPCCODKEEPALIVE;

	int									m_nSNATTToCODPCKeepAliveTimer;

	STRUCT_CODPCTOSNATT_COMMANDREQUEST	m_csCODPCTOSNATT_COMMANDREQUEST;

	bool								m_bCommandAnswerTelegramReceived;
	STRUCT_SNATTTOCODPC_COMMANDANSWER	m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER;


	STRUCT_SNATTTOCODPC_KEEPALIVE		m_csSTRUCT_SNATTTOCODPC_KEEPALIVE;

	#pragma pack(1)
	typedef struct _STRUCT_CFGTABLE
	{
		CString		sCODPC_IPAddress;
		int			nCODPC_Node;
		int			nCODPC_LanguageID;
		CString		sCODPC_Description;
		CString		sMSSQL_ConnectionString;
		CString		sMSSQL_UserID;
		CString		sMSSQL_UserPSW;
		CString		sDDE_IPAddress;
		int			nDDE_IPPort;
		CString		sISPC_IPAddress;
		int			nISPC_IPPort;
		int			nISPC_Node;
		CString		sSNATT_IPAddress;
		int			nSNATT_IPPort;
	} STRUCT_CFGTABLE;
	#pragma pack( )
	STRUCT_CFGTABLE	m_csSTRUCT_CFGTABLE;

	// Diagnostic
	typedef struct _STRUCT_DIAG_ISPC
	{
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_CABINET[DIAG_SIZE_CSCABINET];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_ZONE[DIAG_SIZE_CSZONE];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_BELT[DIAG_SIZE_CSBELT];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_MODULATOR[DIAG_SIZE_CSMODULATOR];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_COIL[DIAG_SIZE_CSCOIL];
		STRUCT_DIAG_BLOCK	csSTRUCT_DIAG_BLOCK_GROUPCABINET[DIAG_SIZE_CSCABINET];
	} STRUCT_DIAG_ISPC;
	STRUCT_DIAG_ISPC		m_csSTRUCT_DIAG_ISPC;

	CString				m_sArrayZONEAL[DIAG_SIZE_CSZONE][64];
	CString				m_sArrayZONEWR[DIAG_SIZE_CSZONE][64];
	CString				m_sArrayCABINETAL[DIAG_SIZE_CSCABINET][64];
	CString				m_sArrayCABINETWR[DIAG_SIZE_CSCABINET][64];
	CString				m_sArrayBELTAL[DIAG_SIZE_CSBELT][64];
	CString				m_sArrayBELTWR[DIAG_SIZE_CSBELT][64];
	CString				m_sArrayMODULATORAL[DIAG_SIZE_CSMODULATOR][64];
	CString				m_sArrayMODULATORWR[DIAG_SIZE_CSMODULATOR][64];
	CString				m_sArrayCOILAL[DIAG_SIZE_CSCOIL][64];
	CString				m_sArrayCOILWR[DIAG_SIZE_CSCOIL][64];

	CString				m_sArrayGROUPCABINETAL[DIAG_SIZE_CSCABINET][64];
	CString				m_sArrayGROUPCABINETWR[DIAG_SIZE_CSCABINET][64];

	BYTE				m_byINFODIAG;

public:
	CMyADO			m_MyADO;
	inline BOOL		OpenSQLConnection();
	int				m_nCallSP;
	long			m_lLabels;
	inline BOOL		ExecuteGET_LABELS_TO_PRINT_QUANTITY();
	inline BOOL		ExecutePRINT_LABELS();

	char			m_szScannerData[100];
};

inline BOOL CCoreAppHnd::IsEveryThingOK()
{
	#ifdef _DEBUG
		return TRUE;
	#else
		if(m_EUIHNetworkLink.IsRunning() == FALSE)
			return FALSE;
		return TRUE;
	#endif
}

inline void CCoreAppHnd::SendHeartBeatRequestToSNATT(void)
{
	CString	sXMLBuffer;
	CString	sTmpBuffer;



	sXMLBuffer.Empty();
	sTmpBuffer.Format(_T("%c%05d"), 0x02,99999);
	sXMLBuffer += sTmpBuffer;

	sXMLBuffer.AppendFormat(_T("<?xml version=\"1.0\" encoding=\"utf-8\"?>"));
	sXMLBuffer.AppendFormat(_T("<HeartbeatRequestTelegram LastUpdate =\"22-Sep-2021 11:10\" Version=\"0.0\" Remarks=\"GTX\">"));
	sTmpBuffer.Format(_T("<HeaderSection SystemName=\"Geni-Ant\" SystemID=\"%d\" InductionStationGroupID=\"%d\" InductionStationLineID=\"%d\" >"),
		9,m_csSTRUCT_CFGTABLE.nCODPC_Node/10, m_csSTRUCT_CFGTABLE.nCODPC_Node % 10);
	sXMLBuffer += sTmpBuffer;
		sTmpBuffer.Format(_T("<TransmissionDateTime>\"%04d-%02d-%02d %02d:%02d:%02d:%03d\"</TransmissionDateTime>"),
			m_csActualTime.GetYear(), m_csActualTime.GetMonth(), m_csActualTime.GetDay(),
			m_csActualTime.GetHour(), m_csActualTime.GetMinute(), m_csActualTime.GetSecond(), 0);
	sXMLBuffer += sTmpBuffer;
	sXMLBuffer += _T("</HeaderSection>");
	sXMLBuffer.AppendFormat(_T("<DataSection>"));
		sTmpBuffer.Format(_T("<Property ID=\"CommunicationWithISPC\">\"%s\"</Property>"), _T("%s"),(m_ISPCNetworkLinkFDXGT.IsRunning()==TRUE?_T("online"):_T("offline")));
		sXMLBuffer += sTmpBuffer;
		sTmpBuffer.Format(_T("<Property ID=\"CommunicationWithEUIH\">\"%s\"</Property>"), _T("disabled"));
		sXMLBuffer += sTmpBuffer;
	sXMLBuffer += _T("</DataSection>");
	sXMLBuffer += _T("</HeartbeatRequestTelegram>");

	sTmpBuffer.Format(_T("%c"), 0x03);
	sXMLBuffer += sTmpBuffer;


	int MsgLen = sXMLBuffer.GetLength();
	MsgLen -= 2;
	sTmpBuffer.Format(_T("%05d"), MsgLen);
	sXMLBuffer.Replace(_T("99999"), sTmpBuffer);


	m_cs_CoreAppHnd.EnterCriticalSection();
		m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = 0;
		m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = (WORD)sXMLBuffer.GetLength();
		::WideCharToMultiByte(CP_ACP, NULL, sXMLBuffer, -1, (char*)&m_csSTRUCT_TCPIPMSG.byMsgDATA, sizeof(m_csSTRUCT_TCPIPMSG.byMsgDATA), NULL, NULL);
		if (m_TCPIPInterfaceSNATTServerFDX.IsRunning() == TRUE)
		{
			m_TCPIPInterfaceSNATTServerFDX.m_FlatQueueOut.PutItem((LPBYTE)&m_csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
			TRACEDIAG(_T("CCoreAppHnd::SendHeartBeatRequestToSNATT \n"));
		}
	m_cs_CoreAppHnd.LeaveCriticalSection();


	CString			sString;
	CFile			TraceFile;
	sString.Format(_T("%s%s%02d%02d%04d.%02d.txt"), PATH__TRACEFILES, _T("Trace_HeartbeatRequestTelegram"), m_csActualTime.GetDay(), m_csActualTime.GetMonth(), m_csActualTime.GetYear(), m_csActualTime.GetHour());
	if (TraceFile.Open(sString, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
	{
		//TraceFile.SeekToEnd();
		//TraceFile.Write(&m_csSTRUCT_TCPIPMSG.byMsgDATA[1], m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH - 2);
		TraceFile.Write(&m_csSTRUCT_TCPIPMSG.byMsgDATA, m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH);
		TraceFile.Write("\r\n", 2);
		TraceFile.Close();
	}
}

inline void CCoreAppHnd::SendCommandRequestToSNATT(STRUCT_CODPCTOSNATT_COMMANDREQUEST &rcsCODPCTOSNATT_COMMANDREQUEST)
{
	CString	sXMLBuffer;
	CString	sTmpBuffer;



	sXMLBuffer.Empty();
	sTmpBuffer.Format(_T("%c%05d"), 0x02,99999);
	sXMLBuffer += sTmpBuffer;

	sXMLBuffer.AppendFormat(_T("<?xml version=\"1.0\" encoding=\"utf-8\"?>"));
	sXMLBuffer.AppendFormat(_T("<CommandRequestTelegram LastUpdate =\"22-Sep-2021 11:10\" Version=\"0.0\" Remarks=\"GTX\">"));
	sTmpBuffer.Format(_T("<HeaderSection SystemName=\"Geni-Ant\" SystemID=\"%d\" InductionStationGroupID=\"%d\" InductionStationLineID=\"%d\" >"),
		9, m_csSTRUCT_CFGTABLE.nCODPC_Node / 10, m_csSTRUCT_CFGTABLE.nCODPC_Node % 10);
	sXMLBuffer += sTmpBuffer;
	sTmpBuffer.Format(_T("<TransmissionDateTime>\"%04d-%02d-%02d %02d:%02d:%02d:%03d\"</TransmissionDateTime>"),
		m_csActualTime.GetYear(), m_csActualTime.GetMonth(), m_csActualTime.GetDay(),
		m_csActualTime.GetHour(), m_csActualTime.GetMinute(), m_csActualTime.GetSecond(), 0);
	sXMLBuffer += sTmpBuffer;
	sXMLBuffer += _T("</HeaderSection>");
	sXMLBuffer.AppendFormat(_T("<CommandSection>"));
	if (rcsCODPCTOSNATT_COMMANDREQUEST.nCmdID == CODPCTOSNATT_COMMANDREQUEST__BARCODE_VALIDATION_REQUEST)
	{
		sTmpBuffer.Format(_T("<Command ID=\"%d\" Remarks=\"Verifica Se Barcode Richiesto Dai Batch\">"), rcsCODPCTOSNATT_COMMANDREQUEST.nCmdID);
		sXMLBuffer += sTmpBuffer;
		sTmpBuffer.Format(_T("<BarcodeDetails UUID=\"%d\" Symbology=\"%S\" AIM=\"%S\" Content=\"%S\" Length=\"%d\" />"),
			rcsCODPCTOSNATT_COMMANDREQUEST.nUUID,
			rcsCODPCTOSNATT_COMMANDREQUEST.szBarcodeSymbologyZ,
			rcsCODPCTOSNATT_COMMANDREQUEST.szBarcodeAIMCodeZ,
			rcsCODPCTOSNATT_COMMANDREQUEST.szBarcodeDataZ,
			rcsCODPCTOSNATT_COMMANDREQUEST.nBarcodeLength);
		sXMLBuffer += sTmpBuffer;
		sXMLBuffer.AppendFormat(_T("</Command>"));
	}
	else if (rcsCODPCTOSNATT_COMMANDREQUEST.nCmdID == CODPCTOSNATT_COMMANDREQUEST__PRINT_BARCODE_REQUEST)
	{
		// JUST FOR FUTURE
		sTmpBuffer.Format(_T("<Command ID = \"%d\" Remarks = \"Richiesta Stampa Etichetta\">"), rcsCODPCTOSNATT_COMMANDREQUEST.nCmdID);
		sXMLBuffer += sTmpBuffer;

		// ... Implementation ... <PrintLabel UUID="1001 " Picking="05943677" Label="ABCDEF1234567890" />

		sXMLBuffer.AppendFormat(_T("</Command>"));
	}
	sXMLBuffer.AppendFormat(_T("</CommandSection>"));
	sXMLBuffer += _T("</CommandRequestTelegram>");

	sTmpBuffer.Format(_T("%c"), 0x03);
	sXMLBuffer += sTmpBuffer;


	int MsgLen = sXMLBuffer.GetLength();
	MsgLen -= 2;
	sTmpBuffer.Format(_T("%05d"), MsgLen);
	sXMLBuffer.Replace(_T("99999"), sTmpBuffer);


	m_cs_CoreAppHnd.EnterCriticalSection();
		m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID = 0;
		m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH = (WORD)sXMLBuffer.GetLength();
		::WideCharToMultiByte(CP_ACP, NULL, sXMLBuffer, -1, (char*)&m_csSTRUCT_TCPIPMSG.byMsgDATA, sizeof(m_csSTRUCT_TCPIPMSG.byMsgDATA), NULL, NULL);
		if (m_TCPIPInterfaceSNATTServerFDX.IsRunning() == TRUE)
		{
			m_TCPIPInterfaceSNATTServerFDX.m_FlatQueueOut.PutItem((LPBYTE)&m_csSTRUCT_TCPIPMSG, (DWORD)(sizeof(STRUCT_TCPIPMSGHEADER) + m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH));
			TRACEDIAG(_T("CCoreAppHnd::SendCommandRequestToSNATT \n"));
		}
	m_cs_CoreAppHnd.LeaveCriticalSection();

	// Clear Msg Flag
	m_bCommandAnswerTelegramReceived = false;

	CString			sString;
	CFile			TraceFile;
	sString.Format(_T("%s%s%02d%02d%04d.%02d.txt"), PATH__TRACEFILES, _T("Trace_CommandRequestTelegram"), m_csActualTime.GetDay(), m_csActualTime.GetMonth(), m_csActualTime.GetYear(), m_csActualTime.GetHour());
	if (TraceFile.Open(sString, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone | CFile::typeBinary) == TRUE)
	{
		//TraceFile.SeekToEnd();
		//TraceFile.Write(&m_csSTRUCT_TCPIPMSG.byMsgDATA[1], m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH - 2);
		TraceFile.Write(&m_csSTRUCT_TCPIPMSG.byMsgDATA, m_csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH);
		TraceFile.Write("\r\n", 2);
		TraceFile.Close();
	}
}

inline void CCoreAppHnd::SendKeepAliveToISPC()
{
	STRUCT_SINGLEMSGCML			csSINGLEMSGCML;
	STRUCT_GENERIC_DATAEXCHANGE	csGENERICDATAEXCHANGE;



	::ZeroMemory(&csGENERICDATAEXCHANGE,sizeof(csGENERICDATAEXCHANGE));

	csGENERICDATAEXCHANGE.wSenderMSGID	 = GENERIC_DATAEXCHANGE_CHC;
	csGENERICDATAEXCHANGE.wSenderWHOID = (WORD) m_csSTRUCT_CFGTABLE.nCODPC_Node;
	csGENERICDATAEXCHANGE.wReceiverMSGID = GENERIC_DATAEXCHANGE_ISPC;
	csGENERICDATAEXCHANGE.wReceiverWHOID = (WORD) m_csSTRUCT_CFGTABLE.nISPC_Node;
	csSINGLEMSGCML.csHeader.wMsgId		= wMSGID_GENERIC_DATAEXCHANGE;
	csSINGLEMSGCML.csHeader.wDataLen	= sizeof(csGENERICDATAEXCHANGE);

	STRUCT_PCCODTOISPC_KEEPALIVE	csPCCODTOISPCKEEPALIVE;
	::ZeroMemory(&csPCCODTOISPCKEEPALIVE,sizeof(csPCCODTOISPCKEEPALIVE));

	csPCCODTOISPCKEEPALIVE.wMsgId = wMSGID_PCCODTOISPC_KEEPALIVE;
	csPCCODTOISPCKEEPALIVE.bStopLoading = FALSE; //(BOOL) (m_csOAMPCTOCODPCKEEPALIVE.csSTOPLDC.byStopType==0?FALSE:TRUE);
	::CopyMemory(&csGENERICDATAEXCHANGE.szGenericData,&csPCCODTOISPCKEEPALIVE,sizeof(csPCCODTOISPCKEEPALIVE));

	::CopyMemory(&csSINGLEMSGCML.byData,&csGENERICDATAEXCHANGE,sizeof(csGENERICDATAEXCHANGE));
	if (m_ISPCNetworkLinkFDXGT.IsRunning() == TRUE)
	{
		m_ISPCNetworkLinkFDXGT.m_FlatQueueOut.PutItem((LPBYTE)&csSINGLEMSGCML, (DWORD)(SIZE_STRUCT_HEADERMSGCML + csSINGLEMSGCML.csHeader.wDataLen));
		TRACEDIAG(_T("CCoreAppHnd::SendKeepAliveToISPC - Sending KA \n"));
	}
}

inline void CCoreAppHnd::SendKeepAliveToMSC()
{
	STRUCT_SINGLEMSGCML			csSINGLEMSGCML;
	STRUCT_GENERIC_DATAEXCHANGE	csGENERICDATAEXCHANGE;



	::ZeroMemory(&csGENERICDATAEXCHANGE, sizeof(csGENERICDATAEXCHANGE));

	csGENERICDATAEXCHANGE.wSenderMSGID = GENERIC_DATAEXCHANGE_CHC;
	csGENERICDATAEXCHANGE.wSenderWHOID = (WORD)m_csSTRUCT_CFGTABLE.nCODPC_Node;
	csGENERICDATAEXCHANGE.wReceiverMSGID = GENERIC_DATAEXCHANGE_MSC;
	csGENERICDATAEXCHANGE.wReceiverWHOID = (WORD)99;// m_csSTRUCT_CFGTABLE.nISPCNode;
	csSINGLEMSGCML.csHeader.wMsgId = wMSGID_GENERIC_DATAEXCHANGE;
	csSINGLEMSGCML.csHeader.wDataLen = sizeof(csGENERICDATAEXCHANGE);

	STRUCT_PCCODTOMSC_KEEPALIVE	csPCCODTOMSCKEEPALIVE;
	::ZeroMemory(&csPCCODTOMSCKEEPALIVE, sizeof(csPCCODTOMSCKEEPALIVE));

	csPCCODTOMSCKEEPALIVE.wMsgId = wMSGID_PCCODTOMSC_KEEPALIVE;
	csPCCODTOMSCKEEPALIVE.bStopSorter = FALSE; //(BOOL) (m_csOAMPCTOCODPCKEEPALIVE.csSTOPLDC.byStopType==0?FALSE:TRUE);
	::CopyMemory(&csGENERICDATAEXCHANGE.szGenericData, &csPCCODTOMSCKEEPALIVE, sizeof(csPCCODTOMSCKEEPALIVE));

	::CopyMemory(&csSINGLEMSGCML.byData, &csGENERICDATAEXCHANGE, sizeof(csGENERICDATAEXCHANGE));
	if (m_EUIHNetworkLink.IsRunning() == TRUE)
	{
		m_EUIHNetworkLink.m_FlatQueueOut.PutItem((LPBYTE)&csSINGLEMSGCML, (DWORD)(SIZE_STRUCT_HEADERMSGCML + csSINGLEMSGCML.csHeader.wDataLen));
		TRACEDIAG(_T("CCoreAppHnd::SendKeepAliveToMSC - Sending KA \n"));
	}
}

inline void CCoreAppHnd::SendManualCodingToISPC()
{
	STRUCT_SINGLEMSGCML			csSINGLEMSGCML;
	STRUCT_GENERIC_DATAEXCHANGE	csGENERICDATAEXCHANGE;



	::ZeroMemory(&csGENERICDATAEXCHANGE,sizeof(csGENERICDATAEXCHANGE));

	csGENERICDATAEXCHANGE.wSenderMSGID	 = GENERIC_DATAEXCHANGE_CHC;
	csGENERICDATAEXCHANGE.wSenderWHOID = (WORD) m_csSTRUCT_CFGTABLE.nCODPC_Node;
	csGENERICDATAEXCHANGE.wReceiverMSGID = GENERIC_DATAEXCHANGE_ISPC;
	csGENERICDATAEXCHANGE.wReceiverWHOID = (WORD) m_csSTRUCT_CFGTABLE.nISPC_Node;
	csSINGLEMSGCML.csHeader.wMsgId		= wMSGID_GENERIC_DATAEXCHANGE;
	csSINGLEMSGCML.csHeader.wDataLen	= sizeof(csGENERICDATAEXCHANGE);

	STRUCT_PCCODTOISPC_CODING	csPCCODTOISPCCODING;
	::ZeroMemory(&csPCCODTOISPCCODING,sizeof(csPCCODTOISPCCODING));

	csPCCODTOISPCCODING.wMsgId = wMSGID_PCCODTOISPC_CODING;
	csPCCODTOISPCCODING.bManualCoded = TRUE;
	//csPCCODTOISPCCODING.byBCType = 'A'; //  m_csPCCODTOPCFOASKCOMMAND.byCommandData[0];
	csPCCODTOISPCCODING.byBCLen = (BYTE) m_csCODPCTOSNATT_COMMANDREQUEST.nBarcodeLength;
	::CopyMemory(&csPCCODTOISPCCODING.szBarcodeZ, &m_csCODPCTOSNATT_COMMANDREQUEST.szBarcodeDataZ, sizeof(csPCCODTOISPCCODING.szBarcodeZ));
//	::ZeroMemory(&csPCCODTOISPCCODING.szBarcodeZ,sizeof(csPCCODTOISPCCODING.szBarcodeZ));
//	::CopyMemory(&csPCCODTOISPCCODING.szBarcodeZ, "GTONELLA", 8);

	::CopyMemory(&csGENERICDATAEXCHANGE.szGenericData,&csPCCODTOISPCCODING,sizeof(csPCCODTOISPCCODING));

	::CopyMemory(&csSINGLEMSGCML.byData,&csGENERICDATAEXCHANGE,sizeof(csGENERICDATAEXCHANGE));
	if (m_ISPCNetworkLinkFDXGT.IsRunning() == TRUE)
	{
		m_ISPCNetworkLinkFDXGT.m_FlatQueueOut.PutItem((LPBYTE)&csSINGLEMSGCML, (DWORD)(SIZE_STRUCT_HEADERMSGCML + csSINGLEMSGCML.csHeader.wDataLen));
		TRACEDIAG(_T("CCoreAppHnd::SendManualCodingToISPC - Sending Manual Coding   BCLen[%3d]   BCBarcode[%S]\n"), csPCCODTOISPCCODING.byBCLen,(char*)&csPCCODTOISPCCODING.szBarcodeZ);
	}
}

inline void CCoreAppHnd::SendManualCodingToMSC()
{
	/*
	STRUCT_SINGLEMSGCML			csSINGLEMSGCML;
	STRUCT_GENERIC_DATAEXCHANGE	csGENERICDATAEXCHANGE;



	::ZeroMemory(&csGENERICDATAEXCHANGE, sizeof(csGENERICDATAEXCHANGE));

	csGENERICDATAEXCHANGE.wSenderMSGID = GENERIC_DATAEXCHANGE_CHC;
	csGENERICDATAEXCHANGE.wSenderWHOID = (WORD)m_csSTRUCT_CFGTABLE.nCODPC_Node;
	csGENERICDATAEXCHANGE.wReceiverMSGID = GENERIC_DATAEXCHANGE_MSC;
	csGENERICDATAEXCHANGE.wReceiverWHOID = (WORD)99; // m_csSTRUCT_CFGTABLE.nISPCNode;
	csSINGLEMSGCML.csHeader.wMsgId = wMSGID_GENERIC_DATAEXCHANGE;
	csSINGLEMSGCML.csHeader.wDataLen = sizeof(csGENERICDATAEXCHANGE);

	STRUCT_PCCODTOMSC_CODING	csPCCODTOMSCCODING;
	::ZeroMemory(&csPCCODTOMSCCODING, sizeof(csPCCODTOMSCCODING));

	csPCCODTOMSCCODING.wMsgId = wMSGID_PCCODTOMSC_CODING;
	csPCCODTOMSCCODING.bManualCoded = TRUE;
	csPCCODTOMSCCODING.byBCType = m_csPCCODTOPCFOASKCOMMAND.byCommandData[0];
	csPCCODTOMSCCODING.byBCLen = m_csPCCODTOPCFOASKCOMMAND.byCommandData[1];
	::CopyMemory(&csPCCODTOMSCCODING.szBarcodeZ, &m_csPCCODTOPCFOASKCOMMAND.byCommandData[2], sizeof(csPCCODTOMSCCODING.szBarcodeZ));

	::CopyMemory(&csGENERICDATAEXCHANGE.szGenericData, &csPCCODTOMSCCODING, sizeof(csPCCODTOMSCCODING));

	::CopyMemory(&csSINGLEMSGCML.byData, &csGENERICDATAEXCHANGE, sizeof(csGENERICDATAEXCHANGE));
	if (m_EUIHNetworkLink.IsRunning() == TRUE)
	{
		m_EUIHNetworkLink.m_FlatQueueOut.PutItem((LPBYTE)&csSINGLEMSGCML, (DWORD)(SIZE_STRUCT_HEADERMSGCML + csSINGLEMSGCML.csHeader.wDataLen));
		TRACEDIAG(_T("CCoreAppHnd::SendManualCodingToMSC - Sending Manual Coding \n"));
	}
	*/
}

inline void CCoreAppHnd::KEYPADPreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message != WM_KEYDOWN)
		return;

	if(pMsg->wParam == 0x2D && pMsg->lParam == 0x520001) // Tasto ZERO del KEYPAD
	{
		pMsg->wParam = 0x60;
		pMsg->lParam = 0x520001;
	}
	if(pMsg->wParam == 0x23 && pMsg->lParam == 0x4F0001) // Tasto UNO del KEYPAD
	{
		pMsg->wParam = 0x61;
		pMsg->lParam = 0x4F0001;
	}
	if(pMsg->wParam == 0x28 && pMsg->lParam == 0x500001) // Tasto DUE del KEYPAD
	{
		pMsg->wParam = 0x62;
		pMsg->lParam = 0x500001;
	}
	if(pMsg->wParam == 0x22 && pMsg->lParam == 0x510001) // Tasto TRE del KEYPAD
	{
		pMsg->wParam = 0x63;
		pMsg->lParam = 0x510001;
	}
	if(pMsg->wParam == 0x25 && pMsg->lParam == 0x4B0001) // Tasto QUATTRO del KEYPAD
	{
		pMsg->wParam = 0x64;
		pMsg->lParam = 0x4B0001;
	}
	if(pMsg->wParam == 0xC && pMsg->lParam == 0x4C0001) // Tasto CINQUE del KEYPAD
	{
		pMsg->wParam = 0x65;
		pMsg->lParam = 0x4C0001;
	}
	if(pMsg->wParam == 0x27 && pMsg->lParam == 0x4D0001) // Tasto SEI del KEYPAD
	{
		pMsg->wParam = 0x66;
		pMsg->lParam = 0x4D0001;
	}
	if(pMsg->wParam == 0x24 && pMsg->lParam == 0x470001) // Tasto SETTE del KEYPAD
	{
		pMsg->wParam = 0x67;
		pMsg->lParam = 0x470001;
	}
	if(pMsg->wParam == 0x26 && pMsg->lParam == 0x480001) // Tasto OTTO del KEYPAD
	{
		pMsg->wParam = 0x68;
		pMsg->lParam = 0x480001;
	}
	if(pMsg->wParam == 0x21 && pMsg->lParam == 0x490001) // Tasto NOVE del KEYPAD
	{
		pMsg->wParam = 0x69;
		pMsg->lParam = 0x490001;
	}
	if((pMsg->wParam == 0x2E || pMsg->wParam == 0x6E)&& pMsg->lParam == 0x530001)  // Tasto ./Del del KEYPAD
	{
		pMsg->wParam = 0;
		pMsg->lParam = 0;
	}
	if(pMsg->wParam == 0xD && pMsg->lParam == 0x11C0001)  // Tasto Enter del KEYPAD
	{
		pMsg->wParam = 0;
		pMsg->lParam = 0;
	}
	if(pMsg->wParam == 0x6D && pMsg->lParam == 0x4A0001)  // Tasto - (meno) del KEYPAD
	{
		pMsg->wParam = 0;
		pMsg->lParam = 0;
	}
}

inline BOOL CCoreAppHnd::OpenSQLConnection()
{
	#if GTXSTDAFX_RUNLOCAL || true
		return TRUE;
	#else

		HRESULT hr;
		hr = m_MyADO.OpenConnection((_bstr_t)m_csSTRUCT_CFGTABLE.sMSSQL_ConnectionString,(_bstr_t)m_csSTRUCT_CFGTABLE.sMSSQL_UserID,(_bstr_t)m_csSTRUCT_CFGTABLE.sMSSQL_UserPSW);
		if(hr != S_OK)
		{
			m_MyADO.Close();
			return FALSE;
		}
	#endif
	return TRUE;
}

inline BOOL CCoreAppHnd::ExecuteGET_LABELS_TO_PRINT_QUANTITY()
{
	m_cs_DataBase.EnterCriticalSection();

	BOOL bRet = TRUE;
	long lRetVal=0;	
	CString sProcName;
	
	try
	{
		if(OpenSQLConnection() == FALSE)
			TRACEDIAG( _T("OpenSQLConnection"));
		CString	sString;
		sProcName = _T("GET_LABELS_TO_PRINT_QUANTITY");
		sString.Format(_T("Sorter%1d.%s"),2,sProcName);
		if(m_MyADO.InitializeStoredProcedure((_bstr_t) sString) != S_OK)
			TRACEDIAG(_T("Initialize"));

		if(m_MyADO.AddParameterReturnValue() != S_OK)
			TRACEDIAG(_T("AddParameterReturnValue"));

//		if(m_MyADO.AddParameterInputText(_T("Committente"),m_csPCFOTOPCCODKEEPALIVE.szCUSTOMER) != S_OK)
//			throw CMyADOException(STORE_PROCEDURE_FAULT_AddParameterInputText,_T("Committente"));

		if(m_MyADO.AddParameterOutputLong(_T("Labels")) != S_OK)
			TRACEDIAG(_T("Add Parameter Labels"));

		if(m_MyADO.ExecuteStoredProcedure() != S_OK)
			TRACEDIAG(_T("Execute"));

		if(m_MyADO.GetParameterReturnValue(&lRetVal) != S_OK)
			TRACEDIAG( _T("GetParameterReturnValue"));
		else
		{
			if(lRetVal != 0)
				TRACEDIAG(_T("STORED PROCEDURE RETURN ERROR"));
		}

		if(m_MyADO.GetParameterLong(_T("Labels"),&m_lLabels) != S_OK)
			TRACEDIAG( _T("Get Parameter Labels"));
	}
	catch(CMyADOException e)
	{
		int nResult = e.GetError();
		TRACEDIAG(_T("SQL Error Result Is %d RetVal %d \n"), nResult, lRetVal);
		m_MyADO.Close();
		bRet = FALSE;
	}
	catch(...)
	{
		bRet = FALSE;
	}

	m_cs_DataBase.LeaveCriticalSection();

	return bRet;
}
inline BOOL CCoreAppHnd::ExecutePRINT_LABELS()
{
	m_cs_DataBase.EnterCriticalSection();

	BOOL bRet = TRUE;
	CString sProcName;
	
	try
	{
		if(OpenSQLConnection() == FALSE)
			TRACEDIAG( _T("OpenSQLConnection"));
		CString	sString;
		sProcName = _T("PRINT_LABELS");
		sString.Format(_T("Sorter%1d.%s"),2,sProcName);
		if(m_MyADO.InitializeStoredProcedure((_bstr_t) sString) != S_OK)
			TRACEDIAG(_T("Initialize"));
		if(m_MyADO.AddParameterReturnValue() != S_OK)
			TRACEDIAG(_T("AddParameterReturnValue"));

//		if(m_MyADO.AddParameterInputText(_T("Committente"),m_csPCFOTOPCCODKEEPALIVE.szCUSTOMER) != S_OK)
//			throw CMyADOException(STORE_PROCEDURE_FAULT_AddParameterInputText,_T("Committente"));

		if (m_MyADO.AddParameterInputLong(_T("LineaIntroduzione"), (long)m_csSTRUCT_CFGTABLE.nISPC_Node) != S_OK)
			TRACEDIAG(_T("LineaIntroduzione"));

		if(m_MyADO.ExecuteStoredProcedure() != S_OK)
			TRACEDIAG(_T("Execute"));

		long lRetVal = 0;
		if(m_MyADO.GetParameterReturnValue(&lRetVal) != S_OK)
			TRACEDIAG(_T("GetParameterReturnValue"));
		else
		{
			if(lRetVal != 0)
				TRACEDIAG(_T("STORED PROCEDURE RETURN ERROR"));
		}
	}
	catch(CMyADOException e)
	{
		int nResult = e.GetError();
		TRACEDIAG(_T("SQL Error Result Is %d \n"), nResult);
		m_MyADO.Close();
		bRet = FALSE;
	}
	catch(...)
	{
		bRet = FALSE;
	}

	m_cs_DataBase.LeaveCriticalSection();

	return bRet;
}

BOOL inline CCoreAppHnd::LoadCODPCApplicationCfgTable(void)
{
	WIN32_FIND_DATA	csFindFileData;
	HANDLE			hFind;
	BOOL			bFound;
	CString			sString;
	CString			sMsg;
	CString			sNodeName;
	::CoInitialize(NULL);
	CXmlDocumentWrapper xmlDoc;



	// Check FILE
	#if GTXSTDAFX_RUNLOCAL
		sString.Format(_T("%s%s"), PATH__XMLFILES, _T("CODPCApplicationCfgTable.xml"));
	#else
		sString.Format(_T("%s%s"), PATH__XMLFILES, _T("CODPCApplicationCfgTable.xml"));
	#endif
	hFind = ::FindFirstFile(sString, &csFindFileData);
	bFound = (BOOL)(hFind == INVALID_HANDLE_VALUE ? FALSE : TRUE);
	::FindClose(hFind);
	if (bFound == FALSE)
	{
		sMsg.Format(_T("CCoreAppHnd::LoadCODPCApplicationCfgTable - File %s Not Found"), sString);
		::AfxMessageBox(sMsg, MB_ICONSTOP);
		return FALSE;
	}

	try
	{
		if (xmlDoc.Load(sString) == TRUE)
		{
			//m_bParsingFailed = FALSE;
			//m_nIdxcsBINID	 = 0;
			if (ParseCODPCApplicationCfgTable(xmlDoc.AsNode()) == FALSE)
				return FALSE;
		}
		else
		{
			sMsg.Format(_T("CCoreAppHnd::LoadCODPCApplicationCfgTable - Error On xmlDoc.Load While Loading File %s"), sString);
			::AfxMessageBox(sMsg, MB_ICONSTOP);
			return FALSE;
		}
	}
	catch (...)
	{
		sMsg.Format(_T("CCoreAppHnd::LoadCODPCApplicationCfgTable - CATCH While Parsing File %s "), sString);
		::AfxMessageBox(sMsg, MB_ICONSTOP);
		return FALSE;
	}
	return TRUE;
}

BOOL inline CCoreAppHnd::ParseCODPCApplicationCfgTable(IDispatch *pNode)
{
	CXmlNodeWrapper	XMLNode(pNode);
	CString	sMsg;
	CString	sNodeName;
	CString	sAttName;
	CString	sAttVal;
	//char	szBuff[256];



	try
	{
		if (XMLNode.NodeType() == "element")
		{
			sNodeName = XMLNode.Name();
			TRACE(_T("element %s \r\n"), sNodeName);
			//sNodeName.MakeLower();
			int nNumNodes = XMLNode.NumNodes();
			if (nNumNodes != 5)
				return FALSE;
			for (int nNode = 0; nNode<XMLNode.NumNodes(); nNode++)
			{
				CXmlNodeWrapper FieldNodes(XMLNode.GetNode(nNode));
				sNodeName = FieldNodes.Name();
				TRACE(_T("Main Node %s \r\n"), sNodeName);
				if (sNodeName.CompareNoCase(_T("CODPC_APPLICATION")) == 0)
				{
					int nNumChildNodes = FieldNodes.NumNodes();
					if (nNumChildNodes != 1)
						return FALSE;
					for (int nChildNodes = 0; nChildNodes<FieldNodes.NumNodes(); nChildNodes++)
					{
						CXmlNodeWrapper ChildNodes(FieldNodes.GetNode(nChildNodes));
						sNodeName = ChildNodes.Name();
						TRACE(_T("ChildNode %s \r\n"), sNodeName);
						//sNodeName.MakeLower();
						if (sNodeName.CompareNoCase(_T("WhoAmI")) != 0)
							return FALSE;
						int nNumAtt = ChildNodes.NumAttributes();
						if (nNumAtt != 4)
							return FALSE;
						for (int nAtt = 0; nAtt<ChildNodes.NumAttributes(); nAtt++)
						{
							sAttName = ChildNodes.GetAttribName(nAtt);
							TRACE(_T("Attribute %s \r\n"), sAttName);
							if (sAttName.CompareNoCase(_T("IPAddress")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.sCODPC_IPAddress = sAttVal;
							}
							else if (sAttName.CompareNoCase(_T("Node")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.nCODPC_Node = _wtoi(sAttVal);
							}
							else if (sAttName.CompareNoCase(_T("LanguageID")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.nCODPC_LanguageID = _wtoi(sAttVal);
							}
							else if (sAttName.CompareNoCase(_T("Description")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.sCODPC_Description = sAttVal;
							}
							else
								return FALSE;
						}
					}
				}
				else if (sNodeName.CompareNoCase(_T("CODPC_MSSQL")) == 0)
				{
					int nNumChildNodes = FieldNodes.NumNodes();
					if (nNumChildNodes != 1)
						return FALSE;
					for (int nChildNodes = 0; nChildNodes<FieldNodes.NumNodes(); nChildNodes++)
					{
						CXmlNodeWrapper ChildNodes(FieldNodes.GetNode(nChildNodes));
						sNodeName = ChildNodes.Name();
						TRACE(_T("ChildNode %s \r\n"), sNodeName);
						//sNodeName.MakeLower();
						if (sNodeName.CompareNoCase(_T("MyADO")) != 0)
							return FALSE;
						int nNumAtt = ChildNodes.NumAttributes();
						if (nNumAtt != 3)
							return FALSE;
						for (int nAtt = 0; nAtt<ChildNodes.NumAttributes(); nAtt++)
						{
							sAttName = ChildNodes.GetAttribName(nAtt);
							TRACE(_T("Attribute %s \r\n"), sAttName);
							if (sAttName.CompareNoCase(_T("ConnectionString")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.sMSSQL_ConnectionString = sAttVal;
							}
							else if (sAttName.CompareNoCase(_T("UserID")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.sMSSQL_UserID = sAttVal;
							}
							else if (sAttName.CompareNoCase(_T("UserPSW")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.sMSSQL_UserPSW = sAttVal;
							}
							else
								return FALSE;
						}
					}
				}
				else if (sNodeName.CompareNoCase(_T("CODPC_DDE")) == 0)
				{
					int nNumChildNodes = FieldNodes.NumNodes();
					if (nNumChildNodes != 1)
						return FALSE;
					for (int nChildNodes = 0; nChildNodes < FieldNodes.NumNodes(); nChildNodes++)
					{
						CXmlNodeWrapper ChildNodes(FieldNodes.GetNode(nChildNodes));
						sNodeName = ChildNodes.Name();
						TRACE(_T("ChildNode %s \r\n"), sNodeName);
						//sNodeName.MakeLower();
						if (sNodeName.CompareNoCase(_T("WhoIsDDE")) != 0)
							return FALSE;
						int nNumAtt = ChildNodes.NumAttributes();
						if (nNumAtt != 2)
							return FALSE;
						for (int nAtt = 0; nAtt < ChildNodes.NumAttributes(); nAtt++)
						{
							sAttName = ChildNodes.GetAttribName(nAtt);
							TRACE(_T("Attribute %s \r\n"), sAttName);
							if (sAttName.CompareNoCase(_T("IPAddress")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.sDDE_IPAddress = sAttVal;
							}
							else if (sAttName.CompareNoCase(_T("IPPort")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP, NULL, sAttVal, -1, szBuff, sizeof(szBuff), NULL, NULL);
								//m_csSTRUCT_CFGTABLE.nDDE_IPPort = atoi(szBuff);
								m_csSTRUCT_CFGTABLE.nDDE_IPPort = _wtoi(sAttVal);
							}
							else
								return FALSE;
						}
					}
				}
				else if (sNodeName.CompareNoCase(_T("CODPC_ISPC")) == 0)
				{
					int nNumChildNodes = FieldNodes.NumNodes();
					if (nNumChildNodes != 1)
						return FALSE;
					for (int nChildNodes = 0; nChildNodes < FieldNodes.NumNodes(); nChildNodes++)
					{
						CXmlNodeWrapper ChildNodes(FieldNodes.GetNode(nChildNodes));
						sNodeName = ChildNodes.Name();
						TRACE(_T("ChildNode %s \r\n"), sNodeName);
						//sNodeName.MakeLower();
						if (sNodeName.CompareNoCase(_T("WhoIsISPC")) != 0)
							return FALSE;
						int nNumAtt = ChildNodes.NumAttributes();
						if (nNumAtt != 3)
							return FALSE;
						for (int nAtt = 0; nAtt < ChildNodes.NumAttributes(); nAtt++)
						{
							sAttName = ChildNodes.GetAttribName(nAtt);
							TRACE(_T("Attribute %s \r\n"), sAttName);
							if (sAttName.CompareNoCase(_T("IPAddress")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.sISPC_IPAddress = sAttVal;
							}
							else if (sAttName.CompareNoCase(_T("IPPort")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP, NULL, sAttVal, -1, szBuff, sizeof(szBuff), NULL, NULL);
								//m_csSTRUCT_CFGTABLE.nDDE_IPPort = atoi(szBuff);
								m_csSTRUCT_CFGTABLE.nISPC_IPPort = _wtoi(sAttVal);
							}
							else if (sAttName.CompareNoCase(_T("Node")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP, NULL, sAttVal, -1, szBuff, sizeof(szBuff), NULL, NULL);
								//m_csSTRUCT_CFGTABLE.nDDE_IPPort = atoi(szBuff);
								m_csSTRUCT_CFGTABLE.nISPC_Node = _wtoi(sAttVal);
							}
							else
								return FALSE;
						}
					}
				}
				else if (sNodeName.CompareNoCase(_T("CODPC_SNATT")) == 0)
				{
					int nNumChildNodes = FieldNodes.NumNodes();
					if (nNumChildNodes != 1)
						return FALSE;
					for (int nChildNodes = 0; nChildNodes < FieldNodes.NumNodes(); nChildNodes++)
					{
						CXmlNodeWrapper ChildNodes(FieldNodes.GetNode(nChildNodes));
						sNodeName = ChildNodes.Name();
						TRACE(_T("ChildNode %s \r\n"), sNodeName);
						//sNodeName.MakeLower();
						if (sNodeName.CompareNoCase(_T("WhoIsSNATT")) != 0)
							return FALSE;
						int nNumAtt = ChildNodes.NumAttributes();
						if (nNumAtt != 2)
							return FALSE;
						for (int nAtt = 0; nAtt < ChildNodes.NumAttributes(); nAtt++)
						{
							sAttName = ChildNodes.GetAttribName(nAtt);
							TRACE(_T("Attribute %s \r\n"), sAttName);
							if (sAttName.CompareNoCase(_T("IPAddress")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP,NULL,sNodeText,-1,szBuff,sizeof(szBuff),NULL,NULL);
								m_csSTRUCT_CFGTABLE.sSNATT_IPAddress = sAttVal;
							}
							else if (sAttName.CompareNoCase(_T("IPPort")) == 0)
							{
								sAttVal = ChildNodes.GetAttribVal(nAtt);
								//::WideCharToMultiByte(CP_ACP, NULL, sAttVal, -1, szBuff, sizeof(szBuff), NULL, NULL);
								//m_csSTRUCT_CFGTABLE.nDDE_IPPort = atoi(szBuff);
								m_csSTRUCT_CFGTABLE.nSNATT_IPPort = _wtoi(sAttVal);
							}
							else
								return FALSE;
						}
					}
				}
				else
					return FALSE;
			}
		}
		else
			return FALSE;
	}
	catch (...)
	{
		sMsg.Format(_T("CCoreAppHnd::ParseCODPCApplicationCfgTable - CATCH While Parsing"));
		::AfxMessageBox(sMsg, MB_ICONSTOP);
		return FALSE;
	}
	return TRUE;
}


