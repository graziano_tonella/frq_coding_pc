
// CodingPCSnattGoldenGooseDlg.h : header file
//

#pragma once

#include "..\..\OAMCommonDefinesVS2017\BtnST.h"
//#include "ShadeButtonST.h"
#include "..\..\OAMCommonDefinesVS2017\ColorStatic.h"

#include "EXTERN.H"

// CCodingPCSnattGoldenGooseDlg dialog
class CCodingPCSnattGoldenGooseDlg : public CDialogEx
{
// Construction
public:
	CCodingPCSnattGoldenGooseDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CODINGPCSNATTGOLDENGOOSE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementation
protected:
	HICON	m_hIcon;
	UINT	m_uTimerID;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnMoving(UINT fwSide, LPRECT pRect);
	DECLARE_MESSAGE_MAP()

private:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	void	OnSystemExit();
	void	UpdateOnTimer();

	LRESULT	On_MYWM_UPDATE_DATEANDTIME(WPARAM wParam, LPARAM lParam);
	LRESULT	On_MYWM_SHOW_ISPCALARM_MESSAGE(WPARAM wParam, LPARAM lParam);
	LRESULT	On_MYWM_UPDATE_CODING(WPARAM wParam, LPARAM lParam);

	CColorStatic	m_stcDATA_SYSTEMTIME;
	CColorStatic	m_stcDATA_SYSTEMDATE;

	//	CColorStatic	m_stcTEXT__CHN_EUIH;
	//	CColorStatic	m_stcDATA__CHN_EUIH;

	CColorStatic	m_stcTEXT__CHN_SNATT;
	CColorStatic	m_stcDATA__CHN_SNATT;

	CColorStatic	m_stcTEXT__CHN_ISPC;
	CColorStatic	m_stcDATA__CHN_ISPC;

	CColorStatic	m_stcTEXT_MSSQL;
	CColorStatic	m_stcDATA_MSSQL;

	CColorStatic	m_stcBOX_BATCH;
	CColorStatic	m_stcTEXT_PICKING;
	CColorStatic	m_stcTEXT_MISSING;
	CColorStatic	m_stcDATA_PICKING[MAX_RUNNING_BATCHES];
	CColorStatic	m_stcDATA_MISSING[MAX_RUNNING_BATCHES];

	CColorStatic	m_stcBOX_CHUTE;
	CColorStatic	m_stcDATA_CHUTE;

	CColorStatic	m_stcBOX_COLLIPRINT;
	CColorStatic	m_stcDATA_COLLIPRINT;

	CButtonST		m_btnLABELS;
	CColorStatic	m_stcTEXT_CMDDONE;

	CColorStatic	m_stcTEXT_SKU;
	CColorStatic	m_stcDATA_SKU;

	CColorStatic	m_stcTEXT_INFO;
	CColorStatic	m_stcDATA_INFO;
	CColorStatic	m_stcTEXT_ALARM;

	void			ISPCAlarmsHandling(void);
	void			BuildAlarmOrWarningList(void);
	void			InsertAlarmOrWarningIntoCLISTBOX(DWORD dwAlarm, int nOfs, LPCTSTR lpszAlarmType, CString* psAlarmMsg);

	void			INFOAlarmsHandling(void);
	void			SendManualCodingToSNATT(void);

public:
	afx_msg void OnEnChangeEditSku();

private:
	void		CallExecuteGET_LABELS_TO_PRINT_QUANTITY(BOOL bExecuteImmediately);
	int			m_nResetCmdDone;

public:

	CBrush		m_bkBackGroundColor;
	CBrush		m_bkBackGroundLightGray;

	COLORREF	m_crBackGroundColor;

	CBrush			m_bkBackGroundCODFIELDS;
	COLORREF		m_crBackGroundCODFIELDS;


	afx_msg void OnBnClickedBtnLabels();

	CListBox m_ListBoxALARMS;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
