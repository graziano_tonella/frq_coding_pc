//////////////////////////////////////////////////////////////////
// NAME:					ThreadSNATT.cpp						//
// LAST MODIFICATION DATE:	27-Sep-2021							//
// AUTHOR:					Graziano Tonella					//
// FILE TYPE:				Source C++							//
// PURPOSE:					APPLICATION Messages Handler		//
//////////////////////////////////////////////////////////////////
//
#include "pch.h"

#include "EXTERN.H"
//
// BEGIN THREAD
//
UINT ThreadSNATT(LPVOID)
{
	BOOL				bIsThreadRunning = TRUE;
	STRUCT_TCPIPMSG		csSTRUCT_TCPIPMSG;
	DWORD				dwBytesCopied;
	CString				sXMLTelegram;
	int					nStartNode;
	int					nItemLen;
	CString				sName;
	int					nNode;



	while (bIsThreadRunning)
	{
		//
		// Wait MailBox Messages
		//
		if (!G_CoreAppHnd.m_FlatQueueSNATT.GetCount())
			G_CoreAppHnd.m_pArrayTHREADS[THREAD__SNATT]->SuspendThread();
		//
		if (G_CoreAppHnd.m_FlatQueueSNATT.GetItem((LPBYTE)&csSTRUCT_TCPIPMSG, TRUE, &dwBytesCopied) != QUEUEGT_OK)
		{
			G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - Unknown Message ID %X - Len %d \n"), csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID, csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH);
			continue;
		}
		//
		switch (csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID)
		{
		case wMSGID_GENERIC__KILLTHREAD:
			bIsThreadRunning = FALSE;
			break;
		case wMSGID_SNATT__TELEGRAM:
			//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM \n"));
			sXMLTelegram.Empty();
			sXMLTelegram.Format(_T("%*S"), csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH, csSTRUCT_TCPIPMSG.byMsgDATA);
			G_CoreAppHnd.m_cs_DataBase.EnterCriticalSection();
				if (sXMLTelegram.Find(_T("<HeartbeatAcknowledgeTelegram"), 0) != -1)
				{
					G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <HeartbeatAcknowledgeTelegram> \n"));

					G_CoreAppHnd.m_nSNATTToCODPCKeepAliveTimer = 10;
					::ZeroMemory(&G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE, sizeof(G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE));
					nStartNode = 0;
					for (int nRB = 0; nRB < MAX_RUNNING_BATCHES; nRB++)
					{
						sName = _T("Picking Number=\"");
						if ((nNode = sXMLTelegram.Find(sName, nStartNode)) != -1)
						{
							nStartNode = nNode + sName.GetLength();
							//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <HeartbeatAcknowledgeTelegram> Picking    nNode %d    nStartNode %d  \n"), nNode,nStartNode);
							if ((nNode = sXMLTelegram.Find(_T("\""), nStartNode)) != -1)
							{
								nItemLen = nNode - nStartNode;
								//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <HeartbeatAcknowledgeTelegram>  Picking   nNode %d    nItemLen %d  \n"), nNode, nItemLen);
								for (int nIx = 0; nIx < nItemLen && nIx < SIZEOF_PICKING; nIx++)
								{
									G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE.wzPickingNumberZ[nRB][nIx] = sXMLTelegram.GetAt(nStartNode + nIx);
								}

								sName = _T("Qty=\"");
								nStartNode = nNode;
								if ((nNode = sXMLTelegram.Find(sName, nStartNode)) != -1)
								{
									nStartNode = nNode + sName.GetLength();
									//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <HeartbeatAcknowledgeTelegram> Qty   nNode %d    nStartNode %d  \n"), nNode, nStartNode);
									if ((nNode = sXMLTelegram.Find(_T("\""), nStartNode)) != -1)
									{
										nItemLen = nNode - nStartNode;
										//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <HeartbeatAcknowledgeTelegram>  Qty   nNode %d    nItemLen %d  \n"), nNode, nItemLen);
										for (int nIx = 0; nIx < nItemLen && nIx < SIZEOF_MISSINGITEMS; nIx++)
										{
											G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE.wszMissingItemsZ[nRB][nIx] = sXMLTelegram.GetAt(nStartNode + nIx);
										}
									}
								}
							}
							sName = _T("AvailableChutes=\"");
							nStartNode = 0;
							if ((nNode = sXMLTelegram.Find(sName, nStartNode)) != -1)
							{
								nStartNode = nNode + sName.GetLength();
								//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <HeartbeatAcknowledgeTelegram> AvailableChutes    nNode %d    nStartNode %d  \n"), nNode,nStartNode);
								if ((nNode = sXMLTelegram.Find(_T("\""), nStartNode)) != -1)
								{
									nItemLen = nNode - nStartNode;
									//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <HeartbeatAcknowledgeTelegram>  AvailableChutes   nNode %d    nItemLen %d  \n"), nNode, nItemLen);
									wchar_t wszTmp[10+1];
									wszTmp[0] = _T('\0');
									for (int nIx = 0; nIx < nItemLen && nIx < 10; nIx++)
									{
										wszTmp[nIx+0] = sXMLTelegram.GetAt(nStartNode + nIx);
										wszTmp[nIx+1] = _T('\0');
									}
									G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_KEEPALIVE.nAvailableChutes = _wtoi(wszTmp);
								}
							}
						}
					}
				}
				else if (sXMLTelegram.Find(_T("<CommandAcknowledTelegram"), 0) != -1)
				{
					G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <CommandAcknowledgeTelegram> \n"));

					nStartNode = 0;
					sName = _T("Accepted=\"");
					if ((nNode = sXMLTelegram.Find(sName, nStartNode)) != -1)
					{
						nStartNode = nNode + sName.GetLength();
						//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <CommandAcknowledTelegram>  Accepted   nNode %d    nStartNode %d  \n"), nNode, nStartNode);
						if ((nNode = sXMLTelegram.Find(_T("\""), nStartNode)) != -1)
						{
							nItemLen = nNode - nStartNode;
							//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <CommandAcknowledTelegram>  Accepted   nNode %d    nItemLen %d  \n"), nNode, nItemLen);
							CString	sTrueOrFalse;
							sTrueOrFalse.Empty();
							for (int nIx = 0; nIx < nItemLen; nIx++)
							{
								sTrueOrFalse += sXMLTelegram.GetAt(nStartNode + nIx);
							}
							//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <CommandAcknowledTelegram>  Accepted   sTrueOrFalse %s  \n"), sTrueOrFalse);
							//
							sName = _T("MsgToShow=\"");
							nStartNode = nNode;
							if ((nNode = sXMLTelegram.Find(sName, nStartNode)) != -1)
							{
								nStartNode = nNode + sName.GetLength();
								//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <CommandAcknowledTelegram> MsgToShow   nNode %d    nStartNode %d  \n"), nNode, nStartNode);
								if ((nNode = sXMLTelegram.Find(_T("\""), nStartNode)) != -1)
								{
									nItemLen = nNode - nStartNode;
									//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <CommandAcknowledTelegram>  MsgToShow   nNode %d    nItemLen %d  \n"), nNode, nItemLen);
									G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.sMsgToShow.Empty();
									for (int nIx = 0; nIx < nItemLen; nIx++)
									{
										G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.sMsgToShow += sXMLTelegram.GetAt(nStartNode + nIx);
									}
									//G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <CommandAcknowledTelegram>  sMsgToShow %s  \n"), G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.sMsgToShow);
								}
							}
							if (sTrueOrFalse.CompareNoCase(_T("true")) == 0)
							{
								G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.bAccepted = true;
								G_CoreAppHnd.m_bCommandAnswerTelegramReceived = true;
							}
							else if (sTrueOrFalse.CompareNoCase(_T("false")) == 0)
							{
								G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.bAccepted = false;
								G_CoreAppHnd.m_bCommandAnswerTelegramReceived = true;
							}
							G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM - <CommandAcknowledTelegram>  bAccepted %d\n"), G_CoreAppHnd.m_csSTRUCT_SNATTTOCODPC_COMMANDANSWER.bAccepted);
						}
					}
				}
				else
				{
					G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_SNATT__TELEGRAM NOT FOUND %40.40S \n"), csSTRUCT_TCPIPMSG.byMsgDATA);
				}
			G_CoreAppHnd.m_cs_DataBase.LeaveCriticalSection();
			break;

/*
		case wMSGID_GENERIC_DATAEXCHANGE:
			//
			switch(wMsgId)
			{
			case wMSGID_ISPCTOPCCOD_KEEPALIVE:
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_ISPCTOPCCOD_KEEPALIVE \n"));
				G_CoreAppHnd.m_cs_DataBase.EnterCriticalSection();
					::CopyMemory(&G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE,&csGENERICDATAEXCHANGE.szGenericData,sizeof(G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE));
				G_CoreAppHnd.m_cs_DataBase.LeaveCriticalSection();
				G_CoreAppHnd.m_wISPCTOCODPCKeepAliveTimer = 10;
				G_CoreAppHnd.SendKeepAliveToISPC();
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_ISPCTOCODPC_KEEPALIVE   bIsLDCReady[%d]   bIsEnabledManualCoding[%d] \n"),
							G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE.bIsLDCReady,
							G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE.bIsEnabledManualCoding);
				break;
			case wMSGID_MSCTOPCCOD_KEEPALIVE:
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_MSCTOPCCOD_KEEPALIVE \n"));
				G_CoreAppHnd.m_cs_DataBase.EnterCriticalSection();
					::CopyMemory(&G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE, &csGENERICDATAEXCHANGE.szGenericData, sizeof(G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE));
				G_CoreAppHnd.m_cs_DataBase.LeaveCriticalSection();
				G_CoreAppHnd.m_wMSCTOCODPCKeepAliveTimer = 10;
				G_CoreAppHnd.SendKeepAliveToMSC();
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_MSCTOPCCOD_KEEPALIVE   bIsSorterReady[%d]   bIsEnabledManualCoding[%d] \n"),
					G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE.bIsSorterReady,
					G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE.bIsEnabledManualCoding);
				break;
			case wMSGID_PCFOTOCODPC_KEEPALIVE:
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_PCFOTOCODPC_KEEPALIVE \n"));
				G_CoreAppHnd.m_cs_DataBase.EnterCriticalSection();
					::CopyMemory(&G_CoreAppHnd.m_csPCFOTOCODPCKEEPALIVE,&csGENERICDATAEXCHANGE.szGenericData,sizeof(G_CoreAppHnd.m_csPCFOTOCODPCKEEPALIVE));
				G_CoreAppHnd.m_cs_DataBase.LeaveCriticalSection();
				
				break;
			case wMSGID_PCFOTOCODPC_ANSWERCOMMAND:
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_PCFOTOCODPC_ANSWERCOMMAND \n"));
				::CopyMemory(&G_CoreAppHnd.m_csPCFOTOPCCODANSWERCOMMAND,&csGENERICDATAEXCHANGE.szGenericData,sizeof(G_CoreAppHnd.m_csPCFOTOPCCODANSWERCOMMAND));
				G_CoreAppHnd.m_bCommandAnswerTelegramReceived = true;
				::PostMessage(G_hInterfaceDlg,ON_MYWM_UPDATE_CODING,ON_MYWM_UPDATE_CODING,0);
				break;
			default:
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - Unknown wMSGID_GENERIC_DATAEXCHANGE wMsgId %d \n"),wMsgId);
				break;
			}
			break;
*/
		case wMSGID_WAKEUP__ONE_SECOND:
			if(G_CoreAppHnd.m_nSNATTToCODPCKeepAliveTimer > 0)
				G_CoreAppHnd.m_nSNATTToCODPCKeepAliveTimer--;
			break;
		case wMSGID_WAKEUP__FIVE_SECONDS:
			G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - wMSGID_WAKEUP__FIVE_SECONDS \n"));
			G_CoreAppHnd.SendHeartBeatRequestToSNATT();
			break;
		default:
			G_CoreAppHnd.TRACEDIAG(_T("ThreadSNATT - Unknown Message ID %X - Len %d \n"), csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgID, csSTRUCT_TCPIPMSG.csSTRUCT_TCPIPMSGHEADER.wMsgLENGTH);
			break;
		}
	}
	//
	return 0;
}
