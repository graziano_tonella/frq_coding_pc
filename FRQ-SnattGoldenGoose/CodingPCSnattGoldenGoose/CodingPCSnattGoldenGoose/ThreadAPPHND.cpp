//////////////////////////////////////////////////////////////////
// NAME:					ThreadAPPHND.cpp					//
// LAST MODIFICATION DATE:	27-Sep-2021							//
// AUTHOR:					Graziano Tonella					//
// FILE TYPE:				Source C++							//
// PURPOSE:					APPLICATION Messages Handler		//
//////////////////////////////////////////////////////////////////
//
#include "pch.h"

#include "EXTERN.H"
//
// BEGIN THREAD
//
UINT ThreadAPPHND(LPVOID)
{
	BOOL				bIsThreadRunning = TRUE;
	STRUCT_SINGLEMSGCML	csSINGLEMSGCML;
	DWORD				dwBytesCopied;

	//
	STRUCT_GENERIC_DATAEXCHANGE		csGENERICDATAEXCHANGE;
	WORD	wMsgId;
	//
	//
	//
	while(bIsThreadRunning)
	{
		//
		// Wait MailBox Messages
		//
		if(!G_CoreAppHnd.m_FlatQueueAPPHND.GetCount())
			G_CoreAppHnd.m_pArrayTHREADS[THREAD__APPHND]->SuspendThread();
		//
		if(G_CoreAppHnd.m_FlatQueueAPPHND.GetItem((LPBYTE)&csSINGLEMSGCML,TRUE,&dwBytesCopied) != QUEUEGT_OK)
		{
			G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - Unknown Message ID %X - Len %d \n"),csSINGLEMSGCML.csHeader.wMsgId,csSINGLEMSGCML.csHeader.wDataLen);
			continue;
		}
		//
		switch(csSINGLEMSGCML.csHeader.wMsgId)
		{
		case wMSGID_GENERIC__KILLTHREAD:
			bIsThreadRunning = FALSE;
			break;
		case wMSGID_GENERIC_DATAEXCHANGE:
			::CopyMemory(&csGENERICDATAEXCHANGE,&csSINGLEMSGCML.byData,sizeof(csGENERICDATAEXCHANGE));
			::CopyMemory(&wMsgId,&csGENERICDATAEXCHANGE.szGenericData,sizeof(wMsgId));
			/*
			G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND::ExamineGenericDataExchangeMessageCHE   wSenderMSGID[%05u]   wSenderWHOID[%05u]   wReceiverMSGID[%05u]   wReceiverWHOID[%05u]   wMsgId[%04X] \n"),
				csGENERICDATAEXCHANGE.wSenderMSGID,
				csGENERICDATAEXCHANGE.wSenderWHOID,
				csGENERICDATAEXCHANGE.wReceiverMSGID,
				csGENERICDATAEXCHANGE.wReceiverWHOID,
				wMsgId);
			*/
			//
			switch(wMsgId)
			{
			case wMSGID_ISPCTOPCCOD_KEEPALIVE:
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_ISPCTOPCCOD_KEEPALIVE \n"));
				G_CoreAppHnd.m_cs_DataBase.EnterCriticalSection();
					::CopyMemory(&G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE,&csGENERICDATAEXCHANGE.szGenericData,sizeof(G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE));
				G_CoreAppHnd.m_cs_DataBase.LeaveCriticalSection();
				//::PostMessage(G_hInterfaceDlg, ON_MYWM_UPDATE_CODING, ON_MYWM_UPDATE_CODING, 0);
				G_CoreAppHnd.m_wISPCTOCODPCKeepAliveTimer = 10;
				G_CoreAppHnd.SendKeepAliveToISPC();
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_ISPCTOCODPC_KEEPALIVE   bIsLDCReady[%d]   bIsEnabledManualCoding[%d] \n"),
							G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE.bIsLDCReady,
							G_CoreAppHnd.m_csISPCTOPCCODKEEPALIVE.bIsEnabledManualCoding);
				break;
			case wMSGID_MSCTOPCCOD_KEEPALIVE:
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_MSCTOPCCOD_KEEPALIVE \n"));
				G_CoreAppHnd.m_cs_DataBase.EnterCriticalSection();
					::CopyMemory(&G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE, &csGENERICDATAEXCHANGE.szGenericData, sizeof(G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE));
				G_CoreAppHnd.m_cs_DataBase.LeaveCriticalSection();
				G_CoreAppHnd.m_wMSCTOCODPCKeepAliveTimer = 10;
				G_CoreAppHnd.SendKeepAliveToMSC();
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_MSCTOPCCOD_KEEPALIVE   bIsSorterReady[%d]   bIsEnabledManualCoding[%d] \n"),
					G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE.bIsSorterReady,
					G_CoreAppHnd.m_csMSCTOPCCODKEEPALIVE.bIsEnabledManualCoding);
				break;
			default:
				G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - Unknown wMSGID_GENERIC_DATAEXCHANGE wMsgId %d \n"),wMsgId);
				break;
			}
			break;
		#define wMSGID_ISPCTORTX_DIAGNOSTIC	60000
		case wMSGID_ISPCTORTX_DIAGNOSTIC:
		#undef	wMSGID_ISPCTORTX_DIAGNOSTIC
			STRUCT_ISPCTORTX_DIAGNOSTIC	csDIAGNOSTIC;
			::CopyMemory(&csDIAGNOSTIC, &csSINGLEMSGCML.byData, sizeof(csDIAGNOSTIC));
			G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - wMSGID_ISPCTORTX_DIAGNOSTIC byDeviceType[%d]   wObjectID[%d]   byObjectType[%d] \n"), csDIAGNOSTIC.byDeviceType, csDIAGNOSTIC.wObjectID, csDIAGNOSTIC.byObjectType);
			if(csDIAGNOSTIC.byDeviceType == DEVICE_ID_IS)
			{
				int nIdx = csDIAGNOSTIC.wObjectID;
				switch(csDIAGNOSTIC.byObjectType)
				{
				case DIAG_ID_CSCABINET:
					if(nIdx >= 0 && nIdx < DIAG_SIZE_CSCABINET)
						::CopyMemory(&G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nIdx],&csDIAGNOSTIC.csDiag,sizeof(STRUCT_DIAG_BLOCK));
					for (int nIx = 0; nIx < 10; nIx++)
					{
						G_CoreAppHnd.TRACEDIAG(_T("DIAG_ID_CSCABINET - nIdx %02d   dwAlarm[0]=%08X   dwAlarm[1]=%08X \n"),
							nIx,
							G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nIx].dwAlarm[0],
							G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_CABINET[nIx].dwAlarm[1]);
					}
					break;
				case DIAG_ID_CSZONE:
					if(nIdx >= 0 && nIdx < DIAG_SIZE_CSZONE)
						::CopyMemory(&G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_ZONE[nIdx],&csDIAGNOSTIC.csDiag,sizeof(STRUCT_DIAG_BLOCK));
					break;
				case DIAG_ID_CSBELT:
					if (nIdx >= 0 && nIdx < DIAG_SIZE_CSBELT)
						::CopyMemory(&G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_BELT[nIdx], &csDIAGNOSTIC.csDiag, sizeof(STRUCT_DIAG_BLOCK));
					break;
				case DIAG_ID_CSMODULATOR:
					if (nIdx >= 0 && nIdx < DIAG_SIZE_CSMODULATOR)
						::CopyMemory(&G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_MODULATOR[nIdx], &csDIAGNOSTIC.csDiag, sizeof(STRUCT_DIAG_BLOCK));
					break;
				case DIAG_ID_CSCOIL:
					if (nIdx >= 0 && nIdx < DIAG_SIZE_CSCOIL)
						::CopyMemory(&G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_COIL[nIdx], &csDIAGNOSTIC.csDiag, sizeof(STRUCT_DIAG_BLOCK));
					break;
				default:
					break;
				}
			}
			if (csDIAGNOSTIC.byDeviceType == DEVICE_ID_IG)
			{
				int nIdx = csDIAGNOSTIC.wObjectID;
				switch (csDIAGNOSTIC.byObjectType)
				{
				case DIAG_ID_CSCABINET:
					if (nIdx >= 0 && nIdx < DIAG_SIZE_CSCABINET)
						::CopyMemory(&G_CoreAppHnd.m_csSTRUCT_DIAG_ISPC.csSTRUCT_DIAG_BLOCK_GROUPCABINET[nIdx], &csDIAGNOSTIC.csDiag, sizeof(STRUCT_DIAG_BLOCK));
					break;
				default:
					break;
				}
			}
			break;
		case wMSGID_OAMTORTX_SYSTEM:	// ShutDown
			#ifndef _DEBUG
				G_CoreAppHnd.ShutDownWINDOWSNT();
			#endif
			break;
		case wMSGID_WAKEUP__ONE_SECOND:
			if (G_CoreAppHnd.m_wISPCTOCODPCKeepAliveTimer)
				G_CoreAppHnd.m_wISPCTOCODPCKeepAliveTimer--;

			if (G_CoreAppHnd.m_wMSCTOCODPCKeepAliveTimer)
				G_CoreAppHnd.m_wMSCTOCODPCKeepAliveTimer--;

			break;
		default:
			G_CoreAppHnd.TRACEDIAG(_T("ThreadAPPHND - Unknown Message ID %X - Len %d \n"),csSINGLEMSGCML.csHeader.wMsgId,csSINGLEMSGCML.csHeader.wDataLen);
			break;
		}
	}
	//
	return 0;
}
